jQuery(document).ready(function($)
{

	jQuery('input[name="image"][value=""]').addClass('required-entry required-file');
	jQuery('input[name="image"]').addClass('validate-file-ext');

	jQuery('#request_form_products table#request_form_products_table tbody td.a-center input').attr('name' , 'request_form_products[]');

	jQuery('#request_form_products').on('click', 'table.actions [type="button"]' , function(){
		var timesRun = 0;
		var loading_mask = jQuery('#loading-mask');
		var interval = setInterval(function(){
		    timesRun += 1;

		    if( jQuery(loading_mask).css('display') == 'none' )
		    {
				jQuery('#request_form_products table#request_form_products_table tbody td.a-center input').attr('name' , 'request_form_products[]');
		        clearInterval(interval);
		    }

		    //just retry and limit
		    if(timesRun === 30)
		    {
		    	console.log('CLear and loading not found: ' + timesRun);
		        clearInterval(interval);
		    }
		    //do whatever here..
		}, 100);
	});

	var add_validators_execute = setInterval(function()
	{

		//lazy workaround for require-config
		if (typeof Validation === "function")
		{
			Validation.add('validate-file-ext', 'Invalid File!', function(value)
			{
				var ext = value.split('.').pop().toLowerCase();
				if(jQuery(arguments[1]).attr('value') != "" && jQuery(arguments[1]).val() == "")
				{
					return true;
				}
				if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1)
				{
					return false;
				}
				return true;
			});

			clearInterval(add_validators_execute);
			console.log('added custom validations on jquery.validate');
		}
		else
		{
			console.log('waiting for jquery.validate');
		}
	}, 200);

});