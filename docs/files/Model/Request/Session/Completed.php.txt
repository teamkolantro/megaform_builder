<?php

class MegaForm_Builder_Model_Request_Session_Completed extends MegaForm_Builder_Model_Abstract
{
    const MODULE_MODEL        = "builder";
    const MODULE_MODEL_ENTITY = "request_session_completed";

    protected function _construct()
    {
        $this->_init(self::MODULE_MODEL . "/" . self::MODULE_MODEL_ENTITY);
    }

}

