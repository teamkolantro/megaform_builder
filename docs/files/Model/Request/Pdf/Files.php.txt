<?php

class MegaForm_Builder_Model_Request_Pdf_Files extends MegaForm_Builder_Model_Abstract
{
    const MODULE_MODEL        = "builder";
    const MODULE_MODEL_ENTITY = "request_pdf_files";

    protected function _construct()
    {
        $this->_init(self::MODULE_MODEL . "/" . self::MODULE_MODEL_ENTITY);
    }

}

