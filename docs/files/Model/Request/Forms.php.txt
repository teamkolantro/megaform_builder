<?php

use MegaForm_Builder_Model_Request_Form_Products as FORMS_PRODUCTS_MODEL;

class MegaForm_Builder_Model_Request_Forms extends Mage_Core_Model_Abstract
{
    const MODULE_MODEL        = "builder";
    const MODULE_MODEL_ENTITY = "request_forms";

    protected function _construct()
    {
        $this->_init(self::MODULE_MODEL . "/" . self::MODULE_MODEL_ENTITY);
    }

    /**
     * Provide available options as a value/label array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $collections = $this->getCollection();

        $collections_options = array();

        foreach ($collections as $collection)
        {
            $collections_options[$collection->getData('id')] = array(
                'value' => $collection->getData('form_code'), 'label' => $collection->getData('name'),
                );
        }

        return $collections_options;
    }

    /**
     * Retrieve array of product id's for category
     *
     * array($productId => $position)
     *
     * @return array
     */
    public function getFormProductIds()
    {
        if (!$this->getId()) {
            return array();
        }

        $form_products = Mage::getModel(FORMS_PRODUCTS_MODEL::MODULE_MODEL . "/" . FORMS_PRODUCTS_MODEL::MODULE_MODEL_ENTITY)
                    ->getCollection()
                    ->addFieldToFilter('request_form_id', array('eq' => $this->getId()));

        $form_products_ids = array();

        foreach ($form_products as $form_product)
        {
            $form_products_ids[$form_product->getData('id')] = $form_product->getData('product_id');
        }

        return $form_products_ids;
    }

    public function getCollectionToOptionArray()
    {
        $collections = $this->getCollection();

        $collections_options = array();

        foreach ($collections as $collection)
        {
            $collections_options[$collection->getData('id')] = $collection->getData('name');
        }

        return $collections_options;
    }

    public function getMaxSortPosition()
    {
        $maxSort = $this->getCollection()
            ->addFieldToSelect('sort')
            ->setOrder('sort', 'DESC')
            ->setPageSize(1)
            ->setCurPage(1)
            ->getData();

        return $maxSort[0]['sort'] + 1;
    }

}

