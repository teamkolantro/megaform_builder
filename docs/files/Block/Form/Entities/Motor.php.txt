<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Form_Entities_Abstract as FE_Abstract;

/**
 * Form Entities Entities Motor
 *
 * This handles each form as a whole entity form motor
 * Other Example :
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Fire.html">Fire</a>
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Itp.html">Itp</a>
 *
 * This class also dispatch events from abstract parent.
 * Events:
 * - form_builder_render_field_before
 * - form_builder_render_field_after
 * - form_builder_render_element_before
 * - form_builder_render_element_after
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Form_Entities_Motor extends FE_Abstract
{
    /**
     * Contains all data of field sets with elements per fieldset array
     *
     * This variable should be passed to self::$static_form_field_rows
     * for the parent abstract late static bindings
     *
     * Object representation
     * <code>
     * Varien_Object
     * (
     * &nbsp; &nbsp; _data::protected =&gt;
     * &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset1 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset2 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset3 =&gt; Varien_Object(),
     * &nbsp; &nbsp; )
     * )
     * </code>
     *
     * @var Varien_Object
     */
    private $form_field_rows;

    /**
     * Array of fieldsets
     *
     * Field sets is being divided by group in array
     *
     * Array representaion
     * <code>
     * array
     * (
     * &nbsp; &nbsp; &nbsp;array
     * &nbsp; &nbsp; &nbsp;(
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name1,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name2
     * &nbsp; &nbsp; &nbsp;),
     * &nbsp; &nbsp; &nbsp;array
     * &nbsp; &nbsp; &nbsp;(
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name3,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name4
     * &nbsp; &nbsp; &nbsp;)
     * )
     * </code>
     *
     * @var mixed
     */
    public $page_sets = array();

    /**
     * Request Forms
     *
     * Default : 'request_forms'
     *
     * @var string
     */
    const REQUEST_FORMS = 'request_forms';

    /**
     * On Success Url
     *
     * Default : 'on_success_url'
     *
     * @var string
     */
    const ON_SUCCESS_URL = 'on_success_url';

    /**
     * Personal information
     *
     * For fieldset key
     *
     * Default : 'personal_information'
     *
     * @var string
     */
    const PERSONAL_INFORMATION = 'personal_information';

    /**
     * Contact Information
     *
     * For fieldset key
     *
     * Default : 'contact_information'
     *
     * @var string
     */
    const CONTACT_INFORMATION = 'contact_information';

    /**
     * Location Vehicle
     *
     * For fieldset key
     *
     * Default : 'location_vehicle'
     *
     * @var string
     */
    const LOCATION_VEHICLE = 'location_vehicle';

    /**
     * Insurance Required
     *
     * For fieldset key
     *
     * Default : 'insurance_required'
     *
     * @var string
     */
    const INSURANCE_REQUIRED = 'insurance_required';

    /**
     * Insurance Required Others
     *
     * For fieldset key
     *
     * Default : 'insurance_required_others'
     *
     * @var string
     */
    const INSURANCE_REQUIRED_OTHERS = 'insurance_required_others';

    /**
     * Insurance Required Car Details
     *
     * For fieldset key
     *
     * Default : 'insurance_required_car_details'
     *
     * @var string
     */
    const INSURANCE_REQUIRED_CAR_DETAILS = 'insurance_required_car_details';

    /**
     * Value Vehicle
     *
     * For fieldset key
     *
     * Default : 'value_vehicle'
     *
     * @var string
     */
    const VALUE_VEHICLE = 'value_vehicle';

    /**
     * Promo Code
     *
     * For fieldset key
     *
     * Default : 'promo_code'
     *
     * @var string
     */
    const PROMO_CODE = 'promo_code';

    /**
     * Save Options
     *
     * For fieldset key
     *
     * Default : 'save_options'
     *
     * @var string
     */
    const SAVE_OPTIONS = 'save_options';

    /**
     * Class contructor...
     *
     * Sets the self::$static_form_field_rows from property form_field_rows
     *
     */
    public function __construct()
    {
        $this->_init();
        /**
         * Late Static binding para magamit ng parent ung entity ng child kahit property pa xa ng parent ganun.
         */
        self::$static_form_field_rows = $this->form_field_rows;

        parent::__construct();
    }

    /**
     * Initiator
     *
     * Intiates and prepare entities and values for form rendering process
     * Calls to methods
     * - <a href="#method_initEntities">_initEntities</a>
     * - <a href="#method_processEntities">_processEntities</a>
     * - <a href="#method_buildFieldItems">_buildFieldItems</a>
     * - <a href="#method_assignPageSets">_initEntities</a>
     *
     */
    public function _init()
    {
        $this->_initEntities();
        $this->_processEntities();
        $this->_buildFieldItems();
        $this->_assignPageSets();
    }

    /**
     * Assign Page Sets
     *
     * This sets the field set in grouped array to property <a href="#property_page_sets">page_sets</a>
     */
    public function _assignPageSets()
    {
        $this->page_sets[] = array(
            self::PERSONAL_INFORMATION,
            self::CONTACT_INFORMATION,
            self::LOCATION_VEHICLE,
        );

        $this->page_sets[] = array(
            self::INSURANCE_REQUIRED,
            self::INSURANCE_REQUIRED_OTHERS,
            self::INSURANCE_REQUIRED_CAR_DETAILS,
            self::VALUE_VEHICLE,
            self::PROMO_CODE,
            self::SAVE_OPTIONS,
        );
    }

    /**
     * Initialize Entities
     *
     * Entities are basically the fieldsets
     *
     * This method sets all fieldsets/entities instantiate
     * each fieldset to be an instance of Varien_Object
     * and sets to <a href="#property_form_field_rows">form_field_rows</a>
     * as whole Varien_Object
     *
     * Data representation
     * <code>
     * Varien_Object
     * (
     * &nbsp; &nbsp; _data::protected =&gt;
     * &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset1 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset2 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset3 =&gt; Varien_Object(),
     * &nbsp; &nbsp; )
     * )
     * </code>
     *
     */
    public function _initEntities()
    {
        $this->form_field_rows = new Varien_Object(
            array(
                self::REQUEST_FORMS                  => new Varien_Object(),
                self::ON_SUCCESS_URL                 => new Varien_Object(),
                self::PERSONAL_INFORMATION           => new Varien_Object(),
                self::CONTACT_INFORMATION            => new Varien_Object(),
                self::LOCATION_VEHICLE               => new Varien_Object(),
                self::INSURANCE_REQUIRED             => new Varien_Object(),
                self::INSURANCE_REQUIRED_OTHERS      => new Varien_Object(),
                self::INSURANCE_REQUIRED_CAR_DETAILS => new Varien_Object(),
                self::VALUE_VEHICLE                  => new Varien_Object(),
                self::PROMO_CODE                     => new Varien_Object(),
                self::SAVE_OPTIONS                   => new Varien_Object(),
            )
        );
    }

    /**
     * Process Entities
     *
     * Process all fieldsets attributes
     *
     * This method sets all attributes for every single fieldset
     *
     * Fieldset can contain
     * - Group Type     (Single or Multiple)
     * - title          Field set header text as title
     * - header_info    Field set header info text as a string rendered below fieldset header
     * - attributes     Ay html 5 attrbutes developer may think usefull.
     *
     * Data representation
     * <code>
     * fieldset1 =&gt; Object&nbsp;
     * (
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //If the elements is single
     * &nbsp;&nbsp; &nbsp;or
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //This will group the elements as one fieldset
     * )
     * -&gt;addData
     * (
     * &nbsp;&nbsp; &nbsp;title =&gt; Field set title
     * &nbsp;&nbsp; &nbsp;header_info =&gt; Fieldset info //this will appear below the fieldset title
     * )
     * </code>
     *
     */
    public function _processEntities()
    {

        $this->form_field_rows->getData(self::PERSONAL_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'PERSONAL INFORMATION',
                'header_info' => 'Please provide the personal information of the person who will travel.',
            )
        );

        $this->form_field_rows->getData(self::CONTACT_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'CONTACT INFORMATION',
                'header_info' => 'Note: Please provide either Telephone Number or Mobile Number',
            )
        );

        $this->form_field_rows->getData(self::LOCATION_VEHICLE)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'Location of Vehicle',
            )
        );

        $this->form_field_rows->getData(self::INSURANCE_REQUIRED)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'Insurance Required',
                'header_info' => 'For more information about our Motor Coverage, <a href="/products/motor.html">click here.</a>',
            )
        );

        $this->form_field_rows->getData(self::INSURANCE_REQUIRED_OTHERS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'INSURANCE REQUIRED OTHERS',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['insurance_required-type_insurance']",
                    'data-fieldset-group'           => "{'type_insurance':['Comprehensive', 'Comprehensive with CTPL']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::INSURANCE_REQUIRED_CAR_DETAILS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'INSURANCE REQUIRED CAR DETAILS',
            )
        );

        $this->form_field_rows->getData(self::VALUE_VEHICLE)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'VALUE OF VEHICLE',
            )
        );

        $this->form_field_rows->getData(self::PROMO_CODE)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'Promo Code',
            )
        );

        $this->form_field_rows->getData(self::SAVE_OPTIONS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => '',
                'header_info' => '',
            )
        );

    }

    /**
     * Builld Field Items
     *
     * Build all items in each fieldset
     *
     * This method sets add all kinds of elements in each fieldset,
     * whereas each element has an attribute and this method add it to it's respective element.
     *
     * Element can contain
     * - column             Column number in string, please refer to bootstrap, example is if column is 4 then it is col-md-4
     * - type               Element type to be rendered, but this only accepts only if there's an available class entity. Found in \MegaForm\Builder\Block\Adminhtml\Renderer\[Element_Type].php
     * - required           If value is 1 then element will have an attribute of required
     * - placeholder        This is for html palceholder attribute
     * - attributes         Should be a set of array whereas array (attribute-name => attribute-value)
     * - item_note          This is an item note that will be display under an element
     * - options            This is only applicable for select type of elements, and should be in array format
     * - label              This is only applicable for label type of elements, and should be in string format
     * - clearfix_before    If it's value 1 then the element will have a clearfix before it renders
     * - clearfix_after    If it's value 1 then the element will have a clearfix after it renders
     *
     * Data representation
     * <code>
     * fieldset1 =&gt; Object&nbsp;
     * (
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //If the elements is single
     * &nbsp;&nbsp; &nbsp;or
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //This will group the elements as one fieldset
     * )
     * -&gt;addData
     * (
     * &nbsp;&nbsp; &nbsp;title =&gt; Field set title
     * &nbsp;&nbsp; &nbsp;header_info =&gt; Fieldset info //this will appear below the fieldset title
     * )
     * </code>
     *
     */
    public function _buildFieldItems()
    {
        $this->form_field_rows->getData(self::PERSONAL_INFORMATION)->addData(
            array(
                'elements' => array(
                    'first_name'  => $this->_buildElements('first_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'First Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'middle_name' => $this->_buildElements('middle_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Middle Name',
                            'attributes'  => array(
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'last_name'   => $this->_buildElements('last_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'policy_code' => $this->_buildElements('policy_code',
                        array(
                            'column' => '4',
                            'type'   => 'hidden',
                        )
                    ),
                ),
            )
        );

        $email_regex_validation = '^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))';

        $this->form_field_rows->getData(self::CONTACT_INFORMATION)->addData(
            array(
                'elements' => array(
                    'email'            => $this->_buildElements('email',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Email Address *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'false',
                                'data-mask-placeholder'    => '',
                                'data-bv-regexp-message'   => 'Please enter a valid email address',
                                'pattern'                  => $email_regex_validation,
                                'data-bv-remote'           => 'false',
                                'data-bv-remote-message'   => 'The email address you have entered is already registered. Please login your account.',
                                'data-bv-remote-name'      => 'email',
                                'data-bv-remote-url'       => Mage::getBaseUrl() . '/promocode/index/isemailnew/',
                            ),
                        )
                    ),
                    'telephone_number' => $this->_buildElements('telephone_number',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Telephone Number',
                            'item_note'   => "Sample Format: 02-1234567",
                            'attributes'  => array(
                                'data-mask-config'       => '99-9999999',
                                'data-mask-placeholder'  => '',
                                'pattern'                => "^^\(?([0-9]{2})\)?[-. ]?([0-9]{7})$",
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'true',
                            ),
                        )
                    ),
                    'mobile_number'    => $this->_buildElements('mobile_number',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Mobile Number *',
                            'item_note'   => "Sample Format: 63-900-1234567",
                            'attributes'  => array(
                                'data-mask-config'         => '99-999-9999999',
                                'data-mask-placeholder'    => '',
                                'data-bv-excluded'         => 'false',
                                'data-bv-message'          => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'pattern'                  => "^^\(?([0-9]{2})\)?[-. ]?([0-9]{3})?[-. ]?([0-9]{7})$",
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::LOCATION_VEHICLE)->addData(
            array(
                'elements' => array(
                    'province' => $this->_buildElements('province',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Province *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'city'     => $this->_buildElements('city',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'City *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'barangay' => $this->_buildElements('barangay',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Barangay *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::INSURANCE_REQUIRED)->addData(
            array(
                'elements' => array(
                    'for_rent'       => $this->_buildElements('for_rent',
                        array(
                            'column'     => '12',
                            'type'       => 'checkbox',
                            'required'   => 0,
                            'label'      => 'Is your vehicle used for rent or hire?',
                            'attributes' => array(
                                'data-bv-excluded'         => 'true',
                                'data-on-save-exclude'     => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'type_insurance' => $this->_buildElements('type_insurance',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Type of Insurance *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Comprehensive',
                                    'value' => 'Comprehensive',
                                ),
                                array(
                                    'label' => 'CTPL',
                                    'value' => 'CTPL',
                                ),
                                array(
                                    'label' => 'Comprehensive with CTPL',
                                    'value' => 'Comprehensive with CTPL',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::INSURANCE_REQUIRED_OTHERS)->addData(
            array(
                'elements' => array(
                    'bodily_injury' => $this->_buildElements('bodily_injury',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            'attributes' => array(
                                'data-bv-excluded'         => 'true',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Bodily Injury (VTPL-BI) *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::INSURANCE_REQUIRED_CAR_DETAILS)->addData(
            array(
                'elements' => array(
                    'year_model' => $this->_buildElements('year_model',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => Mage::helper('vehicleinfo')->getMakeYearOptions(),
                        )
                    ),
                    'brand'      => $this->_buildElements('brand',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Brand *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'model_name' => $this->_buildElements('model_name',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Model Name *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::VALUE_VEHICLE)->addData(
            array(
                'elements' => array(
                    'fmv' => $this->_buildElements('fmv',
                        array(
                            'column'      => '6',
                            'type'        => 'slider',
                            'required'    => 0,
                            'placeholder' => '',
                            'attributes'  => array(
                                'data-min-val'         => "10000",
                                'data-max-val'         => "100000",
                                'data-default-val'     => "12345",
                                'data-on-save-exclude' => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::PROMO_CODE)->addData(
            array(
                'elements' => array(
                    'promo_code' => $this->_buildElements('promo_code',
                        array(
                            'column'      => '3',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Promo Code',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::SAVE_OPTIONS)->addData(
            array(
                'elements' => array(
                    'info' => $this->_buildElements('info',
                        array(
                            'column' => '12',
                            'type'   => 'label',
                            'label'  => 'If you want to save your information to get a quote for at a later date, click save.',
                        )
                    ),
                ),
            )
        );
    }

}

