<?php

use MegaForm_Builder_Helper_Data as CURR_HELPER;
use MegaForm_Builder_Model_Items as CURR_MODEL;

class MegaForm_Builder_Block_View extends Mage_Core_Block_Template
{
    public function getConfigEnabled()
    {
        return Mage::helper(CURR_HELPER::HELPER_KEY)->getConfigEnabled();
    }

    public function getPageItems()
    {
        $collection_results = Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)
            ->getCollection()
            ->getAllEnabled();
        $collection_results->setOrder('name', 'ASC');

        return $collection_results;
    }
}

