<?php
class MegaForm_Builder_Block_Adminhtml_Quoterequest_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('quoterequest_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('builder')->__('Quote Request'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('builder')->__('Quote Request'),
          'title'     => Mage::helper('builder')->__('Quote Request'),
          'content'   => $this->getLayout()->createBlock('builder/adminhtml_quoterequest_edit_tab_form')->toHtml(),
      ));
      return parent::_beforeToHtml();
  }
}

