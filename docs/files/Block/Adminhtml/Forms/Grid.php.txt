<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Adminhtml_Forms as BASE_BLOCK;
use MegaForm_Builder_Helper_Data as CURR_HELPER;
use MegaForm_Builder_Model_Request_Forms as CURR_MODEL;

/**
 * Admin Form Grid Container
 *
 * This renders the grid table rows
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Adminhtml_Forms_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId(strtolower(BASE_BLOCK::BLOCK_CONTROLLER) . "Grid");
        $this->setDefaultSort("id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare grid collection
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns
     */
    protected function _prepareColumns()
    {
        $this->addColumn("id", array(
            "header" => Mage::helper("adminhtml")->__("ID"),
            "align"  => "right",
            "width"  => "50px",
            "type"   => "number",
            "index"  => "id",
        ));

        $this->addColumn("name", array(
            "header" => Mage::helper("adminhtml")->__("Name"),
            "index"  => "name",
        ));

        $this->addColumn("form_code", array(
            "header" => Mage::helper("adminhtml")->__("Form Code"),
            "index"  => "form_code",
        ));

        $this->addColumn("date_created", array(
            "header" => Mage::helper("adminhtml")->__("Date Created"),
            "index"  => "date_created",
        ));

        $this->addColumn("date_modified", array(
            "header" => Mage::helper("adminhtml")->__("Date Modified"),
            "index"  => "date_modified",
        ));

        // $this->addColumn('status', array(
        //     'header'  => Mage::helper('adminhtml')->__('Status'),
        //     'index'   => 'status',
        //     'type'    => 'options',
        //     'options' => CURR_HELPER::getStatusOptionArray(),
        // ));

        return parent::_prepareColumns();
    }

    /**
     * Get row edit url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    // protected function _prepareMassaction()
    // {
    //     $this->setMassactionIdField('id');
    //     $this->getMassactionBlock()->setFormFieldName('ids');
    //     $this->getMassactionBlock()->setUseSelectAll(true);
    //     $this->getMassactionBlock()->addItem('remove_' . BASE_BLOCK::BLOCK_CONTROLLER, array(
    //         'label'   => Mage::helper('adminhtml')->__('Remove Item'),
    //         'url'     => $this->getUrl('*/adminhtml_' . BASE_BLOCK::BLOCK_CONTROLLER . '/massRemove'),
    //         'confirm' => Mage::helper('adminhtml')->__('Are you sure you want to delete this item?'),
    //     ));
    //     return $this;
    // }

}

