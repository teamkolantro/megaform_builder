function sidebarResizable()
{
    var min = 250;
    var max = 3600;
    var mainmin = 200;

    if($('.span2.sidebar').width())
    {
        min = $('.span2.sidebar').width();
    }

    var split_bar = '<div id="split-bar" style="display: block; width: 10px; z-index: 2; float: right; right: 0px; background-color: cornflowerblue; height: 100%; cursor: col-resize;"></div>';
    split_bar = jQuery(split_bar);

    $('.span2.sidebar').prepend(split_bar);

    $('#split-bar').mousedown(function(e)
    {
        $('#split-bar').css('height', $('#split-bar').siblings('.accordion').height());
        e.preventDefault();
        $(document).mousemove(function(e)
        {
            e.preventDefault();
            var x = e.pageX - $('.span2.sidebar').offset().left;
            if (x > min && x < max && e.pageX < ($(window).width() - mainmin))
            {
                $('.span2.sidebar').css("width", x);
                // $('#main').css("margin-left", x);
            }
        })
    });
}
$(document).ready(function(){
    sidebarResizable();
});

$(document).mouseup(function(e)
{
    $(document).unbind('mousemove');
});
