<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Model_Request_Forms as CURR_MODEL;
use MegaForm_Builder_Model_Request_Session_Completed as REQUEST_SESSION_COMPLETED_MODEL;

/**
 * Helper Data
 *
 * This class is the default helper for the modules,
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Settings Default Value
     *
     * Default : 'null'
     *
     * @var null
     */
    const SETTINGS_DEFAULT_VALUE = null;

    /**
     * Config Enabled XML PATH
     *
     * Default : 'builder_section/builder_group/enable'
     *
     * @var string
     */
    const CONFIG_ENABLED = 'builder_section/builder_group/enable';

    /**
     * Status Enabled
     *
     * Default : '1'
     *
     * @var int
     */
    const STATUS_ENABLED = 1;

    /**
     * Status Disabled
     *
     * Default : '0'
     *
     * @var int
     */
    const STATUS_DISABLED = 0;

    /**
     * After event name
     *
     * Default : 'after'
     *
     * @var string
     */
    const EVENT_AFTER = "after";

    /**
     * Before event name
     *
     * Default : 'before'
     *
     * @var string
     */
    const EVENT_BEFORE = "before";

    /**
     * Module title
     *
     * Default : 'Form Builder'
     *
     * @var string
     */
    const MODULE_TITLE = "Form Builder";

    /**
     * Helper key
     *
     * Default : 'builder'
     *
     * @var string
     */
    const HELPER_KEY = "builder";

    /**
     * Type Comprehensive Label
     *
     * Default : 'Comprehensive'
     *
     * @var string
     */
    const TYPE_CMP_LABEL = "Comprehensive";

    /**
     * Type CTPL Label
     *
     * Default : 'CTPL'
     *
     * @var string
     */
    const TYPE_CTPL_LABEL = "CTPL";

    /**
     * Type Comprehensive with CTPL Label
     *
     * Default : 'Comprehensive with CTPL'
     *
     * @var string
     */
    const TYPE_CMP_CTPL_LABEL = "Comprehensive with CTPL";

    /**
     * Type Comprehensive acronym
     *
     * Default : 'cmp'
     *
     * @var string
     */
    const TYPE_CMP = "cmp";

    /**
     * Type CTPL acronym
     *
     * Default : 'ctpl'
     *
     * @var string
     */
    const TYPE_CTPL = "ctpl";

    /**
     * Type Comprehensive with CTPL acronym
     *
     * Default : 'cmp-ctpl'
     *
     * @var string
     */
    const TYPE_CMP_CTPL = "cmp-ctpl";

    /**
     * Refract Default
     *
     * Force a value to be null if no value is set
     *
     * @param mixed $value Value ot be refreacted, Default (null)
     * @return string|null Returns null if no value
     */
    public function reFractDefault($value = null)
    {
        if (empty($value))
        {
            return self::SETTINGS_DEFAULT_VALUE;
        }
        else
        {
            return $value;
        }
    }

    /**
     * Get Module title
     *
     * Return MODULE TITLE constant property
     *
     * @return string Returns module title
     */
    public function getModuleTitle()
    {
        return self::MODULE_TITLE;
    }

    /**
     * Get Config Enabled
     *
     * This returns a boolean result depending on the module registry config enable key
     *
     * @return boolean Result from module registry config
     */
    public function getConfigEnabled()
    {
        $value = trim(Mage::getStoreConfig(self::CONFIG_ENABLED, Mage::app()->getStore()));
        return $this->reFractDefault($value);
    }

    /**
     * Get Status Option Array
     *
     * Build an option array for status
     *
     * @static
     * @return array Status option array
     */
    public static function getStatusOptionArray()
    {
        return array(
            self::STATUS_ENABLED  => Mage::helper('adminhtml')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('adminhtml')->__('Disabled'),
        );
    }

    /**
     * Get Cart Item Products
     *
     * Getting all items in cart checkout session
     *
     * @return mixed Cart items, instance of catalog product each array
     */
    public function getCartItemProducts()
    {
        $cartItems = array();

        $cart = Mage::getSingleton('checkout/session')->getQuote();
        foreach ($cart->getAllItems() as $item)
        {
            if ($item->getParentItemId())
            {
                continue;
            }

            $cartItems[] = $item->getProduct();
        }

        return $cartItems;

    }

    /**
     * Embed Form Variables
     *
     * Replace all variables with corresponding key in array of $vars.
     * This will also load the datas by form_key so you dont need to pass the form variables
     *
     * @param string $form_key Form key, Default('')
     * @param string $html_template Html template in string, Default('')
     * @param mixed $extras Extra variables  to be embeded, Default('')
     * @param mixed $callback_specials Life is hard but still you can pass an anonymous function with key then it will be callbacked here.
     * @return string Html string from $html_template that variables has been replaced.
     */
    public function embedFormVariables($form_key = '', $html_template = "", $extras = "", $callback_specials)
    {
        if ($form_key)
        {
            $request_session_completed = Mage::getModel('builder/request_session_completed')->load($form_key, 'form_key');

            if ($request_session_completed->getId())
            {
                $request_form_records = Mage::getModel('builder/request_form_records')
                    ->load($request_session_completed->getId(), 'request_session_completed_id');

                if ($request_form_records->getId())
                {

                    $request_form_records_form_json_datas = json_decode($request_form_records->getData('form_json_datas'));
                    $session_result_json_datas            = json_decode($request_form_records->getData('session_result_json_datas'));
                    $request_form_records_form_json_datas = $this->objToArray($request_form_records_form_json_datas);
                    $session_result_json_datas            = $this->objToArray($session_result_json_datas);
                    $extras                               = $this->objToArray($extras);

                    if ($request_form_records_form_json_datas['form_code'] == "motor")
                    {
                        switch (strtolower($request_form_records_form_json_datas['insurance_required']['type_insurance']))
                        {
                            case strtolower(self::TYPE_CMP_LABEL):
                                $type_of_inssurance = self::TYPE_CMP;
                                break;
                            case strtolower(self::TYPE_CTPL_LABEL):
                                $type_of_inssurance = self::TYPE_CTPL;
                                break;
                            case strtolower(self::TYPE_CMP_CTPL_LABEL):
                                $type_of_inssurance = self::TYPE_CMP_CTPL;
                                break;
                            default:
                                $type_of_inssurance = self::TYPE_CMP_CTPL;
                                break;
                        }

                        $extras['extras']['coverage.limit_of_liability.html'] = Mage::app()->getLayout()
                            ->createBlock('core/template')
                            ->setTemplate('megaform_builder/templates/pdf/motor/extra.coverage.limit-of-liability.' . $type_of_inssurance . '.html')
                            ->toHtml();

                        $html_template = $this->embedVariables($this->flatifyArray($extras), $html_template, $callback_specials);
                    }

                    $result = array_merge($request_form_records_form_json_datas, $extras);
                    $result = array_merge($result, $session_result_json_datas);
                    $result = $this->flatifyArray($result);

                    $html_template = $this->embedVariables($result, $html_template, $callback_specials);

                    // fn_mrk($request_form_records_form_json_datas, $result, $this->flatifyArray($extras), $html_template);
                }
            }
        }
        return $html_template;
    }

    /**
     * Embed Variables
     *
     * Replace all variables with corresponding key in array of $vars.
     * This method does not loads data in form but rather can accept variables for the template to use.
     *
     * @param string $vars Form key, Default('')
     * @param string $html_template Html template in string, Default('')
     * @param mixed $callback_specials Life is hard but still you can pass an anonymous function with key then it will be callbacked here.
     * @return string Html string from $html_template that variables has been replaced.
     */
    public function embedVariables($vars = '', $html_template = '', $callback_specials = array())
    {
        foreach ($vars as $key => $value)
        {
            if (is_numeric($value))
            {
                $found = false;
                foreach (array_keys($callback_specials) as $search)
                {
                    if (strpos($key, $search) !== false)
                    {
                        $found = true;
                    }
                }
                if ($found)
                {
                    /***
                     * CLOSURE : Anonymous function callback
                     */
                    $vars[$key] = $callback_specials[$search]($value);
                }
            }
        }

        foreach ($vars as $key => $value)
        {
            $html_template = str_replace('{{' . $key . '}}', $value, $html_template);
        }

        // fn_print_r($vars);

        return $html_template;

    }

    /**
     * Prepare Email Variables
     *
     * Replace all variables with corresponding key in array of $vars.
     * This will also load the datas by form_key so you dont need to pass the form variables
     *
     *
     * @param string $vars Form key, Default('')
     * @return array Email datas body, datas, subject, from_email
     */
    public function prepareEmailVariables($form_key = '')
    {
        $request_session_completed = Mage::getModel('builder/request_session_completed')->load($form_key, 'form_key');

        $extras = array(
            'extras' => array('date_now' => Mage::getModel('core/date')->date('F j Y')),
        );

        $extras['extras']['request.session.completed'] = $request_session_completed->getData();

        if ($request_session_completed->getId())
        {
            $request_form_records = Mage::getModel('builder/request_form_records')
                ->load($request_session_completed->getId(), 'request_session_completed_id');
            $redirect_link   = Mage::getBaseUrl() . "/quote/getquote/index/form_key/" . $request_session_completed->getFormKey();
            $current_product = Mage::getModel('catalog/product')->load($request_session_completed->getProductId());

            $extras['extras']['catalog.product.name'] = $current_product->getName();

            $link = '<a href="' . $redirect_link . '" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=' . $redirect_link . '&amp;source=gmail&amp;ust=1508668796504000&amp;usg=AFQjCNHC33lXVFB4TE2UpaL13HFspdHYAQ">here</a>.</p>';

            $extras['extras']['saved.quote.link'] = $link;

            if ($request_form_records->getId())
            {
                $form_json_datas = json_decode($request_form_records->getData('form_json_datas'));
                $form_json_datas = $this->objToArray($form_json_datas);
                $result          = array();
                $result          = array_merge($form_json_datas, $extras);
                $result          = $this->flatifyArray($result);
            }
        }

        $EMAIL_TEMPLATES_MODEL = Mage::getModel('builder/request_email_templates')->load($request_session_completed->getData('request_form_id'), 'request_form_id');

        $body = $this->embedVariables($result, $EMAIL_TEMPLATES_MODEL->getEmailTemplate());

        return array(
            'body'       => $body,
            'datas'      => $result,
            'subject'    => "Your " . $extras['extras']['catalog.product.name'] . " Application",
            'from_email' => Mage::getStoreConfig('trans_email/ident_support/email'),
        );
    }

    /**
     * Send Form Email
     *
     * This sends out email using magento generic mail sending implementation.
     *
     * @param string $from_name Form Name, Default('')
     * @param string $to_email To Email, Default('')
     * @param string $subject Email Subject, Default('')
     * @param string $body Email Body, Default('')
     * @param string $type Email Type, Default('html')
     * @return Mage_Core_Model_Email instance of Mage_Core_Model_Email
     */
    public function sendFormMail($from_name = "", $from_email = "", $to_email = "", $subject = "", $body = "", $type = 'html')
    {
        $emailTemplate = Mage::getModel('core/email');
        $emailTemplate->setFromName($from_name);
        $emailTemplate->setFromEmail($from_email);
        $emailTemplate->setBody($body);
        $emailTemplate->setSubject($subject);
        $emailTemplate->setType($type);
        $emailTemplate->setToEmail($to_email);
        $emailTemplate->send();
        return $emailTemplate;
    }

    /**
     * Object to Array
     *
     * It accepts array and object and convert to full array in recursive
     *
     * @param mixed $obj Object to be converted
     * @param mixed $arr Array referenced as we it loops deeper
     * @return array Full converted array from object
     */
    public function objToArray($obj = array(), &$arr = array())
    {

        if (empty($obj))
        {
            return array();
        }

        if (!is_object($obj) && !is_array($obj))
        {
            $arr = $obj;
            return $arr;
        }

        foreach ($obj as $key => $value)
        {
            if (!empty($value))
            {
                $arr[$key] = array();
                $this->objToArray($value, $arr[$key]);
            }
            else
            {
                $arr[$key] = $value;
            }
        }
        return $arr;
    }

    /**
     * Flatify Array
     *
     * It flattens a multi dimensional aray into one flat array with delimiter glue.
     *
     * @param mixed $array Multi Array data to be flatten
     * @param string $prefix Prefix to be prepended in start of array key
     * @param string $glue Delimeter to be glue between parent child array relation.
     * @return array One flat array | One dimensional array
     */
    public function flatifyArray($array, $prefix = '', $glue = ".")
    {
        $result = array();

        foreach ($array as $key => $value)
        {
            $new_key = $prefix . (empty($prefix) ? '' : $glue) . $key;

            if (is_array($value))
            {
                $result = array_merge($result, $this->flatifyArray($value, $new_key));
            }
            else
            {
                $result[$new_key] = $value;
            }
        }

        return $result;
    }

    /**
     * UnFlatify Array
     *
     * Don't preserve array tree
     *
     * It converts one flat array into multi dimensional
     * array with parent child relation.
     * But this only converts last instance of the array tree
     *
     * @param mixed $array Array data to be inflate
     * @param string $prefix Prefix to be prepended in start of array key
     * @param string $glue Delimeter to be glue between parent child array relation.
     * @return array Multi dimensional array
     */
    public function unFlatifyArray($array, $prefix = '', $glue = ".")
    {
        $result = array();
        foreach ($array as $key => $value)
        {
            $new_key = $prefix . (empty($prefix) ? '' : $glue) . $key;
            $key_ar  = explode(".", $key);

            if (count($key_ar) > 1)
            {
                $first  = array_shift($key_ar);
                $key_ar = implode(".", $key_ar);
                $froms  = $this->unFlatifyArray(array($key_ar => $value));

                foreach ($froms as $key => $from)
                {
                    if (is_array($from))
                    {
                        $result[$first][$key] = $from;
                        // $result[$first] = array_merge($result[$first] , $from);
                    }
                    else
                    {
                        $result[$first][$key] = $from;
                    }
                }
            }
            else
            {
                $result[$key_ar[0]] = $value;
            }
        }

        return $result;
    }

    /**
     * UnFlatify Arrays
     *
     * Preserves array tree
     *
     * It converts one flat array into multi dimensional array with parent child relation
     * But this preserves all array tree
     *
     * @param mixed $array Array data to be inflate
     * @param string $glue Delimeter to be glue between parent child array relation.
     * @return array Multi dimensional array
     */
    public function unFlatifyArrays($arrays = array(), $glue = ".")
    {
        $new_arrays = array();

        foreach ($arrays as $key => $value)
        {
            $per_array  = Mage::helper('builder')->unFlatifyArray(array($key => $value));
            $new_arrays = array_replace_recursive($new_arrays, $per_array);
        }

        return $new_arrays;
    }

    /**
     * Varien Objectify array
     *
     * Preserves array object tree
     *
     * It converts each multi dimensional array into instance of Varien_Object with parent child relation
     *
     * @param mixed $array Array data to be inflate
     * @param string $glue Delimeter to be glue between parent child array relation.
     * @return Varien_Object new instance of Varien_Object
     */
    public function varienObjectifyArray($array)
    {
        $result = array();
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $result[$key] = $this->varienObjectifyArray($value);
            }
            else
            {
                $result[$key] = $value;
            }
        }

        return new Varien_Object($result);
    }

    /**
     * Stack Group Multiple
     *
     * It stacks multi arrray into new multi array but with the array key in a groupd of array
     *
     * @param array $_data Array data to be stacked, Default(empty array)
     * @return array  Stackked arrays
     */
    public function stackGroupMultiple($_data = array())
    {
        $new_data = array();

        if (is_null($_data))
        {
            $_data = array();
        }

        $_keys = array_keys($_data);

        foreach ($_keys as $name)
        {
            foreach ($_data[$name] as $key => $value)
            {
                $new_data[$key][$name] = $value;
            }
        }

        return $new_data;
    }

    /**
     * Reformat Date
     *
     * This method is handy for converting date string into another format of date string.
     *
     * @param string $date_string Date string to be formatted, Default('')
     * @param string $format_identification Format of the date string, this must be in php date format compliance, Example : 'Y-m-d' Default('')
     * @param string $format_output Output format of the date string, this must be in php date format compliance, Example : 'Y-m-d' Default('')
     * @return string Formatted date string
     */
    public function reformatDate($date_string = '', $format_identification = '', $format_output = '')
    {
        $format_identification_default = 'Y-m-d';
        $format_output_default         = 'Y-m-d';

        if (empty($format_identification))
        {
            $format_identification = $format_identification_default;
        }

        if (empty($format_output))
        {
            $format_output = $format_output_default;
        }

        if (empty($date_string))
        {
            return $date_string;
        }

        $date = DateTime::createFromFormat($format_identification, $date_string);

        return $date->format($format_output);
    }

    /**
     * Get Cart Item Products Id's
     *
     * Getting all items product id in cart checkout
     *
     * @return array Array set of product ids from cart tiems
     */
    public function getCartItemProductsIds()
    {

        $quote      = Mage::getModel('checkout/cart')->getQuote();
        $productIds = array();

        foreach ($quote->getAllItems() as $item)
        {
            // if ($item->getProductType() == 'simple')
            // {
            $productIds[] = $item->getProductId();
            // }
        }
        return $productIds;
    }

    /**
     * Get Customer Id
     *
     * Getting customer ID
     *
     * @return int Customer id, Return 0 if none
     */
    public function getCustomerId()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn())
        {
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            return $customerData->getId();
        }
        return 0;

    }

    /**
     * Generate Form Key
     *
     * Generates form key and set it into current session.
     * This implements like the singleton implementation,
     * thus it won't create another form key if form key exists already.
     *
     * Form key format is in
     * <code>
     * md5(session_id() + time())
     * </code>
     *
     * @return string Mega Build Form Key in string format
     */
    public function generateFormKey()
    {
        //generate new form key if exist or none
        if (empty(Mage::getSingleton('core/session')->getMegaBuildFormKey()) || $this->isKeyUsed(Mage::getSingleton('core/session')->getMegaBuildFormKey()))
        {
            Mage::getSingleton('core/session')->setMegaBuildFormKey(md5(session_id() + time()));
        }

        return Mage::getSingleton('core/session')->getMegaBuildFormKey();
    }

    /**
     * Get Form Pdf Url
     *
     * Generates form pdf by form key found on url query as 'previous_form_key'
     *
     * @return string Form Pdf accessible url
     */
    public function getFormPdfUrl()
    {
        $_url = "";
        if (Mage::app()->getRequest()->getParam('previous_form_key', false))
        {
            $request_session_completed = Mage::getModel('builder/request_session_completed')->load(Mage::app()->getRequest()->getParam('previous_form_key'), 'form_key');

            if ($request_session_completed->getId())
            {
                $form_code         = Mage::getModel('builder/request_forms')->load($request_session_completed->getRequestFormId());
                $request_pdf_files = Mage::helper("builder/domPdf")->quickPdf($form_code->getFormCode());
                // $request_pdf_files = Mage::getModel('builder/request_pdf_files')->load($request_session_completed->getData('request_pdf_files_id'));

                if ($request_pdf_files->getId())
                {
                    $_url = Mage::getBaseUrl() . $request_pdf_files->getData('url');
                }
            }
        }
        return $_url;
    }

    /**
     * Is Key Used
     *
     * Checking if key is used on session completed
     *
     * @param string|null $key Form key
     * @return int Returns the session key id if it matches on database table then zero if none.
     */
    public function isKeyUsed($key = null)
    {
        if (empty($key) || is_null($key))
        {
            return 0;
        }

        $REQUEST_SESSION_COMPLETED_MODEL = Mage::getModel(REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL . "/" . REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL_ENTITY)->load($key, 'form_key');

        if ($REQUEST_SESSION_COMPLETED_MODEL->getId())
        {
            return $REQUEST_SESSION_COMPLETED_MODEL->getId();
        }

        return 0;

    }

    /**
     * Update Dis Separator
     *
     * This replace all double backslashes in the string.
     *
     * @param string $path String to be cleaned
     * @return string Cleand path
     */
    public function updateDirSepereator($path)
    {
        return str_replace('\\', DS, $path);
    }

    /**
     * Get Assets
     *
     * More like hard coded assets,
     * this method returns all assets in string to be printed out in frontend
     *
     * @return string Html assets
     */
    public function getAssets()
    {
        $assets = '<link rel="stylesheet" href="' . Mage::getBaseUrl() . '/js/bootstrapvalidator-master/dist/css/bootstrapValidator.css"/>';
        $assets .= '<script type="text/javascript" src="' . Mage::getBaseUrl() . '/js/bootstrapvalidator-master/dist/js/bootstrapValidator.js"></script>';
        $assets .= '<script type="text/javascript" src="' . Mage::getBaseUrl() . '/js/moment-develop/min/moment-with-locales.min.js"></script>';
        // $assets .= '<link rel="stylesheet" href="' . Mage::getBaseUrl() . '/js/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.css"/>';
        // $assets .=  '<script type="text/javascript" src="' . Mage::getBaseUrl() . '/js/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js"></script>';

        $assets .= '<script type="text/javascript" src="' . Mage::getBaseUrl() . '/js/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.js"></script>';
        // $assets .= '<link rel="stylesheet" href="/skin/frontend/default/default/megaform_builder/css/main.css"/>';
        $assets .= '<link rel="stylesheet" href="' . Mage::getDesign()->getSkinUrl('megaform_builder/css/main.css') . '"/>';

        return $assets;
    }

    /**
     * Is Cart Form Products Exists
     *
     * This method checks if the form products is existing on cart items.
     *
     * @param string $form_code Form Code
     * @return boolean Returns true if one of cart items is matched in form products ids
     */
    public function isCartFormProductExists($form_code = "")
    {
        $_cart_item_ids = Mage::helper('builder')->getCartItemProductsIds();

        $form_product_ds = Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)
            ->load($form_code, 'form_code')
            ->getFormProductIds();

        if (!(count(array_intersect($form_product_ds, $_cart_item_ids)) > 0))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Get Form Code By Form Key
     *
     * This method gets the corresponding form code by form key
     *
     * @param string $form_key Form Key
     * @return boolean|string Returns form code if exists then false if not
     */
    public function getFormCodeByFormKey($form_key = "")
    {
        $model = Mage::getModel('builder/request_session_completed')->load($form_key, 'form_key');
        if ($model->getId())
        {
            $form = Mage::getModel('builder/request_forms')->load($model->getRequestFormId());
            if ($form->getId())
            {
                return $form->getFormCode();
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    /**
     * Get Datas By Form Key Possible
     *
     * This method gets the possible form matches of two forms in common keys,
     * and it returns the relevant matches datas.
     *
     * @param string $form_key Form Key
     * @param string $previous_form_key Previous Form Key
     * @return builder_request_form_records instance of builder/request_form_records model
     */
    public function getDatasByFormKeyPossible($form_code = "", $previous_form_key = "")
    {
        $collections = Mage::getModel('builder/request_form_records')->getCollection();

        if (!empty($form_code))
        {
            $collections->addFieldToFilter(
                array('form_json_datas'),
                array(
                    array('like' => '%' . '"form_code":"' . $form_code . '"' . '%'),
                )
            );
        }

        if (!empty($form_key))
        {
            $collections->addFieldToFilter(
                array('form_json_datas'),
                array(
                    array('like' => '%' . '"previous_form_key":"' . $previous_form_key . '"' . '%'),
                )
            );
        }

        return $collections;
    }

    /**
     * Get Datas By Form Key Recursive
     *
     * It fetches all form data in backwards motion.
     * so for example if you supplied the form key
     * then it loops and gets all form datas by previous_form_key,
     * until previous_form_key is empty.
     *
     * <code>
     * array
     * (
     * &nbsp;&nbsp; &nbsp;form_code_1 = array
     * &nbsp;&nbsp; &nbsp;(
     * &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;datas
     * &nbsp;&nbsp; &nbsp;),
     * &nbsp;&nbsp; &nbsp;form_code_2 = array
     * &nbsp;&nbsp; &nbsp;(
     * &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;datas
     * &nbsp;&nbsp; &nbsp;)
     * )
     * </code>
     *
     * @param string $form_key Form Key
     * @return array Set of all form data with form code array keys
     */
    public function getDatasByFormKeyRecursive($form_key = "")
    {
        $fetched = array();

        if (!empty($form_key))
        {
            $fetched = $this->getDatasByFormKey($form_key);
        }

        $fetches[] = $fetched;

        if (!empty($fetched['previous_form_key']))
        {
            $new_fetches = $this->getDatasByFormKeyRecursive($fetched['previous_form_key']);
            $fetches     = array_merge($fetches, $new_fetches);
        }
        else
        {
            return $fetches;
        }

        $new_fetches = array();
        foreach ($fetches as $key => $fetched)
        {
            $new_fetches[@$fetched['form_code']] = $fetched;
        }

        return $new_fetches;
    }

    /**
     * Get Datas By Form Key Single
     *
     * It fetches all form data
     *
     * @param string $form_key Form Key
     * @return array Set of all form data
     */
    public function getDatasByFormKey($form_key = "")
    {
        if (empty($form_key))
        {
            return $form_key;
        }

        $request_session_completed = Mage::getModel('builder/request_session_completed')->load($form_key, 'form_key');

        $result = array();

        $extras = array(
            'extras' => array('date_now' => Mage::getModel('core/date')->date('F j Y')),
        );

        $extras['extras']['request.session.completed'] = $request_session_completed->getData();

        if ($request_session_completed->getId())
        {
            $request_form_records = Mage::getModel('builder/request_form_records')
                ->load($request_session_completed->getId(), 'request_session_completed_id');
            $redirect_link   = Mage::getBaseUrl() . "/quote/getquote/index/form_key/" . $request_session_completed->getFormKey();
            $current_product = Mage::getModel('catalog/product')->load($request_session_completed->getProductId());

            $extras['extras']['catalog.product.name'] = $current_product->getName();

            $link = '<a href="' . $redirect_link . '" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=' . $redirect_link . '&amp;source=gmail&amp;ust=1508668796504000&amp;usg=AFQjCNHC33lXVFB4TE2UpaL13HFspdHYAQ">here</a>.</p>';

            $extras['extras']['saved.quote.link'] = $link;

            if ($request_form_records->getId())
            {
                $session_result_json_datas = json_decode($request_form_records->getData('session_result_json_datas'));
                $session_result_json_datas = $this->objToArray($session_result_json_datas);
                $form_json_datas           = json_decode($request_form_records->getData('form_json_datas'));
                $form_json_datas           = $this->objToArray($form_json_datas);
                $result                    = array_merge($session_result_json_datas, $extras);
                $result                    = array_merge($result, $form_json_datas);
                $result                    = $this->flatifyArray($result);
            }
        }

        return $result;
    }

    /**
     * Get Form Pdf Template
     *
     * It gets the string template of form pdf template
     *
     * @param string $form_code Form Key, Default ('')
     * @return string Pdf template
     */
    public function getFormPdfTemplate($form_code = '')
    {
        $REQUEST_FORMS_MODEL = Mage::getModel('builder/request_forms')->load($form_code, 'form_code');
        $PDF_TEMPLATES_MODEL = Mage::getModel('builder/request_pdf_templates')->load($REQUEST_FORMS_MODEL->getId(), 'request_form_id');
        return $PDF_TEMPLATES_MODEL->getData('pdf_template');
    }

    /**
     * Get Static Nationalities Options
     *
     * Returns hardcoded nationalities for option purposes
     *
     * @return string Array sets of nationalities
     */
    public function getStaticNationalitiesOptions()
    {
        $nationals = array(
            'Afghan',
            'Albanian',
            'Algerian',
            'American',
            'Andorran',
            'Angolan',
            'Antiguans',
            'Argentinean',
            'Armenian',
            'Australian',
            'Austrian',
            'Azerbaijani',
            'Bahamian',
            'Bahraini',
            'Bangladeshi',
            'Barbadian',
            'Barbudans',
            'Batswana',
            'Belarusian',
            'Belgian',
            'Belizean',
            'Beninese',
            'Bhutanese',
            'Bolivian',
            'Bosnian',
            'Brazilian',
            'British',
            'Bruneian',
            'Bulgarian',
            'Burkinabe',
            'Burmese',
            'Burundian',
            'Cambodian',
            'Cameroonian',
            'Canadian',
            'Cape Verdean',
            'Central African',
            'Chadian',
            'Chilean',
            'Chinese',
            'Colombian',
            'Comoran',
            'Congolese',
            'Costa Rican',
            'Croatian',
            'Cuban',
            'Cypriot',
            'Czech',
            'Danish',
            'Djibouti',
            'Dominican',
            'Dutch',
            'East Timorese',
            'Ecuadorean',
            'Egyptian',
            'Emirian',
            'Equatorial Guinean',
            'Eritrean',
            'Estonian',
            'Ethiopian',
            'Fijian',
            'Filipino',
            'Finnish',
            'French',
            'Gabonese',
            'Gambian',
            'Georgian',
            'German',
            'Ghanaian',
            'Greek',
            'Grenadian',
            'Guatemalan',
            'Guinea-Bissauan',
            'Guinean',
            'Guyanese',
            'Haitian',
            'Herzegovinian',
            'Honduran',
            'Hungarian',
            'I-Kiribati',
            'Icelander',
            'Indian',
            'Indonesian',
            'Iranian',
            'Iraqi',
            'Irish',
            'Israeli',
            'Italian',
            'Ivorian',
            'Jamaican',
            'Japanese',
            'Jordanian',
            'Kazakhstani',
            'Kenyan',
            'Kittian and Nevisian',
            'Kuwaiti',
            'Kyrgyz',
            'Laotian',
            'Latvian',
            'Lebanese',
            'Liberian',
            'Libyan',
            'Liechtensteiner',
            'Lithuanian',
            'Luxembourger',
            'Macedonian',
            'Malagasy',
            'Malawian',
            'Malaysian',
            'Maldivan',
            'Malian',
            'Maltese',
            'Marshallese',
            'Mauritanian',
            'Mauritian',
            'Mexican',
            'Micronesian',
            'Moldovan',
            'Monacan',
            'Mongolian',
            'Moroccan',
            'Mosotho',
            'Motswana',
            'Mozambican',
            'Namibian',
            'Nauruan',
            'Nepalese',
            'New Zealander',
            'Nicaraguan',
            'Nigerian',
            'Nigerien',
            'North Korean',
            'Northern Irish',
            'Norwegian',
            'Omani',
            'Pakistani',
            'Palauan',
            'Panamanian',
            'Papua New Guinean',
            'Paraguayan',
            'Peruvian',
            'Polish',
            'Portuguese',
            'Qatari',
            'Romanian',
            'Russian',
            'Rwandan',
            'Saint Lucian',
            'Salvadoran',
            'Samoan',
            'San Marinese',
            'Sao Tomean',
            'Saudi',
            'Scottish',
            'Senegalese',
            'Serbian',
            'Seychellois',
            'Sierra Leonean',
            'Singaporean',
            'Slovakian',
            'Slovenian',
            'Solomon Islander',
            'Somali',
            'South African',
            'South Korean',
            'Spanish',
            'Sri Lankan',
            'Sudanese',
            'Surinamer',
            'Swazi',
            'Swedish',
            'Swiss',
            'Syrian',
            'Taiwanese',
            'Tajik',
            'Tanzanian',
            'Thai',
            'Togolese',
            'Tongan',
            'Trinidadian/Tobagonian',
            'Tunisian',
            'Turkish',
            'Tuvaluan',
            'Ugandan',
            'Ukrainian',
            'Uruguayan',
            'Uzbekistani',
            'Venezuelan',
            'Vietnamese',
            'Welsh',
            'Yemenite',
            'Zambian',
            'Zimbabwean',
        );

        $option_array = array();

        $option_array[] = array(
            'label' => 'Nationality *',
            'value' => '',
        );

        foreach ($nationals as $national)
        {
            $option_array[] = array(
                'value' => $national,
                'label' => $national,
            );
        }

        return $option_array;
    }

}
