<?php

class MegaForm_Builder_Helper_Template extends Mage_Core_Helper_Abstract
{
    const CODE_MOTOR       = 'motor';
    const CODE_FIRE        = 'fire';
    const CODE_ITP         = 'itp';
    const CODE_VEHICLEINFO = 'vehicleInfo';

    public function cleanTemplate($form_summary_template = "")
    {
        $filter                = new Mage_Widget_Model_Template_Filter();
        $form_summary_template = $filter->filter($form_summary_template);
        return $form_summary_template;
    }

    /**
     * To do replace all variables with corresponding datas
     */
    public function embedFormVariables($form_summary_template = "", $extras = "")
    {
        if (Mage::app()->getRequest()->getParam('previous_form_key', false))
        {
            $request_session_completed = Mage::getModel('builder/request_session_completed')->load(Mage::app()->getRequest()->getParam('previous_form_key'), 'form_key');

            if ($request_session_completed->getId())
            {
                $request_form_records = Mage::getModel('builder/request_form_records')
                    ->load($request_session_completed->getId(), 'request_session_completed_id');

                if ($request_form_records->getId())
                {
                    $request_form_records_form_json_datas = json_decode($request_form_records->getData('form_json_datas'));
                    $request_form_records_form_json_datas = $this->objToArray($request_form_records_form_json_datas);
                    $result = array_merge($request_form_records_form_json_datas , $extras);

                    $request_form_records->addData(array('session_result_json_datas' => json_encode($result)))->save();
                    $result                             = $this->flatifyArray($result);

                    $form_summary_template = $this->formProcessSwitcher($request_form_records_form_json_datas['form_code'], $result, $form_summary_template);
                }
            }
        }
        return $form_summary_template;
    }

    public function formProcessSwitcher($form_code = '', $computed = '', $form_summary_template = '')
    {
        switch ($form_code)
        {
            case self::CODE_MOTOR:
                return $this->forMotor($form_code, $computed, $form_summary_template);
                break;
            case self::CODE_ITP:
                return $this->forItp($form_code, $computed, $form_summary_template);
                break;
            case self::CODE_VEHICLEINFO:
                return $this->forVehicleInfo($form_code, $computed, $form_summary_template);
                break;
            case self::CODE_FIRE:
                return $this->forFire($form_code, $computed, $form_summary_template);
                break;

            default:
                return $form_summary_template;
                break;
        }
    }

    public function forMotor($form_code = '', $computed = '', $form_summary_template = '')
    {
        foreach ($computed as $key => $value)
        {
            if (is_numeric($value))
            {
                $found = false;
                foreach (array('premium_computation_wo_aon', 'premium_computation', 'coverage') as $search)
                {
                    if (strpos($key, $search) !== false)
                    {
                        $found = true;
                    }
                }
                if ($found)
                {
                    $computed[$key] = number_format($value, 2);
                }
            }
        }

        foreach ($computed as $key => $value)
        {
            $form_summary_template = str_replace('{{' . $key . '}}', $value, $form_summary_template);
        }

        return $form_summary_template;

    }

    public function forItp()
    {
        # code...
    }

    public function forVehicleInfo()
    {
        # code...
    }

    public function forFire()
    {
        # code...
    }

    public function objToArray($obj = array(), &$arr = array())
    {

        if (!is_object($obj) && !is_array($obj))
        {
            $arr = $obj;
            return $arr;
        }

        foreach ($obj as $key => $value)
        {
            if (!empty($value))
            {
                $arr[$key] = array();
                $this->objToArray($value, $arr[$key]);
            }
            else
            {
                $arr[$key] = $value;
            }
        }
        return $arr;
    }

    public function flatifyArray($array, $prefix = '')
    {
        $result = array();

        foreach ($array as $key => $value)
        {
            $new_key = $prefix . (empty($prefix) ? '' : '.') . $key;

            if (is_array($value))
            {
                $result = array_merge($result, $this->flatifyArray($value, $new_key));
            }
            else
            {
                $result[$new_key] = $value;
            }
        }

        return $result;
    }
}
