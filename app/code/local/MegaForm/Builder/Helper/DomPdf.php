<?php

require_once Mage::getBaseDir('lib') . DS . 'dompdf' . DS . 'autoload.inc.php';

use Dompdf\Dompdf;
use Dompdf\Options;
use MegaForm_Builder_Model_Request_Pdf_Files as PDF_FILES_MODEL;

class MegaForm_Builder_Helper_DomPdf extends Mage_Core_Helper_Abstract
{
    /**
     * Session folder MD5 ( encryptedSessionId + requestFormKey )
     */
    public $sessionFolder = '';

    /**
     * base folder of media pdf
     */
    public $mediaFolder = "mega_form_pdf";

    /**
     * base file name
     */
    public $base_pdf_name = "form_request";

    /**
     * pdf
     */
    public $pdf;

    /**
     * page
     */
    public $page;

    /**
     * template_file
     */
    public $template_file;

    /**
     * font
     */
    public $font;

    /**
     * Model : REQUEST_FORMS_MODEL
     */
    public $REQUEST_FORMS_MODEL;

    /**
     * Model : PDF_TEMPLATES_MODEL
     */
    public $PDF_TEMPLATES_MODEL;

    /**
     * instance lib/Dompdf
     */
    public $dompdf;

    /**
     * instance lib/Dompdf/Options
     */
    public $Dompdf_Options;

    /**
     * paper size
     */
    public $paper_size = 'A4';

    /**
     * Css files
     */
    public $_css = [];

    /**
     * Constant Entities
     */
    const ORIENTATION_LANDSCAPE = 'landscape';
    const ORIENTATION_PORTRAIT  = 'portrait';
    const WYSWIG_PAGE_BREAK     = '<!-- pagebreak -->';
    const PAGE_BREAK            = '<div style="page-break-before: always;"></div>';

    const TEST_VARIABLE       = "{{test_variable}}";
    const TEST_VARIABLE_VALUE = " Hey your test variable here is replaced! ";

    const DEAULT_FONT = "Arial, Helvetica, sans-serif";

    /**
     * Class Contructor
     */
    public function __construct()
    {
        $this->Dompdf_Options = new Options();

        $this->Dompdf_Options->set('defaultFont', self::DEAULT_FONT);
        $this->Dompdf_Options->set('isRemoteEnabled', true);
        $this->Dompdf_Options->set('debugKeepTemp', true);
        $this->Dompdf_Options->set('isHtml5ParserEnabled', true);

        $this->dompdf = new Dompdf($this->Dompdf_Options);

        $this->_css[] = '<link type="text/css" rel="stylesheet" href="http://local.ucpb/js/tiny_mce/themes/advanced/skins/default/content.css">';
        $this->_css[] = '<link type="text/css" rel="stylesheet" href="http://local.ucpb/js/mage/adminhtml/wysiwyg/tiny_mce/themes/advanced/skins/default/content.css">';
    }

    public function addCssFile($css = '')
    {
        $this->css[] = '<link type="text/css" rel="stylesheet" href="' . $css . '">';
    }

    /**
     * This will generate a pdf file
     * @param boolean is_stream = true then it downloads
     * @return String Url path of the file false if not created
     */
    public function generatePdf($form_key = "", $is_stream = false)
    {
        $_template = $this->PDF_TEMPLATES_MODEL->getData('pdf_template');

        /**
         * Render magento cms variables
         */
        $filter             = new Mage_Widget_Model_Template_Filter();
        $_template_filtered = $filter->filter($_template);

        /**
         * Add extra external stylesheets
         */
        $_css               = implode("\n", $this->_css);
        $_template_filtered = $_css . "\n" . $_template_filtered;

        /**
         * Replace page break from tnymce by dompdf bage breaks
         */
        $_template_filtered = str_replace(self::WYSWIG_PAGE_BREAK, self::PAGE_BREAK, $_template_filtered);
        $_template_filtered = str_replace(self::TEST_VARIABLE, self::TEST_VARIABLE_VALUE, $_template_filtered);

        $form_key = Mage::app()->getRequest()->getParam('form_key');

        $number_format = function ($num) {
            return number_format($num, 2);
        };
        $int_format = function ($num) {
            return (int) $num;
        };

        $callback_specials = array(
            'premium_computation_wo_aon'                => $number_format,
            'premium_values'                            => $number_format,
            'premium_computation.net_premium'           => $number_format,
            'premium_computation.doc_stamps'            => $number_format,
            'premium_computation.vat'                   => $number_format,
            'premium_computation.local_government_tax'  => $number_format,
            'premium_computation.interconnectivity'     => $number_format,
            'premium_computation.total_premium'         => $number_format,
            'premium_computation.less_value'            => $number_format,
            'insurance_required_others.bodily_injury'   => $number_format,
            'coverage.compulsary_third_party_liability' => $number_format,
            'coverage.own_damage_and_theft'             => $number_format,
            'coverage.bodily_injury'                    => $number_format,
            'coverage.property_damage'                  => $number_format,
            'coverage.auto_passenger'                   => $number_format,
            'coverage.acts_of_nature'                   => $number_format,
            'coverage.deductible'                       => $number_format,
            'form_datas.value_vehicle.fmv'              => $number_format,
        );

        $extras = array(
            'extras' => array('date_now' => Mage::getModel('core/date')->date('F j, Y')),
        );

        $_template_filtered = Mage::helper('builder')->embedFormVariables($form_key, $_template_filtered, $extras, $callback_specials);

        $this->dompdf->loadHtml($_template_filtered);

        // fn_mrk($this->dompdf);

        $this->dompdf->setPaper($this->paper_size, self::ORIENTATION_PORTRAIT);

        $this->dompdf->render();

        $media_path = Mage::getBaseDir('media') . DS . $this->mediaFolder . DS . $this->sessionFolder;
        $url_path   = Mage::getBaseUrl('media') . DS . $this->mediaFolder . DS . $this->sessionFolder;

        $this->prepareDirectory($media_path);

        $comp_path     = $media_path . DS . $this->base_pdf_name . '.pdf';
        $comp_url_path = $url_path . DS . '\\\\\\' . $this->base_pdf_name . '.pdf';

        $output = $this->dompdf->output();
        file_put_contents($comp_path, $output);

        if (file_exists($comp_path)) {
            $Form_Pdf = Mage::getModel(PDF_FILES_MODEL::MODULE_MODEL . "/" . PDF_FILES_MODEL::MODULE_MODEL_ENTITY);
            $pdf_data = array(
                'name'          => basename($comp_url_path),
                'path'          => str_replace(basename($comp_url_path), '', str_replace(Mage::getBaseDir(), '', $comp_path)),
                'url'           => str_replace(Mage::getBaseUrl(), '', $comp_url_path),
                'date_created'  => Mage::getModel('core/date')->date('Y-m-d H:i:s'),
                'date_modified' => Mage::getModel('core/date')->date('Y-m-d H:i:s'),
            );
            $pdf_data['url']  = $this->normalizePath($pdf_data['url']);
            $pdf_data['path'] = $this->normalizePath($pdf_data['path']);

            $Form_Pdf->setData($pdf_data);
            $Form_Pdf->save();
            if (!$is_stream) {
                return $Form_Pdf;
            }
        }

        //for download only
        if ($is_stream) {
            return $this->dompdf->stream();
        }

        return false;
    }

    public function manualGeneratePdf($form_key = "", $file_full_path = "", $html = "")
    {
        /**
         * Render magento cms variables
         */
        $filter             = new Mage_Widget_Model_Template_Filter();
        $_template_filtered = $filter->filter($html);

        /**
         * Add extra external stylesheets
         */
        $_css               = implode("\n", $this->_css);
        $_template_filtered = $_css . "\n" . $_template_filtered;

        /**
         * Replace page break from tnymce by dompdf bage breaks
         */
        $_template_filtered = str_replace(self::WYSWIG_PAGE_BREAK, self::PAGE_BREAK, $_template_filtered);
        $_template_filtered = str_replace(self::TEST_VARIABLE, self::TEST_VARIABLE_VALUE, $_template_filtered);

        $this->dompdf->loadHtml($_template_filtered);

        $this->dompdf->setPaper($this->paper_size, self::ORIENTATION_PORTRAIT);

        $this->dompdf->render();

        $filename = basename($file_full_path);

        $destination_path =  str_replace($filename, "", $file_full_path);

        /**
        * Safe lock of file extension
        */
        $info = pathinfo($filename);
        $filename = $info['filename'] . '.' . "pdf";

        $media_path = Mage::getBaseDir('media') . DS . $destination_path;
        $url_path   = Mage::getBaseUrl('media') . DS . $destination_path;

        $this->prepareDirectory($media_path);

        $comp_path     = $media_path . DS . $filename;
        $comp_url_path = $url_path . DS . '\\\\\\' . $filename;

        $output = $this->dompdf->output();
        file_put_contents($comp_path, $output);

        $pdf_data = array(
            'name'          => basename($comp_url_path),
            'path'          => str_replace(basename($comp_url_path), '', str_replace(Mage::getBaseDir(), '', $comp_path)),
            'url'           => str_replace(Mage::getBaseUrl(), '', $comp_url_path),
            'date_created'  => Mage::getModel('core/date')->date('Y-m-d H:i:s'),
            'date_modified' => Mage::getModel('core/date')->date('Y-m-d H:i:s'),
        );

        return $pdf_data;
    }

    public function quickPdf($form_code)
    {
        $REQUEST_FORMS_MODEL = Mage::getModel('builder/request_forms')->load($form_code, 'form_code');
        $PDF_TEMPLATES_MODEL = Mage::getModel('builder/request_pdf_templates')->load($REQUEST_FORMS_MODEL->getId(), 'request_form_id');
        $_requestFormKey     = Mage::app()->getRequest()->getParam('previous_form_key');
        //current session id;
        $_encryptedSessionId = Mage::getSingleton('core/session')->getEncryptedSessionId();
        $_sessionFolder      = md5($_encryptedSessionId . $_requestFormKey);
        //todo generate pdf by template
        $this->sessionFolder             = $_sessionFolder;
        $this->PDF_TEMPLATES_MODEL       = $PDF_TEMPLATES_MODEL;
        $request_file                    = $this->generatePdf($_requestFormKey);
        $REQUEST_SESSION_COMPLETED_MODEL = Mage::getModel("builder/request_session_completed")->load($_requestFormKey, 'form_key');
        $REQUEST_SESSION_COMPLETED_MODEL->setRequestPdfFilesId($request_file->getId());
        $REQUEST_SESSION_COMPLETED_MODEL->save();
        return $request_file;
    }

    /**
     * This will fix any misstep on directory seperators
     * @param path string
     * @return path string
     */
    public function normalizePath($path)
    {
        $parts    = array(); // Array to build a new path from the good parts
        $path     = str_replace('\\', '/', $path); // Replace backslashes with forwardslashes
        $path     = preg_replace('/\/+/', '/', $path); // Combine multiple slashes into a single slash
        $segments = explode('/', $path); // Collect path segments
        $test     = ''; // Initialize testing variable
        foreach ($segments as $segment) {
            if ($segment != '.') {
                $test = array_pop($parts);
                if (is_null($test)) {
                    $parts[] = $segment;
                } else if ($segment == '..') {
                    if ($test == '..') {
                        $parts[] = $test;
                    }

                    if ($test == '..' || $test == '') {
                        $parts[] = $segment;
                    }

                } else {
                    $parts[] = $test;
                    $parts[] = $segment;
                }
            }
        }
        return implode('/', $parts);
    }

    /**
     * This will prepare the directory with htaccess
     * @return void
     */
    public function prepareDirectory($full_path = '')
    {
        $io = new Varien_Io_File();
        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $full_path));
        if ($io->fileExists('.htaccess') && !$io->isWriteable('.htaccess')) {
            // file does not exist or is not readable
            return;
        }

        $content = "Order Allow,Deny \n";
        $content .= "Deny from all \n";
        $content .= '<Files ~ "\.(gif|jpg|png|pdf)$">' . " \n";
        $content .= "Allow from all \n";
        $content .= "</Files> \n";

        $io->streamOpen('.htaccess');
        $io->streamWrite($content);
        $io->streamClose();

        return;
    }
}
