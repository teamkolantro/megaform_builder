<?php

use MegaForm_Builder_Model_Request_Pdf_Files as PDF_FILES_MODEL;

class MegaForm_Builder_Helper_Pdf extends Mage_Core_Helper_Abstract
{
    /**
     * Session folder MD5 ( encryptedSessionId + requestFormKey )
     */
    public $sessionFolder = '';

    /**
     * base folder of media pdf
     */
    public $mediaFolder = "mega_form_pdf";

    /**
     * base file name
     */
    public $base_pdf_name = "form_request";

    /**
     * instance Zend_Pdf
     */
    public $pdf;

    /**
     * instance Zend_Pdf_Page
     */
    public $page;

    /**
     * instance Zend_Pdf_Font
     */
    public $font;

    /**
     * Constant entities
     */
    const FONT_SIZE       = 12;
    const MARGIN_LEFT     = 40;
    const MARGIN_TOP      = 40;
    const COL_WIDTH       = 100;
    const COL_LEFT_MARGIN = 10;
    const COL_SEPARATOR   = '|';
    const ROW_HEIGHT      = 20;

    public function _construct()
    {
        parent::_construct();
    }

    public function _init()
    {
        $this->pdf  = new Zend_Pdf();
        $this->page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
        $this->font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
    }

    /**
     * This will generate a pdf file
     * @return String Url path of the file false if not created
     */
    public function generatePdf()
    {
        $this->_init();
        $row = 1;
        $x   = self::MARGIN_LEFT;
        $y   = self::MARGIN_TOP;

        $this->page->setFont($this->font, self::FONT_SIZE);

        $this->page->drawText('ABCD', $x, $this->page->getHeight() - $y);
        $x += self::COL_WIDTH;
        $x += self::COL_LEFT_MARGIN;
        $x = self::MARGIN_LEFT;
        $y += self::ROW_HEIGHT;

        $this->pdf->pages[] = $this->page;

        $media_path = Mage::getBaseDir('media') . DS . $this->mediaFolder . DS . $this->sessionFolder;
        $url_path   = Mage::getBaseUrl('media') . DS . $this->mediaFolder . DS . $this->sessionFolder;

        $this->prepareDirectory($media_path);

        $comp_path     = $media_path . DS . $this->base_pdf_name . '.pdf';
        $comp_url_path = $url_path . DS . '\\\\\\' . $this->base_pdf_name . '.pdf';

        $this->pdf->save($comp_path, true);

        if (file_exists($comp_path))
        {
            $Form_Pdf = Mage::getModel(PDF_FILES_MODEL::MODULE_MODEL . "/" . PDF_FILES_MODEL::MODULE_MODEL_ENTITY);
            $pdf_data = array(
                'name'          => basename($comp_url_path),
                'path'          => str_replace(basename($comp_url_path), '', str_replace(Mage::getBaseDir(), '', $comp_path)),
                'url'           => str_replace(Mage::getBaseUrl(), '', $comp_url_path),
                'date_created'  => Mage::getModel('core/date')->date('Y-m-d H:i:s'),
                'date_modified' => Mage::getModel('core/date')->date('Y-m-d H:i:s'),
            );
            $pdf_data['url']  = $this->normalizePath($pdf_data['url']);
            $pdf_data['path'] = $this->normalizePath($pdf_data['path']);

            $Form_Pdf->setData($pdf_data);
            $Form_Pdf->save();
            return $Form_Pdf;
        }

        return false;
    }

    public function normalizePath($path)
    {
        $parts    = array(); // Array to build a new path from the good parts
        $path     = str_replace('\\', '/', $path); // Replace backslashes with forwardslashes
        $path     = preg_replace('/\/+/', '/', $path); // Combine multiple slashes into a single slash
        $segments = explode('/', $path); // Collect path segments
        $test     = ''; // Initialize testing variable
        foreach ($segments as $segment)
        {
            if ($segment != '.')
            {
                $test = array_pop($parts);
                if (is_null($test))
                {
                    $parts[] = $segment;
                }
                else if ($segment == '..')
                {
                    if ($test == '..')
                    {
                        $parts[] = $test;
                    }

                    if ($test == '..' || $test == '')
                    {
                        $parts[] = $segment;
                    }

                }
                else
                {
                    $parts[] = $test;
                    $parts[] = $segment;
                }
            }
        }
        return implode('/', $parts);
    }

    public function canonicalize($address)
    {
        $address = explode('/', $address);
        $keys    = array_keys($address, '..');

        foreach ($keys as $keypos => $key)
        {
            array_splice($address, $key - ($keypos * 2 + 1), 2);
        }

        $address = implode('/', $address);
        $address = str_replace('./', '', $address);
    }

    /**
     * This will prepare the directory with htaccess
     * @return void
     */
    public function prepareDirectory($full_path = '')
    {
        $io = new Varien_Io_File();
        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $full_path));
        if ($io->fileExists('.htaccess') && !$io->isWriteable('.htaccess'))
        {
            // file does not exist or is not readable
            return;
        }

        $content = "Order Allow,Deny \n";
        $content .= "Deny from all \n";
        $content .= '<Files ~ "\.(gif|jpg|png|pdf)$">' . " \n";
        $content .= "Allow from all \n";
        $content .= "</Files> \n";

        $io->streamOpen('.htaccess');
        $io->streamWrite($content);
        $io->streamClose();

        return;
    }

    public function domPdf($value='')
    {
    	# code...
    }
}
