<?php

/**
 * Data update populate request forms
 */

$templates[1] = '';
$templates[1] .= "<style><!--\n";
$templates[1] .=  ".template-wrapper {\n";
$templates[1] .=  "      word-break: break-word;\n";
$templates[1] .=  "      /*border: 1px solid;*/\n";
$templates[1] .=  "}\n";
$templates[1] .=  ".template-wrapper.a4 {\n";
$templates[1] .=  "      width:695px;\n";
$templates[1] .=  "height:842px;\n";
$templates[1] .=  "}\n";
$templates[1] .=  "--></style>\n";
$templates[1] .=  "<div class=\"template-wrapper a4\">\n";
$templates[1] .=  "<p><img height=\"248\" src=\"{{media url=\"wysiwyg/porto/aboutus/about-image.jpg\"}}\" width=\"703\" /></p>\n";
$templates[1] .=  "<h1>ITP Template</h1>\n";
$templates[1] .=  "<table border=\"0\">\n";
$templates[1] .=  "<tbody>\n";
$templates[1] .=  "<tr>\n";
$templates[1] .=  "<td>Table titie</td>\n";
$templates[1] .=  "<td></td>\n";
$templates[1] .=  "</tr>\n";
$templates[1] .=  "<tr>\n";
$templates[1] .=  "<td>\n";
$templates[1] .=  "<table border=\"1\">\n";
$templates[1] .=  "<tbody>\n";
$templates[1] .=  "<tr>\n";
$templates[1] .=  "<td>date 1</td>\n";
$templates[1] .=  "<td>data 2</td>\n";
$templates[1] .=  "</tr>\n";
$templates[1] .=  "<tr>\n";
$templates[1] .=  "<td>{{variable_1}}</td>\n";
$templates[1] .=  "<td>{{variable_2}}</td>\n";
$templates[1] .=  "</tr>\n";
$templates[1] .=  "<tr>\n";
$templates[1] .=  "<td>data 3</td>\n";
$templates[1] .=  "<td>data 4</td>\n";
$templates[1] .=  "</tr>\n";
$templates[1] .=  "</tbody>\n";
$templates[1] .=  "</table>\n";
$templates[1] .=  "</td>\n";
$templates[1] .=  "<td></td>\n";
$templates[1] .=  "</tr>\n";
$templates[1] .=  "</tbody>\n";
$templates[1] .=  "</table>\n";
$templates[1] .=  "<p><!-- pagebreak --></p>\n";
$templates[1] .=  "<h2>What is Lorem Ipsum?</h2>\n";
$templates[1] .=  "<p style=\"text-align: justify;\"><strong> Lorem Ipsum </strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n";
$templates[1] .=  "<p><!-- pagebreak --></p>\n";
$templates[1] .=  "<div>\n";
$templates[1] .=  "<h2>Why do we use it?</h2>\n";
$templates[1] .=  "<p style=\"text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and</p>\n";
$templates[1] .=  "</div>\n";
$templates[1] .=  "</div>";



$templates[2] = '';
$templates[2] .= "<style><!--\n";
$templates[2] .=  ".template-wrapper {\n";
$templates[2] .=  "      word-break: break-word;\n";
$templates[2] .=  "      /*border: 1px solid;*/\n";
$templates[2] .=  "}\n";
$templates[2] .=  ".template-wrapper.a4 {\n";
$templates[2] .=  "      width:695px;\n";
$templates[2] .=  "height:842px;\n";
$templates[2] .=  "}\n";
$templates[2] .=  "--></style>\n";
$templates[2] .=  "<div class=\"template-wrapper a4\">\n";
$templates[2] .=  "<p><img height=\"248\" src=\"{{media url=\"wysiwyg/porto/aboutus/about-image.jpg\"}}\" width=\"703\" /></p>\n";
$templates[2] .=  "<h1>ITP Template</h1>\n";
$templates[2] .=  "<table border=\"0\">\n";
$templates[2] .=  "<tbody>\n";
$templates[2] .=  "<tr>\n";
$templates[2] .=  "<td>Table titie</td>\n";
$templates[2] .=  "<td></td>\n";
$templates[2] .=  "</tr>\n";
$templates[2] .=  "<tr>\n";
$templates[2] .=  "<td>\n";
$templates[2] .=  "<table border=\"1\">\n";
$templates[2] .=  "<tbody>\n";
$templates[2] .=  "<tr>\n";
$templates[2] .=  "<td>date 1</td>\n";
$templates[2] .=  "<td>data 2</td>\n";
$templates[2] .=  "</tr>\n";
$templates[2] .=  "<tr>\n";
$templates[2] .=  "<td>{{variable_1}}</td>\n";
$templates[2] .=  "<td>{{variable_2}}</td>\n";
$templates[2] .=  "</tr>\n";
$templates[2] .=  "<tr>\n";
$templates[2] .=  "<td>data 3</td>\n";
$templates[2] .=  "<td>data 4</td>\n";
$templates[2] .=  "</tr>\n";
$templates[2] .=  "</tbody>\n";
$templates[2] .=  "</table>\n";
$templates[2] .=  "</td>\n";
$templates[2] .=  "<td></td>\n";
$templates[2] .=  "</tr>\n";
$templates[2] .=  "</tbody>\n";
$templates[2] .=  "</table>\n";
$templates[2] .=  "<p><!-- pagebreak --></p>\n";
$templates[2] .=  "<h2>What is Lorem Ipsum?</h2>\n";
$templates[2] .=  "<p style=\"text-align: justify;\"><strong> Lorem Ipsum </strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n";
$templates[2] .=  "<p><!-- pagebreak --></p>\n";
$templates[2] .=  "<div>\n";
$templates[2] .=  "<h2>Why do we use it?</h2>\n";
$templates[2] .=  "<p style=\"text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and</p>\n";
$templates[2] .=  "</div>\n";
$templates[2] .=  "</div>";



$templates[3] = '';
$templates[3] .= "<style><!--\n";
$templates[3] .=  ".template-wrapper {\n";
$templates[3] .=  "      word-break: break-word;\n";
$templates[3] .=  "      /*border: 1px solid;*/\n";
$templates[3] .=  "}\n";
$templates[3] .=  ".template-wrapper.a4 {\n";
$templates[3] .=  "      width:695px;\n";
$templates[3] .=  "height:842px;\n";
$templates[3] .=  "}\n";
$templates[3] .=  "--></style>\n";
$templates[3] .=  "<div class=\"template-wrapper a4\">\n";
$templates[3] .=  "<p><img height=\"248\" src=\"{{media url=\"wysiwyg/porto/aboutus/about-image.jpg\"}}\" width=\"703\" /></p>\n";
$templates[3] .=  "<h1>ITP Template</h1>\n";
$templates[3] .=  "<table border=\"0\">\n";
$templates[3] .=  "<tbody>\n";
$templates[3] .=  "<tr>\n";
$templates[3] .=  "<td>Table titie</td>\n";
$templates[3] .=  "<td></td>\n";
$templates[3] .=  "</tr>\n";
$templates[3] .=  "<tr>\n";
$templates[3] .=  "<td>\n";
$templates[3] .=  "<table border=\"1\">\n";
$templates[3] .=  "<tbody>\n";
$templates[3] .=  "<tr>\n";
$templates[3] .=  "<td>date 1</td>\n";
$templates[3] .=  "<td>data 2</td>\n";
$templates[3] .=  "</tr>\n";
$templates[3] .=  "<tr>\n";
$templates[3] .=  "<td>{{variable_1}}</td>\n";
$templates[3] .=  "<td>{{variable_2}}</td>\n";
$templates[3] .=  "</tr>\n";
$templates[3] .=  "<tr>\n";
$templates[3] .=  "<td>data 3</td>\n";
$templates[3] .=  "<td>data 4</td>\n";
$templates[3] .=  "</tr>\n";
$templates[3] .=  "</tbody>\n";
$templates[3] .=  "</table>\n";
$templates[3] .=  "</td>\n";
$templates[3] .=  "<td></td>\n";
$templates[3] .=  "</tr>\n";
$templates[3] .=  "</tbody>\n";
$templates[3] .=  "</table>\n";
$templates[3] .=  "<p><!-- pagebreak --></p>\n";
$templates[3] .=  "<h2>What is Lorem Ipsum?</h2>\n";
$templates[3] .=  "<p style=\"text-align: justify;\"><strong> Lorem Ipsum </strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n";
$templates[3] .=  "<p><!-- pagebreak --></p>\n";
$templates[3] .=  "<div>\n";
$templates[3] .=  "<h2>Why do we use it?</h2>\n";
$templates[3] .=  "<p style=\"text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and</p>\n";
$templates[3] .=  "</div>\n";
$templates[3] .=  "</div>";



$request_forms = array(
    array(
        'request_form_id' => '1',
        'template'        => $templates[1],
        'date_created'    => strftime('%Y-%m-%d %H:%M:%S', time()),
        'date_modified'   => strftime('%Y-%m-%d %H:%M:%S', time()),
    ),
    array(
        'request_form_id' => '2',
        'template'        => $templates[2],
        'date_created'    => strftime('%Y-%m-%d %H:%M:%S', time()),
        'date_modified'   => strftime('%Y-%m-%d %H:%M:%S', time()),
    ),
    array(
        'request_form_id' => '3',
        'template'        => $templates[3],
        'date_created'    => strftime('%Y-%m-%d %H:%M:%S', time()),
        'date_modified'   => strftime('%Y-%m-%d %H:%M:%S', time()),
    ),
);

foreach ($request_forms as $request_form)
{
    Mage::getModel('builder/request_pdf_templates')
        ->setData($request_form)
        ->save();
}
