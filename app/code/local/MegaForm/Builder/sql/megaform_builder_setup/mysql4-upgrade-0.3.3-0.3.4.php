<?php
$installer = $this;
$installer->startSetup();

/**
 * Data update populate request forms
 */
$request_forms = array(
    array(
        'name'       => 'Personal Info',
        'form_code'  => 'personalInfo',
    )
);

foreach ($request_forms as $request_form)
{
    Mage::getModel('builder/request_forms')
        ->setData($request_form)
        ->save();
}

$installer->endSetup();
