<?php

/**
 * Data update populate request forms
 */
$request_forms = array(
    array(
        'name'       => 'Personal Info ITP',
        'form_code'  => 'personalInfoItp',
    )
);

foreach ($request_forms as $request_form)
{
    Mage::getModel('builder/request_forms')
        ->setData($request_form)
        ->save();
}
