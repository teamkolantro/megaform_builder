<?php
$installer = $this;
$installer->startSetup();

/**
 * Data update populate request forms
 */
foreach (Mage::getModel('builder/request_forms')->getCollection() as $request_form)
{
    $request_form->setIsSystem(1)->save();
}

$installer->endSetup();
