<?php
$installer = $this;
$installer->startSetup();

$collections = Mage::getModel('builder/request_forms')
    ->getCollection()
    ->addFieldToFilter('form_code', 'personalInfo');

foreach ($collections as $request_form)
{
    $request_form->delete();
}

/**
 * Data update populate request forms
 */
$request_forms = array(
    array(
        'name'      => 'Personal Info',
        'form_code' => 'personalInfo',
    ),
);

foreach ($request_forms as $request_form)
{
    Mage::getModel('builder/request_forms')
        ->setData($request_form)
        ->save();
}

$installer->endSetup();
