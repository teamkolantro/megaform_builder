<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->changeColumn($installer->getTable('builder/request_pdf_templates'), 'template', 'pdf_template',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable' => false,
            'default'  => '',
        ));

$installer->endSetup();
