<?php

/**
 * Data update populate request forms
 */
$request_forms = array(
    array(
        'name'       => 'Vehicle Info',
        'form_code'  => 'vehicle-info',
    )
);

foreach ($request_forms as $request_form)
{
    Mage::getModel('builder/request_forms')
        ->setData($request_form)
        ->save();
}
