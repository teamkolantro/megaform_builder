<?php
$installer = $this;
$installer->startSetup();

$installer->run("-- DROP TABLE IF EXISTS {$this->getTable('builder/request_email_templates')};");

/**
 *
 */
$table = $installer->getConnection()->newTable($installer->getTable('builder/request_email_templates'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        'identity' => true,
    ), 'Primary ID')
    ->addColumn('request_form_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'request_form_id')
    ->addColumn('email_template', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        'default'  => '',
    ), 'email_template')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'position')
    ->addColumn('date_created', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Created Date')
    ->addColumn('date_modified', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Update Date')
    ->setComment('Request Forms Email Templates');

$installer->getConnection()->createTable($table);

/**
 *
 */
$table = $installer->getConnection()->newTable($installer->getTable('builder/request_summary_templates'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        'identity' => true,
    ), 'Primary ID')
    ->addColumn('request_form_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'request_form_id')
    ->addColumn('summary_template', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        'default'  => '',
    ), 'summary_template')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'position')
    ->addColumn('date_created', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Created Date')
    ->addColumn('date_modified', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Update Date')
    ->setComment('Request Forms Email Templates');

$installer->getConnection()->createTable($table);

$installer->getConnection()
    ->addColumn($installer->getTable('builder/request_forms'), 'is_system', array(
        'type'     => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'length'   => 2,
        'nullable' => false,
        'default'  => '0',
        'comment'  => 'Is System',
    ));

/**
 * Data update populate request forms
 */
foreach (Mage::getModel('builder/request_forms')->getCollection() as $request_form)
{
    $request_form->setIsSystem(1)->save();
}

$installer->endSetup();
