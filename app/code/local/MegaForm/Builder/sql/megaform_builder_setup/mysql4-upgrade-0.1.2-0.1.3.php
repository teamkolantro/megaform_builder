<?php

/**
 * Data update populate request forms
 */
$request_forms = array(
    array(
        'name'       => 'ITP',
        'form_code'  => 'itp',
    ),
    array(
        'name'       => 'Fire',
        'form_code'  => 'fire',
    ),
    array(
        'name'       => 'Motor',
        'form_code'  => 'motor',
    ),
);

foreach ($request_forms as $request_form)
{
    Mage::getModel('builder/request_forms')
        ->setData($request_form)
        ->save();
}
