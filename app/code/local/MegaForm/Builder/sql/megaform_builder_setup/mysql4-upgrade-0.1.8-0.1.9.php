<?php
$installer = $this;
$installer->startSetup();

/**
 *
 */

$installer->getConnection()
    ->addColumn($installer->getTable('builder/request_form_records'), 'array_key_id', array(
        'type'     => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'length'   => 255,
        'default'  => '0',
        'after'    => 'request_session_completed_id', // column name to insert new column after
        'comment'  => 'array_key_id',
    ));
$installer->getConnection()
    ->addColumn($installer->getTable('builder/request_form_records'), 'parent_id', array(
        'type'     => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'length'   => 255,
        'default'  => '0',
        'after'    => 'request_session_completed_id', // column name to insert new column after
        'comment'  => 'parent_id',
    ));

$installer->endSetup();
