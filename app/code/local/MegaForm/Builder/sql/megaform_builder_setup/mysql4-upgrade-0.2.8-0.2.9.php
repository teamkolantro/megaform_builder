<?php
$installer = $this;
$installer->startSetup();

/**
 *
 */
$installer->getConnection()
    ->addColumn($installer->getTable('builder/request_form_records'), 'session_result_json_datas', array(
        'type'     => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable' => true,
        'default'  => '',
        'after'    => 'form_json_datas', // column name to insert new column after
        'comment'  => 'session_result_json_datas',
    ));

$installer->endSetup();
