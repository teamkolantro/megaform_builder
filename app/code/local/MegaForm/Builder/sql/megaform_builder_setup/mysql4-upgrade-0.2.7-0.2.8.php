<?php
$installer = $this;
$installer->startSetup();

$html = "";
$html .= "<table border=\"0\" class=\"table\">\n";
$html .= "<thead>\n";
$html .= "<tr><th>Premium Computation</th><th>Option I w/ AON</th><th>Option II w/ AON</th></tr>\n";
$html .= "</thead>\n";
$html .= "<tbody>\n";
$html .= "<tr>\n";
$html .= "<td>Net Premium</td>\n";
$html .= "<td>Php {{premium_computation.net_premium}}</td>\n";
$html .= "<td>Php {{premium_computation_wo_aon.net_premium}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Plus:</td>\n";
$html .= "<td></td>\n";
$html .= "<td></td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Doc Stamps</td>\n";
$html .= "<td>{{premium_computation.doc_stamps}}</td>\n";
$html .= "<td>{{premium_computation_wo_aon.doc_stamps}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>VAT</td>\n";
$html .= "<td>{{premium_computation.vat}}</td>\n";
$html .= "<td>{{premium_computation_wo_aon.vat}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Local Government Tax</td>\n";
$html .= "<td>{{premium_computation.local_government_tax}}</td>\n";
$html .= "<td>{{premium_computation_wo_aon.local_government_tax}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Gross Premium</td>\n";
$html .= "<td>{{premium_computation.total_premium}}</td>\n";
$html .= "<td>{{premium_computation_wo_aon.total_premium}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Less {{premium_computation.less_percent}}%</td>\n";
$html .= "<td>{{premium_computation.less_value}}</td>\n";
$html .= "<td>{{premium_computation_wo_aon.less_value}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Gross Premium</td>\n";
$html .= "<td>{{premium_computation.final_amount_due}}</td>\n";
$html .= "<td>{{premium_computation_wo_aon.final_amount_due}}</td>\n";
$html .= "</tr>\n";
$html .= "</tbody>\n";
$html .= "</table>\n";
$html .= "<p></p>\n";
$html .= "<p><strong>Documentary Stamps Tax</strong></p>\n";
$html .= "<p>Due to BIR implementation of EDST(Electronic Documentary Stamp Tax) system effective July 1, 2010, policy holders are mandated to pay the DST portion of the premium once the policy is issued. Refund on DST for cancelled policies is not allowed.</p>\n";
$html .= "<table border=\"0\" class=\"table\">\n";
$html .= "<thead>\n";
$html .= "<tr><th colspan=\"2\">Coverage</th></tr>\n";
$html .= "</thead>\n";
$html .= "<tbody>\n";
$html .= "<tr>\n";
$html .= "<td>Limit of Liability</td>\n";
$html .= "<td></td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Compulsary Third Party Liability</td>\n";
$html .= "<td>{{coverage.compulsary_third_party_liability}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Own Damage and Theft</td>\n";
$html .= "<td>{{coverage.own_damage_and_theft}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Bodily Injury</td>\n";
$html .= "<td>{{coverage.bodily_injury}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Property Damage</td>\n";
$html .= "<td>{{coverage.property_damage}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Auto Personal Accident</td>\n";
$html .= "<td>{{coverage.auto_passenger}}</td>\n";
$html .= "</tr>\n";
$html .= "<tr>\n";
$html .= "<td>Acts of Nature</td>\n";
$html .= "<td>{{coverage.acts_of_nature}}</td>\n";
$html .= "</tr>\n";
$html .= "</tbody>\n";
$html .= "</table>\n";
$html .= "<p><em>Deductible Php {{coverage.deductible}} </em></p>\n";
$html .= "<p><em>Disclaimer:</em></p>\n";
$html .= "<p><em>The above quotation is applicable only for the unit describ and shall be valid up to 30 days from the date of quotation.</em></p>\n";
$html .= "<p><em>Undeclared non-standard accessories will not be covered. For your protection please declare all non-standard accessories, it's brand/model and purchase price which shall be subject to approval and additional premium shall be charged.</em></p>\n";
$html .= "<p></p>\n";
$html .= "<h3><strong>Insured Vehicle</strong></h3>\n";
$html .= "<p><strong>Personal Information</strong></p>\n";
$html .= "<p>Name : {{form_datas.personal_information.first_name}} {{form_datas.personal_information.middle_name}} {{form_datas.personal_information.last_name}}</p>\n";
$html .= "<p><strong>Location of Vechicle</strong></p>\n";
$html .= "<p>Address: {{form_datas.location_vehicle.city}} {{form_datas.location_vehicle.province}} {{form_datas.location_vehicle.barangay}} </p>\n";
$html .= "<p><strong>Contact Information</strong></p>\n";
$html .= "<p>Telephone Number: {{form_datas.contact_information.telephone_number}} </p>\n";
$html .= "<p>Mobile Number:  {{form_datas.contact_information.mobile_number}} </p>\n";
$html .= "<p>Email: {{form_datas.contact_information.email}}</p>\n";
$html .= "<p><strong>Vehicle Information</strong></p>\n";
$html .= "<p>Type of inssurance: {{form_datas.insurance_required.type_insurance}}</p>\n";
$html .= "<p>Period of inssurance for CTPL: 1 year</p>\n";
$html .= "<p>Year: {{form_datas.insurance_required_car_details.year_model}} </p>\n";
$html .= "<p>Model: {{form_datas.insurance_required_car_details.model_name}}</p>\n";
$html .= "<p>Market Value: Php 2133123</p>\n";
$html .= "<p></p>\n";
$html .= "<p></p>";

/**
 * Data update summary template of motor
 */
$templates = Mage::getModel('builder/request_summary_templates')
    ->getCollection()
    ->addFieldToFilter('request_form_id', 3);

foreach ($templates as $template)
{
    $template->setSummaryTemplate($html)->save();
}

$installer->endSetup();
