<?php

/**
 * Upgrade data to bulk update of datas date
 */
$request_forms = Mage::getModel('builder/request_forms')
    ->getCollection();

foreach ($request_forms as $request_form)
{
    $request_form
    	->setDateCreated(strftime('%Y-%m-%d %H:%M:%S', time()))
    	->setDateModified(strftime('%Y-%m-%d %H:%M:%S', time()))
        ->save();
}
