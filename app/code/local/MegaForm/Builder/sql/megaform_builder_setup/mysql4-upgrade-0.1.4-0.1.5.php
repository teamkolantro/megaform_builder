<?php
$installer = $this;
$installer->startSetup();

$installer->run("-- DROP TABLE IF EXISTS {$this->getTable('builder/request_pdf_files')};");

/**
 *
 */
$table = $installer->getConnection()->newTable($installer->getTable('builder/request_pdf_files'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        'identity' => true,
    ), 'Primary ID')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false,
        'default'  => '',
    ), 'Name')
    ->addColumn('path', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false,
        'default'  => '',
    ), 'Path')
    ->addColumn('url', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false,
        'default'  => '',
    ), 'Url')
    ->addColumn('date_created', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Created Date')
    ->addColumn('date_modified', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Update Date')
    ->setComment('Request Forms table');

$installer->getConnection()->createTable($table);

/**
 *
 */
$table = $installer->getConnection()->newTable($installer->getTable('builder/request_session_completed'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        'identity' => true,
    ), 'Primary ID')
    ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'user_id')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'product_id')
    ->addColumn('form_key', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false,
        'default'  => '',
    ), 'form_key')
    ->addColumn('session_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => false,
        'default'  => '',
    ), 'session_id')
    ->addColumn('request_pdf_files_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'request_pdf_files_id')
    ->addColumn('request_form_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'request_form_id')
    ->addColumn('date_created', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Created Date')
    ->addColumn('date_modified', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Update Date')
    ->setComment('Request Forms table');

$installer->getConnection()->createTable($table);

/**
 *
 */
$table = $installer->getConnection()->newTable($installer->getTable('builder/request_form_products'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        'identity' => true,
    ), 'Primary ID')
    ->addColumn('request_form_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'request_form_id')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'product_id')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'position')
    ->addColumn('date_created', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Created Date')
    ->addColumn('date_modified', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Update Date')
    ->setComment('Request Forms table');

$installer->getConnection()->createTable($table);

$installer->endSetup();
