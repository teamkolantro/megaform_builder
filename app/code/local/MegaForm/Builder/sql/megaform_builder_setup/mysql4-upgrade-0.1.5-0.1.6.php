<?php
$installer = $this;
$installer->startSetup();

$installer->run("-- DROP TABLE IF EXISTS {$this->getTable('builder/request_pdf_templates')};");

/**
 *
 */
$table = $installer->getConnection()->newTable($installer->getTable('builder/request_pdf_templates'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        'identity' => true,
    ), 'Primary ID')
    ->addColumn('request_form_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'request_form_id')
    ->addColumn('template', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable' => false,
        'default'  => '',
    ), 'template')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'position')
    ->addColumn('date_created', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Created Date')
    ->addColumn('date_modified', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => true,
        'default'  => null,
    ), 'Update Date')
    ->setComment('Request Forms table');

$installer->getConnection()->createTable($table);

$installer->endSetup();
