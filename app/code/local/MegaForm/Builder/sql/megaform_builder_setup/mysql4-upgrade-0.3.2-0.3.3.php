<?php
$installer = $this;
$installer->startSetup();

$html = "";

$html .= "<style><!--\n";
$html .= "	.template-wrapper {\n";
$html .= "		word-break: break-word;\n";
$html .= "		/*border: 1px solid;*/\n";
$html .= "	}\n";
$html .= "	.template-wrapper.a4 {\n";
$html .= "		width:695px;\n";
$html .= "		height:842px;\n";
$html .= "		font-size:10.7px;\n";
$html .= "	}\n";
$html .= "	.template-wrapper.a4 * {\n";
$html .= "		font-size:10.7px;\n";
$html .= "	}\n";
$html .= "--></style>\n";
$html .= "<div class=\"template-wrapper a4\">\n";
$html .= "	<p><img height=\"102\" src=\"{{media url=\"wysiwyg/megaform_builder/images/motor_pdf_banner.png\"}}\" width=\"703\" /></p>\n";
$html .= "	<p></p>\n";
$html .= "	<p><span>{{extras.date_now}}</span></p>\n";
$html .= "	<p></p>\n";
$html .= "	<p></p>\n";
$html .= "	<p><span>Ms./Mr. {{personal_information.first_name}} {{personal_information.middle_name}} {{personal_information.last_name}}</span></p>\n";
$html .= "	<p><span>{{location_vehicle.barangay}}, {{location_vehicle.province}},&nbsp;{{location_vehicle.city}}</span></p>\n";
$html .= "	<p></p>\n";
$html .= "	<p><span>Dear Ms./Mr.&nbsp;{{personal_information.first_name}}</span></p>\n";
$html .= "	<p><span>We are pleased to submit our Motor Car insurance quotation as follows:</span></p>\n";
$html .= "	<table border=\"0\" style=\"width: 710px; height: 276px;\">\n";
$html .= "		<tbody>\n";
$html .= "			<tr>\n";
$html .= "				<td><span>Insured's Name&nbsp;</span></td>\n";
$html .= "				<td><span>:</span></td>\n";
$html .= "				<td><span>{{personal_information.first_name}} {{personal_information.middle_name}} {{personal_information.last_name}}</span></td>\n";
$html .= "			</tr>\n";
$html .= "			<tr>\n";
$html .= "				<td><span>Insured's Unit&nbsp;</span></td>\n";
$html .= "				<td><span>:</span></td>\n";
$html .= "				<td><span>{{insurance_required_car_details.year_model.label}} {{insurance_required_car_details.model_name.label}} {{insurance_required_car_details.brand.label}}</span></td>\n";
$html .= "			</tr>\n";
$html .= "			<tr>\n";
$html .= "				<td><span>{{form_datas.insurance_required.type_insurance}} Period of Insurance</span></td>\n";
$html .= "				<td><span>:</span></td>\n";
$html .= "				<td><span>{{coverage.period_of_inssurance}} year(s)&nbsp;</span></td>\n";
$html .= "			</tr>\n";
$html .= "			<tr>\n";
$html .= "				<td><span><em>&nbsp;&nbsp;&nbsp;Coverage&nbsp;</em></span></td>\n";
$html .= "				<td><span>:</span></td>\n";
$html .= "				<td></td>\n";
$html .= "			</tr>\n";
$html .= "			<tr>\n";
$html .= "				<td colspan=\"3\">{{extras.coverage.limit_of_liability.html}}</td>\n";
$html .= "			</tr>\n";
$html .= "			<tr>\n";
$html .= "				<td><span><em>&nbsp;&nbsp;&nbsp;Annual Premium&nbsp;</em></span></td>\n";
$html .= "				<td><span>:</span></td>\n";
$html .= "				<td><span><strong>Php {{premium_computation.total_premium}}&nbsp;</strong></span></td>\n";
$html .= "			</tr>\n";
$html .= "			<tr>\n";
$html .= "				<td><span><em>&nbsp;&nbsp;&nbsp;Less {{premium_computation.less_percent}}%&nbsp;</em></span></td>\n";
$html .= "				<td><span>:</span></td>\n";
$html .= "				<td><span><strong>Php ({{premium_computation.less_value}})&nbsp;</strong></span></td>\n";
$html .= "			</tr>\n";
$html .= "			<tr>\n";
$html .= "				<td><span><em>&nbsp;&nbsp;&nbsp;Final Amount Due&nbsp;&nbsp;</em></span></td>\n";
$html .= "				<td><span>:</span></td>\n";
$html .= "				<td><span><strong>Php {{premium_computation.final_amount_due}}</strong></span></td>\n";
$html .= "			</tr>\n";
$html .= "		</tbody>\n";
$html .= "	</table>\n";
$html .= "	<p></p>\n";
$html .= "	<p><span>The above quotation is subject to standard policy terms and conditions. Please feel free to call us at tel. No. 811-8329 should there be anything you</span><br /><span>wish to clarify.</span></p>\n";
$html .= "	<p><span>Thank you very much.</span></p>\n";
$html .= "	<p><span>Sincerely yours</span><br /><span>UCPB General Insurance Company Inc</span></p>\n";
$html .= "	<p></p>\n";
$html .= "	<p style=\"text-align: center;\"><img height=\"96\" src=\"{{media url=\"wysiwyg/megaform_builder/images/motor_pdf_footer.png\"}}\" width=\"690\" /></p>\n";
$html .= "</div>\n";

/**
 * Data update summary template of motor
 */
$templates = Mage::getModel('builder/request_pdf_templates')
    ->getCollection()
    ->addFieldToFilter('request_form_id', 3);

foreach ($templates as $template)
{
    $template->setPdfTemplate($html)->save();
}

$installer->endSetup();
