<?php
$installer = $this;
$installer->startSetup();
$installer->run("
    DROP TABLE `{$installer->getTable('builder/request_form_records')}`;
");
$installer->endSetup();
