<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Helper_Data as CURR_HELPER;
use MegaForm_Builder_Model_Request_Forms as REQUEST_FORMS_MODEL;
use MegaForm_Builder_Model_Request_Form_Records as REQUEST_FORM_RECORDS_MODEL;
use MegaForm_Builder_Model_Request_Pdf_Templates as PDF_TEMPLATES_MODEL;
use MegaForm_Builder_Model_Request_Session_Completed as REQUEST_SESSION_COMPLETED_MODEL;

/**
 * Form Controller
 *
 * This class handles forms url controller process,
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_FormController extends Mage_Core_Controller_Front_Action
{
    /**
     * Request Form Key
     *
     * for form key
     *
     * Default : 'null'
     *
     * @var string
     */
    protected $requestFormKey = null;

    /**
     * Session Customer ID
     *
     * Must be on the session customer id
     *
     * Default : '0'
     *
     * @var int
     */
    protected $customerDataId = 0;

    /**
     * Cart Items
     *
     * Sets of items of array
     *
     * Default : 'empty array'
     *
     * @var array
     */
    protected $cartItems = [];

    /**
     * Encrypted Session ID
     *
     * This must be from the encrypted session id
     *
     * Default : ''
     *
     * @var string
     */
    protected $encryptedSessionId = '';

    /**
     * Session Folder
     *
     * Session folder MD5 ( encryptedSessionId + requestFormKey )
     *
     * Default : ''
     *
     * @var string
     */
    protected $sessionFolder = '';

    /**
     * Event Prefix
     *
     * This concatinates the prefix for event dispatch
     *
     * Default : 'form_builder'
     *
     * @var string
     */
    public $event_prefix = "form_builder";

    /**
     * Class contructor...
     */
    public function _construct()
    {
        parent::_construct();
    }

    /**
     * Process Action
     *
     * Handles the data saving.
     *
     * This always fires when in request is ajax.
     *
     * This also dispatch events
     * - form_builder_session_data_save_before
     * - form_builder_session_data_save_after
     *
     * @return void|string Return a json string if request an ajax otherwise redirect to on success url
     */
    public function processAction()
    {
        $this->requestFormKey = Mage::app()->getRequest()->getParam('form_key');
        $sessionFormKey       = Mage::getSingleton('core/session')->getMegaBuildFormKey();

        $previos = Mage::getModel(REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL . "/" . REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL_ENTITY);

        if (!empty($this->requestFormKey))
        {
            $previos->load($this->requestFormKey, 'form_key');
        }

        $refUrl     = Mage::getSingleton('core/url')->parseUrl($this->_getRefererUrl());
        $currentUrl = Mage::getSingleton('core/url')->parseUrl($currentUrl);

        $newSessionFormKey = Mage::helper(CURR_HELPER::HELPER_KEY)->generateFormKey();

        //commented previous condition for it is unstable of session key
        // if ($this->requestFormKey != $sessionFormKey && (!$previos->getId()))
        //instead us the host of referer to base url
        if ($refUrl->getHost() != $currentUrl->getHost())
        {
            return $this->_redirect("/");
        }

        if (!empty(Mage::app()->getRequest()->getParam('form_key', null)) && (!$previos->getId()))
        {
            return $this->_redirect("/");
        }

        /**
         * Getting customer ID
         */
        $this->customerDataId = Mage::helper(CURR_HELPER::HELPER_KEY)->getCustomerId();

        /**
         * Getting all items in cart checkout
         */
        $this->cartItems = Mage::helper(CURR_HELPER::HELPER_KEY)->getCartItemProducts();

        $this->product_last_id = 0;
        foreach ($this->cartItems as $key => $value)
        {
            $this->product_last_id = $value->getId();
        }

        //current session id;
        $this->encryptedSessionId = Mage::getSingleton('core/session')->getEncryptedSessionId();

        $this->sessionFolder = md5($this->encryptedSessionId . $this->requestFormKey);

        $REQUEST_FORMS_MODEL = Mage::getModel(REQUEST_FORMS_MODEL::MODULE_MODEL . "/" . REQUEST_FORMS_MODEL::MODULE_MODEL_ENTITY)
            ->load($this->getRequest()->getPost('form_code'), 'form_code');

        $PDF_TEMPLATES_MODEL = Mage::getModel(PDF_TEMPLATES_MODEL::MODULE_MODEL . "/" . PDF_TEMPLATES_MODEL::MODULE_MODEL_ENTITY)
            ->load($REQUEST_FORMS_MODEL->getId(), 'request_form_id');

        $REQUEST_SESSION_COMPLETED_MODEL = Mage::getModel(REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL . "/" . REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL_ENTITY);

        if(empty($this->requestFormKey))
        {
            $this->requestFormKey = $newSessionFormKey;
        }

        $request_session_data            = array();
        $request_session_data['user_id'] = $this->customerDataId;
        //temporary enabled update of product id even if session form key is saved
        // if(!$previos->getId())
        // {
        $request_session_data['product_id'] = $this->product_last_id;
        // }
        $request_session_data['form_key']             = $this->requestFormKey;
        $request_session_data['session_id']           = $this->encryptedSessionId;
        $request_session_data['request_pdf_files_id'] = 0;
        $request_session_data['request_form_id']      = $REQUEST_FORMS_MODEL->getId();
        $request_session_data['date_modified']        = Mage::getModel('core/date')->date('Y-m-d H:i:s');

        $request_session_data = new Varien_Object($request_session_data);

        //todo dispatch event before save
        Mage::dispatchEvent($this->event_prefix . "_session_data_save_" . CURR_HELPER::EVENT_BEFORE, array('request_session_data' => $request_session_data));

        $REQUEST_SESSION_COMPLETED_MODEL->setData($request_session_data->getData());

        if ($previos->getId())
        {
            $REQUEST_SESSION_COMPLETED_MODEL->setId($previos->getId());
        }
        else
        {
            $REQUEST_SESSION_COMPLETED_MODEL->setDateCreated(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
        }

        $REQUEST_SESSION_COMPLETED_MODEL->save();

        $this->getRequest()->setPost('form_key',$this->requestFormKey);

        //todo dispatch event after save
        Mage::dispatchEvent($this->event_prefix . "_session_data_save_" . CURR_HELPER::EVENT_AFTER, array('REQUEST_SESSION_COMPLETED_MODEL' => $REQUEST_SESSION_COMPLETED_MODEL));

        //still not working
        Mage::getSingleton("MegaForm_Builder_Model_Request_Session_Completed")
            ->addData($REQUEST_SESSION_COMPLETED_MODEL->getData());

        $this->saveFormRecords($REQUEST_SESSION_COMPLETED_MODEL->getId());

        // Mage::getSingleton('core/session')->addSuccess('Request is succesfully saved.');

        if ($this->getRequest()->isXmlHttpRequest())
        {
            echo json_encode($REQUEST_SESSION_COMPLETED_MODEL->getData());
            exit();
        }
        else
        {
            $this->_redirectUrl(Mage::getBaseUrl() . trim($this->getRequest()->getPost("on_success_url"), "/"));
            return true;
        }

        //todo redirect to next url
    }

    /**
     * Save Form Records
     *
     * Handles the data saving and or updating of form records.
     *
     * This also dispatch events
     * - form_builder_form_data_save_before
     * - form_builder_form_data_save_after
     *
     * @param int $session_completed_id Default(0) Its the primary key
     * @return REQUEST_FORM_RECORDS_MODEL Return an instance of REQUEST_FORM_RECORDS_MODEL
     */
    public function saveFormRecords($session_completed_id = 0)
    {
        $stacked_posts = $this->getRequest()->getPost();

        //unset($stacked_posts['form_key']);

        $REQUEST_FORM_RECORDS_MODEL = Mage::getModel(REQUEST_FORM_RECORDS_MODEL::MODULE_MODEL . "/" . REQUEST_FORM_RECORDS_MODEL::MODULE_MODEL_ENTITY);

        $previos = Mage::getModel(REQUEST_FORM_RECORDS_MODEL::MODULE_MODEL . "/" . REQUEST_FORM_RECORDS_MODEL::MODULE_MODEL_ENTITY)->load($session_completed_id, 'request_session_completed_id');

        $form_datas = array(
            'request_session_completed_id' => $session_completed_id,
            'form_json_datas'              => json_encode($stacked_posts),
            'date_modified'                => Mage::getModel('core/date')->date('Y-m-d H:i:s'),
        );

        //todo dispatch event before save
        $form_datas = new Varien_Object($form_datas);
        Mage::dispatchEvent($this->event_prefix . "_form_data_save_" . CURR_HELPER::EVENT_BEFORE, array('form_code' => $this->getRequest()->getPost('form_code'), 'form_datas' => $form_datas));

        $REQUEST_FORM_RECORDS_MODEL->setData(
            $form_datas->getData()
        );

        if ($previos->getId())
        {
            $REQUEST_FORM_RECORDS_MODEL->setId($previos->getId());
        }
        else
        {
            $REQUEST_FORM_RECORDS_MODEL->setDateCreated(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
        }

        $REQUEST_FORM_RECORDS_MODEL->save();

        //todo dispatch event before save
        Mage::dispatchEvent($this->event_prefix . "_form_data_save_" . CURR_HELPER::EVENT_AFTER, array('form_code' => $this->getRequest()->getPost('form_code'), 'REQUEST_FORM_RECORDS_MODEL' => $REQUEST_FORM_RECORDS_MODEL));

        return $REQUEST_FORM_RECORDS_MODEL;
    }

    /**
     * View Action
     *
     * Load and renders layout.
     *
     * @return void
     */
    public function viewAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Pdf Action
     *
     * This method is only for developers it simulates the generation of pdf.
     *
     * It generates and prints the file details thus also stopping the script.
     *
     * @return void
     */
    public function pdfAction()
    {
        //wala muna
        $PDF_TEMPLATES_MODEL = Mage::getModel(PDF_TEMPLATES_MODEL::MODULE_MODEL . "/" . PDF_TEMPLATES_MODEL::MODULE_MODEL_ENTITY)
            ->load(3, 'request_form_id');

        //todo generate pdf by template
        $pdf_helper                      = Mage::helper(CURR_HELPER::HELPER_KEY . "/domPdf");
        $pdf_helper->sessionFolder       = "test";
        $pdf_helper->PDF_TEMPLATES_MODEL = $PDF_TEMPLATES_MODEL;
        $request_file                    = $pdf_helper->generatePdf('asd' , true);

        fn_mrk($request_file);
    }

}
