<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Adminhtml_Forms as BASE_BLOCK;
use MegaForm_Builder_Helper_Data as CURR_HELPER;
use MegaForm_Builder_Model_Request_Email_Templates as EMAIL_TEMPLATES_MODEL;
use MegaForm_Builder_Model_Request_Forms as CURR_MODEL;
use MegaForm_Builder_Model_Request_Form_Products as FORM_PRODUCTS_MODEL;
use MegaForm_Builder_Model_Request_Pdf_Templates as PDF_TEMPLATES_MODEL;
use MegaForm_Builder_Model_Request_Summary_Templates as SUMMARY_TEMPLATES_MODEL;

/**
 * Form Controller
 *
 * This class handles forms url controller process,
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Adminhtml_FormsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Controller Active Menu
     *
     * For admin menu highlight
     *
     * Default : 'megaform/builder'
     *
     * @var string
     */
    const CONTROLLER_ACTIVE_MENU = "megaform/builder";

    /**
     * Current Contoller
     *
     * Current controller name
     *
     * Default : 'Forms'
     *
     * @var string
     */
    const CURRENT_CONTROLLER = "Forms";

    /**
     * Is allowed
     *
     * This is for acl forceing to true
     *
     * @return boolean Always return true
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Initialize Action
     *
     * It sets the admin active menu and adds the corresponding breadcrumbs
     *
     * @return MegaForm_Builder_Adminhtml_FormsController
     */
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu(self::CONTROLLER_ACTIVE_MENU)->_addBreadcrumb(Mage::helper("adminhtml")->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . self::CURRENT_CONTROLLER . " Manager"), Mage::helper("adminhtml")->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . self::CURRENT_CONTROLLER . " Manager"));
        return $this;
    }

    /**
     * index Action
     *
     * It handles the index route and renders layout
     *
     * @return void
     */
    public function indexAction()
    {
        $this->_title($this->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . self::CURRENT_CONTROLLER . ""));
        $this->_title($this->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . self::CURRENT_CONTROLLER . " Manager"));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Get Form Products Action
     *
     * It handles and renders the tab grid
     *
     * @return void
     */
    public function getFormProductsAction()
    {
        $this->loadLayout();
        // the block name must exist in the amyout file in step 4
        $this->getLayout()->getBlock('builder.edit.tab.grid');
        $this->renderLayout();
    }

    /**
     * Grid Action
     * Display list of products related to current category
     *
     * @return void
     */
    public function gridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('builder/adminhtml_forms_edit_tab_products_grid', 'request.form.product.grid')
                ->toHtml()
        );
    }

    /**
     * Edit Action
     *
     * Renders the edit form
     *
     * @return void
     */
    public function editAction()
    {
        $this->_title($this->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . self::CURRENT_CONTROLLER . ""));
        $this->_title($this->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . self::CURRENT_CONTROLLER . ""));

        $id    = $this->getRequest()->getParam("id");
        $model = Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)->load($id);

        if ($model->getId())
        {
            $this->_title($this->__("Edit Item"));

            $pdf_template = Mage::getModel(PDF_TEMPLATES_MODEL::MODULE_MODEL . "/" . PDF_TEMPLATES_MODEL::MODULE_MODEL_ENTITY)
                ->load($model->getId(), 'request_form_id');

            $email_template = Mage::getModel(EMAIL_TEMPLATES_MODEL::MODULE_MODEL . "/" . EMAIL_TEMPLATES_MODEL::MODULE_MODEL_ENTITY)
                ->load($model->getId(), 'request_form_id');

            $summary_template = Mage::getModel(SUMMARY_TEMPLATES_MODEL::MODULE_MODEL . "/" . SUMMARY_TEMPLATES_MODEL::MODULE_MODEL_ENTITY)
                ->load($model->getId(), 'request_form_id');

            $model->setPdfTemplate($pdf_template);
            $model->setEmailTemplate($email_template);
            $model->setSummaryTemplate($summary_template);

            Mage::register(BASE_BLOCK::SESSION_FORM_DATA . "_data", $model);

        }
        else
        {
            $this->_title($this->__("New Item"));

            $model = Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY);

            $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
            if (!empty($data))
            {
                $model->setData($data);
            }

            Mage::register(BASE_BLOCK::SESSION_FORM_DATA . "_data", $model);
        }

        $this->loadLayout();

        $this->_setActiveMenu(self::CONTROLLER_ACTIVE_MENU);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . self::CURRENT_CONTROLLER . " Manager"), Mage::helper("adminhtml")->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . self::CURRENT_CONTROLLER . " Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Item Description"), Mage::helper("adminhtml")->__("Item Description"));

        $this->_addContent($this->getLayout()->createBlock(BASE_BLOCK::BLOCK_GROUP . "/adminhtml_" . BASE_BLOCK::BLOCK_CONTROLLER . "_edit"))->_addLeft($this->getLayout()->createBlock(BASE_BLOCK::BLOCK_GROUP . "/adminhtml_" . BASE_BLOCK::BLOCK_CONTROLLER . "_edit_tabs"));
        $this->renderLayout();
    }

    /**
     * New Action
     *
     * Forward to edit action
     *
     * @return MegaForm_Builder_Adminhtml_FormsController
     */
    public function newAction()
    {
        // We just forward the new action to a blank edit form
        $this->_forward('edit');
        return $this;
    }

    /**
     * Save Action
     *
     * Handles the data saving and redirect to current controller url
     *
     * @return void
     */
    public function saveAction()
    {
        $post_data = $this->getRequest()->getPost();

        if ($post_data)
        {
            $post_data['date_modified'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
            $target_id                  = (int) $this->getRequest()->getParam("id");

            try {

                if (empty($target_id))
                {
                    $post_data['request_forms']['date_created'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
                }

                //add dates to  request_forms
                $post_data['request_forms']['date_modified'] = $post_data['date_modified'];

                $model_data = Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY);
                $model_data->setData($post_data['request_forms']);

                /**
                 * Check Record Duplicate
                 */
                $model_check_data = Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)
                    ->getCollection()
                    ->addFieldToFilter('name', array('like' => $post_data['name']))
                    ->addFieldToFilter('id', array('neq' => $target_id));

                if ($model_check_data->getSize())
                {
                    Mage::getSingleton("adminhtml/session")->addError(Mage::helper("adminhtml")->__("Name `" . $post_data['name'] . "` already."));
                    Mage::getSingleton("adminhtml/session")->{"set" . ucwords(BASE_BLOCK::SESSION_FORM_DATA) . "Data"}($this->getRequest()->getPost());

                    if ($this->getRequest()->getParam("back") && !$model_check_data->getSize())
                    {
                        $this->_redirect("*/*/edit", array("id" => $model_data->getId()));
                        return;
                    }

                    $this->_redirect("*/*/new");
                    return;
                }

                if (!empty($target_id))
                {
                    $model_data = $model_data->setId($target_id);
                }

                $model_data->save();

                /**
                 * Templates
                 */
                $this->updatePdfTemplates($model_data, $post_data);
                $this->updateEmailTemplates($model_data, $post_data);
                $this->updateSummaryTemplates($model_data, $post_data);

                Mage::getModel(FORM_PRODUCTS_MODEL::MODULE_MODEL . "/" . FORM_PRODUCTS_MODEL::MODULE_MODEL_ENTITY)
                    ->updateFormProducts($model_data->getId(), $post_data['request_form_products']);

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully saved!"));
                Mage::getSingleton("adminhtml/session")->{"set" . ucwords(BASE_BLOCK::SESSION_FORM_DATA) . "Data"}(false);

                if ($this->getRequest()->getParam("back"))
                {
                    $this->_redirect("*/*/edit", array("id" => $model_data->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            }
            catch (Exception $e)
            {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->{"set" . ucwords(BASE_BLOCK::SESSION_FORM_DATA) . "Data"}($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }

    /**
     * Update Pdf Templates
     *
     * Updates or save a pdf template
     *
     * @return void
     */
    public function updatePdfTemplates($model_data, $post_data)
    {
        $PDF_TEMPLATES_MODEL = Mage::getModel(PDF_TEMPLATES_MODEL::MODULE_MODEL . "/" . PDF_TEMPLATES_MODEL::MODULE_MODEL_ENTITY)
            ->load($model_data->getId(), 'request_form_id');

        $PDF_TEMPLATES_MODEL->setPdfTemplate($post_data['form_pdf_templates']['pdf_template']);
        $PDF_TEMPLATES_MODEL->setRequestFormId($model_data->getId());

        if ($PDF_TEMPLATES_MODEL->getId())
        {
            $PDF_TEMPLATES_MODEL->setDateModified(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
        }
        else
        {
            $PDF_TEMPLATES_MODEL->setDateCreated(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
        }

        $PDF_TEMPLATES_MODEL->save();
    }

    /**
     * Update Email Templates
     *
     * Updates or save a email template
     *
     * @return void
     */
    public function updateEmailTemplates($model_data, $post_data)
    {
        $EMAIL_TEMPLATES_MODEL = Mage::getModel(EMAIL_TEMPLATES_MODEL::MODULE_MODEL . "/" . EMAIL_TEMPLATES_MODEL::MODULE_MODEL_ENTITY)
            ->load($model_data->getId(), 'request_form_id');

        $EMAIL_TEMPLATES_MODEL->setEmailTemplate($post_data['form_email_templates']['email_template']);
        $EMAIL_TEMPLATES_MODEL->setRequestFormId($model_data->getId());

        if ($EMAIL_TEMPLATES_MODEL->getId())
        {
            $EMAIL_TEMPLATES_MODEL->setDateModified(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
        }
        else
        {
            $EMAIL_TEMPLATES_MODEL->setDateCreated(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
        }

        $EMAIL_TEMPLATES_MODEL->save();
    }

    /**
     * Update Summary Templates
     *
     * Updates or save a summary template
     *
     * @return void
     */
    public function updateSummaryTemplates($model_data, $post_data)
    {
        $SUMMARY_TEMPLATES_MODEL = Mage::getModel(SUMMARY_TEMPLATES_MODEL::MODULE_MODEL . "/" . SUMMARY_TEMPLATES_MODEL::MODULE_MODEL_ENTITY)
            ->load($model_data->getId(), 'request_form_id');

        $SUMMARY_TEMPLATES_MODEL->setSummaryTemplate($post_data['form_summary_templates']['summary_template']);
        $SUMMARY_TEMPLATES_MODEL->setRequestFormId($model_data->getId());

        if ($SUMMARY_TEMPLATES_MODEL->getId())
        {
            $SUMMARY_TEMPLATES_MODEL->setDateModified(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
        }
        else
        {
            $SUMMARY_TEMPLATES_MODEL->setDateCreated(Mage::getModel('core/date')->date('Y-m-d H:i:s'));
        }

        $SUMMARY_TEMPLATES_MODEL->save();
    }

    /**
     * Delete Action
     *
     * Handles the delete action and redirect to current url
     *
     * @return void
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0)
        {
            try {

                $this->_deleteItem($this->getRequest()->getParam("id"));

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted!"));
                $this->_redirect("*/*/");
            }
            catch (Exception $e)
            {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    /**
     * Mass Remove Action
     *
     * Handles the multiple delete action and redirect to current url
     *
     * @return void
     */
    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id)
            {
                $this->_deleteItem($id);
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed!"));
        }
        catch (Exception $e)
        {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete Item
     *
     * Delete single item in database with id
     *
     * @param int|string $id The item primary key
     * @return void
     */
    public function _deleteItem($id = '')
    {
        $model = Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)->load($id);
        $this->removeFile($model->getData('image'));
        $model->delete();
    }

    /**
     * Remove File
     *
     * Delete single file in directory
     *
     * @param string $image_file Image File Path
     * @return void
     */
    public function removeFile($image_file = "")
    {
        if (!empty($image_file))
        {
            $io = new Varien_Io_File();
            $io->rm(Mage::getBaseDir('media') . DS . implode(DS, explode('/', $image_file)));
        }
    }
}
