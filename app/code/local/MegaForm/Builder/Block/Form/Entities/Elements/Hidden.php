<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Form Entities Elements Hidden
 *
 * This handles every hidden element of the form entity
 *
 * Default template path is
 * <code>
 *  megaform_builder/form/elements/hidden.phtml
 * </code>
 *
 * Sample Html Output
 * <code>
 * &lt;div class=&quot;col-md-6 hide&quot; style=&quot;display: none;&quot;&gt;
 * &nbsp; &nbsp; &lt;input class=&quot;form-control&quot; data-builder-default-value=&quot;Form-Value&quot; name=&quot;field_name[element_name]&quot; placeholder=&quot;Placeholder&quot; attribute1=&quot;attribute1-value&quot; readonly=&quot;readonly&quot; type=&quot;hidden&quot; value=&quot;Form-Value&quot;&gt;
 * &nbsp; &nbsp; &lt;/input&gt;
 * &lt;/div&gt;
 * </code>
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Form_Entities_Elements_Hidden extends MegaForm_Builder_Block_Form_Entities_Elements_Abstract
{
    /**
     * Element clasification.
     *
     * Element is hidden.
     *
     * @var string
     * @access private
     */
    private $element = "hidden";

    /**
     * Class constructor
     *
     * Sets the template path in dynamic string.
     *
     * <code>
     *    $this->_template_path = $this->form_element_path . $this->element . ".phtml";
     * </code>
     *
     * Template path psuedo:
     *   + Sets the template path              (<a href="../classes/MegaForm_Builder_Block_Form_Entities_Elements_Abstract.html#property__options">form_element_path</a>)
     *   + Set the template name               (<a href="#property_element">element</a>)
     *
     */
    public function __construct()
    {
        $this->_template_path = $this->form_element_path . $this->element . ".phtml";
        parent::__construct();
    }
}
