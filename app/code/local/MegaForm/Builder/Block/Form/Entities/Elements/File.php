<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Form Entities Elements File
 *
 * This handles every file element of the form entity
 *
 * Default template path is
 * <code>
 *  megaform_builder/form/elements/file.phtml
 * </code>
 *
 * Sample Html Output
 * <code>
 * &lt;div class=&quot;col-md-12&quot;&gt;
 * &nbsp; &nbsp; &lt;div class=&quot;uploader&quot;&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &lt;input accept=&quot;image/gif, image/jpg, image/jpeg, image/png, image/bmp, application/pdf&quot; class=&quot;form-control fileUpload&quot; data-builder-default-value=&quot;&quot; data-on-save-exclude=&quot;true&quot; id=&quot;file-upload-0&quot; name=&quot;field_name[element_name][]&quot; placeholder=&quot;Value&quot; type=&quot;file&quot; value=&quot;&quot;&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;label for=&quot;file-upload-0&quot; id=&quot;file-drag&quot;&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;file-img hidden&quot;&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;img alt=&quot;Preview&quot; id=&quot;file-image&quot; src=&quot;#&quot;/&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div id=&quot;start&quot;&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;i aria-hidden=&quot;true&quot; class=&quot;fa fa-download&quot;&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/i&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Select a file or drag here
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;hidden&quot; id=&quot;notimage&quot;&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Please select an image
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;span class=&quot;btn btn-primary&quot; id=&quot;file-upload-btn&quot;&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Select a file
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/span&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;div class=&quot;hidden&quot; id=&quot;response&quot;&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/div&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &lt;/label&gt;
 * &nbsp; &nbsp; &nbsp; &nbsp; &lt;/input&gt;
 * &nbsp; &nbsp; &lt;/div&gt;
 * &lt;/div&gt;
 * </code>
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Form_Entities_Elements_File extends MegaForm_Builder_Block_Form_Entities_Elements_Abstract
{
    /**
     * Element clasification.
     *
     * Element is file.
     *
     * @var string
     * @access private
     */
    private $element = "file";

    /**
     * Class constructor
     *
     * Sets the template path in dynamic string.
     *
     * <code>
     *    $this->_template_path = $this->form_element_path . $this->element . ".phtml";
     * </code>
     *
     * Template path psuedo:
     *   + Sets the template path              (<a href="../classes/MegaForm_Builder_Block_Form_Entities_Elements_Abstract.html#property__options">form_element_path</a>)
     *   + Set the template name               (<a href="#property_element">element</a>)
     *
     */
    public function __construct()
    {
        $this->_template_path = $this->form_element_path . $this->element . ".phtml";
        parent::__construct();
    }
}
