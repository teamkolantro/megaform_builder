<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Form_Entities_Abstract as FE_Abstract;

/**
 * Form Entities Entities Personal Info
 *
 * This handles each form as a whole entity form personal info for itp
 * Other Example :
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Fire.html">Fire</a>
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Itp.html">Itp</a>
 *
 * This class also dispatch events from abstract parent.
 * Events:
 * - form_builder_render_field_before
 * - form_builder_render_field_after
 * - form_builder_render_element_before
 * - form_builder_render_element_after
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Form_Entities_PersonalInfoItp extends FE_Abstract
{
    /**
     * Contains all data of field sets with elements per fieldset array
     *
     * This variable should be passed to self::$static_form_field_rows
     * for the parent abstract late static bindings
     *
     * Object representation
     * <code>
     * Varien_Object
     * (
     * &nbsp; &nbsp; _data::protected =&gt;
     * &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset1 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset2 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset3 =&gt; Varien_Object(),
     * &nbsp; &nbsp; )
     * )
     * </code>
     *
     * @var Varien_Object
     */
    private $form_field_rows;

    /**
     * Array of fieldsets
     *
     * Field sets is being divided by group in array
     *
     * Array representaion
     * <code>
     * array
     * (
     * &nbsp; &nbsp; &nbsp;array
     * &nbsp; &nbsp; &nbsp;(
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name1,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name2
     * &nbsp; &nbsp; &nbsp;),
     * &nbsp; &nbsp; &nbsp;array
     * &nbsp; &nbsp; &nbsp;(
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name3,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name4
     * &nbsp; &nbsp; &nbsp;)
     * )
     * </code>
     *
     * @var mixed
     */
    public $page_sets = array();

    /**
     * Request Forms
     *
     * Default : 'request_forms'
     *
     * @var string
     */
    const REQUEST_FORMS = 'request_forms';

    /**
     * On Success Url
     *
     * Default : 'on_success_url'
     *
     * @var string
     */
    const ON_SUCCESS_URL = 'on_success_url';

    /**
     * Extras
     *
     * For fieldset key
     *
     * Default : 'extras'
     *
     * @var string
     */
    const EXTRAS = 'extras';

    /**
     * Checkout
     *
     * For fieldset key
     *
     * Default : 'checkout'
     *
     * @var string
     */
    const CHECKOUT = 'checkout';

    /**
     * Individual Personal Information
     *
     * For fieldset key
     *
     * Default : 'indiviual_personal_information'
     *
     * @var string
     */
    const INDIVIUAL_PERSONAL_INFORMATION = 'indiviual_personal_information';

    /**
     * Family Personal Information
     *
     * For fieldset key
     *
     * Default : 'family_personal_information'
     *
     * @var string
     */
    const FAMILY_PERSONAL_INFORMATION = 'family_personal_information';

    /**
     * Group Information
     *
     * For fieldset key
     *
     * Default : 'group_information'
     *
     * @var string
     */
    const GROUP_INFORMATION = 'group_information';

    /**
     * Company Information
     *
     * For fieldset key
     *
     * Default : 'company_information'
     *
     * @var string
     */
    const COMPANY_INFORMATION = 'company_information';

    /**
     * Mailing Address
     *
     * For fieldset key
     *
     * Default : 'mailing_address'
     *
     * @var string
     */
    const MAILING_ADDRESS = 'mailing_address';

    /**
     * Secondary Mailing Address
     *
     * For fieldset key
     *
     * Default : 'secondary_mailing_address'
     *
     * @var string
     */
    const SECONDARY_MAILING_ADDRESS = 'secondary_mailing_address';

    /**
     * Group Contact Person
     *
     * For fieldset key
     *
     * Default : 'group_contact_person'
     *
     * @var string
     */
    const GROUP_CONTACT_PERSON = 'group_contact_person';

    /**
     * Company Contact Person
     *
     * For fieldset key
     *
     * Default : 'company_contact_person'
     *
     * @var string
     */
    const COMPANY_CONTACT_PERSON = 'company_contact_person';

    /**
     * Group Contact Person Mailing Address
     *
     * For fieldset key
     *
     * Default : 'group_contact_person_mailing_address'
     *
     * @var string
     */
    const GROUP_CONTACT_PERSON_MAILING_ADDRESS = 'group_contact_person_mailing_address';

    /**
     * Group Contact Person Secondary Mailing Address
     *
     * For fieldset key
     *
     * Default : 'group_contact_person_secondary_mailing_address'
     *
     * @var string
     */
    const GROUP_CONTACT_PERSON_SECONDARY_MAILING_ADDRESS = 'group_contact_person_secondary_mailing_address';

    /**
     * Company Contact Person Mailing Address
     *
     * For fieldset key
     *
     * Default : 'company_contact_person_mailing_address'
     *
     * @var string
     */
    const COMPANY_CONTACT_PERSON_MAILING_ADDRESS = 'company_contact_person_mailing_address';

    /**
     * Company Contact Person Secondary Mailing Address
     *
     * For fieldset key
     *
     * Default : 'company_contact_person_secondary_mailing_address'
     *
     * @var string
     */
    const COMPANY_CONTACT_PERSON_SECONDARY_MAILING_ADDRESS = 'company_contact_person_secondary_mailing_address';

    /**
     * Parent Guardian Information
     *
     * For fieldset key
     *
     * Default : 'parent_guardian_information'
     *
     * @var string
     */
    const PARENT_GUARDIAN_INFORMATION = 'parent_guardian_information';

    /**
     * Contact Information
     *
     * For fieldset key
     *
     * Default : 'contact_information'
     *
     * @var string
     */
    const CONTACT_INFORMATION = 'contact_information';

    /**
     * Person To Contact
     *
     * For fieldset key
     *
     * Default : 'person_to_contact'
     *
     * @var string
     */
    const PERSON_TO_CONTACT = 'person_to_contact';

    /**
     * Beneficiaries
     *
     * For fieldset key
     *
     * Default : 'beneficiaries'
     *
     * @var string
     */
    const BENEFICIARIES = 'beneficiaries';

    /**
     * Agent Information
     *
     * For fieldset key
     *
     * Default : 'agent_information'
     *
     * @var string
     */
    const AGENT_INFORMATION = 'agent_information';

    /**
     * Class contructor...
     *
     * Sets the self::$static_form_field_rows from property form_field_rows
     *
     */
    public function __construct()
    {
        $this->_init();

        /**
         * Late Static binding para magamit ng parent ung entity ng child kahit property pa xa ng parent ganun.
         */
        self::$static_form_field_rows = $this->form_field_rows;

        parent::__construct();
    }

    public function _init()
    {
        $this->_initEntities();
        $this->_processEntities();
        $this->_buildFieldItems();
        $this->_assignPageSets();
    }

    /**
     * Assign Page Sets
     *
     * This sets the field set in grouped array to property <a href="#property_page_sets">page_sets</a>
     */
    public function _assignPageSets()
    {
        $this->page_sets[] = array(
            self::EXTRAS,
            self::CHECKOUT,
            self::INDIVIUAL_PERSONAL_INFORMATION,
            self::FAMILY_PERSONAL_INFORMATION,
            self::GROUP_INFORMATION,
            self::COMPANY_INFORMATION,
            self::MAILING_ADDRESS,
            self::SECONDARY_MAILING_ADDRESS,
            self::GROUP_CONTACT_PERSON,
            self::COMPANY_CONTACT_PERSON,
            self::GROUP_CONTACT_PERSON_MAILING_ADDRESS,
            self::GROUP_CONTACT_PERSON_SECONDARY_MAILING_ADDRESS,
            self::COMPANY_CONTACT_PERSON_MAILING_ADDRESS,
            self::COMPANY_CONTACT_PERSON_SECONDARY_MAILING_ADDRESS,
        );

        $this->page_sets[] = array(
            self::CONTACT_INFORMATION,
            self::PARENT_GUARDIAN_INFORMATION,
            self::PERSON_TO_CONTACT,
            self::BENEFICIARIES,
            self::AGENT_INFORMATION,
        );
    }

    /**
     * Initialize Entities
     *
     * Entities are basically the fieldsets
     *
     * This method sets all fieldsets/entities instantiate
     * each fieldset to be an instance of Varien_Object
     * and sets to <a href="#property_form_field_rows">form_field_rows</a>
     * as whole Varien_Object
     *
     * Data representation
     * <code>
     * Varien_Object
     * (
     * &nbsp; &nbsp; _data::protected =&gt;
     * &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset1 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset2 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset3 =&gt; Varien_Object(),
     * &nbsp; &nbsp; )
     * )
     * </code>
     *
     */
    public function _initEntities()
    {
        $this->form_field_rows = new Varien_Object(
            array(
                self::REQUEST_FORMS                                    => new Varien_Object(),
                self::ON_SUCCESS_URL                                   => new Varien_Object(),
                self::EXTRAS                                           => new Varien_Object(),
                self::CHECKOUT                                         => new Varien_Object(),

                self::INDIVIUAL_PERSONAL_INFORMATION                   => new Varien_Object(),
                self::FAMILY_PERSONAL_INFORMATION                      => new Varien_Object(),
                self::GROUP_INFORMATION                                => new Varien_Object(),
                self::COMPANY_INFORMATION                              => new Varien_Object(),
                self::MAILING_ADDRESS                                  => new Varien_Object(),
                self::SECONDARY_MAILING_ADDRESS                        => new Varien_Object(),
                self::GROUP_CONTACT_PERSON                             => new Varien_Object(),
                self::COMPANY_CONTACT_PERSON                           => new Varien_Object(),
                self::GROUP_CONTACT_PERSON_MAILING_ADDRESS             => new Varien_Object(),
                self::GROUP_CONTACT_PERSON_SECONDARY_MAILING_ADDRESS   => new Varien_Object(),
                self::COMPANY_CONTACT_PERSON_MAILING_ADDRESS           => new Varien_Object(),
                self::COMPANY_CONTACT_PERSON_SECONDARY_MAILING_ADDRESS => new Varien_Object(),
                self::PARENT_GUARDIAN_INFORMATION                      => new Varien_Object(),
                self::CONTACT_INFORMATION                              => new Varien_Object(),
                self::PERSON_TO_CONTACT                                => new Varien_Object(),
                self::BENEFICIARIES                                    => new Varien_Object(),
                self::AGENT_INFORMATION                                => new Varien_Object(),
            )
        );
    }
    /**
     * Process Entities
     *
     * Process all fieldsets attributes
     *
     * This method sets all attributes for every single fieldset
     *
     * Fieldset can contain
     * - Group Type     (Single or Multiple)
     * - title          Field set header text as title
     * - header_info    Field set header info text as a string rendered below fieldset header
     * - attributes     Ay html 5 attrbutes developer may think usefull.
     *
     * Data representation
     * <code>
     * fieldset1 =&gt; Object&nbsp;
     * (
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //If the elements is single
     * &nbsp;&nbsp; &nbsp;or
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //This will group the elements as one fieldset
     * )
     * -&gt;addData
     * (
     * &nbsp;&nbsp; &nbsp;title =&gt; Field set title
     * &nbsp;&nbsp; &nbsp;header_info =&gt; Fieldset info //this will appear below the fieldset title
     * )
     * </code>
     *
     */
    public function _processEntities()
    {
        $this->form_field_rows->getData(self::EXTRAS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => '',
            )
        );

        $this->form_field_rows->getData(self::CHECKOUT)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => '',
            )
        );

        $this->form_field_rows->getData(self::INDIVIUAL_PERSONAL_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'PERSONAL INFORMATION',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['form_type-form_type']",
                    'data-fieldset-group'           => "{'form_type':['Individual']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::FAMILY_PERSONAL_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'PERSONAL INFORMATION',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['form_type-form_type']",
                    'data-fieldset-group'           => "{'form_type':['Family']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'GROUP INFORMATION',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['form_type-form_type']",
                    'data-fieldset-group'           => "{'form_type':['Group']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'COMPANY INFORMATION',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['form_type-form_type']",
                    'data-fieldset-group'           => "{'form_type':['Company']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::MAILING_ADDRESS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'MAILING ADDRESS',
            )
        );

        $this->form_field_rows->getData(self::SECONDARY_MAILING_ADDRESS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'SECONDARY MAILING ADDRESS',
            )
        );

        $this->form_field_rows->getData(self::GROUP_CONTACT_PERSON)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'CONTACT PERSON',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['form_type-form_type']",
                    'data-fieldset-group'           => "{'form_type':['Group']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_CONTACT_PERSON)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'CONTACT PERSON',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['form_type-form_type']",
                    'data-fieldset-group'           => "{'form_type':['Company']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_CONTACT_PERSON_MAILING_ADDRESS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'MAILING ADDRESS',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['form_type-form_type']",
                    'data-fieldset-group'           => "{'form_type':['Group']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_CONTACT_PERSON_SECONDARY_MAILING_ADDRESS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'SECONDARY MAILING ADDRESS',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['form_type-form_type']",
                    'data-fieldset-group'           => "{'form_type':['Group']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_CONTACT_PERSON_MAILING_ADDRESS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'MAILING ADDRESS',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['form_type-form_type']",
                    'data-fieldset-group'           => "{'form_type':['Company']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_CONTACT_PERSON_SECONDARY_MAILING_ADDRESS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'SECONDARY MAILING ADDRESS',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['form_type-form_type']",
                    'data-fieldset-group'           => "{'form_type':['Company']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::PARENT_GUARDIAN_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'PARENT/GUARDIAN INFORMATION',
                'header_info' => 'All persons who are 18 years old or younger is required to have a guardian when getting an insurance contract. The guardian should not accompany the insured during the travel. If the guardian will accompany the insured, please choose group, then family.',
            )
        );

        $this->form_field_rows->getData(self::CONTACT_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'CONTACT INFORMATION',
            )
        );

        $this->form_field_rows->getData(self::PERSON_TO_CONTACT)->addData(
            $this->getGroupTypeMultiple()
        )->addData(
            array(
                'title' => 'PERSON TO CONTACT IN CASE OF EMERGENCY',
            )
        );

        $this->form_field_rows->getData(self::BENEFICIARIES)->addData(
            $this->getGroupTypeMultiple()
        )->addData(
            array(
                'title' => 'BENEFICIARIES',
            )
        );

        $this->form_field_rows->getData(self::AGENT_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'AGENT INFORMATION',
                'header_info' => 'Do you have an agent with UCPB GEN?',
            )
        );
    }

    /**
     * Build all items in each field
     */
    public function _buildFieldItems()
    {
        $this->form_field_rows->getData(self::EXTRAS)->addData(
            array(
                'elements' => array(
                    'type_of_coverage' => $this->_buildElements('type_of_coverage',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'Type of coverage',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::CHECKOUT)->addData(
            array(
                'elements' => array(
                    'firstname' => $this->_buildElements('firstname',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'firstname',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'lastname'  => $this->_buildElements('lastname',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'lastname',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'address'   => $this->_buildElements('address',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'address',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'city'      => $this->_buildElements('city',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'city',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'postcode'  => $this->_buildElements('postcode',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'postcode',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'tel_no'    => $this->_buildElements('tel_no',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'tel_no',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'country'   => $this->_buildElements('country',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'country',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'email'     => $this->_buildElements('email',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'email',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'price'     => $this->_buildElements('price',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'price',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'currency'  => $this->_buildElements('currency',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'currency',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::INDIVIUAL_PERSONAL_INFORMATION)->addData(
            array(
                'elements' => array(
                    'previous_form_key'         => $this->_buildElements('previous_form_key',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'Summary Form Key',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'salutation'                => $this->_buildElements('salutation',
                        array(
                            'column'      => '4',
                            'type'        => 'select',
                            'required'    => 1,
                            'placeholder' => 'Salutation *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'     => array(
                                array(
                                    'label' => 'Salutation *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Amb.',
                                    'value' => 'Amb.',
                                ),
                                array(
                                    'label' => 'Atty.',
                                    'value' => 'Atty.',
                                ),
                                array(
                                    'label' => 'Dr.',
                                    'value' => 'Dr.',
                                ),
                                array(
                                    'label' => 'Engr.',
                                    'value' => 'Engr.',
                                ),
                                array(
                                    'label' => 'Mr.',
                                    'value' => 'Mr.',
                                ),
                                array(
                                    'label' => 'Mrs.',
                                    'value' => 'Mrs.',
                                ),
                                array(
                                    'label' => 'Ms.',
                                    'value' => 'Ms.',
                                ),
                                array(
                                    'label' => 'Sps.',
                                    'value' => 'Sps.',
                                ),
                            ),
                        )
                    ),
                    'last_name'                 => $this->_buildElements('last_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'first_name'                => $this->_buildElements('first_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'First Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'middle_initial'            => $this->_buildElements('middle_initial',
                        array(
                            'column'          => '4',
                            'type'            => 'text',
                            'required'        => 0,
                            'placeholder'     => 'Middle Initial',
                            'clearfix_before' => 1,
                            'attributes'      => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z]$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'suffix'                    => $this->_buildElements('suffix',
                        array(
                            'column'         => '4',
                            'type'           => 'select',
                            'required'       => 0,
                            'placeholder'    => 'Suffix',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[A-Za-z_.-]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Suffix',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Engr.',
                                    'value' => 'Engr.',
                                ),
                                array(
                                    'label' => 'II',
                                    'value' => 'II',
                                ),
                                array(
                                    'label' => 'III',
                                    'value' => 'III',
                                ),
                                array(
                                    'label' => 'IV',
                                    'value' => 'IV',
                                ),
                                array(
                                    'label' => 'Sr.',
                                    'value' => 'Sr.',
                                ),
                                array(
                                    'label' => 'None',
                                    'value' => 'None',
                                ),
                            ),
                        )
                    ),
                    'status'                    => $this->_buildElements('status',
                        array(
                            'column'          => '4',
                            'type'            => 'select',
                            'placeholder'     => 'Status *',
                            'required'        => 1,
                            'clearfix_before' => 1,
                            //custom attributes
                            'attributes'      => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'         => array(
                                array(
                                    'label' => 'Status *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Single',
                                    'value' => 'Single',
                                ),
                                array(
                                    'label' => 'Married',
                                    'value' => 'Married',
                                ),
                                array(
                                    'label' => 'Separated',
                                    'value' => 'Separated',
                                ),
                                array(
                                    'label' => 'Divorced',
                                    'value' => 'Divorced',
                                ),
                                array(
                                    'label' => 'Widowed',
                                    'value' => 'Widowed',
                                ),
                            ),
                        )
                    ),
                    'nationality'               => $this->_buildElements('nationality',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => Mage::helper('builder')->getStaticNationalitiesOptions(),
                        )
                    ),
                    'gender'                    => $this->_buildElements('gender',
                        array(
                            'column'         => '4',
                            'type'           => 'select',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            'placeholder'    => 'Gender *',
                            //custom attributes
                            'attributes'     => array(
                                'data-bv-excluded' => 'false',
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Gender *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Female',
                                    'value' => 'Female',
                                ),
                                array(
                                    'label' => 'Male',
                                    'value' => 'Male',
                                ),
                            ),
                        )
                    ),
                    'birth_date'                => $this->_buildElements('birth_date',
                        array(
                            'column'      => '4',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Birthdate *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'place_of_birth'            => $this->_buildElements('place_of_birth',
                        array(
                            'column'         => '4',
                            'type'           => 'text',
                            'placeholder'    => 'Place of Birth',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^(?=.*?[A-Za-z])[a-zA-Z0-9 _-]+$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'occupation'                => $this->_buildElements('occupation',
                        array(
                            'column'      => '4',
                            'type'        => 'select',
                            'required'    => 1,
                            'placeholder' => 'occupation *',
                            //custom attributes
                            'attributes'  => array(
                                'data-bv-excluded' => 'false',
                            ),
                            'options'     => array(
                                array(
                                    'label' => 'Occupation *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'ACCOUNTANT',
                                    'value' => 'ACCOUNTANT',
                                ),
                                array(
                                    'label' => 'ACTOR',
                                    'value' => 'ACTOR',
                                ),
                                array(
                                    'label' => 'ACTUARY',
                                    'value' => 'ACTUARY',
                                ),
                                array(
                                    'label' => 'ADJUSTER',
                                    'value' => 'ADJUSTER',
                                ),
                                array(
                                    'label' => 'ADMINISTRATOR',
                                    'value' => 'ADMINISTRATOR',
                                ),
                                array(
                                    'label' => 'ADVISER',
                                    'value' => 'ADVISER',
                                ),
                                array(
                                    'label' => 'AGENT',
                                    'value' => 'AGENT',
                                ),
                                array(
                                    'label' => 'AGRICULTURIST',
                                    'value' => 'AGRICULTURIST',
                                ),
                                array(
                                    'label' => 'AIDE',
                                    'value' => 'AIDE',
                                ),
                                array(
                                    'label' => 'ANALYST',
                                    'value' => 'ANALYST',
                                ),
                                array(
                                    'label' => 'ANNOUNCER',
                                    'value' => 'ANNOUNCER',
                                ),
                                array(
                                    'label' => 'APPRAISER',
                                    'value' => 'APPRAISER',
                                ),
                                array(
                                    'label' => 'APPRENTICE',
                                    'value' => 'APPRENTICE',
                                ),
                                array(
                                    'label' => 'ARCHITECT',
                                    'value' => 'ARCHITECT',
                                ),
                                array(
                                    'label' => 'ARTIST',
                                    'value' => 'ARTIST',
                                ),
                                array(
                                    'label' => 'ASSISTANT',
                                    'value' => 'ASSISTANT',
                                ),
                                array(
                                    'label' => 'ASSOCIATE',
                                    'value' => 'ASSOCIATE',
                                ),
                                array(
                                    'label' => 'ASSOCIATE JUSTICE',
                                    'value' => 'ASSOCIATE JUSTICE',
                                ),
                                array(
                                    'label' => 'ASST. VICE PRESIDENT',
                                    'value' => 'ASST. VICE PRESIDENT',
                                ),
                                array(
                                    'label' => 'ATHLETE',
                                    'value' => 'ATHLETE',
                                ),
                                array(
                                    'label' => 'ATTENDANT',
                                    'value' => 'ATTENDANT',
                                ),
                                array(
                                    'label' => 'AUDITOR',
                                    'value' => 'AUDITOR',
                                ),
                                array(
                                    'label' => 'AUXILLARIES TENDER',
                                    'value' => 'AUXILLARIES TENDER',
                                ),
                                array(
                                    'label' => 'AVIATOR',
                                    'value' => 'AVIATOR',
                                ),
                                array(
                                    'label' => 'BABY SITTER',
                                    'value' => 'BABY SITTER',
                                ),
                                array(
                                    'label' => 'BACK MARKER',
                                    'value' => 'BACK MARKER',
                                ),
                                array(
                                    'label' => 'BAKER',
                                    'value' => 'BAKER',
                                ),
                                array(
                                    'label' => 'BANKER',
                                    'value' => 'BANKER',
                                ),
                                array(
                                    'label' => 'BARANGAY TANOD',
                                    'value' => 'BARANGAY TANOD',
                                ),
                                array(
                                    'label' => 'BARBER',
                                    'value' => 'BARBER',
                                ),
                                array(
                                    'label' => 'BARTENDER',
                                    'value' => 'BARTENDER',
                                ),
                                array(
                                    'label' => 'BENDER',
                                    'value' => 'BENDER',
                                ),
                                array(
                                    'label' => 'BOARD DIRECTOR',
                                    'value' => 'BOARD DIRECTOR',
                                ),
                                array(
                                    'label' => 'BOARD MEMBER',
                                    'value' => 'BOARD MEMBER',
                                ),
                                array(
                                    'label' => 'BOARD OF TRUSTEE',
                                    'value' => 'BOARD OF TRUSTEE',
                                ),
                                array(
                                    'label' => 'BOOKKEEPER',
                                    'value' => 'BOOKKEEPER',
                                ),
                                array(
                                    'label' => 'BUILDER',
                                    'value' => 'BUILDER',
                                ),
                                array(
                                    'label' => 'CONDUCTOR',
                                    'value' => 'CONDUCTOR',
                                ),
                                array(
                                    'label' => 'BUSINESSMAN',
                                    'value' => 'BUSINESSMAN',
                                ),
                                array(
                                    'label' => 'BUYER',
                                    'value' => 'BUYER',
                                ),
                                array(
                                    'label' => 'CADDY MASTER',
                                    'value' => 'CADDY MASTER',
                                ),
                                array(
                                    'label' => 'CADET',
                                    'value' => 'CADET',
                                ),
                                array(
                                    'label' => 'CAMERAMAN',
                                    'value' => 'CAMERAMAN',
                                ),
                                array(
                                    'label' => 'CANVASSER',
                                    'value' => 'CANVASSER',
                                ),
                                array(
                                    'label' => 'CAPTAIN',
                                    'value' => 'CAPTAIN',
                                ),
                                array(
                                    'label' => 'CAR GROOMER',
                                    'value' => 'CAR GROOMER',
                                ),
                                array(
                                    'label' => 'CAREGIVER',
                                    'value' => 'CAREGIVER',
                                ),
                                array(
                                    'label' => 'CARETAKER',
                                    'value' => 'CARETAKER',
                                ),
                                array(
                                    'label' => 'CARPENTER',
                                    'value' => 'CARPENTER',
                                ),
                                array(
                                    'label' => 'CASHIER',
                                    'value' => 'CASHIER',
                                ),
                                array(
                                    'label' => 'CEO',
                                    'value' => 'CEO',
                                ),
                                array(
                                    'label' => 'CHAIRMAN',
                                    'value' => 'CHAIRMAN',
                                ),
                                array(
                                    'label' => 'CHAIRMAN OF THE BOARD',
                                    'value' => 'CHAIRMAN OF THE BOARD',
                                ),
                                array(
                                    'label' => 'CHAIRPERSON',
                                    'value' => 'CHAIRPERSON',
                                ),
                                array(
                                    'label' => 'CHAUFFEUR',
                                    'value' => 'CHAUFFEUR',
                                ),
                                array(
                                    'label' => 'CHECKER',
                                    'value' => 'CHECKER',
                                ),
                                array(
                                    'label' => 'CHEF',
                                    'value' => 'CHEF',
                                ),
                                array(
                                    'label' => 'CHEMIST',
                                    'value' => 'CHEMIST',
                                ),
                                array(
                                    'label' => 'CHIEF',
                                    'value' => 'CHIEF',
                                ),
                                array(
                                    'label' => 'CHOREOGRAPHER',
                                    'value' => 'CHOREOGRAPHER',
                                ),
                                array(
                                    'label' => 'CINEMATOGRAPHER',
                                    'value' => 'CINEMATOGRAPHER',
                                ),
                                array(
                                    'label' => 'CLEANER',
                                    'value' => 'CLEANER',
                                ),
                                array(
                                    'label' => 'CLERGYMAN',
                                    'value' => 'CLERGYMAN',
                                ),
                                array(
                                    'label' => 'CLERK',
                                    'value' => 'CLERK',
                                ),
                                array(
                                    'label' => 'COACH',
                                    'value' => 'COACH',
                                ),
                                array(
                                    'label' => 'COLLECTOR',
                                    'value' => 'COLLECTOR',
                                ),
                                array(
                                    'label' => 'COMMISSIONER',
                                    'value' => 'COMMISSIONER',
                                ),
                                array(
                                    'label' => 'CONSTITUENTS',
                                    'value' => 'CONSTITUENTS',
                                ),
                                array(
                                    'label' => 'CONSTRUCTION WORKER',
                                    'value' => 'CONSTRUCTION WORKER',
                                ),
                                array(
                                    'label' => 'CONSULTANT',
                                    'value' => 'CONSULTANT',
                                ),
                                array(
                                    'label' => 'CONTRACTOR',
                                    'value' => 'CONTRACTOR',
                                ),
                                array(
                                    'label' => 'CONTROLLER',
                                    'value' => 'CONTROLLER',
                                ),
                                array(
                                    'label' => 'COORDINATOR',
                                    'value' => 'COORDINATOR',
                                ),
                                array(
                                    'label' => 'COUNCILOR',
                                    'value' => 'COUNCILOR',
                                ),
                                array(
                                    'label' => 'COUNSEL',
                                    'value' => 'COUNSEL',
                                ),
                                array(
                                    'label' => 'COUNSELOR',
                                    'value' => 'COUNSELOR',
                                ),
                                array(
                                    'label' => 'CREW',
                                    'value' => 'CREW',
                                ),
                                array(
                                    'label' => 'CUSTODIAN',
                                    'value' => 'CUSTODIAN',
                                ),
                                array(
                                    'label' => 'DANCER',
                                    'value' => 'DANCER',
                                ),
                                array(
                                    'label' => 'DEALER',
                                    'value' => 'DEALER',
                                ),
                                array(
                                    'label' => 'DELIVERY MAN',
                                    'value' => 'DELIVERY MAN',
                                ),
                                array(
                                    'label' => 'DENTIST',
                                    'value' => 'DENTIST',
                                ),
                                array(
                                    'label' => 'DESIGNER',
                                    'value' => 'DESIGNER',
                                ),
                                array(
                                    'label' => 'DEVELOPER',
                                    'value' => 'DEVELOPER',
                                ),
                                array(
                                    'label' => 'DIPLOMAT',
                                    'value' => 'DIPLOMAT',
                                ),
                                array(
                                    'label' => 'DIRECTOR',
                                    'value' => 'DIRECTOR',
                                ),
                                array(
                                    'label' => 'DISC JOCKEY',
                                    'value' => 'DISC JOCKEY',
                                ),
                                array(
                                    'label' => 'DISHWASHER',
                                    'value' => 'DISHWASHER',
                                ),
                                array(
                                    'label' => 'DISPATCHER',
                                    'value' => 'DISPATCHER',
                                ),
                                array(
                                    'label' => 'DISTRIBUTOR',
                                    'value' => 'DISTRIBUTOR',
                                ),
                                array(
                                    'label' => 'DOCTOR',
                                    'value' => 'DOCTOR',
                                ),
                                array(
                                    'label' => 'DRAFTSMAN',
                                    'value' => 'DRAFTSMAN',
                                ),
                                array(
                                    'label' => 'DRILLER',
                                    'value' => 'DRILLER',
                                ),
                                array(
                                    'label' => 'DRIVER',
                                    'value' => 'DRIVER',
                                ),
                                array(
                                    'label' => 'EDITOR',
                                    'value' => 'EDITOR',
                                ),
                                array(
                                    'label' => 'ELECTRICIAN',
                                    'value' => 'ELECTRICIAN',
                                ),
                                array(
                                    'label' => 'EMBALMER',
                                    'value' => 'EMBALMER',
                                ),
                                array(
                                    'label' => 'EMPLOYEE',
                                    'value' => 'EMPLOYEE',
                                ),
                                array(
                                    'label' => 'ENCODER',
                                    'value' => 'ENCODER',
                                ),
                                array(
                                    'label' => 'ENGINEER',
                                    'value' => 'ENGINEER',
                                ),
                                array(
                                    'label' => 'ENGRAVER',
                                    'value' => 'ENGRAVER',
                                ),
                                array(
                                    'label' => 'ENTERTAINER',
                                    'value' => 'ENTERTAINER',
                                ),
                                array(
                                    'label' => 'ENTREPRENUER',
                                    'value' => 'ENTREPRENUER',
                                ),
                                array(
                                    'label' => 'EXECUTIVE',
                                    'value' => 'EXECUTIVE',
                                ),
                                array(
                                    'label' => 'EXPATRIATE',
                                    'value' => 'EXPATRIATE',
                                ),
                                array(
                                    'label' => 'EXPEDITER',
                                    'value' => 'EXPEDITER',
                                ),
                                array(
                                    'label' => 'FABRICATOR',
                                    'value' => 'FABRICATOR',
                                ),
                                array(
                                    'label' => 'FARMER',
                                    'value' => 'FARMER',
                                ),
                                array(
                                    'label' => 'FINANCIAL ANALYST',
                                    'value' => 'FINANCIAL ANALYST',
                                ),
                                array(
                                    'label' => 'FOREIGNER',
                                    'value' => 'FOREIGNER',
                                ),
                                array(
                                    'label' => 'FOREMAN',
                                    'value' => 'FOREMAN',
                                ),
                                array(
                                    'label' => 'FORENSIC',
                                    'value' => 'FORENSIC',
                                ),
                                array(
                                    'label' => 'FORESTER',
                                    'value' => 'FORESTER',
                                ),
                                array(
                                    'label' => 'GARDENER',
                                    'value' => 'GARDENER',
                                ),
                                array(
                                    'label' => 'GASOLINE BOY',
                                    'value' => 'GASOLINE BOY',
                                ),
                                array(
                                    'label' => 'GAUFER',
                                    'value' => 'GAUFER',
                                ),
                                array(
                                    'label' => 'GEOLOGIST',
                                    'value' => 'GEOLOGIST',
                                ),
                                array(
                                    'label' => 'GRINDER',
                                    'value' => 'GRINDER',
                                ),
                                array(
                                    'label' => 'GROUNDSKEEPER',
                                    'value' => 'GROUNDSKEEPER',
                                ),
                                array(
                                    'label' => 'GUARD',
                                    'value' => 'GUARD',
                                ),
                                array(
                                    'label' => 'HAIR DRESSER',
                                    'value' => 'HAIR DRESSER',
                                ),
                                array(
                                    'label' => 'HELPER',
                                    'value' => 'HELPER',
                                ),
                                array(
                                    'label' => 'HOTELIER',
                                    'value' => 'HOTELIER',
                                ),
                                array(
                                    'label' => 'HOUSEKEEPER',
                                    'value' => 'HOUSEKEEPER',
                                ),
                                array(
                                    'label' => 'HOUSEWIFE',
                                    'value' => 'HOUSEWIFE',
                                ),
                                array(
                                    'label' => 'HR',
                                    'value' => 'HR',
                                ),
                                array(
                                    'label' => 'INSPECTOR',
                                    'value' => 'INSPECTOR',
                                ),
                                array(
                                    'label' => 'INSTALLER',
                                    'value' => 'INSTALLER',
                                ),
                                array(
                                    'label' => 'INSTRUCTOR',
                                    'value' => 'INSTRUCTOR',
                                ),
                                array(
                                    'label' => 'INVESTIGATOR',
                                    'value' => 'INVESTIGATOR',
                                ),
                                array(
                                    'label' => 'JANITOR',
                                    'value' => 'JANITOR',
                                ),
                                array(
                                    'label' => 'JEWELER',
                                    'value' => 'JEWELER',
                                ),
                                array(
                                    'label' => 'JOURNALIST',
                                    'value' => 'JOURNALIST',
                                ),
                                array(
                                    'label' => 'JUDGE',
                                    'value' => 'JUDGE',
                                ),
                                array(
                                    'label' => 'LABORER',
                                    'value' => 'LABORER',
                                ),
                                array(
                                    'label' => 'LAWYER',
                                    'value' => 'LAWYER',
                                ),
                                array(
                                    'label' => 'LEADMAN',
                                    'value' => 'LEADMAN',
                                ),
                                array(
                                    'label' => 'LESSOR',
                                    'value' => 'LESSOR',
                                ),
                                array(
                                    'label' => 'LIAISON',
                                    'value' => 'LIAISON',
                                ),
                                array(
                                    'label' => 'LIFEGUARD',
                                    'value' => 'LIFEGUARD',
                                ),
                                array(
                                    'label' => 'LINEMAN',
                                    'value' => 'LINEMAN',
                                ),
                                array(
                                    'label' => 'LIVESTOCKS RAISERS',
                                    'value' => 'LIVESTOCKS RAISERS',
                                ),
                                array(
                                    'label' => 'LUBEMAN',
                                    'value' => 'LUBEMAN',
                                ),
                                array(
                                    'label' => 'MACHINIST',
                                    'value' => 'MACHINIST',
                                ),
                                array(
                                    'label' => 'MAID',
                                    'value' => 'MAID',
                                ),
                                array(
                                    'label' => 'MAINTENANCE',
                                    'value' => 'MAINTENANCE',
                                ),
                                array(
                                    'label' => 'MANAGER',
                                    'value' => 'MANAGER',
                                ),
                                array(
                                    'label' => 'MANICURIST',
                                    'value' => 'MANICURIST',
                                ),
                                array(
                                    'label' => 'MARKETING',
                                    'value' => 'MARKETING',
                                ),
                                array(
                                    'label' => 'MARSHALL',
                                    'value' => 'MARSHALL',
                                ),
                                array(
                                    'label' => 'MASON',
                                    'value' => 'MASON',
                                ),
                                array(
                                    'label' => 'MASSEUSE',
                                    'value' => 'MASSEUSE',
                                ),
                                array(
                                    'label' => 'MAYOR',
                                    'value' => 'MAYOR',
                                ),
                                array(
                                    'label' => 'MECHANIC',
                                    'value' => 'MECHANIC',
                                ),
                                array(
                                    'label' => 'MEDICAL REPRESENTATIVE',
                                    'value' => 'MEDICAL REPRESENTATIVE',
                                ),
                                array(
                                    'label' => 'MEDICAL TECHNOLOGIST',
                                    'value' => 'MEDICAL TECHNOLOGIST',
                                ),
                                array(
                                    'label' => 'MEMBER',
                                    'value' => 'MEMBER',
                                ),
                                array(
                                    'label' => 'MERCHANDISING',
                                    'value' => 'MERCHANDISING',
                                ),
                                array(
                                    'label' => 'MERCHANT',
                                    'value' => 'MERCHANT',
                                ),
                                array(
                                    'label' => 'MESSENGER',
                                    'value' => 'MESSENGER',
                                ),
                                array(
                                    'label' => 'METAL FABRICATION',
                                    'value' => 'METAL FABRICATION',
                                ),
                                array(
                                    'label' => 'MIDWIFE',
                                    'value' => 'MIDWIFE',
                                ),
                                array(
                                    'label' => 'MILITARY',
                                    'value' => 'MILITARY',
                                ),
                                array(
                                    'label' => 'MINISTER',
                                    'value' => 'MINISTER',
                                ),
                                array(
                                    'label' => 'MISSIONARY',
                                    'value' => 'MISSIONARY',
                                ),
                                array(
                                    'label' => 'MUSICIAN',
                                    'value' => 'MUSICIAN',
                                ),
                                array(
                                    'label' => 'NURSE',
                                    'value' => 'NURSE',
                                ),
                                array(
                                    'label' => 'NUTRITIONIST',
                                    'value' => 'NUTRITIONIST',
                                ),
                                array(
                                    'label' => 'OFFICER',
                                    'value' => 'OFFICER',
                                ),
                                array(
                                    'label' => 'OILER',
                                    'value' => 'OILER',
                                ),
                                array(
                                    'label' => 'ON THE JOB TRAINING (OJT)',
                                    'value' => 'ON THE JOB TRAINING (OJT)',
                                ),
                                array(
                                    'label' => 'ONLINE ARTICLE WRITER',
                                    'value' => 'ONLINE ARTICLE WRITER',
                                ),
                                array(
                                    'label' => 'OPERATOR',
                                    'value' => 'OPERATOR',
                                ),
                                array(
                                    'label' => 'OPTICIAN',
                                    'value' => 'OPTICIAN',
                                ),
                                array(
                                    'label' => 'OVERSEAS FILIPINO WORKER',
                                    'value' => 'OVERSEAS FILIPINO WORKER',
                                ),
                                array(
                                    'label' => 'OWNER',
                                    'value' => 'OWNER',
                                ),
                                array(
                                    'label' => 'PACKER',
                                    'value' => 'PACKER',
                                ),
                                array(
                                    'label' => 'PAINTER',
                                    'value' => 'PAINTER',
                                ),
                                array(
                                    'label' => 'PASTOR',
                                    'value' => 'PASTOR',
                                ),
                                array(
                                    'label' => 'PERSONNEL',
                                    'value' => 'PERSONNEL',
                                ),
                                array(
                                    'label' => 'PHARMACIST',
                                    'value' => 'PHARMACIST',
                                ),
                                array(
                                    'label' => 'PHOTOGRAPHER',
                                    'value' => 'PHOTOGRAPHER',
                                ),
                                array(
                                    'label' => 'PILOT',
                                    'value' => 'PILOT',
                                ),
                                array(
                                    'label' => 'PLUMBER',
                                    'value' => 'PLUMBER',
                                ),
                                array(
                                    'label' => 'POLISHER',
                                    'value' => 'POLISHER',
                                ),
                                array(
                                    'label' => 'PORTER',
                                    'value' => 'PORTER',
                                ),
                                array(
                                    'label' => 'PRESIDENT',
                                    'value' => 'PRESIDENT',
                                ),
                                array(
                                    'label' => 'PRIEST',
                                    'value' => 'PRIEST',
                                ),
                                array(
                                    'label' => 'PRODUCER',
                                    'value' => 'PRODUCER',
                                ),
                                array(
                                    'label' => 'PROFESSOR',
                                    'value' => 'PROFESSOR',
                                ),
                                array(
                                    'label' => 'PROGRAMMER',
                                    'value' => 'PROGRAMMER',
                                ),
                                array(
                                    'label' => 'PROPRIETOR',
                                    'value' => 'PROPRIETOR',
                                ),
                                array(
                                    'label' => 'PURCHASER',
                                    'value' => 'PURCHASER',
                                ),
                                array(
                                    'label' => 'REAL ESTATE BROKER',
                                    'value' => 'REAL ESTATE BROKER',
                                ),
                                array(
                                    'label' => 'REALTOR',
                                    'value' => 'REALTOR',
                                ),
                                array(
                                    'label' => 'RECEPTIONIST',
                                    'value' => 'RECEPTIONIST',
                                ),
                                array(
                                    'label' => 'REPORTER',
                                    'value' => 'REPORTER',
                                ),
                                array(
                                    'label' => 'RESEARCHER',
                                    'value' => 'RESEARCHER',
                                ),
                                array(
                                    'label' => 'RETIRED',
                                    'value' => 'RETIRED',
                                ),
                                array(
                                    'label' => 'REWINDER',
                                    'value' => 'REWINDER',
                                ),
                                array(
                                    'label' => 'RIDER',
                                    'value' => 'RIDER',
                                ),
                                array(
                                    'label' => 'RIGGER',
                                    'value' => 'RIGGER',
                                ),
                                array(
                                    'label' => 'SALES',
                                    'value' => 'SALES',
                                ),
                                array(
                                    'label' => 'SALESMAN',
                                    'value' => 'SALESMAN',
                                ),
                                array(
                                    'label' => 'SCAFFOLDER',
                                    'value' => 'SCAFFOLDER',
                                ),
                                array(
                                    'label' => 'SEAFARER',
                                    'value' => 'SEAFARER',
                                ),
                                array(
                                    'label' => 'SEAMAN',
                                    'value' => 'SEAMAN',
                                ),
                                array(
                                    'label' => 'SECRETARY',
                                    'value' => 'SECRETARY',
                                ),
                                array(
                                    'label' => 'SELF-EMPLOYED',
                                    'value' => 'SELF-EMPLOYED',
                                ),
                                array(
                                    'label' => 'SELLER',
                                    'value' => 'SELLER',
                                ),
                                array(
                                    'label' => 'SEMI-RETIRED BUSINESSMAN',
                                    'value' => 'SEMI-RETIRED BUSINESSMAN',
                                ),
                                array(
                                    'label' => 'SENIOR CITIZEN',
                                    'value' => 'SENIOR CITIZEN',
                                ),
                                array(
                                    'label' => 'SERVICER',
                                    'value' => 'SERVICER',
                                ),
                                array(
                                    'label' => 'SEWER',
                                    'value' => 'SEWER',
                                ),
                                array(
                                    'label' => 'SHEET HELPER',
                                    'value' => 'SHEET HELPER',
                                ),
                                array(
                                    'label' => 'SPECIALIST',
                                    'value' => 'SPECIALIST',
                                ),
                                array(
                                    'label' => 'STAFF',
                                    'value' => 'STAFF',
                                ),
                                array(
                                    'label' => 'STEELMAN',
                                    'value' => 'STEELMAN',
                                ),
                                array(
                                    'label' => 'STENOGRAPHER',
                                    'value' => 'STENOGRAPHER',
                                ),
                                array(
                                    'label' => 'STEWARD',
                                    'value' => 'STEWARD',
                                ),
                                array(
                                    'label' => 'STOCKMAN',
                                    'value' => 'STOCKMAN',
                                ),
                                array(
                                    'label' => 'STORE KEEPER',
                                    'value' => 'STORE KEEPER',
                                ),
                                array(
                                    'label' => 'STRATEGIST',
                                    'value' => 'STRATEGIST',
                                ),
                                array(
                                    'label' => 'STUDENT',
                                    'value' => 'STUDENT',
                                ),
                                array(
                                    'label' => 'SUPERVISOR',
                                    'value' => 'SUPERVISOR',
                                ),
                                array(
                                    'label' => 'SURVEYOR',
                                    'value' => 'SURVEYOR',
                                ),
                                array(
                                    'label' => 'SWEEPER',
                                    'value' => 'SWEEPER',
                                ),
                                array(
                                    'label' => 'TAILOR',
                                    'value' => 'TAILOR',
                                ),
                                array(
                                    'label' => 'TEACHER',
                                    'value' => 'TEACHER',
                                ),
                                array(
                                    'label' => 'TECHNICIAN',
                                    'value' => 'TECHNICIAN',
                                ),
                                array(
                                    'label' => 'TELLER',
                                    'value' => 'TELLER',
                                ),
                                array(
                                    'label' => 'THERAPIST',
                                    'value' => 'THERAPIST',
                                ),
                                array(
                                    'label' => 'TIME KEEPER',
                                    'value' => 'TIME KEEPER',
                                ),
                                array(
                                    'label' => 'TINSMITH',
                                    'value' => 'TINSMITH',
                                ),
                                array(
                                    'label' => 'TIREMAN',
                                    'value' => 'TIREMAN',
                                ),
                                array(
                                    'label' => 'TRADER',
                                    'value' => 'TRADER',
                                ),
                                array(
                                    'label' => 'TRAINEE',
                                    'value' => 'TRAINEE',
                                ),
                                array(
                                    'label' => 'TRAINOR',
                                    'value' => 'TRAINOR',
                                ),
                                array(
                                    'label' => 'TRAVELER',
                                    'value' => 'TRAVELER',
                                ),
                                array(
                                    'label' => 'TREASURER',
                                    'value' => 'TREASURER',
                                ),
                                array(
                                    'label' => 'UNDERWRITER',
                                    'value' => 'UNDERWRITER',
                                ),
                                array(
                                    'label' => 'UNEMPLOYED',
                                    'value' => 'UNEMPLOYED',
                                ),
                                array(
                                    'label' => 'UPHOLSTERER',
                                    'value' => 'UPHOLSTERER',
                                ),
                                array(
                                    'label' => 'VENDOR',
                                    'value' => 'VENDOR',
                                ),
                                array(
                                    'label' => 'VETERINARIAN',
                                    'value' => 'VETERINARIAN',
                                ),
                                array(
                                    'label' => 'VICE PRESIDENT',
                                    'value' => 'VICE PRESIDENT',
                                ),
                                array(
                                    'label' => 'VOLUNTEER',
                                    'value' => 'VOLUNTEER',
                                ),
                                array(
                                    'label' => 'WAITER',
                                    'value' => 'WAITER',
                                ),
                                array(
                                    'label' => 'WASHER',
                                    'value' => 'WASHER',
                                ),
                                array(
                                    'label' => 'WATCHMAN',
                                    'value' => 'WATCHMAN',
                                ),
                                array(
                                    'label' => 'WELDER',
                                    'value' => 'WELDER',
                                ),
                                array(
                                    'label' => 'WORKER',
                                    'value' => 'WORKER',
                                ),
                                array(
                                    'label' => 'WRITER',
                                    'value' => 'WRITER',
                                ),
                                array(
                                    'label' => 'Others',
                                    'value' => 'Others',
                                ),
                            ),
                        )
                    ),
                    'tax_identification_number' => $this->_buildElements('tax_identification_number',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Tax Identification Number *',
                            'item_note'   => "Sample Format: 012-345-678-901",
                            'attributes'  => array(
                                'data-mask-config'       => '999-999-999-999',
                                'data-mask-placeholder'  => '',
                                'pattern'                => "([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})$",
                                'data-bv-regexp-message' => 'TIN Number is invalid',
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'false',
                            ),
                        )
                    ),
                    'employer'                  => $this->_buildElements('employer',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Employer *',
                            'attributes'  => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z. ]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::FAMILY_PERSONAL_INFORMATION)->addData(
            array(
                'elements' => array(
                    'previous_form_key'         => $this->_buildElements('previous_form_key',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'Summary Form Key',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'salutation'                => $this->_buildElements('salutation',
                        array(
                            'column'      => '4',
                            'type'        => 'select',
                            'required'    => 1,
                            'placeholder' => 'Salutation *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'     => array(
                                array(
                                    'label' => 'Salutation *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Amb.',
                                    'value' => 'Amb.',
                                ),
                                array(
                                    'label' => 'Atty.',
                                    'value' => 'Atty.',
                                ),
                                array(
                                    'label' => 'Dr.',
                                    'value' => 'Dr.',
                                ),
                                array(
                                    'label' => 'Engr.',
                                    'value' => 'Engr.',
                                ),
                                array(
                                    'label' => 'Mr.',
                                    'value' => 'Mr.',
                                ),
                                array(
                                    'label' => 'Mrs.',
                                    'value' => 'Mrs.',
                                ),
                                array(
                                    'label' => 'Ms.',
                                    'value' => 'Ms.',
                                ),
                                array(
                                    'label' => 'Sps.',
                                    'value' => 'Sps.',
                                ),
                            ),
                        )
                    ),
                    'last_name'                 => $this->_buildElements('last_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'first_name'                => $this->_buildElements('first_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'First Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'middle_initial'            => $this->_buildElements('middle_initial',
                        array(
                            'column'          => '4',
                            'type'            => 'text',
                            'required'        => 0,
                            'placeholder'     => 'Middle Initial',
                            'clearfix_before' => 1,
                            'attributes'      => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z]$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'suffix'                    => $this->_buildElements('suffix',
                        array(
                            'column'         => '4',
                            'type'           => 'select',
                            'required'       => 0,
                            'placeholder'    => 'Suffix',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[A-Za-z_.-]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Suffix',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Engr.',
                                    'value' => 'Engr.',
                                ),
                                array(
                                    'label' => 'II',
                                    'value' => 'II',
                                ),
                                array(
                                    'label' => 'III',
                                    'value' => 'III',
                                ),
                                array(
                                    'label' => 'IV',
                                    'value' => 'IV',
                                ),
                                array(
                                    'label' => 'Sr.',
                                    'value' => 'Sr.',
                                ),
                                array(
                                    'label' => 'None',
                                    'value' => 'None',
                                ),
                            ),
                        )
                    ),
                    'status'                    => $this->_buildElements('status',
                        array(
                            'column'          => '4',
                            'type'            => 'select',
                            'placeholder'     => 'Status *',
                            'required'        => 1,
                            'clearfix_before' => 1,
                            //custom attributes
                            'attributes'      => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'         => array(
                                array(
                                    'label' => 'Status *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Single',
                                    'value' => 'Single',
                                ),
                                array(
                                    'label' => 'Married',
                                    'value' => 'Married',
                                ),
                                array(
                                    'label' => 'Separated',
                                    'value' => 'Separated',
                                ),
                                array(
                                    'label' => 'Divorced',
                                    'value' => 'Divorced',
                                ),
                                array(
                                    'label' => 'Widowed',
                                    'value' => 'Widowed',
                                ),
                            ),
                        )
                    ),
                    'nationality'               => $this->_buildElements('nationality',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => Mage::helper('builder')->getStaticNationalitiesOptions(),
                        )
                    ),
                    'gender'                    => $this->_buildElements('gender',
                        array(
                            'column'         => '4',
                            'type'           => 'select',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            'placeholder'    => 'Gender *',
                            //custom attributes
                            'attributes'     => array(
                                'data-bv-excluded' => 'false',
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Gender *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Female',
                                    'value' => 'Female',
                                ),
                                array(
                                    'label' => 'Male',
                                    'value' => 'Male',
                                ),
                            ),
                        )
                    ),
                    'birth_date'                => $this->_buildElements('birth_date',
                        array(
                            'column'      => '4',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Birthdate *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'place_of_birth'            => $this->_buildElements('place_of_birth',
                        array(
                            'column'         => '4',
                            'type'           => 'text',
                            'placeholder'    => 'Place of Birth',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^(?=.*?[A-Za-z])[a-zA-Z0-9 _-]+$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'occupation'                => $this->_buildElements('occupation',
                        array(
                            'column'      => '4',
                            'type'        => 'select',
                            'required'    => 1,
                            'placeholder' => 'occupation *',
                            //custom attributes
                            'attributes'  => array(
                                'data-bv-excluded' => 'false',
                            ),
                            'options'     => array(
                                array(
                                    'label' => 'Occupation *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'ACCOUNTANT',
                                    'value' => 'ACCOUNTANT',
                                ),
                                array(
                                    'label' => 'ACTOR',
                                    'value' => 'ACTOR',
                                ),
                                array(
                                    'label' => 'ACTUARY',
                                    'value' => 'ACTUARY',
                                ),
                                array(
                                    'label' => 'ADJUSTER',
                                    'value' => 'ADJUSTER',
                                ),
                                array(
                                    'label' => 'ADMINISTRATOR',
                                    'value' => 'ADMINISTRATOR',
                                ),
                                array(
                                    'label' => 'ADVISER',
                                    'value' => 'ADVISER',
                                ),
                                array(
                                    'label' => 'AGENT',
                                    'value' => 'AGENT',
                                ),
                                array(
                                    'label' => 'AGRICULTURIST',
                                    'value' => 'AGRICULTURIST',
                                ),
                                array(
                                    'label' => 'AIDE',
                                    'value' => 'AIDE',
                                ),
                                array(
                                    'label' => 'ANALYST',
                                    'value' => 'ANALYST',
                                ),
                                array(
                                    'label' => 'ANNOUNCER',
                                    'value' => 'ANNOUNCER',
                                ),
                                array(
                                    'label' => 'APPRAISER',
                                    'value' => 'APPRAISER',
                                ),
                                array(
                                    'label' => 'APPRENTICE',
                                    'value' => 'APPRENTICE',
                                ),
                                array(
                                    'label' => 'ARCHITECT',
                                    'value' => 'ARCHITECT',
                                ),
                                array(
                                    'label' => 'ARTIST',
                                    'value' => 'ARTIST',
                                ),
                                array(
                                    'label' => 'ASSISTANT',
                                    'value' => 'ASSISTANT',
                                ),
                                array(
                                    'label' => 'ASSOCIATE',
                                    'value' => 'ASSOCIATE',
                                ),
                                array(
                                    'label' => 'ASSOCIATE JUSTICE',
                                    'value' => 'ASSOCIATE JUSTICE',
                                ),
                                array(
                                    'label' => 'ASST. VICE PRESIDENT',
                                    'value' => 'ASST. VICE PRESIDENT',
                                ),
                                array(
                                    'label' => 'ATHLETE',
                                    'value' => 'ATHLETE',
                                ),
                                array(
                                    'label' => 'ATTENDANT',
                                    'value' => 'ATTENDANT',
                                ),
                                array(
                                    'label' => 'AUDITOR',
                                    'value' => 'AUDITOR',
                                ),
                                array(
                                    'label' => 'AUXILLARIES TENDER',
                                    'value' => 'AUXILLARIES TENDER',
                                ),
                                array(
                                    'label' => 'AVIATOR',
                                    'value' => 'AVIATOR',
                                ),
                                array(
                                    'label' => 'BABY SITTER',
                                    'value' => 'BABY SITTER',
                                ),
                                array(
                                    'label' => 'BACK MARKER',
                                    'value' => 'BACK MARKER',
                                ),
                                array(
                                    'label' => 'BAKER',
                                    'value' => 'BAKER',
                                ),
                                array(
                                    'label' => 'BANKER',
                                    'value' => 'BANKER',
                                ),
                                array(
                                    'label' => 'BARANGAY TANOD',
                                    'value' => 'BARANGAY TANOD',
                                ),
                                array(
                                    'label' => 'BARBER',
                                    'value' => 'BARBER',
                                ),
                                array(
                                    'label' => 'BARTENDER',
                                    'value' => 'BARTENDER',
                                ),
                                array(
                                    'label' => 'BENDER',
                                    'value' => 'BENDER',
                                ),
                                array(
                                    'label' => 'BOARD DIRECTOR',
                                    'value' => 'BOARD DIRECTOR',
                                ),
                                array(
                                    'label' => 'BOARD MEMBER',
                                    'value' => 'BOARD MEMBER',
                                ),
                                array(
                                    'label' => 'BOARD OF TRUSTEE',
                                    'value' => 'BOARD OF TRUSTEE',
                                ),
                                array(
                                    'label' => 'BOOKKEEPER',
                                    'value' => 'BOOKKEEPER',
                                ),
                                array(
                                    'label' => 'BUILDER',
                                    'value' => 'BUILDER',
                                ),
                                array(
                                    'label' => 'CONDUCTOR',
                                    'value' => 'CONDUCTOR',
                                ),
                                array(
                                    'label' => 'BUSINESSMAN',
                                    'value' => 'BUSINESSMAN',
                                ),
                                array(
                                    'label' => 'BUYER',
                                    'value' => 'BUYER',
                                ),
                                array(
                                    'label' => 'CADDY MASTER',
                                    'value' => 'CADDY MASTER',
                                ),
                                array(
                                    'label' => 'CADET',
                                    'value' => 'CADET',
                                ),
                                array(
                                    'label' => 'CAMERAMAN',
                                    'value' => 'CAMERAMAN',
                                ),
                                array(
                                    'label' => 'CANVASSER',
                                    'value' => 'CANVASSER',
                                ),
                                array(
                                    'label' => 'CAPTAIN',
                                    'value' => 'CAPTAIN',
                                ),
                                array(
                                    'label' => 'CAR GROOMER',
                                    'value' => 'CAR GROOMER',
                                ),
                                array(
                                    'label' => 'CAREGIVER',
                                    'value' => 'CAREGIVER',
                                ),
                                array(
                                    'label' => 'CARETAKER',
                                    'value' => 'CARETAKER',
                                ),
                                array(
                                    'label' => 'CARPENTER',
                                    'value' => 'CARPENTER',
                                ),
                                array(
                                    'label' => 'CASHIER',
                                    'value' => 'CASHIER',
                                ),
                                array(
                                    'label' => 'CEO',
                                    'value' => 'CEO',
                                ),
                                array(
                                    'label' => 'CHAIRMAN',
                                    'value' => 'CHAIRMAN',
                                ),
                                array(
                                    'label' => 'CHAIRMAN OF THE BOARD',
                                    'value' => 'CHAIRMAN OF THE BOARD',
                                ),
                                array(
                                    'label' => 'CHAIRPERSON',
                                    'value' => 'CHAIRPERSON',
                                ),
                                array(
                                    'label' => 'CHAUFFEUR',
                                    'value' => 'CHAUFFEUR',
                                ),
                                array(
                                    'label' => 'CHECKER',
                                    'value' => 'CHECKER',
                                ),
                                array(
                                    'label' => 'CHEF',
                                    'value' => 'CHEF',
                                ),
                                array(
                                    'label' => 'CHEMIST',
                                    'value' => 'CHEMIST',
                                ),
                                array(
                                    'label' => 'CHIEF',
                                    'value' => 'CHIEF',
                                ),
                                array(
                                    'label' => 'CHOREOGRAPHER',
                                    'value' => 'CHOREOGRAPHER',
                                ),
                                array(
                                    'label' => 'CINEMATOGRAPHER',
                                    'value' => 'CINEMATOGRAPHER',
                                ),
                                array(
                                    'label' => 'CLEANER',
                                    'value' => 'CLEANER',
                                ),
                                array(
                                    'label' => 'CLERGYMAN',
                                    'value' => 'CLERGYMAN',
                                ),
                                array(
                                    'label' => 'CLERK',
                                    'value' => 'CLERK',
                                ),
                                array(
                                    'label' => 'COACH',
                                    'value' => 'COACH',
                                ),
                                array(
                                    'label' => 'COLLECTOR',
                                    'value' => 'COLLECTOR',
                                ),
                                array(
                                    'label' => 'COMMISSIONER',
                                    'value' => 'COMMISSIONER',
                                ),
                                array(
                                    'label' => 'CONSTITUENTS',
                                    'value' => 'CONSTITUENTS',
                                ),
                                array(
                                    'label' => 'CONSTRUCTION WORKER',
                                    'value' => 'CONSTRUCTION WORKER',
                                ),
                                array(
                                    'label' => 'CONSULTANT',
                                    'value' => 'CONSULTANT',
                                ),
                                array(
                                    'label' => 'CONTRACTOR',
                                    'value' => 'CONTRACTOR',
                                ),
                                array(
                                    'label' => 'CONTROLLER',
                                    'value' => 'CONTROLLER',
                                ),
                                array(
                                    'label' => 'COORDINATOR',
                                    'value' => 'COORDINATOR',
                                ),
                                array(
                                    'label' => 'COUNCILOR',
                                    'value' => 'COUNCILOR',
                                ),
                                array(
                                    'label' => 'COUNSEL',
                                    'value' => 'COUNSEL',
                                ),
                                array(
                                    'label' => 'COUNSELOR',
                                    'value' => 'COUNSELOR',
                                ),
                                array(
                                    'label' => 'CREW',
                                    'value' => 'CREW',
                                ),
                                array(
                                    'label' => 'CUSTODIAN',
                                    'value' => 'CUSTODIAN',
                                ),
                                array(
                                    'label' => 'DANCER',
                                    'value' => 'DANCER',
                                ),
                                array(
                                    'label' => 'DEALER',
                                    'value' => 'DEALER',
                                ),
                                array(
                                    'label' => 'DELIVERY MAN',
                                    'value' => 'DELIVERY MAN',
                                ),
                                array(
                                    'label' => 'DENTIST',
                                    'value' => 'DENTIST',
                                ),
                                array(
                                    'label' => 'DESIGNER',
                                    'value' => 'DESIGNER',
                                ),
                                array(
                                    'label' => 'DEVELOPER',
                                    'value' => 'DEVELOPER',
                                ),
                                array(
                                    'label' => 'DIPLOMAT',
                                    'value' => 'DIPLOMAT',
                                ),
                                array(
                                    'label' => 'DIRECTOR',
                                    'value' => 'DIRECTOR',
                                ),
                                array(
                                    'label' => 'DISC JOCKEY',
                                    'value' => 'DISC JOCKEY',
                                ),
                                array(
                                    'label' => 'DISHWASHER',
                                    'value' => 'DISHWASHER',
                                ),
                                array(
                                    'label' => 'DISPATCHER',
                                    'value' => 'DISPATCHER',
                                ),
                                array(
                                    'label' => 'DISTRIBUTOR',
                                    'value' => 'DISTRIBUTOR',
                                ),
                                array(
                                    'label' => 'DOCTOR',
                                    'value' => 'DOCTOR',
                                ),
                                array(
                                    'label' => 'DRAFTSMAN',
                                    'value' => 'DRAFTSMAN',
                                ),
                                array(
                                    'label' => 'DRILLER',
                                    'value' => 'DRILLER',
                                ),
                                array(
                                    'label' => 'DRIVER',
                                    'value' => 'DRIVER',
                                ),
                                array(
                                    'label' => 'EDITOR',
                                    'value' => 'EDITOR',
                                ),
                                array(
                                    'label' => 'ELECTRICIAN',
                                    'value' => 'ELECTRICIAN',
                                ),
                                array(
                                    'label' => 'EMBALMER',
                                    'value' => 'EMBALMER',
                                ),
                                array(
                                    'label' => 'EMPLOYEE',
                                    'value' => 'EMPLOYEE',
                                ),
                                array(
                                    'label' => 'ENCODER',
                                    'value' => 'ENCODER',
                                ),
                                array(
                                    'label' => 'ENGINEER',
                                    'value' => 'ENGINEER',
                                ),
                                array(
                                    'label' => 'ENGRAVER',
                                    'value' => 'ENGRAVER',
                                ),
                                array(
                                    'label' => 'ENTERTAINER',
                                    'value' => 'ENTERTAINER',
                                ),
                                array(
                                    'label' => 'ENTREPRENUER',
                                    'value' => 'ENTREPRENUER',
                                ),
                                array(
                                    'label' => 'EXECUTIVE',
                                    'value' => 'EXECUTIVE',
                                ),
                                array(
                                    'label' => 'EXPATRIATE',
                                    'value' => 'EXPATRIATE',
                                ),
                                array(
                                    'label' => 'EXPEDITER',
                                    'value' => 'EXPEDITER',
                                ),
                                array(
                                    'label' => 'FABRICATOR',
                                    'value' => 'FABRICATOR',
                                ),
                                array(
                                    'label' => 'FARMER',
                                    'value' => 'FARMER',
                                ),
                                array(
                                    'label' => 'FINANCIAL ANALYST',
                                    'value' => 'FINANCIAL ANALYST',
                                ),
                                array(
                                    'label' => 'FOREIGNER',
                                    'value' => 'FOREIGNER',
                                ),
                                array(
                                    'label' => 'FOREMAN',
                                    'value' => 'FOREMAN',
                                ),
                                array(
                                    'label' => 'FORENSIC',
                                    'value' => 'FORENSIC',
                                ),
                                array(
                                    'label' => 'FORESTER',
                                    'value' => 'FORESTER',
                                ),
                                array(
                                    'label' => 'GARDENER',
                                    'value' => 'GARDENER',
                                ),
                                array(
                                    'label' => 'GASOLINE BOY',
                                    'value' => 'GASOLINE BOY',
                                ),
                                array(
                                    'label' => 'GAUFER',
                                    'value' => 'GAUFER',
                                ),
                                array(
                                    'label' => 'GEOLOGIST',
                                    'value' => 'GEOLOGIST',
                                ),
                                array(
                                    'label' => 'GRINDER',
                                    'value' => 'GRINDER',
                                ),
                                array(
                                    'label' => 'GROUNDSKEEPER',
                                    'value' => 'GROUNDSKEEPER',
                                ),
                                array(
                                    'label' => 'GUARD',
                                    'value' => 'GUARD',
                                ),
                                array(
                                    'label' => 'HAIR DRESSER',
                                    'value' => 'HAIR DRESSER',
                                ),
                                array(
                                    'label' => 'HELPER',
                                    'value' => 'HELPER',
                                ),
                                array(
                                    'label' => 'HOTELIER',
                                    'value' => 'HOTELIER',
                                ),
                                array(
                                    'label' => 'HOUSEKEEPER',
                                    'value' => 'HOUSEKEEPER',
                                ),
                                array(
                                    'label' => 'HOUSEWIFE',
                                    'value' => 'HOUSEWIFE',
                                ),
                                array(
                                    'label' => 'HR',
                                    'value' => 'HR',
                                ),
                                array(
                                    'label' => 'INSPECTOR',
                                    'value' => 'INSPECTOR',
                                ),
                                array(
                                    'label' => 'INSTALLER',
                                    'value' => 'INSTALLER',
                                ),
                                array(
                                    'label' => 'INSTRUCTOR',
                                    'value' => 'INSTRUCTOR',
                                ),
                                array(
                                    'label' => 'INVESTIGATOR',
                                    'value' => 'INVESTIGATOR',
                                ),
                                array(
                                    'label' => 'JANITOR',
                                    'value' => 'JANITOR',
                                ),
                                array(
                                    'label' => 'JEWELER',
                                    'value' => 'JEWELER',
                                ),
                                array(
                                    'label' => 'JOURNALIST',
                                    'value' => 'JOURNALIST',
                                ),
                                array(
                                    'label' => 'JUDGE',
                                    'value' => 'JUDGE',
                                ),
                                array(
                                    'label' => 'LABORER',
                                    'value' => 'LABORER',
                                ),
                                array(
                                    'label' => 'LAWYER',
                                    'value' => 'LAWYER',
                                ),
                                array(
                                    'label' => 'LEADMAN',
                                    'value' => 'LEADMAN',
                                ),
                                array(
                                    'label' => 'LESSOR',
                                    'value' => 'LESSOR',
                                ),
                                array(
                                    'label' => 'LIAISON',
                                    'value' => 'LIAISON',
                                ),
                                array(
                                    'label' => 'LIFEGUARD',
                                    'value' => 'LIFEGUARD',
                                ),
                                array(
                                    'label' => 'LINEMAN',
                                    'value' => 'LINEMAN',
                                ),
                                array(
                                    'label' => 'LIVESTOCKS RAISERS',
                                    'value' => 'LIVESTOCKS RAISERS',
                                ),
                                array(
                                    'label' => 'LUBEMAN',
                                    'value' => 'LUBEMAN',
                                ),
                                array(
                                    'label' => 'MACHINIST',
                                    'value' => 'MACHINIST',
                                ),
                                array(
                                    'label' => 'MAID',
                                    'value' => 'MAID',
                                ),
                                array(
                                    'label' => 'MAINTENANCE',
                                    'value' => 'MAINTENANCE',
                                ),
                                array(
                                    'label' => 'MANAGER',
                                    'value' => 'MANAGER',
                                ),
                                array(
                                    'label' => 'MANICURIST',
                                    'value' => 'MANICURIST',
                                ),
                                array(
                                    'label' => 'MARKETING',
                                    'value' => 'MARKETING',
                                ),
                                array(
                                    'label' => 'MARSHALL',
                                    'value' => 'MARSHALL',
                                ),
                                array(
                                    'label' => 'MASON',
                                    'value' => 'MASON',
                                ),
                                array(
                                    'label' => 'MASSEUSE',
                                    'value' => 'MASSEUSE',
                                ),
                                array(
                                    'label' => 'MAYOR',
                                    'value' => 'MAYOR',
                                ),
                                array(
                                    'label' => 'MECHANIC',
                                    'value' => 'MECHANIC',
                                ),
                                array(
                                    'label' => 'MEDICAL REPRESENTATIVE',
                                    'value' => 'MEDICAL REPRESENTATIVE',
                                ),
                                array(
                                    'label' => 'MEDICAL TECHNOLOGIST',
                                    'value' => 'MEDICAL TECHNOLOGIST',
                                ),
                                array(
                                    'label' => 'MEMBER',
                                    'value' => 'MEMBER',
                                ),
                                array(
                                    'label' => 'MERCHANDISING',
                                    'value' => 'MERCHANDISING',
                                ),
                                array(
                                    'label' => 'MERCHANT',
                                    'value' => 'MERCHANT',
                                ),
                                array(
                                    'label' => 'MESSENGER',
                                    'value' => 'MESSENGER',
                                ),
                                array(
                                    'label' => 'METAL FABRICATION',
                                    'value' => 'METAL FABRICATION',
                                ),
                                array(
                                    'label' => 'MIDWIFE',
                                    'value' => 'MIDWIFE',
                                ),
                                array(
                                    'label' => 'MILITARY',
                                    'value' => 'MILITARY',
                                ),
                                array(
                                    'label' => 'MINISTER',
                                    'value' => 'MINISTER',
                                ),
                                array(
                                    'label' => 'MISSIONARY',
                                    'value' => 'MISSIONARY',
                                ),
                                array(
                                    'label' => 'MUSICIAN',
                                    'value' => 'MUSICIAN',
                                ),
                                array(
                                    'label' => 'NURSE',
                                    'value' => 'NURSE',
                                ),
                                array(
                                    'label' => 'NUTRITIONIST',
                                    'value' => 'NUTRITIONIST',
                                ),
                                array(
                                    'label' => 'OFFICER',
                                    'value' => 'OFFICER',
                                ),
                                array(
                                    'label' => 'OILER',
                                    'value' => 'OILER',
                                ),
                                array(
                                    'label' => 'ON THE JOB TRAINING (OJT)',
                                    'value' => 'ON THE JOB TRAINING (OJT)',
                                ),
                                array(
                                    'label' => 'ONLINE ARTICLE WRITER',
                                    'value' => 'ONLINE ARTICLE WRITER',
                                ),
                                array(
                                    'label' => 'OPERATOR',
                                    'value' => 'OPERATOR',
                                ),
                                array(
                                    'label' => 'OPTICIAN',
                                    'value' => 'OPTICIAN',
                                ),
                                array(
                                    'label' => 'OVERSEAS FILIPINO WORKER',
                                    'value' => 'OVERSEAS FILIPINO WORKER',
                                ),
                                array(
                                    'label' => 'OWNER',
                                    'value' => 'OWNER',
                                ),
                                array(
                                    'label' => 'PACKER',
                                    'value' => 'PACKER',
                                ),
                                array(
                                    'label' => 'PAINTER',
                                    'value' => 'PAINTER',
                                ),
                                array(
                                    'label' => 'PASTOR',
                                    'value' => 'PASTOR',
                                ),
                                array(
                                    'label' => 'PERSONNEL',
                                    'value' => 'PERSONNEL',
                                ),
                                array(
                                    'label' => 'PHARMACIST',
                                    'value' => 'PHARMACIST',
                                ),
                                array(
                                    'label' => 'PHOTOGRAPHER',
                                    'value' => 'PHOTOGRAPHER',
                                ),
                                array(
                                    'label' => 'PILOT',
                                    'value' => 'PILOT',
                                ),
                                array(
                                    'label' => 'PLUMBER',
                                    'value' => 'PLUMBER',
                                ),
                                array(
                                    'label' => 'POLISHER',
                                    'value' => 'POLISHER',
                                ),
                                array(
                                    'label' => 'PORTER',
                                    'value' => 'PORTER',
                                ),
                                array(
                                    'label' => 'PRESIDENT',
                                    'value' => 'PRESIDENT',
                                ),
                                array(
                                    'label' => 'PRIEST',
                                    'value' => 'PRIEST',
                                ),
                                array(
                                    'label' => 'PRODUCER',
                                    'value' => 'PRODUCER',
                                ),
                                array(
                                    'label' => 'PROFESSOR',
                                    'value' => 'PROFESSOR',
                                ),
                                array(
                                    'label' => 'PROGRAMMER',
                                    'value' => 'PROGRAMMER',
                                ),
                                array(
                                    'label' => 'PROPRIETOR',
                                    'value' => 'PROPRIETOR',
                                ),
                                array(
                                    'label' => 'PURCHASER',
                                    'value' => 'PURCHASER',
                                ),
                                array(
                                    'label' => 'REAL ESTATE BROKER',
                                    'value' => 'REAL ESTATE BROKER',
                                ),
                                array(
                                    'label' => 'REALTOR',
                                    'value' => 'REALTOR',
                                ),
                                array(
                                    'label' => 'RECEPTIONIST',
                                    'value' => 'RECEPTIONIST',
                                ),
                                array(
                                    'label' => 'REPORTER',
                                    'value' => 'REPORTER',
                                ),
                                array(
                                    'label' => 'RESEARCHER',
                                    'value' => 'RESEARCHER',
                                ),
                                array(
                                    'label' => 'RETIRED',
                                    'value' => 'RETIRED',
                                ),
                                array(
                                    'label' => 'REWINDER',
                                    'value' => 'REWINDER',
                                ),
                                array(
                                    'label' => 'RIDER',
                                    'value' => 'RIDER',
                                ),
                                array(
                                    'label' => 'RIGGER',
                                    'value' => 'RIGGER',
                                ),
                                array(
                                    'label' => 'SALES',
                                    'value' => 'SALES',
                                ),
                                array(
                                    'label' => 'SALESMAN',
                                    'value' => 'SALESMAN',
                                ),
                                array(
                                    'label' => 'SCAFFOLDER',
                                    'value' => 'SCAFFOLDER',
                                ),
                                array(
                                    'label' => 'SEAFARER',
                                    'value' => 'SEAFARER',
                                ),
                                array(
                                    'label' => 'SEAMAN',
                                    'value' => 'SEAMAN',
                                ),
                                array(
                                    'label' => 'SECRETARY',
                                    'value' => 'SECRETARY',
                                ),
                                array(
                                    'label' => 'SELF-EMPLOYED',
                                    'value' => 'SELF-EMPLOYED',
                                ),
                                array(
                                    'label' => 'SELLER',
                                    'value' => 'SELLER',
                                ),
                                array(
                                    'label' => 'SEMI-RETIRED BUSINESSMAN',
                                    'value' => 'SEMI-RETIRED BUSINESSMAN',
                                ),
                                array(
                                    'label' => 'SENIOR CITIZEN',
                                    'value' => 'SENIOR CITIZEN',
                                ),
                                array(
                                    'label' => 'SERVICER',
                                    'value' => 'SERVICER',
                                ),
                                array(
                                    'label' => 'SEWER',
                                    'value' => 'SEWER',
                                ),
                                array(
                                    'label' => 'SHEET HELPER',
                                    'value' => 'SHEET HELPER',
                                ),
                                array(
                                    'label' => 'SPECIALIST',
                                    'value' => 'SPECIALIST',
                                ),
                                array(
                                    'label' => 'STAFF',
                                    'value' => 'STAFF',
                                ),
                                array(
                                    'label' => 'STEELMAN',
                                    'value' => 'STEELMAN',
                                ),
                                array(
                                    'label' => 'STENOGRAPHER',
                                    'value' => 'STENOGRAPHER',
                                ),
                                array(
                                    'label' => 'STEWARD',
                                    'value' => 'STEWARD',
                                ),
                                array(
                                    'label' => 'STOCKMAN',
                                    'value' => 'STOCKMAN',
                                ),
                                array(
                                    'label' => 'STORE KEEPER',
                                    'value' => 'STORE KEEPER',
                                ),
                                array(
                                    'label' => 'STRATEGIST',
                                    'value' => 'STRATEGIST',
                                ),
                                array(
                                    'label' => 'STUDENT',
                                    'value' => 'STUDENT',
                                ),
                                array(
                                    'label' => 'SUPERVISOR',
                                    'value' => 'SUPERVISOR',
                                ),
                                array(
                                    'label' => 'SURVEYOR',
                                    'value' => 'SURVEYOR',
                                ),
                                array(
                                    'label' => 'SWEEPER',
                                    'value' => 'SWEEPER',
                                ),
                                array(
                                    'label' => 'TAILOR',
                                    'value' => 'TAILOR',
                                ),
                                array(
                                    'label' => 'TEACHER',
                                    'value' => 'TEACHER',
                                ),
                                array(
                                    'label' => 'TECHNICIAN',
                                    'value' => 'TECHNICIAN',
                                ),
                                array(
                                    'label' => 'TELLER',
                                    'value' => 'TELLER',
                                ),
                                array(
                                    'label' => 'THERAPIST',
                                    'value' => 'THERAPIST',
                                ),
                                array(
                                    'label' => 'TIME KEEPER',
                                    'value' => 'TIME KEEPER',
                                ),
                                array(
                                    'label' => 'TINSMITH',
                                    'value' => 'TINSMITH',
                                ),
                                array(
                                    'label' => 'TIREMAN',
                                    'value' => 'TIREMAN',
                                ),
                                array(
                                    'label' => 'TRADER',
                                    'value' => 'TRADER',
                                ),
                                array(
                                    'label' => 'TRAINEE',
                                    'value' => 'TRAINEE',
                                ),
                                array(
                                    'label' => 'TRAINOR',
                                    'value' => 'TRAINOR',
                                ),
                                array(
                                    'label' => 'TRAVELER',
                                    'value' => 'TRAVELER',
                                ),
                                array(
                                    'label' => 'TREASURER',
                                    'value' => 'TREASURER',
                                ),
                                array(
                                    'label' => 'UNDERWRITER',
                                    'value' => 'UNDERWRITER',
                                ),
                                array(
                                    'label' => 'UNEMPLOYED',
                                    'value' => 'UNEMPLOYED',
                                ),
                                array(
                                    'label' => 'UPHOLSTERER',
                                    'value' => 'UPHOLSTERER',
                                ),
                                array(
                                    'label' => 'VENDOR',
                                    'value' => 'VENDOR',
                                ),
                                array(
                                    'label' => 'VETERINARIAN',
                                    'value' => 'VETERINARIAN',
                                ),
                                array(
                                    'label' => 'VICE PRESIDENT',
                                    'value' => 'VICE PRESIDENT',
                                ),
                                array(
                                    'label' => 'VOLUNTEER',
                                    'value' => 'VOLUNTEER',
                                ),
                                array(
                                    'label' => 'WAITER',
                                    'value' => 'WAITER',
                                ),
                                array(
                                    'label' => 'WASHER',
                                    'value' => 'WASHER',
                                ),
                                array(
                                    'label' => 'WATCHMAN',
                                    'value' => 'WATCHMAN',
                                ),
                                array(
                                    'label' => 'WELDER',
                                    'value' => 'WELDER',
                                ),
                                array(
                                    'label' => 'WORKER',
                                    'value' => 'WORKER',
                                ),
                                array(
                                    'label' => 'WRITER',
                                    'value' => 'WRITER',
                                ),
                                array(
                                    'label' => 'Others',
                                    'value' => 'Others',
                                ),
                            ),
                        )
                    ),
                    'tax_identification_number' => $this->_buildElements('tax_identification_number',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Tax Identification Number *',
                            'item_note'   => "Sample Format: 012-345-678-901",
                            'attributes'  => array(
                                'data-mask-config'       => '999-999-999-999',
                                'data-mask-placeholder'  => '',
                                'pattern'                => "([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})$",
                                'data-bv-regexp-message' => 'TIN Number is invalid',
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'false',
                            ),
                        )
                    ),
                    'employer'                  => $this->_buildElements('employer',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Employer *',
                            'attributes'  => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z. ]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::MAILING_ADDRESS)->addData(
            array(
                'elements' => array(
                    'province' => $this->_buildElements('province',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Province *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'city'     => $this->_buildElements('city',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'City *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'barangay' => $this->_buildElements('barangay',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Barangay *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'address'  => $this->_buildElements('address',
                        array(
                            'column'      => '12',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'House No. Street Name, Subdivision Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::SECONDARY_MAILING_ADDRESS)->addData(
            array(
                'elements' => array(
                    'province' => $this->_buildElements('province',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 0,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Province',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'city'     => $this->_buildElements('city',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 0,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'City',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'barangay' => $this->_buildElements('barangay',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 0,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Barangay',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'address'  => $this->_buildElements('address',
                        array(
                            'column'      => '12',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'House No. Street Name, Subdivision Name',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_INFORMATION)->addData(
            array(
                'elements' => array(
                    'previous_form_key' => $this->_buildElements('previous_form_key',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'Summary Form Key',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'group_name'        => $this->_buildElements('group_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Group Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_CONTACT_PERSON)->addData(
            array(
                'elements' => array(
                    'previous_form_key'         => $this->_buildElements('previous_form_key',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'Summary Form Key',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'salutation'                => $this->_buildElements('salutation',
                        array(
                            'column'      => '4',
                            'type'        => 'select',
                            'required'    => 1,
                            'placeholder' => 'Salutation *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'     => array(
                                array(
                                    'label' => 'Salutation *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Amb.',
                                    'value' => 'Amb.',
                                ),
                                array(
                                    'label' => 'Atty.',
                                    'value' => 'Atty.',
                                ),
                                array(
                                    'label' => 'Dr.',
                                    'value' => 'Dr.',
                                ),
                                array(
                                    'label' => 'Engr.',
                                    'value' => 'Engr.',
                                ),
                                array(
                                    'label' => 'Mr.',
                                    'value' => 'Mr.',
                                ),
                                array(
                                    'label' => 'Mrs.',
                                    'value' => 'Mrs.',
                                ),
                                array(
                                    'label' => 'Ms.',
                                    'value' => 'Ms.',
                                ),
                                array(
                                    'label' => 'Sps.',
                                    'value' => 'Sps.',
                                ),
                            ),
                        )
                    ),
                    'last_name'                 => $this->_buildElements('last_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'first_name'                => $this->_buildElements('first_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'First Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'middle_initial'            => $this->_buildElements('middle_initial',
                        array(
                            'column'          => '4',
                            'type'            => 'text',
                            'required'        => 0,
                            'placeholder'     => 'Middle Initial',
                            'clearfix_before' => 1,
                            'attributes'      => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z]$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'suffix'                    => $this->_buildElements('suffix',
                        array(
                            'column'         => '4',
                            'type'           => 'select',
                            'required'       => 0,
                            'placeholder'    => 'Suffix',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[A-Za-z_.-]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Suffix',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Engr.',
                                    'value' => 'Engr.',
                                ),
                                array(
                                    'label' => 'II',
                                    'value' => 'II',
                                ),
                                array(
                                    'label' => 'III',
                                    'value' => 'III',
                                ),
                                array(
                                    'label' => 'IV',
                                    'value' => 'IV',
                                ),
                                array(
                                    'label' => 'Sr.',
                                    'value' => 'Sr.',
                                ),
                                array(
                                    'label' => 'None',
                                    'value' => 'None',
                                ),
                            ),
                        )
                    ),
                    'status'                    => $this->_buildElements('status',
                        array(
                            'column'          => '4',
                            'type'            => 'select',
                            'placeholder'     => 'Status *',
                            'required'        => 1,
                            'clearfix_before' => 1,
                            //custom attributes
                            'attributes'      => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'         => array(
                                array(
                                    'label' => 'Status *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Single',
                                    'value' => 'Single',
                                ),
                                array(
                                    'label' => 'Married',
                                    'value' => 'Married',
                                ),
                                array(
                                    'label' => 'Separated',
                                    'value' => 'Separated',
                                ),
                                array(
                                    'label' => 'Divorced',
                                    'value' => 'Divorced',
                                ),
                                array(
                                    'label' => 'Widowed',
                                    'value' => 'Widowed',
                                ),
                            ),
                        )
                    ),
                    'nationality'               => $this->_buildElements('nationality',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => Mage::helper('builder')->getStaticNationalitiesOptions(),
                        )
                    ),
                    'gender'                    => $this->_buildElements('gender',
                        array(
                            'column'         => '4',
                            'type'           => 'select',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            'placeholder'    => 'Gender *',
                            //custom attributes
                            'attributes'     => array(
                                'data-bv-excluded' => 'false',
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Gender *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Female',
                                    'value' => 'Female',
                                ),
                                array(
                                    'label' => 'Male',
                                    'value' => 'Male',
                                ),
                            ),
                        )
                    ),
                    'birth_date'                => $this->_buildElements('birth_date',
                        array(
                            'column'      => '4',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Birthdate *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'place_of_birth'            => $this->_buildElements('place_of_birth',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'placeholder' => 'Place of Birth',
                            'attributes'  => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^(?=.*?[A-Za-z])[a-zA-Z0-9 _-]+$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'occupation'                => $this->_buildElements('occupation',
                        array(
                            'column'         => '4',
                            'type'           => 'select',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            'placeholder'    => 'occupation *',
                            //custom attributes
                            'attributes'     => array(
                                'data-bv-excluded' => 'false',
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Occupation *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'ACCOUNTANT',
                                    'value' => 'ACCOUNTANT',
                                ),
                                array(
                                    'label' => 'ACTOR',
                                    'value' => 'ACTOR',
                                ),
                                array(
                                    'label' => 'ACTUARY',
                                    'value' => 'ACTUARY',
                                ),
                                array(
                                    'label' => 'ADJUSTER',
                                    'value' => 'ADJUSTER',
                                ),
                                array(
                                    'label' => 'ADMINISTRATOR',
                                    'value' => 'ADMINISTRATOR',
                                ),
                                array(
                                    'label' => 'ADVISER',
                                    'value' => 'ADVISER',
                                ),
                                array(
                                    'label' => 'AGENT',
                                    'value' => 'AGENT',
                                ),
                                array(
                                    'label' => 'AGRICULTURIST',
                                    'value' => 'AGRICULTURIST',
                                ),
                                array(
                                    'label' => 'AIDE',
                                    'value' => 'AIDE',
                                ),
                                array(
                                    'label' => 'ANALYST',
                                    'value' => 'ANALYST',
                                ),
                                array(
                                    'label' => 'ANNOUNCER',
                                    'value' => 'ANNOUNCER',
                                ),
                                array(
                                    'label' => 'APPRAISER',
                                    'value' => 'APPRAISER',
                                ),
                                array(
                                    'label' => 'APPRENTICE',
                                    'value' => 'APPRENTICE',
                                ),
                                array(
                                    'label' => 'ARCHITECT',
                                    'value' => 'ARCHITECT',
                                ),
                                array(
                                    'label' => 'ARTIST',
                                    'value' => 'ARTIST',
                                ),
                                array(
                                    'label' => 'ASSISTANT',
                                    'value' => 'ASSISTANT',
                                ),
                                array(
                                    'label' => 'ASSOCIATE',
                                    'value' => 'ASSOCIATE',
                                ),
                                array(
                                    'label' => 'ASSOCIATE JUSTICE',
                                    'value' => 'ASSOCIATE JUSTICE',
                                ),
                                array(
                                    'label' => 'ASST. VICE PRESIDENT',
                                    'value' => 'ASST. VICE PRESIDENT',
                                ),
                                array(
                                    'label' => 'ATHLETE',
                                    'value' => 'ATHLETE',
                                ),
                                array(
                                    'label' => 'ATTENDANT',
                                    'value' => 'ATTENDANT',
                                ),
                                array(
                                    'label' => 'AUDITOR',
                                    'value' => 'AUDITOR',
                                ),
                                array(
                                    'label' => 'AUXILLARIES TENDER',
                                    'value' => 'AUXILLARIES TENDER',
                                ),
                                array(
                                    'label' => 'AVIATOR',
                                    'value' => 'AVIATOR',
                                ),
                                array(
                                    'label' => 'BABY SITTER',
                                    'value' => 'BABY SITTER',
                                ),
                                array(
                                    'label' => 'BACK MARKER',
                                    'value' => 'BACK MARKER',
                                ),
                                array(
                                    'label' => 'BAKER',
                                    'value' => 'BAKER',
                                ),
                                array(
                                    'label' => 'BANKER',
                                    'value' => 'BANKER',
                                ),
                                array(
                                    'label' => 'BARANGAY TANOD',
                                    'value' => 'BARANGAY TANOD',
                                ),
                                array(
                                    'label' => 'BARBER',
                                    'value' => 'BARBER',
                                ),
                                array(
                                    'label' => 'BARTENDER',
                                    'value' => 'BARTENDER',
                                ),
                                array(
                                    'label' => 'BENDER',
                                    'value' => 'BENDER',
                                ),
                                array(
                                    'label' => 'BOARD DIRECTOR',
                                    'value' => 'BOARD DIRECTOR',
                                ),
                                array(
                                    'label' => 'BOARD MEMBER',
                                    'value' => 'BOARD MEMBER',
                                ),
                                array(
                                    'label' => 'BOARD OF TRUSTEE',
                                    'value' => 'BOARD OF TRUSTEE',
                                ),
                                array(
                                    'label' => 'BOOKKEEPER',
                                    'value' => 'BOOKKEEPER',
                                ),
                                array(
                                    'label' => 'BUILDER',
                                    'value' => 'BUILDER',
                                ),
                                array(
                                    'label' => 'CONDUCTOR',
                                    'value' => 'CONDUCTOR',
                                ),
                                array(
                                    'label' => 'BUSINESSMAN',
                                    'value' => 'BUSINESSMAN',
                                ),
                                array(
                                    'label' => 'BUYER',
                                    'value' => 'BUYER',
                                ),
                                array(
                                    'label' => 'CADDY MASTER',
                                    'value' => 'CADDY MASTER',
                                ),
                                array(
                                    'label' => 'CADET',
                                    'value' => 'CADET',
                                ),
                                array(
                                    'label' => 'CAMERAMAN',
                                    'value' => 'CAMERAMAN',
                                ),
                                array(
                                    'label' => 'CANVASSER',
                                    'value' => 'CANVASSER',
                                ),
                                array(
                                    'label' => 'CAPTAIN',
                                    'value' => 'CAPTAIN',
                                ),
                                array(
                                    'label' => 'CAR GROOMER',
                                    'value' => 'CAR GROOMER',
                                ),
                                array(
                                    'label' => 'CAREGIVER',
                                    'value' => 'CAREGIVER',
                                ),
                                array(
                                    'label' => 'CARETAKER',
                                    'value' => 'CARETAKER',
                                ),
                                array(
                                    'label' => 'CARPENTER',
                                    'value' => 'CARPENTER',
                                ),
                                array(
                                    'label' => 'CASHIER',
                                    'value' => 'CASHIER',
                                ),
                                array(
                                    'label' => 'CEO',
                                    'value' => 'CEO',
                                ),
                                array(
                                    'label' => 'CHAIRMAN',
                                    'value' => 'CHAIRMAN',
                                ),
                                array(
                                    'label' => 'CHAIRMAN OF THE BOARD',
                                    'value' => 'CHAIRMAN OF THE BOARD',
                                ),
                                array(
                                    'label' => 'CHAIRPERSON',
                                    'value' => 'CHAIRPERSON',
                                ),
                                array(
                                    'label' => 'CHAUFFEUR',
                                    'value' => 'CHAUFFEUR',
                                ),
                                array(
                                    'label' => 'CHECKER',
                                    'value' => 'CHECKER',
                                ),
                                array(
                                    'label' => 'CHEF',
                                    'value' => 'CHEF',
                                ),
                                array(
                                    'label' => 'CHEMIST',
                                    'value' => 'CHEMIST',
                                ),
                                array(
                                    'label' => 'CHIEF',
                                    'value' => 'CHIEF',
                                ),
                                array(
                                    'label' => 'CHOREOGRAPHER',
                                    'value' => 'CHOREOGRAPHER',
                                ),
                                array(
                                    'label' => 'CINEMATOGRAPHER',
                                    'value' => 'CINEMATOGRAPHER',
                                ),
                                array(
                                    'label' => 'CLEANER',
                                    'value' => 'CLEANER',
                                ),
                                array(
                                    'label' => 'CLERGYMAN',
                                    'value' => 'CLERGYMAN',
                                ),
                                array(
                                    'label' => 'CLERK',
                                    'value' => 'CLERK',
                                ),
                                array(
                                    'label' => 'COACH',
                                    'value' => 'COACH',
                                ),
                                array(
                                    'label' => 'COLLECTOR',
                                    'value' => 'COLLECTOR',
                                ),
                                array(
                                    'label' => 'COMMISSIONER',
                                    'value' => 'COMMISSIONER',
                                ),
                                array(
                                    'label' => 'CONSTITUENTS',
                                    'value' => 'CONSTITUENTS',
                                ),
                                array(
                                    'label' => 'CONSTRUCTION WORKER',
                                    'value' => 'CONSTRUCTION WORKER',
                                ),
                                array(
                                    'label' => 'CONSULTANT',
                                    'value' => 'CONSULTANT',
                                ),
                                array(
                                    'label' => 'CONTRACTOR',
                                    'value' => 'CONTRACTOR',
                                ),
                                array(
                                    'label' => 'CONTROLLER',
                                    'value' => 'CONTROLLER',
                                ),
                                array(
                                    'label' => 'COORDINATOR',
                                    'value' => 'COORDINATOR',
                                ),
                                array(
                                    'label' => 'COUNCILOR',
                                    'value' => 'COUNCILOR',
                                ),
                                array(
                                    'label' => 'COUNSEL',
                                    'value' => 'COUNSEL',
                                ),
                                array(
                                    'label' => 'COUNSELOR',
                                    'value' => 'COUNSELOR',
                                ),
                                array(
                                    'label' => 'CREW',
                                    'value' => 'CREW',
                                ),
                                array(
                                    'label' => 'CUSTODIAN',
                                    'value' => 'CUSTODIAN',
                                ),
                                array(
                                    'label' => 'DANCER',
                                    'value' => 'DANCER',
                                ),
                                array(
                                    'label' => 'DEALER',
                                    'value' => 'DEALER',
                                ),
                                array(
                                    'label' => 'DELIVERY MAN',
                                    'value' => 'DELIVERY MAN',
                                ),
                                array(
                                    'label' => 'DENTIST',
                                    'value' => 'DENTIST',
                                ),
                                array(
                                    'label' => 'DESIGNER',
                                    'value' => 'DESIGNER',
                                ),
                                array(
                                    'label' => 'DEVELOPER',
                                    'value' => 'DEVELOPER',
                                ),
                                array(
                                    'label' => 'DIPLOMAT',
                                    'value' => 'DIPLOMAT',
                                ),
                                array(
                                    'label' => 'DIRECTOR',
                                    'value' => 'DIRECTOR',
                                ),
                                array(
                                    'label' => 'DISC JOCKEY',
                                    'value' => 'DISC JOCKEY',
                                ),
                                array(
                                    'label' => 'DISHWASHER',
                                    'value' => 'DISHWASHER',
                                ),
                                array(
                                    'label' => 'DISPATCHER',
                                    'value' => 'DISPATCHER',
                                ),
                                array(
                                    'label' => 'DISTRIBUTOR',
                                    'value' => 'DISTRIBUTOR',
                                ),
                                array(
                                    'label' => 'DOCTOR',
                                    'value' => 'DOCTOR',
                                ),
                                array(
                                    'label' => 'DRAFTSMAN',
                                    'value' => 'DRAFTSMAN',
                                ),
                                array(
                                    'label' => 'DRILLER',
                                    'value' => 'DRILLER',
                                ),
                                array(
                                    'label' => 'DRIVER',
                                    'value' => 'DRIVER',
                                ),
                                array(
                                    'label' => 'EDITOR',
                                    'value' => 'EDITOR',
                                ),
                                array(
                                    'label' => 'ELECTRICIAN',
                                    'value' => 'ELECTRICIAN',
                                ),
                                array(
                                    'label' => 'EMBALMER',
                                    'value' => 'EMBALMER',
                                ),
                                array(
                                    'label' => 'EMPLOYEE',
                                    'value' => 'EMPLOYEE',
                                ),
                                array(
                                    'label' => 'ENCODER',
                                    'value' => 'ENCODER',
                                ),
                                array(
                                    'label' => 'ENGINEER',
                                    'value' => 'ENGINEER',
                                ),
                                array(
                                    'label' => 'ENGRAVER',
                                    'value' => 'ENGRAVER',
                                ),
                                array(
                                    'label' => 'ENTERTAINER',
                                    'value' => 'ENTERTAINER',
                                ),
                                array(
                                    'label' => 'ENTREPRENUER',
                                    'value' => 'ENTREPRENUER',
                                ),
                                array(
                                    'label' => 'EXECUTIVE',
                                    'value' => 'EXECUTIVE',
                                ),
                                array(
                                    'label' => 'EXPATRIATE',
                                    'value' => 'EXPATRIATE',
                                ),
                                array(
                                    'label' => 'EXPEDITER',
                                    'value' => 'EXPEDITER',
                                ),
                                array(
                                    'label' => 'FABRICATOR',
                                    'value' => 'FABRICATOR',
                                ),
                                array(
                                    'label' => 'FARMER',
                                    'value' => 'FARMER',
                                ),
                                array(
                                    'label' => 'FINANCIAL ANALYST',
                                    'value' => 'FINANCIAL ANALYST',
                                ),
                                array(
                                    'label' => 'FOREIGNER',
                                    'value' => 'FOREIGNER',
                                ),
                                array(
                                    'label' => 'FOREMAN',
                                    'value' => 'FOREMAN',
                                ),
                                array(
                                    'label' => 'FORENSIC',
                                    'value' => 'FORENSIC',
                                ),
                                array(
                                    'label' => 'FORESTER',
                                    'value' => 'FORESTER',
                                ),
                                array(
                                    'label' => 'GARDENER',
                                    'value' => 'GARDENER',
                                ),
                                array(
                                    'label' => 'GASOLINE BOY',
                                    'value' => 'GASOLINE BOY',
                                ),
                                array(
                                    'label' => 'GAUFER',
                                    'value' => 'GAUFER',
                                ),
                                array(
                                    'label' => 'GEOLOGIST',
                                    'value' => 'GEOLOGIST',
                                ),
                                array(
                                    'label' => 'GRINDER',
                                    'value' => 'GRINDER',
                                ),
                                array(
                                    'label' => 'GROUNDSKEEPER',
                                    'value' => 'GROUNDSKEEPER',
                                ),
                                array(
                                    'label' => 'GUARD',
                                    'value' => 'GUARD',
                                ),
                                array(
                                    'label' => 'HAIR DRESSER',
                                    'value' => 'HAIR DRESSER',
                                ),
                                array(
                                    'label' => 'HELPER',
                                    'value' => 'HELPER',
                                ),
                                array(
                                    'label' => 'HOTELIER',
                                    'value' => 'HOTELIER',
                                ),
                                array(
                                    'label' => 'HOUSEKEEPER',
                                    'value' => 'HOUSEKEEPER',
                                ),
                                array(
                                    'label' => 'HOUSEWIFE',
                                    'value' => 'HOUSEWIFE',
                                ),
                                array(
                                    'label' => 'HR',
                                    'value' => 'HR',
                                ),
                                array(
                                    'label' => 'INSPECTOR',
                                    'value' => 'INSPECTOR',
                                ),
                                array(
                                    'label' => 'INSTALLER',
                                    'value' => 'INSTALLER',
                                ),
                                array(
                                    'label' => 'INSTRUCTOR',
                                    'value' => 'INSTRUCTOR',
                                ),
                                array(
                                    'label' => 'INVESTIGATOR',
                                    'value' => 'INVESTIGATOR',
                                ),
                                array(
                                    'label' => 'JANITOR',
                                    'value' => 'JANITOR',
                                ),
                                array(
                                    'label' => 'JEWELER',
                                    'value' => 'JEWELER',
                                ),
                                array(
                                    'label' => 'JOURNALIST',
                                    'value' => 'JOURNALIST',
                                ),
                                array(
                                    'label' => 'JUDGE',
                                    'value' => 'JUDGE',
                                ),
                                array(
                                    'label' => 'LABORER',
                                    'value' => 'LABORER',
                                ),
                                array(
                                    'label' => 'LAWYER',
                                    'value' => 'LAWYER',
                                ),
                                array(
                                    'label' => 'LEADMAN',
                                    'value' => 'LEADMAN',
                                ),
                                array(
                                    'label' => 'LESSOR',
                                    'value' => 'LESSOR',
                                ),
                                array(
                                    'label' => 'LIAISON',
                                    'value' => 'LIAISON',
                                ),
                                array(
                                    'label' => 'LIFEGUARD',
                                    'value' => 'LIFEGUARD',
                                ),
                                array(
                                    'label' => 'LINEMAN',
                                    'value' => 'LINEMAN',
                                ),
                                array(
                                    'label' => 'LIVESTOCKS RAISERS',
                                    'value' => 'LIVESTOCKS RAISERS',
                                ),
                                array(
                                    'label' => 'LUBEMAN',
                                    'value' => 'LUBEMAN',
                                ),
                                array(
                                    'label' => 'MACHINIST',
                                    'value' => 'MACHINIST',
                                ),
                                array(
                                    'label' => 'MAID',
                                    'value' => 'MAID',
                                ),
                                array(
                                    'label' => 'MAINTENANCE',
                                    'value' => 'MAINTENANCE',
                                ),
                                array(
                                    'label' => 'MANAGER',
                                    'value' => 'MANAGER',
                                ),
                                array(
                                    'label' => 'MANICURIST',
                                    'value' => 'MANICURIST',
                                ),
                                array(
                                    'label' => 'MARKETING',
                                    'value' => 'MARKETING',
                                ),
                                array(
                                    'label' => 'MARSHALL',
                                    'value' => 'MARSHALL',
                                ),
                                array(
                                    'label' => 'MASON',
                                    'value' => 'MASON',
                                ),
                                array(
                                    'label' => 'MASSEUSE',
                                    'value' => 'MASSEUSE',
                                ),
                                array(
                                    'label' => 'MAYOR',
                                    'value' => 'MAYOR',
                                ),
                                array(
                                    'label' => 'MECHANIC',
                                    'value' => 'MECHANIC',
                                ),
                                array(
                                    'label' => 'MEDICAL REPRESENTATIVE',
                                    'value' => 'MEDICAL REPRESENTATIVE',
                                ),
                                array(
                                    'label' => 'MEDICAL TECHNOLOGIST',
                                    'value' => 'MEDICAL TECHNOLOGIST',
                                ),
                                array(
                                    'label' => 'MEMBER',
                                    'value' => 'MEMBER',
                                ),
                                array(
                                    'label' => 'MERCHANDISING',
                                    'value' => 'MERCHANDISING',
                                ),
                                array(
                                    'label' => 'MERCHANT',
                                    'value' => 'MERCHANT',
                                ),
                                array(
                                    'label' => 'MESSENGER',
                                    'value' => 'MESSENGER',
                                ),
                                array(
                                    'label' => 'METAL FABRICATION',
                                    'value' => 'METAL FABRICATION',
                                ),
                                array(
                                    'label' => 'MIDWIFE',
                                    'value' => 'MIDWIFE',
                                ),
                                array(
                                    'label' => 'MILITARY',
                                    'value' => 'MILITARY',
                                ),
                                array(
                                    'label' => 'MINISTER',
                                    'value' => 'MINISTER',
                                ),
                                array(
                                    'label' => 'MISSIONARY',
                                    'value' => 'MISSIONARY',
                                ),
                                array(
                                    'label' => 'MUSICIAN',
                                    'value' => 'MUSICIAN',
                                ),
                                array(
                                    'label' => 'NURSE',
                                    'value' => 'NURSE',
                                ),
                                array(
                                    'label' => 'NUTRITIONIST',
                                    'value' => 'NUTRITIONIST',
                                ),
                                array(
                                    'label' => 'OFFICER',
                                    'value' => 'OFFICER',
                                ),
                                array(
                                    'label' => 'OILER',
                                    'value' => 'OILER',
                                ),
                                array(
                                    'label' => 'ON THE JOB TRAINING (OJT)',
                                    'value' => 'ON THE JOB TRAINING (OJT)',
                                ),
                                array(
                                    'label' => 'ONLINE ARTICLE WRITER',
                                    'value' => 'ONLINE ARTICLE WRITER',
                                ),
                                array(
                                    'label' => 'OPERATOR',
                                    'value' => 'OPERATOR',
                                ),
                                array(
                                    'label' => 'OPTICIAN',
                                    'value' => 'OPTICIAN',
                                ),
                                array(
                                    'label' => 'OVERSEAS FILIPINO WORKER',
                                    'value' => 'OVERSEAS FILIPINO WORKER',
                                ),
                                array(
                                    'label' => 'OWNER',
                                    'value' => 'OWNER',
                                ),
                                array(
                                    'label' => 'PACKER',
                                    'value' => 'PACKER',
                                ),
                                array(
                                    'label' => 'PAINTER',
                                    'value' => 'PAINTER',
                                ),
                                array(
                                    'label' => 'PASTOR',
                                    'value' => 'PASTOR',
                                ),
                                array(
                                    'label' => 'PERSONNEL',
                                    'value' => 'PERSONNEL',
                                ),
                                array(
                                    'label' => 'PHARMACIST',
                                    'value' => 'PHARMACIST',
                                ),
                                array(
                                    'label' => 'PHOTOGRAPHER',
                                    'value' => 'PHOTOGRAPHER',
                                ),
                                array(
                                    'label' => 'PILOT',
                                    'value' => 'PILOT',
                                ),
                                array(
                                    'label' => 'PLUMBER',
                                    'value' => 'PLUMBER',
                                ),
                                array(
                                    'label' => 'POLISHER',
                                    'value' => 'POLISHER',
                                ),
                                array(
                                    'label' => 'PORTER',
                                    'value' => 'PORTER',
                                ),
                                array(
                                    'label' => 'PRESIDENT',
                                    'value' => 'PRESIDENT',
                                ),
                                array(
                                    'label' => 'PRIEST',
                                    'value' => 'PRIEST',
                                ),
                                array(
                                    'label' => 'PRODUCER',
                                    'value' => 'PRODUCER',
                                ),
                                array(
                                    'label' => 'PROFESSOR',
                                    'value' => 'PROFESSOR',
                                ),
                                array(
                                    'label' => 'PROGRAMMER',
                                    'value' => 'PROGRAMMER',
                                ),
                                array(
                                    'label' => 'PROPRIETOR',
                                    'value' => 'PROPRIETOR',
                                ),
                                array(
                                    'label' => 'PURCHASER',
                                    'value' => 'PURCHASER',
                                ),
                                array(
                                    'label' => 'REAL ESTATE BROKER',
                                    'value' => 'REAL ESTATE BROKER',
                                ),
                                array(
                                    'label' => 'REALTOR',
                                    'value' => 'REALTOR',
                                ),
                                array(
                                    'label' => 'RECEPTIONIST',
                                    'value' => 'RECEPTIONIST',
                                ),
                                array(
                                    'label' => 'REPORTER',
                                    'value' => 'REPORTER',
                                ),
                                array(
                                    'label' => 'RESEARCHER',
                                    'value' => 'RESEARCHER',
                                ),
                                array(
                                    'label' => 'RETIRED',
                                    'value' => 'RETIRED',
                                ),
                                array(
                                    'label' => 'REWINDER',
                                    'value' => 'REWINDER',
                                ),
                                array(
                                    'label' => 'RIDER',
                                    'value' => 'RIDER',
                                ),
                                array(
                                    'label' => 'RIGGER',
                                    'value' => 'RIGGER',
                                ),
                                array(
                                    'label' => 'SALES',
                                    'value' => 'SALES',
                                ),
                                array(
                                    'label' => 'SALESMAN',
                                    'value' => 'SALESMAN',
                                ),
                                array(
                                    'label' => 'SCAFFOLDER',
                                    'value' => 'SCAFFOLDER',
                                ),
                                array(
                                    'label' => 'SEAFARER',
                                    'value' => 'SEAFARER',
                                ),
                                array(
                                    'label' => 'SEAMAN',
                                    'value' => 'SEAMAN',
                                ),
                                array(
                                    'label' => 'SECRETARY',
                                    'value' => 'SECRETARY',
                                ),
                                array(
                                    'label' => 'SELF-EMPLOYED',
                                    'value' => 'SELF-EMPLOYED',
                                ),
                                array(
                                    'label' => 'SELLER',
                                    'value' => 'SELLER',
                                ),
                                array(
                                    'label' => 'SEMI-RETIRED BUSINESSMAN',
                                    'value' => 'SEMI-RETIRED BUSINESSMAN',
                                ),
                                array(
                                    'label' => 'SENIOR CITIZEN',
                                    'value' => 'SENIOR CITIZEN',
                                ),
                                array(
                                    'label' => 'SERVICER',
                                    'value' => 'SERVICER',
                                ),
                                array(
                                    'label' => 'SEWER',
                                    'value' => 'SEWER',
                                ),
                                array(
                                    'label' => 'SHEET HELPER',
                                    'value' => 'SHEET HELPER',
                                ),
                                array(
                                    'label' => 'SPECIALIST',
                                    'value' => 'SPECIALIST',
                                ),
                                array(
                                    'label' => 'STAFF',
                                    'value' => 'STAFF',
                                ),
                                array(
                                    'label' => 'STEELMAN',
                                    'value' => 'STEELMAN',
                                ),
                                array(
                                    'label' => 'STENOGRAPHER',
                                    'value' => 'STENOGRAPHER',
                                ),
                                array(
                                    'label' => 'STEWARD',
                                    'value' => 'STEWARD',
                                ),
                                array(
                                    'label' => 'STOCKMAN',
                                    'value' => 'STOCKMAN',
                                ),
                                array(
                                    'label' => 'STORE KEEPER',
                                    'value' => 'STORE KEEPER',
                                ),
                                array(
                                    'label' => 'STRATEGIST',
                                    'value' => 'STRATEGIST',
                                ),
                                array(
                                    'label' => 'STUDENT',
                                    'value' => 'STUDENT',
                                ),
                                array(
                                    'label' => 'SUPERVISOR',
                                    'value' => 'SUPERVISOR',
                                ),
                                array(
                                    'label' => 'SURVEYOR',
                                    'value' => 'SURVEYOR',
                                ),
                                array(
                                    'label' => 'SWEEPER',
                                    'value' => 'SWEEPER',
                                ),
                                array(
                                    'label' => 'TAILOR',
                                    'value' => 'TAILOR',
                                ),
                                array(
                                    'label' => 'TEACHER',
                                    'value' => 'TEACHER',
                                ),
                                array(
                                    'label' => 'TECHNICIAN',
                                    'value' => 'TECHNICIAN',
                                ),
                                array(
                                    'label' => 'TELLER',
                                    'value' => 'TELLER',
                                ),
                                array(
                                    'label' => 'THERAPIST',
                                    'value' => 'THERAPIST',
                                ),
                                array(
                                    'label' => 'TIME KEEPER',
                                    'value' => 'TIME KEEPER',
                                ),
                                array(
                                    'label' => 'TINSMITH',
                                    'value' => 'TINSMITH',
                                ),
                                array(
                                    'label' => 'TIREMAN',
                                    'value' => 'TIREMAN',
                                ),
                                array(
                                    'label' => 'TRADER',
                                    'value' => 'TRADER',
                                ),
                                array(
                                    'label' => 'TRAINEE',
                                    'value' => 'TRAINEE',
                                ),
                                array(
                                    'label' => 'TRAINOR',
                                    'value' => 'TRAINOR',
                                ),
                                array(
                                    'label' => 'TRAVELER',
                                    'value' => 'TRAVELER',
                                ),
                                array(
                                    'label' => 'TREASURER',
                                    'value' => 'TREASURER',
                                ),
                                array(
                                    'label' => 'UNDERWRITER',
                                    'value' => 'UNDERWRITER',
                                ),
                                array(
                                    'label' => 'UNEMPLOYED',
                                    'value' => 'UNEMPLOYED',
                                ),
                                array(
                                    'label' => 'UPHOLSTERER',
                                    'value' => 'UPHOLSTERER',
                                ),
                                array(
                                    'label' => 'VENDOR',
                                    'value' => 'VENDOR',
                                ),
                                array(
                                    'label' => 'VETERINARIAN',
                                    'value' => 'VETERINARIAN',
                                ),
                                array(
                                    'label' => 'VICE PRESIDENT',
                                    'value' => 'VICE PRESIDENT',
                                ),
                                array(
                                    'label' => 'VOLUNTEER',
                                    'value' => 'VOLUNTEER',
                                ),
                                array(
                                    'label' => 'WAITER',
                                    'value' => 'WAITER',
                                ),
                                array(
                                    'label' => 'WASHER',
                                    'value' => 'WASHER',
                                ),
                                array(
                                    'label' => 'WATCHMAN',
                                    'value' => 'WATCHMAN',
                                ),
                                array(
                                    'label' => 'WELDER',
                                    'value' => 'WELDER',
                                ),
                                array(
                                    'label' => 'WORKER',
                                    'value' => 'WORKER',
                                ),
                                array(
                                    'label' => 'WRITER',
                                    'value' => 'WRITER',
                                ),
                                array(
                                    'label' => 'Others',
                                    'value' => 'Others',
                                ),
                            ),
                        )
                    ),
                    'tax_identification_number' => $this->_buildElements('tax_identification_number',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Tax Identification Number *',
                            'item_note'   => "Sample Format: 012-345-678-901",
                            'attributes'  => array(
                                'data-mask-config'       => '999-999-999-999',
                                'data-mask-placeholder'  => '',
                                'pattern'                => "([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})$",
                                'data-bv-regexp-message' => 'TIN Number is invalid',
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'false',
                            ),
                        )
                    ),
                    'employer'                  => $this->_buildElements('employer',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Employer *',
                            'attributes'  => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z. ]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'designation'               => $this->_buildElements('designation',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'placeholder' => 'Designation',
                            'attributes'  => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z. ]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_CONTACT_PERSON_MAILING_ADDRESS)->addData(
            array(
                'elements' => array(
                    'province' => $this->_buildElements('province',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Province *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'city'     => $this->_buildElements('city',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'City *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'barangay' => $this->_buildElements('barangay',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Barangay *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'address'  => $this->_buildElements('address',
                        array(
                            'column'      => '12',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'House No. Street Name, Subdivision Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_CONTACT_PERSON_SECONDARY_MAILING_ADDRESS)->addData(
            array(
                'elements' => array(
                    'province' => $this->_buildElements('province',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 0,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Province',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'city'     => $this->_buildElements('city',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 0,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'City',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'barangay' => $this->_buildElements('barangay',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 0,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Barangay',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'address'  => $this->_buildElements('address',
                        array(
                            'column'      => '12',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'House No. Street Name, Subdivision Name',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_INFORMATION)->addData(
            array(
                'elements' => array(
                    'previous_form_key'         => $this->_buildElements('previous_form_key',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'Summary Form Key',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'company_name'              => $this->_buildElements('company_name',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Company Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'date_of_inc'               => $this->_buildElements('date_of_inc',
                        array(
                            'column'      => '6',
                            'type'        => 'date',
                            'required'    => 0,
                            'placeholder' => 'Date of Incorporation *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                            ),
                        )
                    ),
                    'accomodations'             => $this->_buildElements('accomodations',
                        array(
                            'column'      => '6',
                            'type'        => 'select',
                            'required'    => 0,
                            'placeholder' => 'Accomodations',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'     => array(
                                array(
                                    'label' => 'Accomodations',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Accounting',
                                    'value' => 'Accounting',
                                ),
                                array(
                                    'label' => 'Advertising',
                                    'value' => 'Advertising',
                                ),
                                array(
                                    'label' => 'Aerospace',
                                    'value' => 'Aerospace',
                                ),
                                array(
                                    'label' => 'Agriculture &amp; Agribusiness',
                                    'value' => 'Agriculture &amp; Agribusiness',
                                ),
                                array(
                                    'label' => 'Air Transportation',
                                    'value' => 'Air Transportation',
                                ),
                                array(
                                    'label' => 'Apparel &amp; Accessories',
                                    'value' => 'Apparel &amp; Accessories',
                                ),
                                array(
                                    'label' => 'Auto',
                                    'value' => 'Auto',
                                ),
                                array(
                                    'label' => 'Banking',
                                    'value' => 'Banking',
                                ),
                                array(
                                    'label' => 'Beauty &amp; Cosmetics',
                                    'value' => 'Beauty &amp; Cosmetics',
                                ),
                                array(
                                    'label' => 'Biotechnology',
                                    'value' => 'Biotechnology',
                                ),
                                array(
                                    'label' => 'Chemical',
                                    'value' => 'Chemical',
                                ),
                                array(
                                    'label' => 'Communications',
                                    'value' => 'Communications',
                                ),
                                array(
                                    'label' => 'Computer',
                                    'value' => 'Computer',
                                ),
                                array(
                                    'label' => 'Construction',
                                    'value' => 'Construction',
                                ),
                                array(
                                    'label' => 'Consulting',
                                    'value' => 'Consulting',
                                ),
                                array(
                                    'label' => 'Consumer Products',
                                    'value' => 'Consumer Products',
                                ),
                                array(
                                    'label' => 'Education',
                                    'value' => 'Education',
                                ),
                                array(
                                    'label' => 'Electronics',
                                    'value' => 'Electronics',
                                ),
                                array(
                                    'label' => 'Employment',
                                    'value' => 'Employment',
                                ),
                                array(
                                    'label' => 'Entertainment &amp; Recreation',
                                    'value' => 'Entertainment &amp; Recreation',
                                ),
                                array(
                                    'label' => 'Fashion',
                                    'value' => 'Fashion',
                                ),
                                array(
                                    'label' => 'Financial Services',
                                    'value' => 'Financial Services',
                                ),
                                array(
                                    'label' => 'Food &amp; Beverage',
                                    'value' => 'Food &amp; Beverage',
                                ),
                                array(
                                    'label' => 'Health',
                                    'value' => 'Health',
                                ),
                                array(
                                    'label' => 'Information',
                                    'value' => 'Information',
                                ),
                                array(
                                    'label' => 'Information Technology',
                                    'value' => 'Information Technology',
                                ),
                                array(
                                    'label' => 'Insurance',
                                    'value' => 'Insurance',
                                ),
                                array(
                                    'label' => 'Journalism &amp; News',
                                    'value' => 'Journalism &amp; News',
                                ),
                                array(
                                    'label' => 'Legal Services',
                                    'value' => 'Legal Services',
                                ),
                                array(
                                    'label' => 'Manufacturing',
                                    'value' => 'Manufacturing',
                                ),
                                array(
                                    'label' => 'Media &amp; Broadcasting',
                                    'value' => 'Media &amp; Broadcasting',
                                ),
                                array(
                                    'label' => 'Medical Devices &amp; Supplies',
                                    'value' => 'Medical Devices &amp; Supplies',
                                ),
                                array(
                                    'label' => 'Motion Pictures &amp; Video',
                                    'value' => 'Motion Pictures &amp; Video',
                                ),
                                array(
                                    'label' => 'Music',
                                    'value' => 'Music',
                                ),
                                array(
                                    'label' => 'Pharmaceutical',
                                    'value' => 'Pharmaceutical',
                                ),
                                array(
                                    'label' => 'Public Administration',
                                    'value' => 'Public Administration',
                                ),
                                array(
                                    'label' => 'Publishing',
                                    'value' => 'Publishing',
                                ),
                                array(
                                    'label' => 'Real Estate',
                                    'value' => 'Real Estate',
                                ),
                                array(
                                    'label' => 'Retail',
                                    'value' => 'Retail',
                                ),
                                array(
                                    'label' => 'Service',
                                    'value' => 'Service',
                                ),
                                array(
                                    'label' => 'Sports',
                                    'value' => 'Sports',
                                ),
                                array(
                                    'label' => 'Technology',
                                    'value' => 'Technology',
                                ),
                                array(
                                    'label' => 'Telecommunications',
                                    'value' => 'Telecommunications',
                                ),
                                array(
                                    'label' => 'Transportation',
                                    'value' => 'Transportation',
                                ),
                                array(
                                    'label' => 'Travel',
                                    'value' => 'Travel',
                                ),
                                array(
                                    'label' => 'Utilities',
                                    'value' => 'Utilities',
                                ),
                                array(
                                    'label' => 'Video Game',
                                    'value' => 'Video Game',
                                ),
                                array(
                                    'label' => 'Web Services',
                                    'value' => 'Web Services',
                                ),
                            ),
                        )
                    ),
                    'tax_identification_number' => $this->_buildElements('tax_identification_number',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Tax Identification Number *',
                            'item_note'   => "Sample Format: 012-345-678-901",
                            'attributes'  => array(
                                'data-mask-config'       => '999-999-999-999',
                                'data-mask-placeholder'  => '',
                                'pattern'                => "([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})$",
                                'data-bv-regexp-message' => 'TIN Number is invalid',
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'false',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_CONTACT_PERSON)->addData(
            array(
                'elements' => array(
                    'previous_form_key'         => $this->_buildElements('previous_form_key',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'Summary Form Key',
                            'attributes'  => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'salutation'                => $this->_buildElements('salutation',
                        array(
                            'column'      => '4',
                            'type'        => 'select',
                            'required'    => 1,
                            'placeholder' => 'Salutation *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'     => array(
                                array(
                                    'label' => 'Salutation *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Amb.',
                                    'value' => 'Amb.',
                                ),
                                array(
                                    'label' => 'Atty.',
                                    'value' => 'Atty.',
                                ),
                                array(
                                    'label' => 'Dr.',
                                    'value' => 'Dr.',
                                ),
                                array(
                                    'label' => 'Engr.',
                                    'value' => 'Engr.',
                                ),
                                array(
                                    'label' => 'Mr.',
                                    'value' => 'Mr.',
                                ),
                                array(
                                    'label' => 'Mrs.',
                                    'value' => 'Mrs.',
                                ),
                                array(
                                    'label' => 'Ms.',
                                    'value' => 'Ms.',
                                ),
                                array(
                                    'label' => 'Sps.',
                                    'value' => 'Sps.',
                                ),
                            ),
                        )
                    ),
                    'last_name'                 => $this->_buildElements('last_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'first_name'                => $this->_buildElements('first_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'First Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'middle_initial'            => $this->_buildElements('middle_initial',
                        array(
                            'column'          => '4',
                            'type'            => 'text',
                            'required'        => 0,
                            'placeholder'     => 'Middle Initial',
                            'clearfix_before' => 1,
                            'attributes'      => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z]$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'suffix'                    => $this->_buildElements('suffix',
                        array(
                            'column'         => '4',
                            'type'           => 'select',
                            'required'       => 0,
                            'placeholder'    => 'Suffix',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[A-Za-z_.-]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Suffix',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Engr.',
                                    'value' => 'Engr.',
                                ),
                                array(
                                    'label' => 'II',
                                    'value' => 'II',
                                ),
                                array(
                                    'label' => 'III',
                                    'value' => 'III',
                                ),
                                array(
                                    'label' => 'IV',
                                    'value' => 'IV',
                                ),
                                array(
                                    'label' => 'Sr.',
                                    'value' => 'Sr.',
                                ),
                                array(
                                    'label' => 'None',
                                    'value' => 'None',
                                ),
                            ),
                        )
                    ),
                    'status'                    => $this->_buildElements('status',
                        array(
                            'column'          => '4',
                            'type'            => 'select',
                            'placeholder'     => 'Status *',
                            'required'        => 1,
                            'clearfix_before' => 1,
                            //custom attributes
                            'attributes'      => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'         => array(
                                array(
                                    'label' => 'Status *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Single',
                                    'value' => 'Single',
                                ),
                                array(
                                    'label' => 'Married',
                                    'value' => 'Married',
                                ),
                                array(
                                    'label' => 'Separated',
                                    'value' => 'Separated',
                                ),
                                array(
                                    'label' => 'Divorced',
                                    'value' => 'Divorced',
                                ),
                                array(
                                    'label' => 'Widowed',
                                    'value' => 'Widowed',
                                ),
                            ),
                        )
                    ),
                    'nationality'               => $this->_buildElements('nationality',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => Mage::helper('builder')->getStaticNationalitiesOptions(),
                        )
                    ),
                    'gender'                    => $this->_buildElements('gender',
                        array(
                            'column'         => '4',
                            'type'           => 'select',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            'placeholder'    => 'Gender *',
                            //custom attributes
                            'attributes'     => array(
                                'data-bv-excluded' => 'false',
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Gender *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Female',
                                    'value' => 'Female',
                                ),
                                array(
                                    'label' => 'Male',
                                    'value' => 'Male',
                                ),
                            ),
                        )
                    ),
                    'birth_date'                => $this->_buildElements('birth_date',
                        array(
                            'column'      => '4',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Birthdate *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'place_of_birth'            => $this->_buildElements('place_of_birth',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'placeholder' => 'Place of Birth',
                            'attributes'  => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^(?=.*?[A-Za-z])[a-zA-Z0-9 _-]+$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'occupation'                => $this->_buildElements('occupation',
                        array(
                            'column'      => '4',
                            'type'        => 'select',
                            'required'    => 1,
                            'placeholder' => 'occupation *',
                            //custom attributes
                            'attributes'  => array(
                                'data-bv-excluded' => 'false',
                            ),
                            'options'     => array(
                                array(
                                    'label' => 'Occupation *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'ACCOUNTANT',
                                    'value' => 'ACCOUNTANT',
                                ),
                                array(
                                    'label' => 'ACTOR',
                                    'value' => 'ACTOR',
                                ),
                                array(
                                    'label' => 'ACTUARY',
                                    'value' => 'ACTUARY',
                                ),
                                array(
                                    'label' => 'ADJUSTER',
                                    'value' => 'ADJUSTER',
                                ),
                                array(
                                    'label' => 'ADMINISTRATOR',
                                    'value' => 'ADMINISTRATOR',
                                ),
                                array(
                                    'label' => 'ADVISER',
                                    'value' => 'ADVISER',
                                ),
                                array(
                                    'label' => 'AGENT',
                                    'value' => 'AGENT',
                                ),
                                array(
                                    'label' => 'AGRICULTURIST',
                                    'value' => 'AGRICULTURIST',
                                ),
                                array(
                                    'label' => 'AIDE',
                                    'value' => 'AIDE',
                                ),
                                array(
                                    'label' => 'ANALYST',
                                    'value' => 'ANALYST',
                                ),
                                array(
                                    'label' => 'ANNOUNCER',
                                    'value' => 'ANNOUNCER',
                                ),
                                array(
                                    'label' => 'APPRAISER',
                                    'value' => 'APPRAISER',
                                ),
                                array(
                                    'label' => 'APPRENTICE',
                                    'value' => 'APPRENTICE',
                                ),
                                array(
                                    'label' => 'ARCHITECT',
                                    'value' => 'ARCHITECT',
                                ),
                                array(
                                    'label' => 'ARTIST',
                                    'value' => 'ARTIST',
                                ),
                                array(
                                    'label' => 'ASSISTANT',
                                    'value' => 'ASSISTANT',
                                ),
                                array(
                                    'label' => 'ASSOCIATE',
                                    'value' => 'ASSOCIATE',
                                ),
                                array(
                                    'label' => 'ASSOCIATE JUSTICE',
                                    'value' => 'ASSOCIATE JUSTICE',
                                ),
                                array(
                                    'label' => 'ASST. VICE PRESIDENT',
                                    'value' => 'ASST. VICE PRESIDENT',
                                ),
                                array(
                                    'label' => 'ATHLETE',
                                    'value' => 'ATHLETE',
                                ),
                                array(
                                    'label' => 'ATTENDANT',
                                    'value' => 'ATTENDANT',
                                ),
                                array(
                                    'label' => 'AUDITOR',
                                    'value' => 'AUDITOR',
                                ),
                                array(
                                    'label' => 'AUXILLARIES TENDER',
                                    'value' => 'AUXILLARIES TENDER',
                                ),
                                array(
                                    'label' => 'AVIATOR',
                                    'value' => 'AVIATOR',
                                ),
                                array(
                                    'label' => 'BABY SITTER',
                                    'value' => 'BABY SITTER',
                                ),
                                array(
                                    'label' => 'BACK MARKER',
                                    'value' => 'BACK MARKER',
                                ),
                                array(
                                    'label' => 'BAKER',
                                    'value' => 'BAKER',
                                ),
                                array(
                                    'label' => 'BANKER',
                                    'value' => 'BANKER',
                                ),
                                array(
                                    'label' => 'BARANGAY TANOD',
                                    'value' => 'BARANGAY TANOD',
                                ),
                                array(
                                    'label' => 'BARBER',
                                    'value' => 'BARBER',
                                ),
                                array(
                                    'label' => 'BARTENDER',
                                    'value' => 'BARTENDER',
                                ),
                                array(
                                    'label' => 'BENDER',
                                    'value' => 'BENDER',
                                ),
                                array(
                                    'label' => 'BOARD DIRECTOR',
                                    'value' => 'BOARD DIRECTOR',
                                ),
                                array(
                                    'label' => 'BOARD MEMBER',
                                    'value' => 'BOARD MEMBER',
                                ),
                                array(
                                    'label' => 'BOARD OF TRUSTEE',
                                    'value' => 'BOARD OF TRUSTEE',
                                ),
                                array(
                                    'label' => 'BOOKKEEPER',
                                    'value' => 'BOOKKEEPER',
                                ),
                                array(
                                    'label' => 'BUILDER',
                                    'value' => 'BUILDER',
                                ),
                                array(
                                    'label' => 'CONDUCTOR',
                                    'value' => 'CONDUCTOR',
                                ),
                                array(
                                    'label' => 'BUSINESSMAN',
                                    'value' => 'BUSINESSMAN',
                                ),
                                array(
                                    'label' => 'BUYER',
                                    'value' => 'BUYER',
                                ),
                                array(
                                    'label' => 'CADDY MASTER',
                                    'value' => 'CADDY MASTER',
                                ),
                                array(
                                    'label' => 'CADET',
                                    'value' => 'CADET',
                                ),
                                array(
                                    'label' => 'CAMERAMAN',
                                    'value' => 'CAMERAMAN',
                                ),
                                array(
                                    'label' => 'CANVASSER',
                                    'value' => 'CANVASSER',
                                ),
                                array(
                                    'label' => 'CAPTAIN',
                                    'value' => 'CAPTAIN',
                                ),
                                array(
                                    'label' => 'CAR GROOMER',
                                    'value' => 'CAR GROOMER',
                                ),
                                array(
                                    'label' => 'CAREGIVER',
                                    'value' => 'CAREGIVER',
                                ),
                                array(
                                    'label' => 'CARETAKER',
                                    'value' => 'CARETAKER',
                                ),
                                array(
                                    'label' => 'CARPENTER',
                                    'value' => 'CARPENTER',
                                ),
                                array(
                                    'label' => 'CASHIER',
                                    'value' => 'CASHIER',
                                ),
                                array(
                                    'label' => 'CEO',
                                    'value' => 'CEO',
                                ),
                                array(
                                    'label' => 'CHAIRMAN',
                                    'value' => 'CHAIRMAN',
                                ),
                                array(
                                    'label' => 'CHAIRMAN OF THE BOARD',
                                    'value' => 'CHAIRMAN OF THE BOARD',
                                ),
                                array(
                                    'label' => 'CHAIRPERSON',
                                    'value' => 'CHAIRPERSON',
                                ),
                                array(
                                    'label' => 'CHAUFFEUR',
                                    'value' => 'CHAUFFEUR',
                                ),
                                array(
                                    'label' => 'CHECKER',
                                    'value' => 'CHECKER',
                                ),
                                array(
                                    'label' => 'CHEF',
                                    'value' => 'CHEF',
                                ),
                                array(
                                    'label' => 'CHEMIST',
                                    'value' => 'CHEMIST',
                                ),
                                array(
                                    'label' => 'CHIEF',
                                    'value' => 'CHIEF',
                                ),
                                array(
                                    'label' => 'CHOREOGRAPHER',
                                    'value' => 'CHOREOGRAPHER',
                                ),
                                array(
                                    'label' => 'CINEMATOGRAPHER',
                                    'value' => 'CINEMATOGRAPHER',
                                ),
                                array(
                                    'label' => 'CLEANER',
                                    'value' => 'CLEANER',
                                ),
                                array(
                                    'label' => 'CLERGYMAN',
                                    'value' => 'CLERGYMAN',
                                ),
                                array(
                                    'label' => 'CLERK',
                                    'value' => 'CLERK',
                                ),
                                array(
                                    'label' => 'COACH',
                                    'value' => 'COACH',
                                ),
                                array(
                                    'label' => 'COLLECTOR',
                                    'value' => 'COLLECTOR',
                                ),
                                array(
                                    'label' => 'COMMISSIONER',
                                    'value' => 'COMMISSIONER',
                                ),
                                array(
                                    'label' => 'CONSTITUENTS',
                                    'value' => 'CONSTITUENTS',
                                ),
                                array(
                                    'label' => 'CONSTRUCTION WORKER',
                                    'value' => 'CONSTRUCTION WORKER',
                                ),
                                array(
                                    'label' => 'CONSULTANT',
                                    'value' => 'CONSULTANT',
                                ),
                                array(
                                    'label' => 'CONTRACTOR',
                                    'value' => 'CONTRACTOR',
                                ),
                                array(
                                    'label' => 'CONTROLLER',
                                    'value' => 'CONTROLLER',
                                ),
                                array(
                                    'label' => 'COORDINATOR',
                                    'value' => 'COORDINATOR',
                                ),
                                array(
                                    'label' => 'COUNCILOR',
                                    'value' => 'COUNCILOR',
                                ),
                                array(
                                    'label' => 'COUNSEL',
                                    'value' => 'COUNSEL',
                                ),
                                array(
                                    'label' => 'COUNSELOR',
                                    'value' => 'COUNSELOR',
                                ),
                                array(
                                    'label' => 'CREW',
                                    'value' => 'CREW',
                                ),
                                array(
                                    'label' => 'CUSTODIAN',
                                    'value' => 'CUSTODIAN',
                                ),
                                array(
                                    'label' => 'DANCER',
                                    'value' => 'DANCER',
                                ),
                                array(
                                    'label' => 'DEALER',
                                    'value' => 'DEALER',
                                ),
                                array(
                                    'label' => 'DELIVERY MAN',
                                    'value' => 'DELIVERY MAN',
                                ),
                                array(
                                    'label' => 'DENTIST',
                                    'value' => 'DENTIST',
                                ),
                                array(
                                    'label' => 'DESIGNER',
                                    'value' => 'DESIGNER',
                                ),
                                array(
                                    'label' => 'DEVELOPER',
                                    'value' => 'DEVELOPER',
                                ),
                                array(
                                    'label' => 'DIPLOMAT',
                                    'value' => 'DIPLOMAT',
                                ),
                                array(
                                    'label' => 'DIRECTOR',
                                    'value' => 'DIRECTOR',
                                ),
                                array(
                                    'label' => 'DISC JOCKEY',
                                    'value' => 'DISC JOCKEY',
                                ),
                                array(
                                    'label' => 'DISHWASHER',
                                    'value' => 'DISHWASHER',
                                ),
                                array(
                                    'label' => 'DISPATCHER',
                                    'value' => 'DISPATCHER',
                                ),
                                array(
                                    'label' => 'DISTRIBUTOR',
                                    'value' => 'DISTRIBUTOR',
                                ),
                                array(
                                    'label' => 'DOCTOR',
                                    'value' => 'DOCTOR',
                                ),
                                array(
                                    'label' => 'DRAFTSMAN',
                                    'value' => 'DRAFTSMAN',
                                ),
                                array(
                                    'label' => 'DRILLER',
                                    'value' => 'DRILLER',
                                ),
                                array(
                                    'label' => 'DRIVER',
                                    'value' => 'DRIVER',
                                ),
                                array(
                                    'label' => 'EDITOR',
                                    'value' => 'EDITOR',
                                ),
                                array(
                                    'label' => 'ELECTRICIAN',
                                    'value' => 'ELECTRICIAN',
                                ),
                                array(
                                    'label' => 'EMBALMER',
                                    'value' => 'EMBALMER',
                                ),
                                array(
                                    'label' => 'EMPLOYEE',
                                    'value' => 'EMPLOYEE',
                                ),
                                array(
                                    'label' => 'ENCODER',
                                    'value' => 'ENCODER',
                                ),
                                array(
                                    'label' => 'ENGINEER',
                                    'value' => 'ENGINEER',
                                ),
                                array(
                                    'label' => 'ENGRAVER',
                                    'value' => 'ENGRAVER',
                                ),
                                array(
                                    'label' => 'ENTERTAINER',
                                    'value' => 'ENTERTAINER',
                                ),
                                array(
                                    'label' => 'ENTREPRENUER',
                                    'value' => 'ENTREPRENUER',
                                ),
                                array(
                                    'label' => 'EXECUTIVE',
                                    'value' => 'EXECUTIVE',
                                ),
                                array(
                                    'label' => 'EXPATRIATE',
                                    'value' => 'EXPATRIATE',
                                ),
                                array(
                                    'label' => 'EXPEDITER',
                                    'value' => 'EXPEDITER',
                                ),
                                array(
                                    'label' => 'FABRICATOR',
                                    'value' => 'FABRICATOR',
                                ),
                                array(
                                    'label' => 'FARMER',
                                    'value' => 'FARMER',
                                ),
                                array(
                                    'label' => 'FINANCIAL ANALYST',
                                    'value' => 'FINANCIAL ANALYST',
                                ),
                                array(
                                    'label' => 'FOREIGNER',
                                    'value' => 'FOREIGNER',
                                ),
                                array(
                                    'label' => 'FOREMAN',
                                    'value' => 'FOREMAN',
                                ),
                                array(
                                    'label' => 'FORENSIC',
                                    'value' => 'FORENSIC',
                                ),
                                array(
                                    'label' => 'FORESTER',
                                    'value' => 'FORESTER',
                                ),
                                array(
                                    'label' => 'GARDENER',
                                    'value' => 'GARDENER',
                                ),
                                array(
                                    'label' => 'GASOLINE BOY',
                                    'value' => 'GASOLINE BOY',
                                ),
                                array(
                                    'label' => 'GAUFER',
                                    'value' => 'GAUFER',
                                ),
                                array(
                                    'label' => 'GEOLOGIST',
                                    'value' => 'GEOLOGIST',
                                ),
                                array(
                                    'label' => 'GRINDER',
                                    'value' => 'GRINDER',
                                ),
                                array(
                                    'label' => 'GROUNDSKEEPER',
                                    'value' => 'GROUNDSKEEPER',
                                ),
                                array(
                                    'label' => 'GUARD',
                                    'value' => 'GUARD',
                                ),
                                array(
                                    'label' => 'HAIR DRESSER',
                                    'value' => 'HAIR DRESSER',
                                ),
                                array(
                                    'label' => 'HELPER',
                                    'value' => 'HELPER',
                                ),
                                array(
                                    'label' => 'HOTELIER',
                                    'value' => 'HOTELIER',
                                ),
                                array(
                                    'label' => 'HOUSEKEEPER',
                                    'value' => 'HOUSEKEEPER',
                                ),
                                array(
                                    'label' => 'HOUSEWIFE',
                                    'value' => 'HOUSEWIFE',
                                ),
                                array(
                                    'label' => 'HR',
                                    'value' => 'HR',
                                ),
                                array(
                                    'label' => 'INSPECTOR',
                                    'value' => 'INSPECTOR',
                                ),
                                array(
                                    'label' => 'INSTALLER',
                                    'value' => 'INSTALLER',
                                ),
                                array(
                                    'label' => 'INSTRUCTOR',
                                    'value' => 'INSTRUCTOR',
                                ),
                                array(
                                    'label' => 'INVESTIGATOR',
                                    'value' => 'INVESTIGATOR',
                                ),
                                array(
                                    'label' => 'JANITOR',
                                    'value' => 'JANITOR',
                                ),
                                array(
                                    'label' => 'JEWELER',
                                    'value' => 'JEWELER',
                                ),
                                array(
                                    'label' => 'JOURNALIST',
                                    'value' => 'JOURNALIST',
                                ),
                                array(
                                    'label' => 'JUDGE',
                                    'value' => 'JUDGE',
                                ),
                                array(
                                    'label' => 'LABORER',
                                    'value' => 'LABORER',
                                ),
                                array(
                                    'label' => 'LAWYER',
                                    'value' => 'LAWYER',
                                ),
                                array(
                                    'label' => 'LEADMAN',
                                    'value' => 'LEADMAN',
                                ),
                                array(
                                    'label' => 'LESSOR',
                                    'value' => 'LESSOR',
                                ),
                                array(
                                    'label' => 'LIAISON',
                                    'value' => 'LIAISON',
                                ),
                                array(
                                    'label' => 'LIFEGUARD',
                                    'value' => 'LIFEGUARD',
                                ),
                                array(
                                    'label' => 'LINEMAN',
                                    'value' => 'LINEMAN',
                                ),
                                array(
                                    'label' => 'LIVESTOCKS RAISERS',
                                    'value' => 'LIVESTOCKS RAISERS',
                                ),
                                array(
                                    'label' => 'LUBEMAN',
                                    'value' => 'LUBEMAN',
                                ),
                                array(
                                    'label' => 'MACHINIST',
                                    'value' => 'MACHINIST',
                                ),
                                array(
                                    'label' => 'MAID',
                                    'value' => 'MAID',
                                ),
                                array(
                                    'label' => 'MAINTENANCE',
                                    'value' => 'MAINTENANCE',
                                ),
                                array(
                                    'label' => 'MANAGER',
                                    'value' => 'MANAGER',
                                ),
                                array(
                                    'label' => 'MANICURIST',
                                    'value' => 'MANICURIST',
                                ),
                                array(
                                    'label' => 'MARKETING',
                                    'value' => 'MARKETING',
                                ),
                                array(
                                    'label' => 'MARSHALL',
                                    'value' => 'MARSHALL',
                                ),
                                array(
                                    'label' => 'MASON',
                                    'value' => 'MASON',
                                ),
                                array(
                                    'label' => 'MASSEUSE',
                                    'value' => 'MASSEUSE',
                                ),
                                array(
                                    'label' => 'MAYOR',
                                    'value' => 'MAYOR',
                                ),
                                array(
                                    'label' => 'MECHANIC',
                                    'value' => 'MECHANIC',
                                ),
                                array(
                                    'label' => 'MEDICAL REPRESENTATIVE',
                                    'value' => 'MEDICAL REPRESENTATIVE',
                                ),
                                array(
                                    'label' => 'MEDICAL TECHNOLOGIST',
                                    'value' => 'MEDICAL TECHNOLOGIST',
                                ),
                                array(
                                    'label' => 'MEMBER',
                                    'value' => 'MEMBER',
                                ),
                                array(
                                    'label' => 'MERCHANDISING',
                                    'value' => 'MERCHANDISING',
                                ),
                                array(
                                    'label' => 'MERCHANT',
                                    'value' => 'MERCHANT',
                                ),
                                array(
                                    'label' => 'MESSENGER',
                                    'value' => 'MESSENGER',
                                ),
                                array(
                                    'label' => 'METAL FABRICATION',
                                    'value' => 'METAL FABRICATION',
                                ),
                                array(
                                    'label' => 'MIDWIFE',
                                    'value' => 'MIDWIFE',
                                ),
                                array(
                                    'label' => 'MILITARY',
                                    'value' => 'MILITARY',
                                ),
                                array(
                                    'label' => 'MINISTER',
                                    'value' => 'MINISTER',
                                ),
                                array(
                                    'label' => 'MISSIONARY',
                                    'value' => 'MISSIONARY',
                                ),
                                array(
                                    'label' => 'MUSICIAN',
                                    'value' => 'MUSICIAN',
                                ),
                                array(
                                    'label' => 'NURSE',
                                    'value' => 'NURSE',
                                ),
                                array(
                                    'label' => 'NUTRITIONIST',
                                    'value' => 'NUTRITIONIST',
                                ),
                                array(
                                    'label' => 'OFFICER',
                                    'value' => 'OFFICER',
                                ),
                                array(
                                    'label' => 'OILER',
                                    'value' => 'OILER',
                                ),
                                array(
                                    'label' => 'ON THE JOB TRAINING (OJT)',
                                    'value' => 'ON THE JOB TRAINING (OJT)',
                                ),
                                array(
                                    'label' => 'ONLINE ARTICLE WRITER',
                                    'value' => 'ONLINE ARTICLE WRITER',
                                ),
                                array(
                                    'label' => 'OPERATOR',
                                    'value' => 'OPERATOR',
                                ),
                                array(
                                    'label' => 'OPTICIAN',
                                    'value' => 'OPTICIAN',
                                ),
                                array(
                                    'label' => 'OVERSEAS FILIPINO WORKER',
                                    'value' => 'OVERSEAS FILIPINO WORKER',
                                ),
                                array(
                                    'label' => 'OWNER',
                                    'value' => 'OWNER',
                                ),
                                array(
                                    'label' => 'PACKER',
                                    'value' => 'PACKER',
                                ),
                                array(
                                    'label' => 'PAINTER',
                                    'value' => 'PAINTER',
                                ),
                                array(
                                    'label' => 'PASTOR',
                                    'value' => 'PASTOR',
                                ),
                                array(
                                    'label' => 'PERSONNEL',
                                    'value' => 'PERSONNEL',
                                ),
                                array(
                                    'label' => 'PHARMACIST',
                                    'value' => 'PHARMACIST',
                                ),
                                array(
                                    'label' => 'PHOTOGRAPHER',
                                    'value' => 'PHOTOGRAPHER',
                                ),
                                array(
                                    'label' => 'PILOT',
                                    'value' => 'PILOT',
                                ),
                                array(
                                    'label' => 'PLUMBER',
                                    'value' => 'PLUMBER',
                                ),
                                array(
                                    'label' => 'POLISHER',
                                    'value' => 'POLISHER',
                                ),
                                array(
                                    'label' => 'PORTER',
                                    'value' => 'PORTER',
                                ),
                                array(
                                    'label' => 'PRESIDENT',
                                    'value' => 'PRESIDENT',
                                ),
                                array(
                                    'label' => 'PRIEST',
                                    'value' => 'PRIEST',
                                ),
                                array(
                                    'label' => 'PRODUCER',
                                    'value' => 'PRODUCER',
                                ),
                                array(
                                    'label' => 'PROFESSOR',
                                    'value' => 'PROFESSOR',
                                ),
                                array(
                                    'label' => 'PROGRAMMER',
                                    'value' => 'PROGRAMMER',
                                ),
                                array(
                                    'label' => 'PROPRIETOR',
                                    'value' => 'PROPRIETOR',
                                ),
                                array(
                                    'label' => 'PURCHASER',
                                    'value' => 'PURCHASER',
                                ),
                                array(
                                    'label' => 'REAL ESTATE BROKER',
                                    'value' => 'REAL ESTATE BROKER',
                                ),
                                array(
                                    'label' => 'REALTOR',
                                    'value' => 'REALTOR',
                                ),
                                array(
                                    'label' => 'RECEPTIONIST',
                                    'value' => 'RECEPTIONIST',
                                ),
                                array(
                                    'label' => 'REPORTER',
                                    'value' => 'REPORTER',
                                ),
                                array(
                                    'label' => 'RESEARCHER',
                                    'value' => 'RESEARCHER',
                                ),
                                array(
                                    'label' => 'RETIRED',
                                    'value' => 'RETIRED',
                                ),
                                array(
                                    'label' => 'REWINDER',
                                    'value' => 'REWINDER',
                                ),
                                array(
                                    'label' => 'RIDER',
                                    'value' => 'RIDER',
                                ),
                                array(
                                    'label' => 'RIGGER',
                                    'value' => 'RIGGER',
                                ),
                                array(
                                    'label' => 'SALES',
                                    'value' => 'SALES',
                                ),
                                array(
                                    'label' => 'SALESMAN',
                                    'value' => 'SALESMAN',
                                ),
                                array(
                                    'label' => 'SCAFFOLDER',
                                    'value' => 'SCAFFOLDER',
                                ),
                                array(
                                    'label' => 'SEAFARER',
                                    'value' => 'SEAFARER',
                                ),
                                array(
                                    'label' => 'SEAMAN',
                                    'value' => 'SEAMAN',
                                ),
                                array(
                                    'label' => 'SECRETARY',
                                    'value' => 'SECRETARY',
                                ),
                                array(
                                    'label' => 'SELF-EMPLOYED',
                                    'value' => 'SELF-EMPLOYED',
                                ),
                                array(
                                    'label' => 'SELLER',
                                    'value' => 'SELLER',
                                ),
                                array(
                                    'label' => 'SEMI-RETIRED BUSINESSMAN',
                                    'value' => 'SEMI-RETIRED BUSINESSMAN',
                                ),
                                array(
                                    'label' => 'SENIOR CITIZEN',
                                    'value' => 'SENIOR CITIZEN',
                                ),
                                array(
                                    'label' => 'SERVICER',
                                    'value' => 'SERVICER',
                                ),
                                array(
                                    'label' => 'SEWER',
                                    'value' => 'SEWER',
                                ),
                                array(
                                    'label' => 'SHEET HELPER',
                                    'value' => 'SHEET HELPER',
                                ),
                                array(
                                    'label' => 'SPECIALIST',
                                    'value' => 'SPECIALIST',
                                ),
                                array(
                                    'label' => 'STAFF',
                                    'value' => 'STAFF',
                                ),
                                array(
                                    'label' => 'STEELMAN',
                                    'value' => 'STEELMAN',
                                ),
                                array(
                                    'label' => 'STENOGRAPHER',
                                    'value' => 'STENOGRAPHER',
                                ),
                                array(
                                    'label' => 'STEWARD',
                                    'value' => 'STEWARD',
                                ),
                                array(
                                    'label' => 'STOCKMAN',
                                    'value' => 'STOCKMAN',
                                ),
                                array(
                                    'label' => 'STORE KEEPER',
                                    'value' => 'STORE KEEPER',
                                ),
                                array(
                                    'label' => 'STRATEGIST',
                                    'value' => 'STRATEGIST',
                                ),
                                array(
                                    'label' => 'STUDENT',
                                    'value' => 'STUDENT',
                                ),
                                array(
                                    'label' => 'SUPERVISOR',
                                    'value' => 'SUPERVISOR',
                                ),
                                array(
                                    'label' => 'SURVEYOR',
                                    'value' => 'SURVEYOR',
                                ),
                                array(
                                    'label' => 'SWEEPER',
                                    'value' => 'SWEEPER',
                                ),
                                array(
                                    'label' => 'TAILOR',
                                    'value' => 'TAILOR',
                                ),
                                array(
                                    'label' => 'TEACHER',
                                    'value' => 'TEACHER',
                                ),
                                array(
                                    'label' => 'TECHNICIAN',
                                    'value' => 'TECHNICIAN',
                                ),
                                array(
                                    'label' => 'TELLER',
                                    'value' => 'TELLER',
                                ),
                                array(
                                    'label' => 'THERAPIST',
                                    'value' => 'THERAPIST',
                                ),
                                array(
                                    'label' => 'TIME KEEPER',
                                    'value' => 'TIME KEEPER',
                                ),
                                array(
                                    'label' => 'TINSMITH',
                                    'value' => 'TINSMITH',
                                ),
                                array(
                                    'label' => 'TIREMAN',
                                    'value' => 'TIREMAN',
                                ),
                                array(
                                    'label' => 'TRADER',
                                    'value' => 'TRADER',
                                ),
                                array(
                                    'label' => 'TRAINEE',
                                    'value' => 'TRAINEE',
                                ),
                                array(
                                    'label' => 'TRAINOR',
                                    'value' => 'TRAINOR',
                                ),
                                array(
                                    'label' => 'TRAVELER',
                                    'value' => 'TRAVELER',
                                ),
                                array(
                                    'label' => 'TREASURER',
                                    'value' => 'TREASURER',
                                ),
                                array(
                                    'label' => 'UNDERWRITER',
                                    'value' => 'UNDERWRITER',
                                ),
                                array(
                                    'label' => 'UNEMPLOYED',
                                    'value' => 'UNEMPLOYED',
                                ),
                                array(
                                    'label' => 'UPHOLSTERER',
                                    'value' => 'UPHOLSTERER',
                                ),
                                array(
                                    'label' => 'VENDOR',
                                    'value' => 'VENDOR',
                                ),
                                array(
                                    'label' => 'VETERINARIAN',
                                    'value' => 'VETERINARIAN',
                                ),
                                array(
                                    'label' => 'VICE PRESIDENT',
                                    'value' => 'VICE PRESIDENT',
                                ),
                                array(
                                    'label' => 'VOLUNTEER',
                                    'value' => 'VOLUNTEER',
                                ),
                                array(
                                    'label' => 'WAITER',
                                    'value' => 'WAITER',
                                ),
                                array(
                                    'label' => 'WASHER',
                                    'value' => 'WASHER',
                                ),
                                array(
                                    'label' => 'WATCHMAN',
                                    'value' => 'WATCHMAN',
                                ),
                                array(
                                    'label' => 'WELDER',
                                    'value' => 'WELDER',
                                ),
                                array(
                                    'label' => 'WORKER',
                                    'value' => 'WORKER',
                                ),
                                array(
                                    'label' => 'WRITER',
                                    'value' => 'WRITER',
                                ),
                                array(
                                    'label' => 'Others',
                                    'value' => 'Others',
                                ),
                            ),
                        )
                    ),
                    'tax_identification_number' => $this->_buildElements('tax_identification_number',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Tax Identification Number *',
                            'item_note'   => "Sample Format: 012-345-678-901",
                            'attributes'  => array(
                                'data-mask-config'       => '999-999-999-999',
                                'data-mask-placeholder'  => '',
                                'pattern'                => "([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})[-]([0-9]{3})$",
                                'data-bv-regexp-message' => 'TIN Number is invalid',
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'false',
                            ),
                        )
                    ),
                    'employer'                  => $this->_buildElements('employer',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Employer *',
                            'attributes'  => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z. ]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'designation'               => $this->_buildElements('designation',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'placeholder' => 'Designation',
                            'attributes'  => array(
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z. ]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_CONTACT_PERSON_MAILING_ADDRESS)->addData(
            array(
                'elements' => array(
                    'province' => $this->_buildElements('province',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Province *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'city'     => $this->_buildElements('city',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'City *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'barangay' => $this->_buildElements('barangay',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Barangay *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'address'  => $this->_buildElements('address',
                        array(
                            'column'      => '12',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'House No. Street Name, Subdivision Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_CONTACT_PERSON_SECONDARY_MAILING_ADDRESS)->addData(
            array(
                'elements' => array(
                    'province' => $this->_buildElements('province',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 0,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Province',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'city'     => $this->_buildElements('city',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 0,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'City',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'barangay' => $this->_buildElements('barangay',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 0,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Barangay',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'address'  => $this->_buildElements('address',
                        array(
                            'column'      => '12',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'House No. Street Name, Subdivision Name',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::PARENT_GUARDIAN_INFORMATION)->addData(
            array(
                'elements' => array(
                    'last_name'               => $this->_buildElements('last_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'first_name'              => $this->_buildElements('first_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'First Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'relation_to_the_insured' => $this->_buildElements('relation_to_the_insured',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Relationship to the Insured *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $email_regex_validation = '^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))';

        $this->form_field_rows->getData(self::CONTACT_INFORMATION)->addData(
            array(
                'elements' => array(
                    'email'            => $this->_buildElements('email',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Email Address *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'false',
                                'data-mask-placeholder'    => '',
                                'data-bv-regexp-message'   => 'Please enter a valid email address',
                                'pattern'                  => $email_regex_validation,
                            ),
                        )
                    ),
                    'telephone_number' => $this->_buildElements('telephone_number',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Telephone Number',
                            'item_note'   => "Sample Format: 02-1234567",
                            'attributes'  => array(
                                'data-mask-config'       => '99-9999999',
                                'data-mask-placeholder'  => '',
                                'pattern'                => "^^\(?([0-9]{2})\)?[-. ]?([0-9]{7})$",
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'true',
                            ),
                        )
                    ),
                    'mobile_number'    => $this->_buildElements('mobile_number',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Mobile Number *',
                            'item_note'   => "Sample Format: 63-900-1234567",
                            'attributes'  => array(
                                'data-mask-config'         => '99-999-9999999',
                                'data-mask-placeholder'    => '',
                                'data-bv-excluded'         => 'false',
                                'data-bv-message'          => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'pattern'                  => "^^\(?([0-9]{2})\)?[-. ]?([0-9]{3})?[-. ]?([0-9]{7})$",
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::PERSON_TO_CONTACT)->addData(
            array(
                'elements' => array(
                    'member_name'   => $this->_buildElements('member_name',
                        array(
                            'column'      => '3',
                            'type'        => 'text',
                            'placeholder' => 'Member Name',
                            'attributes'  => array(
                                'readonly'                 => 'readonly',
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'name'          => $this->_buildElements('name',
                        array(
                            'column'      => '3',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'mobile_number' => $this->_buildElements('mobile_number',
                        array(
                            'column'      => '2',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Contact Number *',
                            'item_note'   => "Sample Format: 63-900-1234567",
                            'attributes'  => array(
                                'data-mask-config'         => '99-999-9999999',
                                'data-mask-placeholder'    => '',
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-message'          => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'pattern'                  => "^^\(?([0-9]{2})\)?[-. ]?([0-9]{3})?[-. ]?([0-9]{7})$",
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'relation'      => $this->_buildElements('relation',
                        array(
                            'column'      => '2',
                            'type'        => 'select',
                            'required'    => 1,
                            'placeholder' => 'Relation *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),

                            'options'     => array(
                                array(
                                    'label' => 'Relation *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Parent',
                                    'value' => 'Parent',
                                ),
                                array(
                                    'label' => 'Brother',
                                    'value' => 'Brother',
                                ),
                                array(
                                    'label' => 'Sister',
                                    'value' => 'Sister',
                                ),
                                array(
                                    'label' => 'Employer',
                                    'value' => 'Employer',
                                ),
                                array(
                                    'label' => 'Spouse',
                                    'value' => 'Spouse',
                                ),
                                array(
                                    'label' => 'Child',
                                    'value' => 'Child',
                                ),
                                array(
                                    'label' => 'Creditor',
                                    'value' => 'Creditor',
                                ),
                                array(
                                    'label' => 'Others',
                                    'value' => 'Others',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::BENEFICIARIES)->addData(
            array(
                'elements' => array(
                    'member_name' => $this->_buildElements('member_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'placeholder' => 'Member Name',
                            'attributes'  => array(
                                'readonly'                 => 'readonly',
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'name'        => $this->_buildElements('name',
                        array(
                            'column'      => '3',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'relation'    => $this->_buildElements('relation',
                        array(
                            'column'     => '3',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Relation *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Parent',
                                    'value' => 'Parent',
                                ),
                                array(
                                    'label' => 'Brother',
                                    'value' => 'Brother',
                                ),
                                array(
                                    'label' => 'Sister',
                                    'value' => 'Sister',
                                ),
                                array(
                                    'label' => 'Employer',
                                    'value' => 'Employer',
                                ),
                                array(
                                    'label' => 'Spouse',
                                    'value' => 'Spouse',
                                ),
                                array(
                                    'label' => 'Child',
                                    'value' => 'Child',
                                ),
                                array(
                                    'label' => 'Creditor',
                                    'value' => 'Creditor',
                                ),
                                array(
                                    'label' => 'Others',
                                    'value' => 'Others',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::AGENT_INFORMATION)->addData(
            array(
                'elements' => array(
                    'agent_status' => $this->_buildElements('agent_status',
                        array(
                            'column'     => '2',
                            'type'       => 'radios',
                            'required'   => 1,
                            'options'    => array(
                                array(
                                    'label' => 'Yes',
                                    'value' => 'Yes',
                                ),
                                array(
                                    'label' => 'No',
                                    'value' => 'No',
                                ),
                            ),
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'agent_number' => $this->_buildElements('agent_number',
                        array(
                            'column'          => '6',
                            'type'            => 'text',
                            'clearfix_before' => 1,
                            'placeholder'     => 'Enter Name or Agent Number *',
                            'required'        => 1,
                            'attributes'      => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'false',
                                'pattern'                  => "^[a-zA-Z0-9_. ]*$",
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );
    }

}
