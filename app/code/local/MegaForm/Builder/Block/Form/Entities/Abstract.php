<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Helper_Data as CURR_HELPER;
use MegaForm_Builder_Model_Request_Session_Completed as REQUEST_SESSION_COMPLETED_MODEL;

/**
 * Form Entities Entities Abstract
 *
 * This handles each form as a whole entity
 * Example :
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Motor.html">Motor</a>
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Itp.html">Itp</a>
 *
 * This abstract also dispatch events.
 * Events:
 * - form_builder_render_field_before
 * - form_builder_render_field_after
 * - form_builder_render_element_before
 * - form_builder_render_element_after
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
abstract class MegaForm_Builder_Block_Form_Entities_Abstract extends Mage_Core_Block_Template
{
    /**
     * Event prefix used for Magento Event Dispatcher
     *
     * Default : 'form_builder'
     *
     * @var string
     */
    public $event_prefix = "form_builder";

    /**
     * Event render used for Magento Event Dispatcher
     *
     * Default : 'render'
     *
     * @var string
     */
    public $event_render = "render";

    /**
     * Element event name
     *
     * Default : 'element'
     *
     * @var string
     */
    const EVENT_ELEMENT = "element";

    /**
     * Field event name
     *
     * Default : 'field'
     *
     * @var string
     */
    const EVENT_FIELD = "field";

    /**
     * After event name
     *
     * Default : 'after'
     *
     * @var string
     */
    const EVENT_AFTER = "after";

    /**
     * Before event name
     *
     * Default : 'before'
     *
     * @var string
     */
    const EVENT_BEFORE = "before";

    /**
     * Request Forms default motor
     *
     * Default : 'motor'
     *
     * @var string
     */
    private $request_forms = "motor";

    /**
     * On Success Url
     *
     * Default : ''
     *
     * @var string
     */
    private $on_success_url;

    /**
     * Promo Code
     *
     * Default : ''
     *
     * @var string
     */
    private $promo_code;

    /**
     * Form Values
     *
     * Default : ''
     *
     * This must consist of an array structure of
     *
     * <code>
     * array
     * &nbsp; &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;field_name1 =&gt; array
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; element_name1 =&gt; element_value1,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; element_name2 =&gt; element_value2
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ),
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;field_name2 =&gt; array
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; element_name1 =&gt; element_value1,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; element_name2 =&gt; element_value2
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; )
     * &nbsp; &nbsp; &nbsp; )
     * </code>
     *
     * @var array
     */
    private $form_values;

    /**
     * Just a toggle key
     *
     * Default : ''
     *
     * @var string
     */
    private $key_term;

    /**
     * Default not empty message for validation
     *
     * Default : 'This is required and cannot be empty'
     *
     * @var string
     */
    const NOT_EMPTY_MESSAGE             = "This is required and cannot be empty";

    /**
     * Default data not valid message for validation
     *
     * Default : 'This is not valid'
     *
     * @var string
     */
    const DATA_NOT_VALID_MESSAGE        = "This is not valid";

    /**
     * Default data not valid format message for validation
     *
     * Default : 'Please enter a valid Input'
     *
     * @var string
     */
    const DATA_NOT_VALID_FORMAT_MESSAGE = "Please enter a valid Input";

    /**
     * Default data bv date format
     *
     * Default : 'YYYY-MM-DD'
     *
     * BootstrapValidator
     * bv date format
     *
     * @var string
     */
    const DATA_BV_DATE_FORMAT           = "YYYY-MM-DD";

    /**
     * Contains all data of field sets with elements per fieldset
     *
     * Galing sa labas to wah
     * Late Static binding para magamit ng parent ung entity ng child kahit property pa xa ng parent ganun.
     *
     * @var mixed
     */
    public static $static_form_field_rows;

    /**
     * Fieldset group type
     *
     * Default : 'group_type'
     *
     * @var string
     */
    const GROUP_TYPE     = "group_type";

    /**
     * Fieldset group type multiple
     *
     * Default : 'FIELD_MULTIPLE'
     *
     * @var string
     */
    const FIELD_MULTIPLE = "FIELD_MULTIPLE";

    /**
     * Fieldset group type single
     *
     * Default : 'FIELD_SINGLE'
     *
     * @var string
     */
    const FIELD_SINGLE   = "FIELD_SINGLE";

    /**
     * Class constructor
     *
     * Converts form_values from object to array recursive.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->form_values = $this->objToArray($this->getFormValues());
    }

    /**
     * Get Form Values
     *
     * Return decoded form json datas from MegaForm_Builder_Model_Request_Session_Completed Singleton.
     *
     * @return mixed
     */
    public function getFormValues()
    {
        return Mage::getSingleton(REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL . "/" . REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL_ENTITY)->getDecodedFormJsonDatas();
    }

    /**
     * Get group type multiple
     *
     * returns array (group_type => FIELD_MULTIPLE)
     *
     * @return array
     */
    public function getGroupTypeMultiple()
    {
        return array(self::GROUP_TYPE => self::FIELD_MULTIPLE);
    }

    /**
     * Get group type single
     *
     * returns array (group_type => FIELD_SINGLE)
     *
     * @return array
     */
    public function getGroupTypeSingle()
    {
        return array(self::GROUP_TYPE => self::FIELD_SINGLE);
    }

    /**
     * Incase build elements
     *
     * returns array (element_name => new Varien_Object())
     *
     * @param string $name Name of the element
     * @return array $attributes Sets of array attributes
     * @return array new array with varien object instantiated from attribute argument
     */
    public function _buildElements($name = '', $attributes = array())
    {
        return array($name => new parent($attributes));
    }

    /**
     * Final Destination
     *
     * This calls the render fields function pass by the late binded static_form_field_rows
     *
     * @return string Html representation of whole form field and element renders
     */
    public function _toHtml()
    {
        $form_html = $this->renderFields(self::$static_form_field_rows);

        return $form_html;
    }

    /**
     * Render fields
     *
     * This renders all fieldsets contains from $form_fields
     *
     * @param mixed $form_fields Parameter is from late binded static_form_field_rows that consist of form entity
     * @return string Html representation of whole form field and element renders
     */
    public function renderFields($form_fields)
    {
        $captured_form_htmls = array();
        foreach ($form_fields->getData() as $field_name => $form_field)
        {
            if (empty($form_field->getData('elements')))
            {
                continue;
            }

            $form_field->addData(array('field_name' => $field_name));

            $captured_form_htmls[$field_name] = $this->renderField($field_name, $form_field);
        }

        if (count($this->page_sets))
        {
            $captured_form_htmls = $this->dividePages($captured_form_htmls);
        }

        $form = Mage::app()->getLayout()->createBlock('builder/forms')
            ->setData($this->getData())
            ->addData(
                array(
                    // 'include_actions'     => (!Mage::app()->getRequest()->getParam('form_key', false)),
                    'include_actions'     => true,
                    'captured_form_htmls' => $captured_form_htmls,
                )
            )
            ->setTemplate('megaform_builder/form/entities/form-wrapper.phtml')
            ->toHtml();

        return $form;
    }

    /**
     * Divide pages
     *
     * This wrap each all fieldsets html with an div for pagination purpose.
     *
     * @param mixed $captured_form_htmls Array Consist of all fieldsets in html
     * @return array Array Consist of all fieldsets in html wrapped with div divider
     */
    public function dividePages($captured_form_htmls)
    {
        $sets = array();
        foreach ($this->page_sets as $page_key => $page_set)
        {
            foreach ($page_set as $field_name)
            {
                if (array_key_exists($field_name, $captured_form_htmls))
                {
                    if (!isset($sets[$page_key]))
                    {
                        $sets[$page_key] = "\n";
                    }
                    $sets[$page_key] .= $captured_form_htmls[$field_name] . "\n";
                }
            }
            $sets[$page_key] = $this->wrapDivider($sets[$page_key]);
        }

        return $sets;
    }

    /**
     * Wrap divider
     *
     * This wrap each html with an div for pagination purpose.
     *
     * @param string $html_string Html string to be wraped
     * @return string Html string wrapped in div with data-slide attribute
     */
    public function wrapDivider($html_string = '')
    {
        return "<div data-slide> \n" . $html_string . "\n </div> \n";
    }

    /**
     * Render single fieldset of fieldsets
     *
     * This renders fieldset and all it's following element if may contains
     * This method also dispathes the event form_builder_render_field_before and form_builder_render_field_after
     *
     * @param string $field_name Field name
     * @param mixed $form_field Field Data consisting of config, elements, etc...
     * @return string Html representation of single fieldset with all its elements
     */
    public function renderField($field_name, $form_field)
    {

        $field_data = array(
            'field_name' => $field_name,
            'form_field' => $form_field,
        );

        $this->form_values = new Varien_Object($this->form_values);

        //todo dispatch event before save
        Mage::dispatchEvent($this->event_prefix . "_render_field_" . CURR_HELPER::EVENT_BEFORE, array('form_code' => $this->form_code, 'field_data' => $field_data, 'form_values' => $this->form_values));

        $this->form_values = $this->form_values->getData();

        $_field_row = Mage::app()->getLayout()->createBlock('core/template')->setData(
            $field_data
        );

        if ($form_field->getGroupType() == self::FIELD_MULTIPLE)
        {
            $this->form_values[$field_name] = $this->stackMultipleData(@$this->form_values[$field_name]);

            // if (!Mage::app()->getRequest()->getParam('form_key', false) && empty($this->form_values[$field_name]))
            if (empty($this->form_values[$field_name]))
            {
                $this->form_values[$field_name] = array(array_fill_keys(array_keys($form_field->getData('elements')), ''));
            }

            // fn_mrk($this->form_values[$field_name]);

            foreach ($this->form_values[$field_name] as $key => $value)
            {
                $form_set_elements = $this->buildElements($field_name, $form_field, $key);
                $form_elements[]   = Mage::app()->getLayout()->createBlock('core/template')
                    ->setData(array('form_set_elements' => $form_set_elements))
                    ->setTemplate('megaform_builder/form/entities/field-group-wrapper.phtml')->toHtml();
            }

            $_field_row->setTemplate('megaform_builder/form/entities/field-wrapper-multiple.phtml');
        }

        if ($form_field->getGroupType() == self::FIELD_SINGLE)
        {
            $form_elements = $this->buildElements($field_name, $form_field);
            $_field_row->setTemplate('megaform_builder/form/entities/field-wrapper-single.phtml');
        }

        $_field_row->addData(array('form_elements' => $form_elements));
        $build = new Varien_Object(array('html' => $_field_row->toHtml()));

        //todo dispatch event before save
        Mage::dispatchEvent($this->event_prefix . "_render_field_" . CURR_HELPER::EVENT_AFTER, array('form_code' => $this->form_code, 'field_data' => $field_data, 'build' => $build));

        return $build->getHtml();
    }

    /**
     * Stack multiple data
     *
     * This method stack up array by twos of keys
     *
     * @param array $_data Array data
     * @return array Stacked by twos of keys
     */
    public function stackMultipleData($_data = array())
    {
        $new_data = array();

        if (is_null($_data))
        {
            $_data = array();
        }

        $_keys = array_keys($_data);

        foreach ($_keys as $name)
        {
            foreach ($_data[$name] as $key => $value)
            {
                $new_data[$key][$name] = $value;
            }
        }

        return $new_data;
    }

    /**
     * Build Elements
     *
     * This method build each element from sets of array
     *
     * @param string $field_name Field name
     * @param mixed $form_field Form fieldset datas
     * @param mixed $multi_count Multiple count (default 0)
     * @return array Array of elements in html
     */
    public function buildElements($field_name, $form_field, $multi_count = 0)
    {
        $_elements = array();

        if (empty($form_field->getData('elements')))
        {
            return $_elements;
        }

        foreach ($form_field->getData('elements') as $key => $element)
        {
            $values = null;

            $_vkey = ($form_field->getGroupType() == self::FIELD_MULTIPLE) ? $multi_count : $key;

            if (isset($this->form_values[$field_name]) && isset($this->form_values[$field_name][$_vkey]))
            {
                $values = $this->form_values[$field_name][$_vkey];

                if ($form_field->getGroupType() == self::FIELD_MULTIPLE)
                {
                    $values = $values[$key];
                }
            }

            $_elements[] = $this->buildElement($field_name, $form_field, $key, $element, $values);
        }

        return $_elements;
    }

    /**
     * Build Element
     *
     * This method build single element from parameters
     *
     * @param string $field_name Field name
     * @param mixed $form_field Form fieldset datas
     * @param string $key Form Key as element name
     * @param mixed $element Element datas
     * @param string $values Form Values (default '')
     * @return string Html represntation of the single element
     */
    public function buildElement($field_name, $form_field, $key, $element, $values = "")
    {
        // $_element = Mage::getModel('builder/form_entities_elements_' . $element[$key]->getType());
        $_element = Mage::app()->getLayout()->createBlock('builder/form_entities_elements_' . $element[$key]->getType());

        $_build = "";

        if (is_null($_element))
        {
            return $_build;
        }

        $_element->setFieldName($form_field->getFieldName());
        $_element->setFormGroupType($form_field->getGroupType());
        $_element->setElementName($key);
        $_element->setElementData($element[$key]);
        $_element->setFormValues($values);

        Mage::dispatchEvent($this->event_prefix . "_" . $this->event_render . "_" . self::EVENT_ELEMENT . "_" . self::EVENT_BEFORE, array('form_code' => $this->form_code, 'form_values' => $this->form_values, 'element' => $_element));

        $build = new Varien_Object(array('html' => $_element->_toHtml()));

        Mage::dispatchEvent($this->event_prefix . "_" . $this->event_render . "_" . self::EVENT_ELEMENT . "_" . self::EVENT_AFTER, array('form_code' => $this->form_code, 'form_values' => $this->form_values, 'element' => $_element->getData(), 'build' => $build));

        return $build->getHtml();
    }

    /**
     * Object to Array
     *
     * It accepts array and object and convert to full array in recursive
     *
     * @param mixed $obj Object to be converted
     * @param mixed $arr Array referenced as we it loops deeper
     * @return array Full converted array from object
     */
    public function objToArray($obj = array(), &$arr = array())
    {

        if (!is_object($obj) && !is_array($obj))
        {
            $arr = $obj;
            return $arr;
        }

        foreach ($obj as $key => $value)
        {
            if (!empty($value))
            {
                $arr[$key] = array();
                $this->objToArray($value, $arr[$key]);
            }
            else
            {
                $arr[$key] = $value;
            }
        }
        return $arr;
    }
}
