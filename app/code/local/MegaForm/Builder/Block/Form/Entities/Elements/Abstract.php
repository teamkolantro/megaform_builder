<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Form Entities Elements Abstract
 *
 * This handles every entities of the form
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
abstract class MegaForm_Builder_Block_Form_Entities_Elements_Abstract extends Mage_Core_Block_Template
{
    /**
     * The entities may have options that can be pass here
     * @var array
     */
    public $_options = array();

    /**
     * The entities element module path
     *
     * Path string = "megaform_builder/form/elements/"
     *
     * @var string
     */
    public $form_element_path = "megaform_builder/form/elements/";

    /**
     * The entities may have value that can be pass here
     * @var mixed
     */
    public $_value;

    /**
     * The entities may have field name that can be pass here
     * @var string
     */
    public $_field_name;

    /**
     * The entities may have element data that can be pass here
     * @var object
     */
    public $_element_data;

    /**
     * The entities may have template path that can be pass here
     * @var string
     */
    public $_template_path;

    /**
     * The entities may have html string that can be pass here
     * @var string
     */
    public $_block_html;

    /**
     * Class constructor.
     *
     * This sets the template path upon instantiation.
     *
     * @param array $attributes Accepts any attributes in form of array.
     */
    public function __construct($attributes = array())
    {
        $this->setTemplate($this->_template_path);
        parent::__construct($attributes);
    }

    /**
     * Is Form Key Exists.
     *
     * This returns true if there is a form_key in url query param.
     * But as of update in 2016 this is deprecated and only returns false.
     *
     * @return boolean Default false.
     * @deprecated Method deprecated in Release 0.2.0
     */
    public function isFormKeyExist()
    {
        return false; //needs to be edited | disabled formkey disabled form
        return (boolean) Mage::app()->getRequest()->getParam('form_key', false);
    }
}
