<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Form_Entities_Abstract as FE_Abstract;

/**
 * Form Entities Entities Fire
 *
 * This handles each form as a whole entity form for fire
 * Other Example :
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Motor.html">Motor</a>
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Itp.html">Itp</a>
 *
 * This class also dispatch events from abstract parent.
 * Events:
 * - form_builder_render_field_before
 * - form_builder_render_field_after
 * - form_builder_render_element_before
 * - form_builder_render_element_after
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Form_Entities_Fire extends FE_Abstract
{
    /**
     * Contains all data of field sets with elements per fieldset array
     *
     * This variable should be passed to self::$static_form_field_rows
     * for the parent abstract late static bindings
     *
     * Object representation
     * <code>
     * Varien_Object
     * (
     * &nbsp; &nbsp; _data::protected =&gt;
     * &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset1 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset2 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset3 =&gt; Varien_Object(),
     * &nbsp; &nbsp; )
     * )
     * </code>
     *
     * @var Varien_Object
     */
    private $form_field_rows;

    /**
     * Array of fieldsets
     *
     * Field sets is being divided by group in array
     *
     * Array representaion
     * <code>
     * array
     * (
     * &nbsp; &nbsp; &nbsp;array
     * &nbsp; &nbsp; &nbsp;(
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name1,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name2
     * &nbsp; &nbsp; &nbsp;),
     * &nbsp; &nbsp; &nbsp;array
     * &nbsp; &nbsp; &nbsp;(
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name3,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name4
     * &nbsp; &nbsp; &nbsp;)
     * )
     * </code>
     *
     * @var mixed
     */
    public $page_sets = array();

    /**
     * Request Forms
     *
     * Default : 'request_forms'
     *
     * @var string
     */
    const REQUEST_FORMS = 'request_forms';

    /**
     * On Success Url
     *
     * Default : 'on_success_url'
     *
     * @var string
     */
    const ON_SUCCESS_URL = 'on_success_url';

    /**
     * Basic Information
     *
     * For fieldset key
     *
     * Default : 'basic_information'
     *
     * @var string
     */
    const BASIC_INFORMATION    = 'basic_information';
    
    /**
     * Property Information
     *
     * For fieldset key
     *
     * Default : 'property_information'
     *
     * @var string
     */
    const PROPERTY_INFORMATION = 'property_information';
        
    /**
     * Location Risk
     *
     * For fieldset key
     *
     * Default : 'location_risk'
     *
     * @var string
     */
    const LOCATION_RISK        = 'location_risk';
        
    /**
     * Boundary Risk
     *
     * For fieldset key
     *
     * Default : 'boundary_risk'
     *
     * @var string
     */
    const BOUNDARY_RISK        = 'boundary_risk';
        
    /**
     * Requirements
     *
     * For fieldset key
     *
     * Default : 'requirements'
     *
     * @var string
     */
    const REQUIREMENTS         = 'requirements';
        
    /**
     * Promo Code
     *
     * For fieldset key
     *
     * Default : 'promo_code'
     *
     * @var string
     */
    const PROMO_CODE           = 'promo_code';
        
    /**
     * Key Term
     *
     * For fieldset key
     *
     * Default : 'key_term'
     *
     * @var string
     */
    const KEY_TERM             = 'key_term';

    /**
     * Class contructor...
     *
     * Sets the self::$static_form_field_rows from property form_field_rows
     *
     */
    public function __construct()
    {
        $this->_init();
        /**
         * Late Static binding para magamit ng parent ung entity ng child kahit property pa xa ng parent ganun.
         */
        self::$static_form_field_rows = $this->form_field_rows;

        parent::__construct();
    }

    /**
     * Initiator
     *
     * Intiates and prepare entities and values for form rendering process
     * Calls to methods
     * - <a href="#method_initEntities">_initEntities</a>
     * - <a href="#method_processEntities">_processEntities</a>
     * - <a href="#method_buildFieldItems">_buildFieldItems</a>
     * - <a href="#method_assignPageSets">_initEntities</a>
     *
     */
    public function _init()
    {
        $this->_initEntities();
        $this->_processEntities();
        $this->_buildFieldItems();
        $this->_assignPageSets();
    }

    /**
     * Assign Page Sets
     *
     * This sets the field set in grouped array to property <a href="#property_page_sets">page_sets</a>
     */
    public function _assignPageSets()
    {
    }

    /**
     * Initialize Entities
     *
     * Entities are basically the fieldsets
     *
     * This method sets all fieldsets/entities instantiate
     * each fieldset to be an instance of Varien_Object
     * and sets to <a href="#property_form_field_rows">form_field_rows</a>
     * as whole Varien_Object
     *
     * Data representation
     * <code>
     * Varien_Object
     * (
     * &nbsp; &nbsp; _data::protected =&gt;
     * &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset1 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset2 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset3 =&gt; Varien_Object(),
     * &nbsp; &nbsp; )
     * )
     * </code>
     *
     */
    public function _initEntities()
    {
        $this->form_field_rows = new Varien_Object(
            array(

                self::REQUEST_FORMS        => new Varien_Object(),
                self::ON_SUCCESS_URL       => new Varien_Object(),
                self::BASIC_INFORMATION    => new Varien_Object(),
                self::PROPERTY_INFORMATION => new Varien_Object(),
                self::LOCATION_RISK        => new Varien_Object(),
                self::BOUNDARY_RISK        => new Varien_Object(),
                self::REQUIREMENTS         => new Varien_Object(),
                self::PROMO_CODE           => new Varien_Object(),
            )
        );
    }

    /**
     * Process Entities
     *
     * Process all fieldsets attributes
     *
     * This method sets all attributes for every single fieldset
     *
     * Fieldset can contain
     * - Group Type     (Single or Multiple)
     * - title          Field set header text as title
     * - header_info    Field set header info text as a string rendered below fieldset header
     * - attributes     Ay html 5 attrbutes developer may think usefull.
     *
     * Data representation
     * <code>
     * fieldset1 =&gt; Object&nbsp;
     * (
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //If the elements is single
     * &nbsp;&nbsp; &nbsp;or
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //This will group the elements as one fieldset
     * )
     * -&gt;addData
     * (
     * &nbsp;&nbsp; &nbsp;title =&gt; Field set title
     * &nbsp;&nbsp; &nbsp;header_info =&gt; Fieldset info //this will appear below the fieldset title
     * )
     * </code>
     *
     */
    public function _processEntities()
    {

        $this->form_field_rows->getData(self::BASIC_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'BASIC INFORMATION',
                'header_info' => 'Please provide the personal information of the person who will travel.',
            )
        );

        $this->form_field_rows->getData(self::PROPERTY_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'PROPERTY INFORMATION',
            )
        );

        $this->form_field_rows->getData(self::LOCATION_RISK)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'LOCATION OF RISK',
            )
        );

        $this->form_field_rows->getData(self::BOUNDARY_RISK)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'BOUNDARY OF RISK',
            )
        );

        $this->form_field_rows->getData(self::REQUIREMENTS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'REQUIREMENTS',
            )
        );

        $this->form_field_rows->getData(self::PROMO_CODE)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'Promo Code',
            )
        );

    }

    /**
     * Builld Field Items
     *
     * Build all items in each fieldset
     *
     * This method sets add all kinds of elements in each fieldset,
     * whereas each element has an attribute and this method add it to it's respective element.
     *
     * Element can contain
     * - column             Column number in string, please refer to bootstrap, example is if column is 4 then it is col-md-4
     * - type               Element type to be rendered, but this only accepts only if there's an available class entity. Found in \MegaForm\Builder\Block\Adminhtml\Renderer\[Element_Type].php
     * - required           If value is 1 then element will have an attribute of required
     * - placeholder        This is for html palceholder attribute
     * - attributes         Should be a set of array whereas array (attribute-name => attribute-value)
     * - item_note          This is an item note that will be display under an element
     * - options            This is only applicable for select type of elements, and should be in array format
     * - label              This is only applicable for label type of elements, and should be in string format
     * - clearfix_before    If it's value 1 then the element will have a clearfix before it renders
     * - clearfix_after    If it's value 1 then the element will have a clearfix after it renders
     *
     * Data representation
     * <code>
     * fieldset1 =&gt; Object&nbsp;
     * (
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //If the elements is single
     * &nbsp;&nbsp; &nbsp;or
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //This will group the elements as one fieldset
     * )
     * -&gt;addData
     * (
     * &nbsp;&nbsp; &nbsp;title =&gt; Field set title
     * &nbsp;&nbsp; &nbsp;header_info =&gt; Fieldset info //this will appear below the fieldset title
     * )
     * </code>
     *
     */
    public function _buildFieldItems()
    {
        $this->form_field_rows->getData(self::BASIC_INFORMATION)->addData(
            array(
                'elements' => array(
                    'last_name'      => $this->_buildElements('last_name',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name',
                            'attributes'  => array(

                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'first_name'     => $this->_buildElements('first_name',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'First Name',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'address'        => $this->_buildElements('address',
                        array(
                            'column'      => '12',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Address',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'issue_date'     => $this->_buildElements('issue_date',
                        array(
                            'column'      => '6',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Issue Date',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                            ),
                        )
                    ),
                    'inception_date' => $this->_buildElements('inception_date',
                        array(
                            'column'      => '6',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Inception Date',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                            ),
                        )
                    ),
                    'expiry_date'    => $this->_buildElements('expiry_date',
                        array(
                            'column'      => '6',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Expiry Date',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                            ),
                        )
                    ),
                    'policy_code'  => $this->_buildElements('policy_code',
                        array(
                            'column'      => '4',
                            'type'        => 'hidden',
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::PROPERTY_INFORMATION)->addData(
            array(
                'elements' => array(
                    'type_of_property' => $this->_buildElements('type_of_property',
                        array(
                            'column'     => '12',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Type of Property Insured',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::LOCATION_RISK)->addData(
            array(
                'elements' => array(
                    'province' => $this->_buildElements('province',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Province',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'city'     => $this->_buildElements('city',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'City',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'barangay' => $this->_buildElements('barangay',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Barangay',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'block'    => $this->_buildElements('block',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Email Address',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::BOUNDARY_RISK)->addData(
            array(
                'elements' => array(
                    'left'  => $this->_buildElements('left',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Left',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'right' => $this->_buildElements('right',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Right',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'front' => $this->_buildElements('front',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Front',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'rear'  => $this->_buildElements('rear',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Rear',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::REQUIREMENTS)->addData(
            array(
                'elements' => array(
                    'roofing'                => $this->_buildElements('roofing',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Roofing',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'partition_wall_ceiling' => $this->_buildElements('partition_wall_ceiling',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Partition Wall and Ceiling',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'floor_finishes'         => $this->_buildElements('floor_finishes',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Floor Finishes',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'doors'                  => $this->_buildElements('doors',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Doors',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'windows'                => $this->_buildElements('windows',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Windows',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'paint_finish'           => $this->_buildElements('paint_finish',
                        array(
                            'column'     => '6',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Paint Finish',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::PROMO_CODE)->addData(
            array(
                'elements' => array(
                    'promo_code' => $this->_buildElements('promo_code',
                        array(
                            'column'      => '3',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Promo Code',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );
    }

}
