<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Form_Entities_Abstract as FE_Abstract;

/**
 * Form Entities Entities Vehicleinfo
 *
 * This handles each form as a whole entity form Vehicleinfo for motor
 * Other Example :
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Fire.html">Fire</a>
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Itp.html">Itp</a>
 *
 * This class also dispatch events from abstract parent.
 * Events:
 * - form_builder_render_field_before
 * - form_builder_render_field_after
 * - form_builder_render_element_before
 * - form_builder_render_element_after
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Form_Entities_VehicleInfo extends FE_Abstract
{
    /**
     * Contains all data of field sets with elements per fieldset array
     *
     * This variable should be passed to self::$static_form_field_rows
     * for the parent abstract late static bindings
     *
     * Object representation
     * <code>
     * Varien_Object
     * (
     * &nbsp; &nbsp; _data::protected =&gt;
     * &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset1 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset2 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset3 =&gt; Varien_Object(),
     * &nbsp; &nbsp; )
     * )
     * </code>
     *
     * @var Varien_Object
     */
    private $form_field_rows;

    /**
     * Array of fieldsets
     *
     * Field sets is being divided by group in array
     *
     * Array representaion
     * <code>
     * array
     * (
     * &nbsp; &nbsp; &nbsp;array
     * &nbsp; &nbsp; &nbsp;(
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name1,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name2
     * &nbsp; &nbsp; &nbsp;),
     * &nbsp; &nbsp; &nbsp;array
     * &nbsp; &nbsp; &nbsp;(
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name3,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name4
     * &nbsp; &nbsp; &nbsp;)
     * )
     * </code>
     *
     * @var mixed
     */
    public $page_sets = array();

    /**
     * Request Forms
     *
     * Default : 'request_forms'
     *
     * @var string
     */
    const REQUEST_FORMS = 'request_forms';

    /**
     * On Success Url
     *
     * Default : 'on_success_url'
     *
     * @var string
     */
    const ON_SUCCESS_URL = 'on_success_url';

    /**
     * Person To Contact
     *
     * For fieldset key
     *
     * Default : 'person_to_contact'
     *
     * @var string
     */
    const VEHICLE_INFORMATION         = 'vehicle_information';

    /**
     * Accessories Standard
     *
     * For fieldset key
     *
     * Default : 'accessories_standard'
     *
     * @var string
     */
    const ACCESSORIES_STANDARD        = 'accessories_standard';

    /**
     * Accessories Other Standard
     *
     * For fieldset key
     *
     * Default : 'accessories_other_standard'
     *
     * @var string
     */
    const ACCESSORIES_OTHER_STANDARD  = 'accessories_other_standard';

    /**
     * Acessories Non Standard
     *
     * For fieldset key
     *
     * Default : 'accessories_non_standard'
     *
     * @var string
     */
    const ACCESSORIES_NON_STANDARD    = 'accessories_non_standard';

    /**
     * Other Info
     *
     * For fieldset key
     *
     * Default : 'other_info'
     *
     * @var string
     */
    const OTHER_INFO                  = 'other_info';

    /**
     * Coverage Details
     *
     * For fieldset key
     *
     * Default : 'coverage_details'
     *
     * @var string
     */
    const COVERAGE_DETAILS            = 'coverage_details';

    /**
     * Comprehensive Policy Period
     *
     * For fieldset key
     *
     * Default : 'comprehensive_policy_period'
     *
     * @var string
     */
    const COMPREHENSIVE_POLICY_PERIOD = 'comprehensive_policy_period';

    /**
     * Ctpl Policy Period
     *
     * For fieldset key
     *
     * Default : 'ctpl_policy_period'
     *
     * @var string
     */
    const CTPL_POLICY_PERIOD          = 'ctpl_policy_period';

    /**
     * Upload Orcr
     *
     * For fieldset key
     *
     * Default : 'upload_orcr'
     *
     * @var string
     */
    const UPLOAD_ORCR                 = 'upload_orcr';

    /**
     * Class contructor...
     *
     * Sets the self::$static_form_field_rows from property form_field_rows
     *
     */
    public function __construct()
    {
        $this->_init();
        /**
         * Late Static binding para magamit ng parent ung entity ng child kahit property pa xa ng parent ganun.
         */
        self::$static_form_field_rows = $this->form_field_rows;

        parent::__construct();
    }

    /**
     * Initiator
     *
     * Intiates and prepare entities and values for form rendering process
     * Calls to methods
     * - <a href="#method_initEntities">_initEntities</a>
     * - <a href="#method_processEntities">_processEntities</a>
     * - <a href="#method_buildFieldItems">_buildFieldItems</a>
     * - <a href="#method_assignPageSets">_initEntities</a>
     *
     */
    public function _init()
    {
        $this->_initEntities();
        $this->_processEntities();
        $this->_buildFieldItems();
        $this->_assignPageSets();
    }

    /**
     * Assign Page Sets
     *
     * This sets the field set in grouped array to property <a href="#property_page_sets">page_sets</a>
     */
    public function _assignPageSets()
    {
        $this->page_sets[] = array(
            self::VEHICLE_INFORMATION,
            self::ACCESSORIES_STANDARD,
            self::ACCESSORIES_OTHER_STANDARD,
            self::ACCESSORIES_NON_STANDARD,
        );

        $this->page_sets[] = array(
            self::OTHER_INFO,
            self::COVERAGE_DETAILS,
        );

        $this->page_sets[] = array(
            self::COMPREHENSIVE_POLICY_PERIOD,
            self::CTPL_POLICY_PERIOD,
            self::UPLOAD_ORCR,
        );
    }

    /**
     * Initialize Entities
     *
     * Entities are basically the fieldsets
     *
     * This method sets all fieldsets/entities instantiate
     * each fieldset to be an instance of Varien_Object
     * and sets to <a href="#property_form_field_rows">form_field_rows</a>
     * as whole Varien_Object
     *
     * Data representation
     * <code>
     * Varien_Object
     * (
     * &nbsp; &nbsp; _data::protected =&gt;
     * &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset1 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset2 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset3 =&gt; Varien_Object(),
     * &nbsp; &nbsp; )
     * )
     * </code>
     *
     */
    public function _initEntities()
    {
        $this->form_field_rows = new Varien_Object(
            array(
                self::REQUEST_FORMS               => new Varien_Object(),
                self::ON_SUCCESS_URL              => new Varien_Object(),
                self::VEHICLE_INFORMATION         => new Varien_Object(),
                self::ACCESSORIES_STANDARD        => new Varien_Object(),
                self::ACCESSORIES_OTHER_STANDARD  => new Varien_Object(),
                self::ACCESSORIES_NON_STANDARD    => new Varien_Object(),
                self::OTHER_INFO                  => new Varien_Object(),
                self::COVERAGE_DETAILS            => new Varien_Object(),
                self::COMPREHENSIVE_POLICY_PERIOD => new Varien_Object(),
                self::CTPL_POLICY_PERIOD          => new Varien_Object(),
                self::UPLOAD_ORCR                 => new Varien_Object(),
            )
        );
    }

    /**
     * Process Entities
     *
     * Process all fieldsets attributes
     *
     * This method sets all attributes for every single fieldset
     *
     * Fieldset can contain
     * - Group Type     (Single or Multiple)
     * - title          Field set header text as title
     * - header_info    Field set header info text as a string rendered below fieldset header
     * - attributes     Ay html 5 attrbutes developer may think usefull.
     *
     * Data representation
     * <code>
     * fieldset1 =&gt; Object&nbsp;
     * (
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //If the elements is single
     * &nbsp;&nbsp; &nbsp;or
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //This will group the elements as one fieldset
     * )
     * -&gt;addData
     * (
     * &nbsp;&nbsp; &nbsp;title =&gt; Field set title
     * &nbsp;&nbsp; &nbsp;header_info =&gt; Fieldset info //this will appear below the fieldset title
     * )
     * </code>
     *
     */
    public function _processEntities()
    {
        $this->form_field_rows->getData(self::VEHICLE_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'VEHICLE INFORMATION',
                'header_info' => '',
            )
        );

        $this->form_field_rows->getData(self::ACCESSORIES_STANDARD)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'ACCESSORIES STANDARD',
                'header_info' => 'Standard (Built In)',
            )
        );

        $this->form_field_rows->getData(self::ACCESSORIES_OTHER_STANDARD)->addData(
            $this->getGroupTypeMultiple()
        )->addData(
            array(
                'title'       => '',
                'header_info' => 'Other Standard (Built In)',
            )
        );

        $this->form_field_rows->getData(self::ACCESSORIES_NON_STANDARD)->addData(
            $this->getGroupTypeMultiple()
        )->addData(
            array(
                'title'       => '',
                'header_info' => 'Non Standard (Built In)',
            )
        );

        $this->form_field_rows->getData(self::OTHER_INFO)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'VEHICLE INFORMATION',
                'header_info' => '',
            )
        );

        $this->form_field_rows->getData(self::COVERAGE_DETAILS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'Coverage Details',
                'header_info' => '',
            )
        );

        $this->form_field_rows->getData(self::COMPREHENSIVE_POLICY_PERIOD)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'Comprehensive Policy Period',
                'header_info' => '',
            )
        );

        $this->form_field_rows->getData(self::CTPL_POLICY_PERIOD)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'CTPL Policy Period',
                'header_info' => '',
            )
        );

        $this->form_field_rows->getData(self::UPLOAD_ORCR)->addData(
            $this->getGroupTypeMultiple()
        )->addData(
            array(
                'title'       => 'Upload OR/CR',
                'header_info' => 'Allowed types: PNG, JPG, GIF, BMP, PDF',
            )
        );
    }

    /**
     * Builld Field Items
     *
     * Build all items in each fieldset
     *
     * This method sets add all kinds of elements in each fieldset,
     * whereas each element has an attribute and this method add it to it's respective element.
     *
     * Element can contain
     * - column             Column number in string, please refer to bootstrap, example is if column is 4 then it is col-md-4
     * - type               Element type to be rendered, but this only accepts only if there's an available class entity. Found in \MegaForm\Builder\Block\Adminhtml\Renderer\[Element_Type].php
     * - required           If value is 1 then element will have an attribute of required
     * - placeholder        This is for html palceholder attribute
     * - attributes         Should be a set of array whereas array (attribute-name => attribute-value)
     * - item_note          This is an item note that will be display under an element
     * - options            This is only applicable for select type of elements, and should be in array format
     * - label              This is only applicable for label type of elements, and should be in string format
     * - clearfix_before    If it's value 1 then the element will have a clearfix before it renders
     * - clearfix_after    If it's value 1 then the element will have a clearfix after it renders
     *
     * Data representation
     * <code>
     * fieldset1 =&gt; Object&nbsp;
     * (
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //If the elements is single
     * &nbsp;&nbsp; &nbsp;or
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //This will group the elements as one fieldset
     * )
     * -&gt;addData
     * (
     * &nbsp;&nbsp; &nbsp;title =&gt; Field set title
     * &nbsp;&nbsp; &nbsp;header_info =&gt; Fieldset info //this will appear below the fieldset title
     * )
     * </code>
     *
     */
    public function _buildFieldItems()
    {
        $vehicle_information_year     = 2012;
        $vehicle_information_brand    = 'AUDI';
        $vehicle_information_model    = 'A1 Turbo Sportsback 1.4L Gas S-tronic';
        $vehicle_information_seat_cap = '5 Seater';
        $vehicle_information_gpv      = 72327.30;

        $this->form_field_rows->getData(self::VEHICLE_INFORMATION)->addData(
            array(
                'elements' => array(
                    'previous_form_key' => $this->_buildElements('previous_form_key',
                        array(
                            'column'      => '6',
                            'type'        => 'hidden',
                            'placeholder' => 'Summary Form Key',
                            'attributes'  => array(
                                'readonly'    => 'readonly',
                            ),
                        )
                    ),
                    'info'             => $this->_buildElements('info',
                        array(
                            'column' => '12',
                            'type'   => 'label',
                            'label'  => 'Vehicle Information',
                        )
                    ),
                    'year'             => $this->_buildElements('year',
                        array(
                            'column' => '12',
                            'type'   => 'label',
                            'label'  => 'Year: ',
                        )
                    ),
                    'brand'            => $this->_buildElements('brand',
                        array(
                            'column' => '6',
                            'type'   => 'label',
                            'label'  => 'Brand: ',
                        )
                    ),
                    'model'            => $this->_buildElements('model',
                        array(
                            'column' => '6',
                            'type'   => 'label',
                            'label'  => 'Model: ',
                        )
                    ),
                    'seat_capacity'    => $this->_buildElements('seat_capacity',
                        array(
                            'column' => '6',
                            'type'   => 'label',
                            'label'  => 'Seat Capacity: ',
                        )
                    ),
                    'typeofinsurance'              => $this->_buildElements('typeofinsurance',
                        array(
                            'column' => '6',
                            'type'   => 'hidden',
                        )
                    ),
                    'gpv'              => $this->_buildElements('gpv',
                        array(
                            'column' => '6',
                            'type'   => 'label',
                            'label'  => 'Final Amount Due: ' . $this->__('Php '),
                        )
                    ),
                    'cptlgrosspremium'              => $this->_buildElements('cptlgrosspremium',
                        array(
                            'column' => '6',
                            'type'   => 'label',
                            'label'  => 'Final Amount Due w/o AON: ' . $this->__('Php '),
                        )
                    ),
                    'gpvfinalvalue'    => $this->_buildElements('gpvfinalvalue',
                        array(
                            'column' => '6',
                            'type'   => 'label',
                            'label'  => 'Final Amount Due w/ AON: ' . $this->__('Php '),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::ACCESSORIES_STANDARD)->addData(
            array(
                'elements' => array(
                    'aircon'    => $this->_buildElements('aircon',
                        array(
                            'column' => '12',
                            'type'   => 'label',
                            'label'  => '1. Aircon',
                        )
                    ),
                    'stereo'    => $this->_buildElements('stereo',
                        array(
                            'column' => '12',
                            'type'   => 'label',
                            'label'  => '2. Stereo',
                        )
                    ),
                    'magwheels' => $this->_buildElements('magwheels',
                        array(
                            'column' => '12',
                            'type'   => 'label',
                            'label'  => '3. Magwheels',
                        )
                    ),
                    'speaker'   => $this->_buildElements('speaker',
                        array(
                            'column' => '12',
                            'type'   => 'label',
                            'label'  => '4. Speaker',
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::ACCESSORIES_OTHER_STANDARD)->addData(
            array(
                'elements' => array(
                    'other_standard' => $this->_buildElements('other_standard',
                        array(
                            'column'     => '9',
                            'type'       => 'select',
                            'required'   => 1,
                            'options'    => array(
                                array(
                                    'label' => 'Accessory',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Accessory',
                                    'value' => 'Accessory',
                                ),
                            ),
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-on-save-exclude'     => 'false',
                                'data-bv-notempty-message' => 'Please select an Accessory'
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::ACCESSORIES_NON_STANDARD)->addData(
            array(
                'elements' => array(
                    'accessory' => $this->_buildElements('accessory',
                        array(
                            'column'     => '5',
                            'type'       => 'select',
                            'required'   => 1,
                            'options'    => array(
                                array(
                                    'label' => 'Accessory',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Accessory',
                                    'value' => 'Accessory',
                                ),
                            ),
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-on-save-exclude'     => 'true',
                                'data-bv-notempty-message' => 'Please select an Accessory'
                            ),
                        )
                    ),
                    'value'     => $this->_buildElements('value',
                        array(
                            'column'      => '5',
                            'type'        => 'number',
                            'required'    => 1,
                            'placeholder' => 'Value',
                            'attributes'  => array(
                                'data-bv-excluded'     => 'false',
                                'data-on-save-exclude' => 'true',
                                'data-bv-notempty-message' => 'Please input a value'
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::OTHER_INFO)->addData(
            array(
                'elements' => array(
                    'plate_number'                => $this->_buildElements('plate_number',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Plate Number *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z0-9_]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'engine_number'               => $this->_buildElements('engine_number',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 1,
                            'placeholder'    => 'Engine Number *',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,

                            ),
                        )
                    ),
                    'chassis_number'              => $this->_buildElements('chassis_number',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Chassis Number *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'mv_number'                   => $this->_buildElements('mv_number',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            'placeholder'    => 'MV Number *',
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                        )
                    ),
                    'color'                       => $this->_buildElements('color',
                        array(
                            'column'          => '6',
                            'clearfix_before' => 1,
                            'type'            => 'select',
                            'required'        => 1,
                            'placeholder'     => 'Color *',
                            'attributes'      => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                            ),
                            'options'         => array(
                                array(
                                    'label' => 'Color *',
                                    'value' => 'Color'
                                ),
                                array(
                                    'label' => 'Acrylic Red',
                                    'value' => 'Acrylic Red'
                                ),
                                array(
                                    'label' => 'Adobe Gray',
                                    'value' => 'Adobe Gray'
                                ),
                                array(
                                    'label' => 'Alabaster',
                                    'value' => 'Alabaster'
                                ),
                                array(
                                    'label' => 'Alabaster Silver',
                                    'value' => 'Alabaster Silver'
                                ),
                                array(
                                    'label' => 'Alabaster Silver Metalic',
                                    'value' => 'Alabaster Silver Metalic'
                                ),
                                array(
                                    'label' => 'Alaska White',
                                    'value' => 'Alaska White'
                                ),
                                array(
                                    'label' => 'Alegro Green',
                                    'value' => 'Alegro Green'
                                ),
                                array(
                                    'label' => 'Almond Beige',
                                    'value' => 'Almond Beige'
                                ),
                                array(
                                    'label' => 'Almond Blue',
                                    'value' => 'Almond Blue'
                                ),
                                array(
                                    'label' => 'Almond Green',
                                    'value' => 'Almond Green'
                                ),
                            ),
                        )
                    ),
                    'mortgage'                   => $this->_buildElements('mortgage',
                        array(
                            'column'          => '6',
                            'clearfix_before' => 1,
                            'type'            => 'select',
                            'required'        => 0,
                            'placeholder'     => 'Mortgage *',
                            'attributes'      => array(
                                'data-bv-excluded' => 'true',
                            ),
                            'options'         => array(
                                array(
                                    'label' => 'Mortgage',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'BPI Family Bank',
                                    'value' => 'BPI Family Bank'
                                ),
                                array(
                                    'label' => '1st Macro Bank',
                                    'value' => '1st Macro Bank'
                                ),
                                array(
                                    'label' => '1st Valley Bank',
                                    'value' => '1st Valley Bank'
                                ),
                                array(
                                    'label' => '2010 Ventures Entrerprises',
                                    'value' => '2010 Ventures Entrerprises'
                                ),
                            ),
                        )
                    ),
                    'previous_insurance_provider' => $this->_buildElements('previous_insurance_provider',
                        array(
                            'column'          => '6',
                            'clearfix_before' => 1,
                            'type'            => 'select',
                            'required'        => 0,
                            'placeholder'     => 'Previous Insurance Provider',
                            'attributes'      => array(
                                'data-bv-excluded' => 'true',
                            ),
                            'options'         => array(
                                array(
                                    'label' => 'Previous Insurance Provider',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'AFP General Insurance Provider',
                                    'value' => 'AFP General Insurance Provider',
                                ),
                                array(
                                    'label' => 'AIG Philippines Insurance Inc.',
                                    'value' => 'AIG Philippines Insurance Inc.',
                                ),
                                array(
                                    'label' => 'Alliedbanks Insurance Corporation',
                                    'value' => 'Alliedbanks Insurance Corporation',
                                ),
                                array(
                                    'label' => 'Asia Insurance (Philippines) Corporation',
                                    'value' => 'Asia Insurance (Philippines) Corporation',
                                ),
                                array(
                                    'label' => 'Asia United Insurance, Inc.',
                                    'value' => 'Asia United Insurance, Inc.',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::COVERAGE_DETAILS)->addData(
            array(
                'elements' => array(
                    'coveage_details' => $this->_buildElements('coveage_details',
                        array(
                            'column'   => '12',
                            'type'     => 'radios',
                            'required' => 1,
                            'options'  => array(
                                array(
                                    'label' => 'Option 1 - With Acts of Nature: ' . $this->__('Php ') . $this->__(number_format($vehicle_information_gpv, 2)),
                                    'value' => 'Option 1',
                                ),
                                array(
                                    'label' => 'Option 2 - Without Acts of Nature: ' . $this->__('Php ') . $this->__(number_format($vehicle_information_gpv, 2)),
                                    'value' => 'Option 2',
                                ),
                            ),
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPREHENSIVE_POLICY_PERIOD)->addData(
            array(
                'elements' => array(
                    'start_date' => $this->_buildElements('start_date',
                        array(
                            'column'      => '6',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Start Date *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => 'false',
                            ),
                        )
                    ),
                    'start_time' => $this->_buildElements('start_time',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 0,
                            'placeholder'    => '12:00 PM',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'end_date'   => $this->_buildElements('end_date',
                        array(
                            'column'      => '6',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'End Date',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => 'true',
                                'data-date-range-line'     => 'end',
                                'readonly'                 => 'readonly',
                            ),
                        )
                    ),
                    'end_time'   => $this->_buildElements('end_time',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 0,
                            'placeholder'    => '12:00 PM',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::CTPL_POLICY_PERIOD)->addData(
            array(
                'elements' => array(
                    'start_date' => $this->_buildElements('start_date',
                        array(
                            'column'      => '6',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Start Date *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => 'false',
                            ),
                        )
                    ),
                    'start_time' => $this->_buildElements('start_time',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 0,
                            'placeholder'    => '12:00 PM',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                    'end_date'   => $this->_buildElements('end_date',
                        array(
                            'column'      => '6',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'End Date',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => 'true',
                                'data-date-range-line'     => 'end',
                                'readonly'                 => 'readonly',
                            ),
                        )
                    ),
                    'end_time'   => $this->_buildElements('end_time',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 0,
                            'placeholder'    => '12:00 PM',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'readonly' => 'readonly',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::UPLOAD_ORCR)->addData(
            array(
                'elements' => array(
                    'value' => $this->_buildElements('value',
                        array(
                            'column'      => '12',
                            'type'        => 'file',
                            'required'    => 0,
                            'placeholder' => 'Value',
                            'item_note'   => 'Allowed types: PNG, JPG, GIF, BMP, PDF',
                            'attributes'  => array(
                                'data-on-save-exclude' => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

    }
}
