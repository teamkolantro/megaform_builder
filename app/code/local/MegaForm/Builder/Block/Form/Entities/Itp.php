<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Form_Entities_Abstract as FE_Abstract;

/**
 * Form Entities Entities Itp
 *
 * This handles each form as a whole entity form itp
 * Other Example :
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Fire.html">Fire</a>
 * - <a href="../classes/MegaForm_Builder_Block_Form_Entities_Itp.html">Itp</a>
 *
 * This class also dispatch events from abstract parent.
 * Events:
 * - form_builder_render_field_before
 * - form_builder_render_field_after
 * - form_builder_render_element_before
 * - form_builder_render_element_after
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Form_Entities_Itp extends FE_Abstract
{
    /**
     * Contains all data of field sets with elements per fieldset array
     *
     * This variable should be passed to self::$static_form_field_rows
     * for the parent abstract late static bindings
     *
     * Object representation
     * <code>
     * Varien_Object
     * (
     * &nbsp; &nbsp; _data::protected =&gt;
     * &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset1 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset2 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset3 =&gt; Varien_Object(),
     * &nbsp; &nbsp; )
     * )
     * </code>
     *
     * @var Varien_Object
     */
    private $form_field_rows;

    /**
     * Array of fieldsets
     *
     * Field sets is being divided by group in array
     *
     * Array representaion
     * <code>
     * array
     * (
     * &nbsp; &nbsp; &nbsp;array
     * &nbsp; &nbsp; &nbsp;(
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name1,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name2
     * &nbsp; &nbsp; &nbsp;),
     * &nbsp; &nbsp; &nbsp;array
     * &nbsp; &nbsp; &nbsp;(
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name3,
     * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; field_name4
     * &nbsp; &nbsp; &nbsp;)
     * )
     * </code>
     *
     * @var mixed
     */
    public $page_sets = array();

    /**
     * Request Forms
     *
     * Default : 'request_forms'
     *
     * @var string
     */
    const REQUEST_FORMS = 'request_forms';

    /**
     * On Success Url
     *
     * Default : 'on_success_url'
     *
     * @var string
     */
    const ON_SUCCESS_URL = 'on_success_url';

    /**
     * Cover Information
     *
     * For fieldset key
     *
     * Default : 'cover_information'
     *
     * @var string
     */
    const COVER_INFORMATION = 'cover_information';

    /**
     * Individual Personal Information
     *
     * For fieldset key
     *
     * Default : 'individual_personal_information'
     *
     * @var string
     */
    const INDIVIDUAL_PERSONAL_INFORMATION = 'individual_personal_information';

    /**
     * Family Personal Information
     *
     * For fieldset key
     *
     * Default : 'family_personal_information'
     *
     * @var string
     */
    const FAMILY_PERSONAL_INFORMATION = 'family_personal_information';

    /**
     * Group Information
     *
     * For fieldset key
     *
     * Default : 'group_information'
     *
     * @var string
     */
    const GROUP_INFORMATION = 'group_information';

    /**
     * Company Information
     *
     * For fieldset key
     *
     * Default : 'company_information'
     *
     * @var string
     */
    const COMPANY_INFORMATION = 'company_information';

    /**
     * Gruip Contact Person
     *
     * For fieldset key
     *
     * Default : 'group_contact_person'
     *
     * @var string
     */
    const GROUP_CONTACT_PERSON = 'group_contact_person';

    /**
     * Company Contact
     *
     * For fieldset key
     *
     * Default : 'company_contact_person'
     *
     * @var string
     */
    const COMPANY_CONTACT_PERSON = 'company_contact_person';

    /**
     * Company Information
     *
     * For fieldset key
     *
     * Default : 'contact_information'
     *
     * @var string
     */
    const CONTACT_INFORMATION = 'contact_information';

    /**
     * Parent Consent
     *
     * For fieldset key
     *
     * Default : 'parent_consent'
     *
     * @var string
     */
    const PARENT_CONSENT = 'parent_consent';

    /**
     * Travel Information
     *
     * For fieldset key
     *
     * Default : 'travel_information'
     *
     * @var string
     */
    const TRAVEL_INFORMATION = 'travel_information';

    /**
     * Itinerary Options
     *
     * For fieldset key
     *
     * Default : 'itinerary_options'
     *
     * @var string
     */
    const ITINERARY_OPTIONS = 'itinerary_options';

    /**
     * Itinerary Single
     *
     * For fieldset key
     *
     * Default : 'itinerary_single'
     *
     * @var string
     */
    const ITINERARY_SINGLE = 'itinerary_single';

    /**
     * Itinerary Multiple
     *
     * For fieldset key
     *
     * Default : 'itinerary_multiple'
     *
     * @var string
     */
    const ITINERARY_MULTIPLE = 'itinerary_multiple';

    /**
     * Family Members
     *
     * For fieldset key
     *
     * Default : 'family_members'
     *
     * @var string
     */
    const FAMILY_MEMBERS = 'family_members';

    /**
     * Group Members
     *
     * For fieldset key
     *
     * Default : 'group_members'
     *
     * @var string
     */
    const GROUP_MEMBERS = 'group_members';

    /**
     * Company Members
     *
     * For fieldset key
     *
     * Default : 'company_members'
     *
     * @var string
     */
    const COMPANY_MEMBERS = 'company_members';

    /**
     * Package
     *
     * For fieldset key
     *
     * Default : 'package'
     *
     * @var string
     */
    const PACKAGE = 'package';

    /**
     * Promo Code
     *
     * For fieldset key
     *
     * Default : 'promo_code'
     *
     * @var string
     */
    const PROMO_CODE = 'promo_code';

    /**
     * Key Term
     *
     * For fieldset key
     *
     * Default : 'key_term'
     *
     * @var string
     */
    const KEY_TERM = 'key_term';

    /**
     * Save Options
     *
     * For fieldset key
     *
     * Default : 'save_options'
     *
     * @var string
     */
    const SAVE_OPTIONS = 'save_options';

    /**
     * Class contructor...
     *
     * Sets the self::$static_form_field_rows from property form_field_rows
     *
     */
    public function __construct()
    {
        $this->_init();
        /**
         * Late Static binding para magamit ng parent ung entity ng child kahit property pa xa ng parent ganun.
         */
        self::$static_form_field_rows = $this->form_field_rows;

        parent::__construct();
    }

    /**
     * Initiator
     *
     * Intiates and prepare entities and values for form rendering process
     * Calls to methods
     * - <a href="#method_initEntities">_initEntities</a>
     * - <a href="#method_processEntities">_processEntities</a>
     * - <a href="#method_buildFieldItems">_buildFieldItems</a>
     * - <a href="#method_assignPageSets">_initEntities</a>
     *
     */
    public function _init()
    {
        $this->_initEntities();
        $this->_processEntities();
        $this->_buildFieldItems();
        $this->_assignPageSets();
    }

    /**
     * Assign Page Sets
     *
     * This sets the field set in grouped array to property <a href="#property_page_sets">page_sets</a>
     */
    public function _assignPageSets()
    {
        $this->page_sets[] = array(
            self::COVER_INFORMATION,
            self::INDIVIDUAL_PERSONAL_INFORMATION,
            self::FAMILY_PERSONAL_INFORMATION,
            self::GROUP_INFORMATION,
            self::COMPANY_INFORMATION,
            self::GROUP_CONTACT_PERSON,
            self::COMPANY_CONTACT_PERSON,
            self::CONTACT_INFORMATION,
            self::PARENT_CONSENT,
            self::TRAVEL_INFORMATION,
        );

        $this->page_sets[] = array(
            self::ITINERARY_OPTIONS,
            self::ITINERARY_SINGLE,
            self::ITINERARY_MULTIPLE,
            self::FAMILY_MEMBERS,
            self::GROUP_MEMBERS,
            self::COMPANY_MEMBERS,
            self::PACKAGE,
            self::PROMO_CODE,
            self::KEY_TERM,
            self::SAVE_OPTIONS,
        );
    }

    /**
     * Initialize Entities
     *
     * Entities are basically the fieldsets
     *
     * This method sets all fieldsets/entities instantiate
     * each fieldset to be an instance of Varien_Object
     * and sets to <a href="#property_form_field_rows">form_field_rows</a>
     * as whole Varien_Object
     *
     * Data representation
     * <code>
     * Varien_Object
     * (
     * &nbsp; &nbsp; _data::protected =&gt;
     * &nbsp; &nbsp; (
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset1 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset2 =&gt; Varien_Object(),
     * &nbsp; &nbsp; &nbsp; &nbsp; fieldset3 =&gt; Varien_Object(),
     * &nbsp; &nbsp; )
     * )
     * </code>
     *
     */
    public function _initEntities()
    {
        $this->form_field_rows = new Varien_Object(
            array(
                self::REQUEST_FORMS                   => new Varien_Object(),
                self::ON_SUCCESS_URL                  => new Varien_Object(),
                self::COVER_INFORMATION               => new Varien_Object(),
                self::INDIVIDUAL_PERSONAL_INFORMATION => new Varien_Object(),
                self::FAMILY_PERSONAL_INFORMATION     => new Varien_Object(),
                self::GROUP_INFORMATION               => new Varien_Object(),
                self::COMPANY_INFORMATION             => new Varien_Object(),
                self::GROUP_CONTACT_PERSON            => new Varien_Object(),
                self::COMPANY_CONTACT_PERSON          => new Varien_Object(),
                self::CONTACT_INFORMATION             => new Varien_Object(),
                self::PARENT_CONSENT                  => new Varien_Object(),
                self::TRAVEL_INFORMATION              => new Varien_Object(),
                self::ITINERARY_OPTIONS               => new Varien_Object(),
                self::ITINERARY_SINGLE                => new Varien_Object(),
                self::ITINERARY_MULTIPLE              => new Varien_Object(),
                self::FAMILY_MEMBERS                  => new Varien_Object(),
                self::GROUP_MEMBERS                   => new Varien_Object(),
                self::COMPANY_MEMBERS                 => new Varien_Object(),
                self::PACKAGE                         => new Varien_Object(),
                self::PROMO_CODE                      => new Varien_Object(),
                self::KEY_TERM                        => new Varien_Object(),
                self::SAVE_OPTIONS                    => new Varien_Object(),
            )
        );
    }

    /**
     * Process Entities
     *
     * Process all fieldsets attributes
     *
     * This method sets all attributes for every single fieldset
     *
     * Fieldset can contain
     * - Group Type     (Single or Multiple)
     * - title          Field set header text as title
     * - header_info    Field set header info text as a string rendered below fieldset header
     * - attributes     Ay html 5 attrbutes developer may think usefull.
     *
     * Data representation
     * <code>
     * fieldset1 =&gt; Object&nbsp;
     * (
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //If the elements is single
     * &nbsp;&nbsp; &nbsp;or
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //This will group the elements as one fieldset
     * )
     * -&gt;addData
     * (
     * &nbsp;&nbsp; &nbsp;title =&gt; Field set title
     * &nbsp;&nbsp; &nbsp;header_info =&gt; Fieldset info //this will appear below the fieldset title
     * )
     * </code>
     *
     */
    public function _processEntities()
    {
        $this->form_field_rows->getData(self::COVER_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array('title' => 'Please select cover of Insurance')
        );

        $this->form_field_rows->getData(self::INDIVIDUAL_PERSONAL_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'PERSONAL INFORMATION',
                'header_info' => 'Please provide the personal information of the person who will travel.',
                'attributes'  => array(
                    'data-fieldset-group-condition' => "['cover_information-cover_type']",
                    'data-fieldset-group'           => "{'cover_type':['Individual']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::FAMILY_PERSONAL_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'PERSONAL INFORMATION',
                'header_info' => 'Please provide the personal information of the person who will travel.',
                'attributes'  => array(
                    'data-fieldset-group-condition' => "['cover_information-cover_type-group_type']",
                    'data-fieldset-group'           => "{'cover_type':['Group'],'group_type':['Family']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'GROUP INFORMATION',
                'header_info' => "Please provide the information of the members who will be joining the trip.",
                'attributes'  => array(
                    'data-fieldset-group-condition' => "['cover_information-cover_type-group_type']",
                    'data-fieldset-group'           => "{'cover_type':['Group'],'group_type':['Group']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'      => 'COMPANY INFORMATION',
                'attributes' => array(
                    'data-fieldset-group-condition' => "['cover_information-cover_type-group_type']",
                    'data-fieldset-group'           => "{'cover_type':['Group'],'group_type':['Company']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_CONTACT_PERSON)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'CONTACT PERSON',
                'header_info' => 'Please provide the personal information of the contact person of the group. The contact person should be travelling with the group.',
                'attributes'  => array(
                    'data-fieldset-group-condition' => "['cover_information-cover_type-group_type']",
                    'data-fieldset-group'           => "{'cover_type':['Group'],'group_type':['Group']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_CONTACT_PERSON)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'CONTACT PERSON',
                'header_info' => 'Please provide the personal information of the contact person of the group. The contact person should be travelling with the group.',
                'attributes'  => array(
                    'data-fieldset-group-condition' => "['cover_information-cover_type-group_type']",
                    'data-fieldset-group'           => "{'cover_type':['Group'],'group_type':['Company']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::CONTACT_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'CONTACT INFORMATION',
                'header_info' => 'Note: Please provide either Telephone Number or Mobile Number',
            )
        );

        $this->form_field_rows->getData(self::PARENT_CONSENT)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'PARENT/GUARDIAN INFORMATION',
                'header_info' => 'All persons who are 17 years old or younger is required to have a guardian when getting an insurance contract. The guardian should not accompany the insured during the travel. If the guardian will accompany the insured, please choose group, then family.',
            )
        );

        $this->form_field_rows->getData(self::TRAVEL_INFORMATION)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'TRAVEL INFORMATION',
            )
        );

        $this->form_field_rows->getData(self::ITINERARY_OPTIONS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'ITINERARY',
                'header_info' => "Please specify the countries you will travel to and the duration of your trip. \nNote: Please don't include stopovers",
            )
        );

        $this->form_field_rows->getData(self::ITINERARY_SINGLE)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => '',
                'header_info' => "",
            )
        );

        $this->form_field_rows->getData(self::ITINERARY_MULTIPLE)->addData(
            $this->getGroupTypeMultiple()
        )->addData(
            array(
                'title'       => '',
                'header_info' => "",
            )
        );

        $this->form_field_rows->getData(self::FAMILY_MEMBERS)->addData(
            $this->getGroupTypeMultiple()
        )->addData(
            array(
                'title'       => 'FAMILY MEMBERS',
                'header_info' => "Please provide the information of the family members who will be joining the trip. (Maximum of 5 persons with atleast 1 adult)",
                'attributes'  => array(
                    'data-fieldset-group-condition' => "['cover_information-cover_type-group_type']",
                    'data-fieldset-group'           => "{'cover_type':['Group'],'group_type':['Family']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_MEMBERS)->addData(
            $this->getGroupTypeMultiple()
        )->addData(
            array(
                'title'       => 'MEMBERS',
                'header_info' => "Please provide the information of the members who will be joining the trip.",
                'attributes'  => array(
                    'data-fieldset-group-condition' => "['cover_information-cover_type-group_type']",
                    'data-fieldset-group'           => "{'cover_type':['Group'],'group_type':['Group']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_MEMBERS)->addData(
            $this->getGroupTypeMultiple()
        )->addData(
            array(
                'title'       => 'MEMBERS',
                'header_info' => "Please provide the information of the members who will be joining the trip.",
                'attributes'  => array(
                    'data-fieldset-group-condition' => "['cover_information-cover_type-group_type']",
                    'data-fieldset-group'           => "{'cover_type':['Group'],'group_type':['Company']}",
                ),
            )
        );

        $this->form_field_rows->getData(self::PACKAGE)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => 'PACKAGE',
                'header_info' => 'For more information about our International Travel Protect packages, <a href="/products">click here.</a>',
            )
        );

        $this->form_field_rows->getData(self::PROMO_CODE)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => 'Promo Code',
            )
        );

        $this->form_field_rows->getData(self::KEY_TERM)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title' => '',
            )
        );

        $this->form_field_rows->getData(self::SAVE_OPTIONS)->addData(
            $this->getGroupTypeSingle()
        )->addData(
            array(
                'title'       => '',
                'header_info' => '',
            )
        );
    }

    /**
     * Builld Field Items
     *
     * Build all items in each fieldset
     *
     * This method sets add all kinds of elements in each fieldset,
     * whereas each element has an attribute and this method add it to it's respective element.
     *
     * Element can contain
     * - column             Column number in string, please refer to bootstrap, example is if column is 4 then it is col-md-4
     * - type               Element type to be rendered, but this only accepts only if there's an available class entity. Found in \MegaForm\Builder\Block\Adminhtml\Renderer\[Element_Type].php
     * - required           If value is 1 then element will have an attribute of required
     * - placeholder        This is for html palceholder attribute
     * - attributes         Should be a set of array whereas array (attribute-name => attribute-value)
     * - item_note          This is an item note that will be display under an element
     * - options            This is only applicable for select type of elements, and should be in array format
     * - label              This is only applicable for label type of elements, and should be in string format
     * - clearfix_before    If it's value 1 then the element will have a clearfix before it renders
     * - clearfix_after    If it's value 1 then the element will have a clearfix after it renders
     *
     * Data representation
     * <code>
     * fieldset1 =&gt; Object&nbsp;
     * (
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //If the elements is single
     * &nbsp;&nbsp; &nbsp;or
     * &nbsp;&nbsp; &nbsp;$this-&gt;getGroupTypeSingle() //This will group the elements as one fieldset
     * )
     * -&gt;addData
     * (
     * &nbsp;&nbsp; &nbsp;title =&gt; Field set title
     * &nbsp;&nbsp; &nbsp;header_info =&gt; Fieldset info //this will appear below the fieldset title
     * )
     * </code>
     *
     */
    public function _buildFieldItems()
    {
        $this->form_field_rows->getData(self::COVER_INFORMATION)->addData(
            array(
                'elements' => array(
                    'cover_type'  => $this->_buildElements('cover_type',
                        array(
                            'column'  => '12',
                            'type'    => 'radios',
                            'options' => array(
                                array(
                                    'label' => 'Individual',
                                    'value' => 'Individual',
                                ),
                                array(
                                    'label' => 'Group',
                                    'value' => 'Group',
                                ),
                            ),
                        )
                    ),
                    'group_type'  => $this->_buildElements('group_type',
                        array(
                            'column'  => '12',
                            'type'    => 'radios',
                            'options' => array(
                                array(
                                    'label' => 'Family',
                                    'value' => 'Family',
                                ),
                                array(
                                    'label' => 'Group',
                                    'value' => 'Group',
                                ),
                                array(
                                    'label' => 'Company',
                                    'value' => 'Company',
                                ),
                            ),
                        )
                    ),
                    'policy_code' => $this->_buildElements('policy_code',
                        array(
                            'column' => '4',
                            'type'   => 'hidden',
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::INDIVIDUAL_PERSONAL_INFORMATION)->addData(
            array(
                'elements' => array(
                    'first_name'   => $this->_buildElements('first_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'First Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'middle_name'  => $this->_buildElements('middle_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Middle Name',
                            'attributes'  => array(
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'last_name'    => $this->_buildElements('last_name',
                        array(
                            'column'         => '4',
                            'type'           => 'text',
                            'required'       => 1,
                            'placeholder'    => 'Last Name *',
                            'clearfix_after' => '1',
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'birth_date'   => $this->_buildElements('birth_date',
                        array(
                            'column'      => '4',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Date of Birth *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-on-save-exclude'     => 'true',
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'civil_status' => $this->_buildElements('civil_status',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Civil Status *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Single',
                                    'value' => 'Single',
                                ),
                                array(
                                    'label' => 'Married',
                                    'value' => 'Married',
                                ),
                                array(
                                    'label' => 'Separated',
                                    'value' => 'Separated',
                                ),
                                array(
                                    'label' => 'Divorced',
                                    'value' => 'Divorced',
                                ),
                                array(
                                    'label' => 'Widow',
                                    'value' => 'Widow',
                                ),
                            ),
                        )
                    ),
                    'age'          => $this->_buildElements('age',
                        array(
                            'column' => '4',
                            'type'   => 'hidden',
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::FAMILY_PERSONAL_INFORMATION)->addData(
            array(
                'elements' => array(
                    'first_name'   => $this->_buildElements('first_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'First Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'middle_name'  => $this->_buildElements('middle_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Middle Name',
                            'attributes'  => array(
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'false',
                                'pattern'                => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'last_name'    => $this->_buildElements('last_name',
                        array(
                            'column'         => '4',
                            'type'           => 'text',
                            'required'       => 1,
                            'placeholder'    => 'Last Name *',
                            'clearfix_after' => '1',
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                    'birth_date'   => $this->_buildElements('birth_date',
                        array(
                            'column'      => '4',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Date of Birth *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-on-save-exclude'     => 'true',
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'civil_status' => $this->_buildElements('civil_status',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Civil Status *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Married',
                                    'value' => 'Married',
                                ),
                                array(
                                    'label' => 'Separated',
                                    'value' => 'Separated',
                                ),
                                array(
                                    'label' => 'Divorced',
                                    'value' => 'Divorced',
                                ),
                                array(
                                    'label' => 'Widow',
                                    'value' => 'Widow',
                                ),
                            ),
                        )
                    ),
                    'age'          => $this->_buildElements('age',
                        array(
                            'column' => '4',
                            'type'   => 'hidden',
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_INFORMATION)->addData(
            array(
                'elements' => array(
                    'group_name' => $this->_buildElements('group_name',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Group Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_INFORMATION)->addData(
            array(
                'elements' => array(
                    'company_name' => $this->_buildElements('company_name',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Company Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_CONTACT_PERSON)->addData(
            array(
                'elements' => array(
                    'last_name'    => $this->_buildElements('last_name',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'first_name'   => $this->_buildElements('first_name',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            'placeholder'    => 'First Name *',
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'birth_date'   => $this->_buildElements('birth_date',
                        array(
                            'column'      => '6',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Date of Birth *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                //
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-on-save-exclude'     => 'true',
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'civil_status' => $this->_buildElements('civil_status',
                        array(
                            'column'         => '6',
                            'type'           => 'select',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            //custom attributes
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Civil Status *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Single',
                                    'value' => 'Single',
                                ),
                                array(
                                    'label' => 'Married',
                                    'value' => 'Married',
                                ),
                                array(
                                    'label' => 'Separated',
                                    'value' => 'Separated',
                                ),
                                array(
                                    'label' => 'Divorced',
                                    'value' => 'Divorced',
                                ),
                                array(
                                    'label' => 'Widowed',
                                    'value' => 'Widowed',
                                ),
                            ),
                        )
                    ),
                    'designation'  => $this->_buildElements('designation',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Designation',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'age'          => $this->_buildElements('age',
                        array(
                            'column' => '6',
                            'type'   => 'hidden',
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_CONTACT_PERSON)->addData(
            array(
                'elements' => array(
                    'last_name'    => $this->_buildElements('last_name',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'first_name'   => $this->_buildElements('first_name',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            'placeholder'    => 'First Name *',
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'birth_date'   => $this->_buildElements('birth_date',
                        array(
                            'column'      => '6',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Date of Birth *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                //
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-on-save-exclude'     => 'true',
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'civil_status' => $this->_buildElements('civil_status',
                        array(
                            'column'         => '6',
                            'type'           => 'select',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            //custom attributes
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'        => array(
                                array(
                                    'label' => 'Civil Status *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Single',
                                    'value' => 'Single',
                                ),
                                array(
                                    'label' => 'Married',
                                    'value' => 'Married',
                                ),
                                array(
                                    'label' => 'Separated',
                                    'value' => 'Separated',
                                ),
                                array(
                                    'label' => 'Divorced',
                                    'value' => 'Divorced',
                                ),
                                array(
                                    'label' => 'Widowed',
                                    'value' => 'Widowed',
                                ),
                            ),
                        )
                    ),
                    'designation'  => $this->_buildElements('designation',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Designation',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'age'          => $this->_buildElements('age',
                        array(
                            'column' => '6',
                            'type'   => 'hidden',
                        )
                    ),
                ),
            )
        );

        $email_regex_validation = '^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))';

        $this->form_field_rows->getData(self::CONTACT_INFORMATION)->addData(
            array(
                'elements' => array(
                    'email'            => $this->_buildElements('email',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Email Address *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'false',
                                'data-mask-placeholder'    => '',
                                'data-bv-regexp-message'   => 'Please enter a valid email address',
                                'pattern'                  => $email_regex_validation,
                            ),
                        )
                    ),
                    'telephone_number' => $this->_buildElements('telephone_number',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Telephone Number',
                            'item_note'   => "Sample Format: 02-1234567",
                            'attributes'  => array(
                                'data-mask-config'       => '99-9999999',
                                'data-mask-placeholder'  => '',
                                'pattern'                => "^^\(?([0-9]{2})\)?[-. ]?([0-9]{7})$",
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'   => 'true',
                                'data-bv-excluded'       => 'true',
                            ),
                        )
                    ),
                    'mobile_number'    => $this->_buildElements('mobile_number',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Mobile Number *',
                            'item_note'   => "Sample Format: 63-900-1234567",
                            'attributes'  => array(
                                'data-mask-config'         => '99-999-9999999',
                                'data-mask-placeholder'    => '',
                                'data-bv-excluded'         => 'false',
                                'data-bv-message'          => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'pattern'                  => "^^\(?([0-9]{2})\)?[-. ]?([0-9]{3})?[-. ]?([0-9]{7})$",
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::PARENT_CONSENT)->addData(
            array(
                'elements' => array(
                    'last_name'    => $this->_buildElements('last_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'first_name'   => $this->_buildElements('first_name',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'First Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'relationship' => $this->_buildElements('relationship',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Relationship to Insured *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::TRAVEL_INFORMATION)->addData(
            array(
                'elements' => array(
                    'purpose_of_travel' => $this->_buildElements('purpose_of_travel',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Purpose of Travel *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Leisure',
                                    'value' => 'Leisure',
                                ),
                                array(
                                    'label' => 'Office-Based Training / Seminar',
                                    'value' => 'Office-Based Training / Seminar',
                                ),
                                array(
                                    'label' => 'Conference',
                                    'value' => 'Conference',
                                ),
                                array(
                                    'label' => 'Others',
                                    'value' => 'Others',
                                ),
                            ),
                        )
                    ),
                    'purpose_others'    => $this->_buildElements('purpose_others',
                        array(
                            'column'      => '4',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Please Specify *',
                            //custom attributes
                            'attributes'  => array(
                                'data-bv-excluded'       => 'false',
                                'data-on-save-exclude'   => 'true',
                                'pattern'                => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message' => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::ITINERARY_OPTIONS)->addData(
            array(
                'elements' => array(
                    'options'          => $this->_buildElements('options',
                        array(
                            'column'   => '12',
                            'type'     => 'radios',
                            'required' => 0,
                            'options'  => array(
                                array(
                                    'label' => 'Single Trip',
                                    'value' => 'Single Trip',
                                ),
                                array(
                                    'label' => 'Multiple Trip',
                                    'value' => 'Multiple Trip',
                                ),
                            ),
                        )
                    ),
                    'total_no_of_days' => $this->_buildElements('total_no_of_days',
                        array(
                            'column' => '12',
                            'type'   => 'hidden',
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::ITINERARY_SINGLE)->addData(
            array(
                'elements' => array(
                    'destination' => $this->_buildElements('destination',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Destination *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'start_date'  => $this->_buildElements('start_date',
                        array(
                            'column'      => '4',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Departure Date from the Philippines *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                //
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                //date range
                                'data-date-range-id'       => "1",
                                'data-date-range'          => "true",
                                'data-date-range-line'     => "start",
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'end_date'    => $this->_buildElements('end_date',
                        array(
                            'column'      => '4',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Return Date to the Philippines *',
                            //custom attributes
                            'attributes'  => array(
                                'readonly'                 => 'readonly',
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                //
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                //date range
                                'data-date-range-id'       => "1",
                                'data-date-range'          => "true",
                                'data-date-range-line'     => "end",
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'no_of_days'  => $this->_buildElements('no_of_days',
                        array(
                            'column' => '12',
                            'type'   => 'hidden',
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::ITINERARY_MULTIPLE)->addData(
            array(
                'elements' => array(
                    'destination' => $this->_buildElements('destination',
                        array(
                            'column'     => '4',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Destination *',
                                    'value' => '',
                                ),
                            ),
                        )
                    ),
                    'start_date'  => $this->_buildElements('start_date',
                        array(
                            'column'      => '3',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Departure Date from the Philippines *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                //
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                //date range
                                'data-date-range-id'       => "1",
                                'data-date-range'          => "true",
                                'data-date-range-line'     => "start",
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'end_date'    => $this->_buildElements('end_date',
                        array(
                            'column'      => '3',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Return Date to the Philippines *',
                            //custom attributes
                            'attributes'  => array(
                                'readonly'                 => 'readonly',
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                //
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                //date range
                                'data-date-range-id'       => "1",
                                'data-date-range'          => "true",
                                'data-date-range-line'     => "end",
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'no_of_days'  => $this->_buildElements('no_of_days',
                        array(
                            'column' => '12',
                            'type'   => 'hidden',
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::FAMILY_MEMBERS)->addData(
            array(
                'elements' => array(
                    'last_name'    => $this->_buildElements('last_name',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'first_name'   => $this->_buildElements('first_name',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 1,
                            'placeholder'    => 'First Name *',
                            'clearfix_after' => 1,
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'birth_date'   => $this->_buildElements('birth_date',
                        array(
                            'column'      => '5',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Date of Birth *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-on-save-exclude'     => 'true',
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'relationship' => $this->_buildElements('relationship',
                        array(
                            'column'      => '5',
                            'type'        => 'select',
                            'required'    => 1,
                            'placeholder' => 'Relationship *',
                            //custom attributes
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'     => array(
                                array(
                                    'label' => 'Relationship *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Spouse',
                                    'value' => 'Spouse',
                                ),
                                array(
                                    'label' => 'Child',
                                    'value' => 'Child',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::GROUP_MEMBERS)->addData(
            array(
                'elements' => array(
                    'last_name'    => $this->_buildElements('last_name',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'first_name'   => $this->_buildElements('first_name',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            'placeholder'    => 'First Name *',
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'birth_date'   => $this->_buildElements('birth_date',
                        array(
                            'column'      => '4',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Date of Birth *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-on-save-exclude'     => 'true',
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'relationship' => $this->_buildElements('relationship',
                        array(
                            'column'     => '3',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Relationship *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Parent',
                                    'value' => 'Parent',
                                ),
                                array(
                                    'label' => 'Spouse',
                                    'value' => 'Spouse',
                                ),
                                array(
                                    'label' => 'Child',
                                    'value' => 'Child',
                                ),
                                array(
                                    'label' => 'Aunt',
                                    'value' => 'Aunt',
                                ),
                                array(
                                    'label' => 'Uncle',
                                    'value' => 'Uncle',
                                ),
                                array(
                                    'label' => 'Niece',
                                    'value' => 'Niece',
                                ),
                                array(
                                    'label' => 'Nephew',
                                    'value' => 'Nephew',
                                ),
                                array(
                                    'label' => 'Grand Parent',
                                    'value' => 'Grand Parent',
                                ),
                                array(
                                    'label' => 'Grand Child',
                                    'value' => 'Grand Child',
                                ),
                            ),
                        )
                    ),
                    'civil_status' => $this->_buildElements('civil_status',
                        array(
                            'column'     => '3',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Civil Status *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Single',
                                    'value' => 'Single',
                                ),
                                array(
                                    'label' => 'Married',
                                    'value' => 'Married',
                                ),
                                array(
                                    'label' => 'Separated',
                                    'value' => 'Separated',
                                ),
                                array(
                                    'label' => 'Divorced',
                                    'value' => 'Divorced',
                                ),
                                array(
                                    'label' => 'Widowed',
                                    'value' => 'Widowed',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::COMPANY_MEMBERS)->addData(
            array(
                'elements' => array(
                    'last_name'    => $this->_buildElements('last_name',
                        array(
                            'column'      => '6',
                            'type'        => 'text',
                            'required'    => 1,
                            'placeholder' => 'Last Name *',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'first_name'   => $this->_buildElements('first_name',
                        array(
                            'column'         => '6',
                            'type'           => 'text',
                            'required'       => 1,
                            'clearfix_after' => 1,
                            'placeholder'    => 'First Name *',
                            'attributes'     => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'pattern'                  => '^[a-zA-Z ]*$',
                                'data-bv-regexp-message'   => FE_Abstract::DATA_NOT_VALID_FORMAT_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                    'birth_date'   => $this->_buildElements('birth_date',
                        array(
                            'column'      => '4',
                            'type'        => 'date',
                            'required'    => 1,
                            'placeholder' => 'Date of Birth *',
                            //custom attributes
                            'attributes'  => array(
                                // data jquery mask
                                'data-mask-config'         => '9999-99-99',
                                'data-mask-placeholder'    => 'yyyy-mm-dd',
                                // attributes
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-bv-date'             => 'false',
                                'data-bv-date-format'      => FE_Abstract::DATA_BV_DATE_FORMAT,
                                'data-bv-date-message'     => FE_Abstract::DATA_NOT_VALID_MESSAGE,
                                'data-date-range'          => "false",
                                'data-on-save-exclude'     => 'true',
                                'data-birthdate'           => 'true',
                            ),
                        )
                    ),
                    'relationship' => $this->_buildElements('relationship',
                        array(
                            'column'     => '3',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Relationship *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Parent',
                                    'value' => 'Parent',
                                ),
                                array(
                                    'label' => 'Spouse',
                                    'value' => 'Spouse',
                                ),
                                array(
                                    'label' => 'Child',
                                    'value' => 'Child',
                                ),
                                array(
                                    'label' => 'Aunt',
                                    'value' => 'Aunt',
                                ),
                                array(
                                    'label' => 'Uncle',
                                    'value' => 'Uncle',
                                ),
                                array(
                                    'label' => 'Niece',
                                    'value' => 'Niece',
                                ),
                                array(
                                    'label' => 'Nephew',
                                    'value' => 'Nephew',
                                ),
                                array(
                                    'label' => 'Grand Parent',
                                    'value' => 'Grand Parent',
                                ),
                                array(
                                    'label' => 'Grand Child',
                                    'value' => 'Grand Child',
                                ),
                            ),
                        )
                    ),
                    'civil_status' => $this->_buildElements('civil_status',
                        array(
                            'column'     => '3',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Civil Status *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Single',
                                    'value' => 'Single',
                                ),
                                array(
                                    'label' => 'Married',
                                    'value' => 'Married',
                                ),
                                array(
                                    'label' => 'Separated',
                                    'value' => 'Separated',
                                ),
                                array(
                                    'label' => 'Divorced',
                                    'value' => 'Divorced',
                                ),
                                array(
                                    'label' => 'Widowed',
                                    'value' => 'Widowed',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::PACKAGE)->addData(
            array(
                'elements' => array(
                    'package' => $this->_buildElements('package',
                        array(
                            'column'     => '3',
                            'type'       => 'select',
                            'required'   => 1,
                            //custom attributes
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty'         => 'true',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                            'options'    => array(
                                array(
                                    'label' => 'Package *',
                                    'value' => '',
                                ),
                                array(
                                    'label' => 'Silver',
                                    'value' => 'Silver',
                                ),
                                array(
                                    'label' => 'Gold',
                                    'value' => 'Gold',
                                ),
                                array(
                                    'label' => 'Platinum',
                                    'value' => 'Platinum',
                                ),
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::PROMO_CODE)->addData(
            array(
                'elements' => array(
                    'promo_code' => $this->_buildElements('promo_code',
                        array(
                            'column'      => '3',
                            'type'        => 'text',
                            'required'    => 0,
                            'placeholder' => 'Promo Code',
                            'attributes'  => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::KEY_TERM)->addData(
            array(
                'elements' => array(
                    'good_health' => $this->_buildElements('good_health',
                        array(
                            'column'     => '12',
                            'type'       => 'checkbox',
                            'required'   => 1,
                            'label'      => 'I am/we are in good health, free from physical impairment or deformity, and I/am are not travelling to receive medical treatment.',
                            'attributes' => array(
                                'data-bv-excluded'         => 'false',
                                'data-bv-notempty-message' => FE_Abstract::NOT_EMPTY_MESSAGE,
                                'data-on-save-exclude'     => 'true',
                            ),
                        )
                    ),
                ),
            )
        );

        $this->form_field_rows->getData(self::SAVE_OPTIONS)->addData(
            array(
                'elements' => array(
                    'info' => $this->_buildElements('info',
                        array(
                            'column' => '12',
                            'type'   => 'label',
                            'label'  => 'If you want to save your information to get a quote for at a later date, click save.',
                        )
                    ),
                ),
            )
        );
    }
}
