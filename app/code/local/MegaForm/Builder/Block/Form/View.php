<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Helper_Data as CURR_HELPER;
use MegaForm_Builder_Model_Request_Forms as REQUEST_FORMS_MODEL;
use MegaForm_Builder_Model_Request_Form_Records as REQUEST_FORM_RECORDS_MODEL;
use MegaForm_Builder_Model_Request_Session_Completed as REQUEST_SESSION_COMPLETED_MODEL;

/**
 * Block Form View
 *
 * This class handles the rendering of the whole form,
 * thus also handles the rendring of the form when used in widget.
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Form_View extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{

    /**
     * Form Block
     *
     * This gets an object block depends on what form code type
     *
     * @var mixed
     */
    public $form_block;

    /**
     * Form Code
     *
     * This contains form code that is necessary for what form to be rendered.
     *
     * Example:
     * - motor
     * - itp
     * - fire
     * - personalInfo
     * - personalInfoItp
     * - vehicleInfo
     *
     * @var string
     */
    public $form_code;

    /**
     * Class contructor...
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get Form Block
     *
     * Returns the property <a href="#property_form_block">form_block</a>
     *
     * @return mixed Object Block Entity of the form
     */
    public function getFormBlock()
    {
        return $this->form_block;
    }

    /**
     * Get Form Block Built
     *
     * This method is similar to magento block _toHtml(),
     * it returns the form in html string.
     *
     * @return string Form html
     */
    public function getFormBlockBuilt()
    {
        if ($this->getFormBlock())
        {
            return $this->getFormBlock()->getBuilt();
        }
    }

    /**
     * Initialize
     *
     * This method creates a block depending on the form_code,
     *
     * Form Code must be precent in the url query
     * <code>
     *  http://example.com/cms-page1/form_code/itp
     *  http://example.com/modulename/controllername/actionname/form_code/itp
     *  http://example.com/cms-page1?form_code=itp
     *  http://example.com/modulename/controllername/actionname/?form_code=itp
     * </code>
     *
     * Also loads the data of the recednt form if form_key is present.
     * <code>
     *  http://example.com/cms-page1/fofrm_key/as9dn8sam9dsa9dsa8mdsa98dmas98
     *  http://example.com/modulename/controllername/actionname/fofrm_key/as9dn8sam9dsa9dsa8mdsa98dmas98
     *  http://example.com/cms-page1?fofrm_key=as9dn8sam9dsa9dsa8mdsa98dmas98
     *  http://example.com/modulename/controllername/actionname/?fofrm_key=as9dn8sam9dsa9dsa8mdsa98dmas98
     * </code>
     *
     * @return string Form html
     */
    public function _inits()
    {

        $this->form_code = $this->getData('form_code');

        if (!empty(Mage::app()->getRequest()->getParam('form_code')))
        {
            $this->form_code = $this->getRequest()->getParam('form_code');
        }

        $possible_next_count = 0;

        // if($this->getData('form_code') && (!empty(Mage::app()->getRequest()->getParam('previous_form_key'))) )
        // {
        //     $possible_next = Mage::helper(CURR_HELPER::HELPER_KEY)->getDatasByFormKeyPossible($this->getData('form_code') , Mage::app()->getRequest()->getParam('previous_form_key'));
        //     $possible_next_count = $possible_next->getSize();
        //     $form_json_datas = $possible_next->getLastItem()->getData('form_json_datas');
        //     $form_json_datas = json_decode($form_json_datas);
        //     $form_json_datas = Mage::helper(CURR_HELPER::HELPER_KEY)->objToArray($form_json_datas);

        //     if(isset($form_json_datas['form_key']) && !empty($form_json_datas['form_key']))
        //     {
        //         $this->getRequest()->setParam('form_key' , $form_json_datas['form_key']);
        //     }
        //     else
        //     {
        //         $possible_next_count = 0;
        //     }
        // }

        if ($this->form_code && (empty(Mage::app()->getRequest()->getParam('form_key'))) && $possible_next_count == 0)
        {
            // $this->form_block = Mage::getModel('builder/form_entities_' . $this->form_code);
            $this->form_block = Mage::app()->getLayout()->createBlock('builder/form_entities_' . $this->form_code);
            return $this;
        }

        $REQUEST_SESSION_COMPLETED_MODEL = Mage::getModel(REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL . "/" . REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL_ENTITY)
            ->getCollection()
            ->addFieldToFilter('form_key', $this->getRequest()->getParam('form_key'));

        $REQUEST_SESSION_COMPLETED_MODEL
            ->getSelect()
            ->joinLeft(
                array('table2' => Mage::getModel('core/resource')->getTableName(REQUEST_FORM_RECORDS_MODEL::MODULE_MODEL . "/" . REQUEST_FORM_RECORDS_MODEL::MODULE_MODEL_ENTITY)),
                'table2.request_session_completed_id = main_table.id', array('table2.form_json_datas')
            )
            ->joinLeft(
                array('table3' => Mage::getModel('core/resource')->getTableName(REQUEST_FORMS_MODEL::MODULE_MODEL . "/" . REQUEST_FORMS_MODEL::MODULE_MODEL_ENTITY)),
                'table3.id = main_table.request_form_id', array('table3.form_code')
            );

        $REQUEST_SESSION_COMPLETED_MODEL = $REQUEST_SESSION_COMPLETED_MODEL->getLastItem();
        $REQUEST_SESSION_COMPLETED_MODEL->addData(array('decoded_form_json_datas' => json_decode($REQUEST_SESSION_COMPLETED_MODEL->getData('form_json_datas'))));

        Mage::getSingleton(REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL . "/" . REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL_ENTITY)->setData($REQUEST_SESSION_COMPLETED_MODEL->getData());

        if ($this->getFormDatas()->getFormCode())
        {
            // $this->form_block = Mage::getModel('builder/form_entities_' . $this->getFormDatas()->getFormCode());
            $this->form_code = $this->getFormDatas()->getFormCode();
            $this->form_block = Mage::app()->getLayout()->createBlock('builder/form_entities_' . $this->getFormDatas()->getFormCode());
            return $this;
        }
    }

    /**
     * Produce forms list rendered as html
     *
     * This methods creates a block from another,
     * thens sets the form_code and the data of the form if theres any
     * then return its html string.
     *
     * @return string|void Will not return if there is no form block created.
     */
    public function _toHtml()
    {
        $this->_inits();

        if ($this->getFormBlock())
        {
            $this->addData(array('form_code' => $this->form_code));
            $this->getFormBlock()->setData($this->getData());
            return $this->getFormBlock()->_toHtml();
        }
    }

    /**
     * Get Form Datas
     *
     * This returns the singleton of the REQUEST_SESSION_COMPLETED_MODEL if theres any
     *
     * @return mixed Singleton of the REQUEST_SESSION_COMPLETED_MODEL
     */
    public function getFormDatas()
    {

        return Mage::getSingleton(REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL . "/" . REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL_ENTITY);
    }

    /**
     * Get Config Enabled
     *
     * This returns a boolean result depending on the module registry config enable key
     *
     * @return boolean Result from module registry config
     */
    public function getConfigEnabled()
    {
        return Mage::helper(CURR_HELPER::HELPER_KEY)->getConfigEnabled();
    }

    /**
     * Get Form Code
     *
     * This returns the property <a href="#property_form_code"></a>
     *
     * @return string form code
     */
    public function getFormCode()
    {
        return $this->form_code;
    }
}
