<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Adminhtml_Forms as BASE_BLOCK;

/**
 * Admin Form Edit Container
 *
 * This renders the edit form container before tabs
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Adminhtml_Forms_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Class constructor
     */
    public function __construct()
    {

        parent::__construct();
        $this->_objectId   = "id";
        $this->_blockGroup = BASE_BLOCK::BLOCK_GROUP;
        $this->_controller = "adminhtml_" . BASE_BLOCK::BLOCK_CONTROLLER;
        $this->_updateButton("save", "label", Mage::helper("adminhtml")->__("Save Item"));
        $this->_updateButton("delete", "label", Mage::helper("adminhtml")->__("Delete Item"));
        $this->_removeButton("delete");

        $this->_addButton("saveandcontinue", array(
            "label"   => Mage::helper("adminhtml")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class"   => "save",
        ), -100);

        $this->_formScripts[] = "
        function toggleEditor() {
            if (tinyMCE.getInstanceById('block_content') == null) {
                tinyMCE.execCommand('mceAddControl', false, 'block_content');
            } else {
                tinyMCE.execCommand('mceRemoveControl', false, 'block_content');
            }
        }

        function saveAndContinueEdit(){
            editForm.submit($('edit_form').action+'back/edit/');
        }
        ";
    }

    /**
     * Getting the custom text header
     *
     * @return string The text to appear in header
     */
    public function getHeaderText()
    {
        if (Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data") && Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data")->getId())
        {
            return Mage::helper("adminhtml")->__("Edit '%s'", $this->htmlEscape(Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data")->getName()));
        }
        else
        {
            return Mage::helper("adminhtml")->__("Add Item");
        }
    }

}
