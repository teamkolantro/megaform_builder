<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Adminhtml_Forms as BASE_BLOCK;
use MegaForm_Builder_Helper_Data as CURR_HELPER;

/**
 * Admin Form Edit Tabs
 *
 * This renders the edit form tabs
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Adminhtml_Forms_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId(strtolower(BASE_BLOCK::BLOCK_CONTROLLER) . "_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("adminhtml")->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . BASE_BLOCK::CURRENT_BLOCK_ITEM_SINGLE . " Information"));
    }

    /**
     * Adding tab contents before html renders
     *
     * Tab definitions:
     * <ul>
     *   <li>form_products_section in Block Class <a href="../classes/Megaform_Builder_Block_Adminhtml_Forms_Edit_Tab_Products_Grid.html">Megaform_Builder_Block_Adminhtml_Forms_Edit_Tab_Products_Grid</a></li>
     *   <li>form_pdf_template in Block Class <a href="../classes/Megaform_Builder_Block_Adminhtml_Forms_Edit_Tab_Pdf_Template.html">Megaform_Builder_Block_Adminhtml_Forms_Edit_Tab_Pdf_Template</a></li>
     *   <li>form_email_template in Block Class <a href="../classes/Megaform_Builder_Block_Adminhtml_Forms_Edit_Tab_Email_Template.html">Megaform_Builder_Block_Adminhtml_Forms_Edit_Tab_Email_Template</a></li>
     *   <li>form_summary_template in Block Class <a href="../classes/Megaform_Builder_Block_Adminhtml_Forms_Edit_Tab_Summary_Template.html">Megaform_Builder_Block_Adminhtml_Forms_Edit_Tab_Summary_Template</a></li>
     * </ul>
     *
     * @return MegaForm_Builder_Block_Adminhtml_Forms_Edit_Tabs
     */
    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label"   => Mage::helper("adminhtml")->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . BASE_BLOCK::CURRENT_BLOCK_ITEM_SINGLE . " Information"),
            "title"   => Mage::helper("adminhtml")->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . BASE_BLOCK::CURRENT_BLOCK_ITEM_SINGLE . " Information"),
            "content" => $this->getLayout()->createBlock(BASE_BLOCK::BLOCK_GROUP . "/adminhtml_" . BASE_BLOCK::BLOCK_CONTROLLER . "_edit_tab_form")->toHtml(),
        ));

        $this->addTab('form_products_section', array(
            'label'   => Mage::helper('adminhtml')->__('Form Products'),
            'title'   => Mage::helper('adminhtml')->__('Form Products'),
            'content' => $this->getLayout()->createBlock('builder/adminhtml_forms_edit_tab_products_grid')->toHtml(),
        ));

        $this->addTab('form_pdf_template', array(
            'label'   => Mage::helper('adminhtml')->__('Form Pdf Template'),
            'title'   => Mage::helper('adminhtml')->__('Form Pdf Template'),
            'content' => $this->getLayout()->createBlock('builder/adminhtml_forms_edit_tab_pdf_template')->toHtml(),
        ));

        $this->addTab('form_email_template', array(
            'label'   => Mage::helper('adminhtml')->__('Form Email Template'),
            'title'   => Mage::helper('adminhtml')->__('Form Email Template'),
            'content' => $this->getLayout()->createBlock('builder/adminhtml_forms_edit_tab_email_template')->toHtml(),
        ));

        $this->addTab('form_summary_template', array(
            'label'   => Mage::helper('adminhtml')->__('Form Summary Template'),
            'title'   => Mage::helper('adminhtml')->__('Form Summary Template'),
            'content' => $this->getLayout()->createBlock('builder/adminhtml_forms_edit_tab_summary_template')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}
