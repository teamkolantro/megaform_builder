<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Adminhtml_Forms as BASE_BLOCK;
use MegaForm_Builder_Helper_Data as CURR_HELPER;
use MegaForm_Builder_Model_Request_Forms as CURR_MODEL;

/**
 * Admin Form Edit
 *
 * This renders the edit form for admin template
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Adminhtml_Forms_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Adding form elements for editing attribute
     *
     * @return MegaForm_Builder_Block_Adminhtml_Forms_Edit_Tab_Form
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset(strtolower(BASE_BLOCK::BLOCK_CONTROLLER) . '_form', array('legend' => Mage::helper('adminhtml')->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . BASE_BLOCK::CURRENT_BLOCK_ITEM_SINGLE . ' Information')));

        $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('adminhtml')->__('Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'request_forms[name]',
            'readonly'  => 'readonly',
            'maxlength' => 250,
        ));

        $fieldset->addField('form_code', 'text', array(
            'label'     => Mage::helper('adminhtml')->__('Form Code'),
            'class'     => 'required-entry',
            'required'  => true,
            'readonly'  => 'readonly',
            'name'      => 'request_forms[form_code]',
            'maxlength' => 250,
        ));

        // $fieldset->addField('image', 'image', array(
        //     'name'     => 'image',
        //     'class'    => 'required-entry required-file',
        //     'label'    => Mage::helper('adminhtml')->__('Item Image'),
        //     'required' => true,
        //     'note'     => '(*.jpg, *.png, *.gif)',
        // ));

        // $configSettings = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
        //     array(
        //         'add_widgets'              => false,
        //         'add_variables'            => false,
        //         'add_images'               => false,
        //         'files_browser_window_url' => $this->getBaseUrl() . 'admin/cms_wysiwyg_images/index/',
        //     ));

        // $fieldset->addField('short_description', 'editor', array(
        //     'name'     => 'short_description',
        //     'label'    => Mage::helper('adminhtml')->__('Short Description'),
        //     'title'    => Mage::helper('adminhtml')->__('Short Description'),
        //     'style'    => 'height:36em;width:50em',
        //     'required' => true,
        //     'config'   => $configSettings,
        //     'wysiwyg'  => true,
        // ));

        // $fieldset->addField('sort', 'text', array(
        //     'label' => Mage::helper('adminhtml')->__('Sort Order'),
        //     'name'  => 'sort',
        //     'type' => 'hidden',
        //     'class' => 'hide hidden',
        // ));

        // $fieldset->addField('status', 'select', array(
        //     'label'  => Mage::helper('adminhtml')->__('Status'),
        //     'values' => CURR_HELPER::getStatusOptionArray(),
        //     'name'   => 'request_forms[status]',
        // ));

        if (Mage::getSingleton('adminhtml/session')->{"get" . ucwords(BASE_BLOCK::SESSION_FORM_DATA) . "Data"}())
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->{"get" . ucwords(BASE_BLOCK::SESSION_FORM_DATA) . "Data"}());
            Mage::getSingleton('adminhtml/session')->{"set" . ucwords(BASE_BLOCK::SESSION_FORM_DATA) . "Data"}(null);
        }
        elseif (Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data"))
        {
            $form->setValues(Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data")->getData());
            // set sort order
            if (empty(Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data")->getData()))
            {
                $data['sort'] = !empty(Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)->getMaxSortPosition()) ? Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)->getMaxSortPosition() : 1;

                if (!empty($data))
                {
                    $form->setValues($data);
                }
            }
        }

        // fn_mrk(
        //     Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data")->getData()
        // );

        /**
         * Status enabled default value on create
         */
        if (!$this->getRequest()->getParam("id"))
        {
            $form->setValues(array('status' => 1));
        }
        return parent::_prepareForm();
    }
}
