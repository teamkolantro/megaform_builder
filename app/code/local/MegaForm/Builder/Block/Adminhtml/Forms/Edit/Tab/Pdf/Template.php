<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Block_Adminhtml_Forms as BASE_BLOCK;
use MegaForm_Builder_Helper_Data as CURR_HELPER;
use MegaForm_Builder_Model_Request_Pdf_Templates as CURR_MODEL;

/**
 * Admin TabEdit Tab Form Pdf Template
 *
 * This renders the edit form for admin template
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class Megaform_Builder_Block_Adminhtml_Forms_Edit_Tab_Pdf_Template extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('form_pdf_template');
    }

    /**
     * Adding form elements for editing attribute
     *
     * @return Megaform_Builder_Block_Adminhtml_Forms_Edit_Tab_Pdf_Template
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset(strtolower(BASE_BLOCK::BLOCK_CONTROLLER) . '_form', array('legend' => Mage::helper('adminhtml')->__(Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . BASE_BLOCK::CURRENT_BLOCK_ITEM_SINGLE . ' Information')));

        $pdf_configSettings = Mage::getSingleton('cms/wysiwyg_config')->getConfig();

        $pdf_configSettings->addData(array(
            'add_variables'            => true,
            'add_directives'           => true,
            'encode_directives'        => true,
            'plugins'                  => array(),
            'widget_window_url'        => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/widget/index'),
            'directives_url'           => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
            'directives_url_quoted'    => preg_quote(Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive')),
            'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
        ));

        $fieldset->addField('pdf_template', 'editor', array(
            'name'     => 'form_pdf_templates[pdf_template]',
            'label'    => Mage::helper('adminhtml')->__('Template'),
            'title'    => Mage::helper('adminhtml')->__('Template'),
            'style'    => 'height:36em;width:100em',
            'required' => true,
            'config'   => $pdf_configSettings,
            'wysiwyg'  => true,
        ));

        if (Mage::getSingleton('adminhtml/session')->{"get" . ucwords(BASE_BLOCK::SESSION_FORM_DATA) . "Data"}('pdf_template'))
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->{"get" . ucwords(BASE_BLOCK::SESSION_FORM_DATA) . "Data"}('pdf_template')->getData());
            Mage::getSingleton('adminhtml/session')->{"set" . ucwords(BASE_BLOCK::SESSION_FORM_DATA) . "Data"}(null);
        }
        elseif (Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data"))
        {
            if (Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data")->getData('pdf_template'))
            {
                $form->setValues(Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data")->getData('pdf_template')->getData());
                // set sort order
                if (empty(Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data")->getData('pdf_template')->getData()))
                {
                    $data['sort'] = !empty(Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)->getMaxSortPosition()) ? Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)->getMaxSortPosition() : 1;

                    if (!empty($data))
                    {
                        $form->setValues($data);
                    }
                }
            }
        }

        // fn_mrk(
        //     Mage::registry(BASE_BLOCK::SESSION_FORM_DATA . "_data")->getData()
        // );

        /**
         * Status enabled default value on create
         */
        if (!$this->getRequest()->getParam("id"))
        {
            $form->setValues(array('status' => 1));
        }
        return parent::_prepareForm();
    }
}
