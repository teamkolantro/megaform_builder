<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Helper_Data as CURR_HELPER;

/**
 * Admin Block Forms
 *
 * This display admin grid for list of forms
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Adminhtml_Forms extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * The block controller class
     * @var string
     */
    const BLOCK_CONTROLLER = "forms"; // always camelCase dude depending on controller name

    /**
     * The item name in plural
     * @var string
     */
    const CURRENT_BLOCK_ITEM_PLURAL = "Forms"; // always ProperCase dude depending on controller name

    /**
     * The item name in singular
     * @var string
     */
    const CURRENT_BLOCK_ITEM_SINGLE = "Form"; // always ProperCase dude depending on controller name

    /**
     * The block group
     * @var string
     */
    const BLOCK_GROUP = "builder";

    /**
     * The session form data key for data retrieving
     * @var string
     */
    const SESSION_FORM_DATA = "recent";

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->_blockGroup     = self::BLOCK_GROUP;
        $this->_controller     = "adminhtml_" . self::BLOCK_CONTROLLER;
        $this->_headerText     = Mage::helper("adminhtml")->__("Manage " . Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . self::CURRENT_BLOCK_ITEM_PLURAL);
        $this->_addButtonLabel = Mage::helper("adminhtml")->__("Add New " . Mage::helper(CURR_HELPER::HELPER_KEY)->getModuleTitle() . " " . self::CURRENT_BLOCK_ITEM_PLURAL);

        parent::__construct();
    }

    /**
     * Before the layout renders its the prepare layout to be triggered first.
     *
     * This only removes the add button
     *
     * @return MegaForm_Builder_Block_Adminhtml_Forms
     */
    protected function _prepareLayout()
    {
        $this->_removeButton("add");
        return parent::_prepareLayout();
    }

}
