<?php
class MegaForm_Builder_Block_Adminhtml_Quoterequest_Grid extends Mage_Adminhtml_Block_Widget_Grid {
  public function __construct() {
      parent::__construct();
      //$this->setTemplate('travelinsurance/template/widget/gridtravelcoverage.phtml');
      $this->setTemplate('quoterequest/template/widget/quoterequestgrid.phtml');
      $this->setId('quoterequestGrid');
      $this->setDefaultSort('id');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
      //$this->setUseAjax(false);
      //$this->setCountTotals(true);
      //$this->setPagerVisibility(false);
      //$this->setNoFilterMassactionColumn(true);
  }

  protected function _prepareCollection() {
      $quoterequestModel = Mage::getModel('builder/request_session_completed')->getCollection();  //builder/request_form_records
      //echo  'here '.Mage::helper('globalfunction')->sendITPPolicy();
      //$collection = Mage::getModel('travelinsurance/travelconfig')->getCollection();
      $collection = $quoterequestModel;
      /*
      $collection->join(array('completed' => 'builder/request_session_completed'), 'main_table.request_session_completed_id=completed.id', array('completed.request_pdf_files_id as pdf_files_id'))
                 ->addFieldToSelect(array('request_session_completed_id', 'date_created', 'date_modified'));
                 //->addFieldToFilter('user_type', array('neq'=>'report_type'));
      */
      $_lnk = Mage::getBaseUrl();
      $collection->join(array('completed' => 'builder/request_form_records'), 'main_table.id=completed.request_session_completed_id', array('form_json_datas', 'id as completed_id', 'request_session_completed_id as request_session_completed_id', 'date_created', 'date_modified'))
                 ->addFieldToSelect(array('request_pdf_files_id'));
                 //->addFieldToFilter('user_type', array('neq'=>'report_type'));
      $collection->getSelect()->columns('json_extract_c(form_json_datas, \'color\') AS color,
      json_extract_c(form_json_datas, \'form_code\') AS product,
      json_extract_c(form_json_datas, \'policy_code\') AS policy_code,
      json_extract_c(form_json_datas, \'middle_name\') AS midname,
      json_extract_c(form_json_datas, \'last_name\') AS last_name,
      json_extract_c(form_json_datas, \'telephone_number\') AS telephone,
      json_extract_c(form_json_datas, \'mobile_number\') AS mobile,
      json_extract_c(form_json_datas, \'province\') AS province,
      json_extract_c(form_json_datas, \'city\') AS city,
      json_extract_c(form_json_datas, \'barangay\') AS barangay,
      json_extract_c(form_json_datas, \'first_name\') AS first_name,
      json_extract_c(form_json_datas, \'email\') AS email,
      IF(json_extract_c(form_json_datas, \'for_rent\')=\'on\',\'Yes\',\'No\') AS for_rent,
      json_extract_c(form_json_datas, \'type_insurance\') AS typeofinsurance,
        CASE json_extract_c(form_json_datas, \'type_insurance\')
        WHEN \'CTPL\' THEN \'\'
        ELSE (select FORMAT(value,2) from ucpb_aupapremium aupa where (aupa.code= json_extract_c(form_json_datas, \'bodily_injury\')) limit 1)
        END as bodily_injury,
      json_extract_c(form_json_datas, \'year_model.label\') AS year,
      json_extract_c(form_json_datas, \'brand.label\') AS brand,
      json_extract_c(form_json_datas, \'model_name.label\') AS model,
      json_extract_c(form_json_datas, \'remarks_status\') AS remarks_status,
      json_extract_c(form_json_datas, \'cover_type\') AS cover_type,
      json_extract_c(form_json_datas, \'birth_date\') AS birth_date,
      json_extract_c(form_json_datas, \'civil_status\') AS civil_status,
      json_extract_c(form_json_datas, \'relationship\') AS relationship,
      json_extract_c(form_json_datas, \'purpose_others\') AS purpose_others,
      json_extract_c(form_json_datas, \'purpose_of_travel\') AS purpose_of_travel,
      json_extract_c(form_json_datas, \'options\') AS itinerarytype,
      (select countryname from ucpb_travelregion region where (region.id= json_extract_c(form_json_datas, \'destination\')) limit 1) as destination,

      json_extract_c(form_json_datas, \'start_date\') AS start_date,
      json_extract_c(form_json_datas, \'end_date\') AS end_date,
      json_extract_c(form_json_datas, \'package\') AS package,

      (select code from salesrule_coupon s where (s.usage_limit=s.times_used) and (s.email= json_extract_c(form_json_datas, \'email\')) limit 1) as promo_code,
      `completed`.`date_created` as date_formatted,
      (select increment_id from sales_flat_order ordergrid where (ordergrid.quote_id= json_extract_c(form_json_datas, \'quote_entity_id\')) limit 1) as order_id,
      (select url from megaform_builder_request_pdf_files pdf where (pdf.id=main_table.request_pdf_files_id) order by pdf.id DESC limit 1) as formal_quote
      ');
      //form_json_datas
      //json_extract_c(form_json_datas, \'destination\') AS destination,
      //DATE_FORMAT(`completed`.`date_created`, "%c/%e%/%y %I:%i %p") as date_formatted,
      //json_extract_c(form_json_datas, \'quote_entity_id\') AS order_id
      //IF(json_extract_c(session_result_json_datas, \'bodily_injury\')=\'Bodily Injury\',\'Yes\',\'No\') AS bodily_injury,
      //json_extract_c(form_json_datas, \'promo_code\') AS promo_code
      //->where("json_extract_c(form_json_datas, 'form_code') in ('vehicleinfo','motor','itp')"); //->limit(500);  //CASE isschengen WHEN 1 THEN 'Yes' ELSE 'No' END as
      //'id as completed_id', 'request_session_completed_id as request_session_completed_id',
      //$collection->load(true);
      //json_extract_c(form_json_datas, \'for_rent\') AS for_rent
      $report_type = Mage::helper('globalfunction')->getfilterQuoteproduct('name','_report|type','record_type');
      $itpreport_type = Mage::helper('globalfunction')->getfilterQuoteproduct('name','_itpreport|type','record_type');
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      switch($report_type) {
        case 'itp':
          $collection->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('itp')) ");
          //------------------------------------------------------------------------
          switch($itpreport_type) {
            case 'individual':
              $collection->getSelect()->where("(json_extract_c(form_json_datas, 'cover_type') in('individual')) ");
            break;
            default;
              $collection->getSelect()->where("(json_extract_c(form_json_datas, 'cover_type') not in('individual')) ");
            break;
          }
          //------------------------------------------------------------------------
        break;
        case 'fire':
          $collection->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('fire')) ");
        break;
        default;
          $_show = '';//$rateModel->addFieldToFilter('code', array('neq'=>'report_type'));
          $collection->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
        break;
      }
      //$collection->load(true);//exit;
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }
  /*
  protected function _filterDateCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'bodily_injury') like '%$value%') ");
      }

      return $this;
  }
  */


  protected function _filtercivil_statusCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'civil_status') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterbirth_dateCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'birth_date') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterCovertypeCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'cover_type') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterBICallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'bodily_injury') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterEmailCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'email') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterFNameCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'first_name') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterMNameCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'middle_name') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterLNameCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'last_name') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterphoneCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'telephone_number') like '%$value%') ");
      }

      return $this;
  }

  protected function _filtermobileCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'mobile_number') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterprovinceCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'province') like '%$value%') ");
      }

      return $this;
  }

  protected function _filtercityCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'city') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterbarangayCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'barangay') like '%$value%') ");
      }

      return $this;
  }

  protected function _filtervehicleusedCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'for_rent') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterYearCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'year_model.label') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterBrandCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'brand.label') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterModelCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'model_name.label') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterCodeCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'model_name.label') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterRemarksCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'remarks_status') like '%$value%') ");
      }

      return $this;
  }

  protected function _filterPolicyCallback($collection, $column) {
      if (!$value = $column->getFilter()->getValue()) {
          return $this;
      }
      if (empty($value)) {
          //$this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'form_code') in('motor')) ");
      }
      else {
          echo $column;
          $this->getCollection()->getSelect()->where("(json_extract_c(form_json_datas, 'policy_code') like '%$value%') ");
      }

      return $this;
  }
    //--------------------------------------------------------------------------
    public function GUIDEcallback_skus($value, $row, $column, $isExport) {
      $increment_id = $value;
      $_order = Mage::getModel('sales/order')->loadByIncrementId($increment_id);
      $_items = $_order->getAllItems();
      $skus="";
      foreach ($_items as $item) {
      $skus .= $item->getSku().", "; //<br/>
      }
      $skus = chop($skus,", ");
      return $skus;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_last_name($value, $row, $column, $isExport) {
      $id = $value;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      // $result = Mage::helper('builder')->objToArray($result);
      //return $result->parent_consent[1]->last_name;
      //return $result->parent_consent[0];
      //echo $result->itinerary_options->total_no_of_days;
      $id = $result->parent_consent->last_name;
      //return $info;
      //fn_mrk($info);
      //echo '<pre>';var_dump($result);echo'</pre>';
      return $id;
      //return $info; //'here '.$result->itinerary_options->total_no_of_days;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_first_name($value, $row, $column, $isExport) {
      $id = $value;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->parent_consent->first_name;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_cover_type($value, $row, $column, $isExport) {
      $id = $value;
      $itpreport_type = Mage::helper('globalfunction')->getfilterQuoteproduct('name','_itpreport|type','record_type');
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      switch($itpreport_type) {
        case 'company':
         //$id = $id. '-Company';
         $id = 'Company';
        break;
        case 'family':
         //$id = $id. '-Family';
         $id = 'Family';
        break;
      }
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_destination($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $destination = $result->itinerary_options->options;
        if (stripos($destination, 'multiple') !== false) {
          $ids = $result->itinerary_multiple->destination;
          /*
          echo '<pre>';var_dump($ids);echo'</pre>';
          foreach ($ids as $id) {
          $dests .= $id['destination']."<br/>";
          }
          */
          foreach($ids as $key => $value)
          {
            //echo $value[$key]->Service . "<br>";
            $value =  Mage::helper('travelinsurance')->getTravelRegioninfo('id',$value,'countryname');
            $dests .= $value.', '; //<br/>
          }
          $dests = chop($dests,", ");
        }
        else {
          //individual
          $id = $result->itinerary_single->destination;
          $value =  Mage::helper('travelinsurance')->getTravelRegioninfo('id',$id,'countryname');
          $dests = $value;
        }
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_sdate($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $destination = $result->itinerary_options->options;
        if (stripos($destination, 'multiple') !== false) {
          $ids = $result->itinerary_multiple->start_date;
          /*
          echo '<pre>';var_dump($ids);echo'</pre>';
          foreach ($ids as $id) {
          $dests .= $id['destination']."<br/>";
          }
          */
          foreach($ids as $key => $value)
          {
            //echo $value[$key]->Service . "<br>";
            $dests .= $value.', '; //<br/>
          }
          $dests = chop($dests,", ");
        }
        else {
          //individual
          $id = $result->itinerary_single->start_date;
          $dests = $id;
        }
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_edate($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $destination = $result->itinerary_options->options;
        if (stripos($destination, 'multiple') !== false) {
          $ids = $result->itinerary_multiple->end_date;
          /*
          echo '<pre>';var_dump($ids);echo'</pre>';
          foreach ($ids as $id) {
          $dests .= $id['destination']."<br/>";
          }
          */
          foreach($ids as $key => $value)
          {
            //echo $value[$key]->Service . "<br>";
            $dests .= $value.', '; //<br/>
          }
          $dests = chop($dests,", ");
        }
        else {
          //individual
          $id = $result->itinerary_single->end_date;
          $dests = $id;
        }
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_flastname($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->family_members->last_name;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //</br>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_ffirstname($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->family_members->first_name;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_fm_dob($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->family_members->birth_date;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_fm_rel($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->family_members->relationship;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_glastname($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->group_members->last_name;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_gfirstname($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->group_members->first_name;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_g_dob($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->group_members->birth_date;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_g_rel($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->group_members->relationship;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_clastname($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->company_members->last_name;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_cfirstname($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->company_members->first_name;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_c_dob($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->company_members->birth_date;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_c_rel($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->company_members->relationship;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_ffirst_name($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->family_personal_information->first_name;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_fmid_name($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->family_personal_information->middle_name;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_flas_name($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->family_personal_information->last_name;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_fdob($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->family_personal_information->birth_date;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_fstatus($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->family_personal_information->civil_status;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_gfirst_name($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->group_contact_person->first_name;
      return $id;
    }
    //--------------------------------------------------------------------------
    /*
    public function lookup_guardian_gmid_name($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $id = $result->group_contact_person->middle_name;
      return $id;
    }
    */
    //--------------------------------------------------------------------------
    public function lookup_guardian_glas_name($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->group_contact_person->last_name;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_gdob($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->group_contact_person->birth_date;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_gstatus($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->group_contact_person->civil_status;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_cfirst_name($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->company_contact_person->first_name;
      return $id;
    }
    //--------------------------------------------------------------------------
    /*
    public function lookup_guardian_cmid_name($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $id = $result->family_personal_information->middle_name;
      return $id;
    }
    */
    //--------------------------------------------------------------------------
    public function lookup_guardian_clas_name($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->company_contact_person->last_name;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_cdob($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->company_contact_person->birth_date;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_guardian_cstatus($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->company_contact_person->civil_status;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_fm_gstatus($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->group_members->civil_status;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_fm_cstatus($value, $row, $column, $isExport) {
      $id = $value;
      $dests = '';
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $ids = $result->company_members->civil_status;
      foreach($ids as $key => $value) {
        //echo $value[$key]->Service . "<br>";
        $dests .= $value.', '; //<br/>
      }
      $dests = chop($dests,", ");
      return $dests;
    }
    //--------------------------------------------------------------------------
    public function lookup_url($value, $row, $column, $isExport) {
      $_lnk = Mage::getBaseUrl();
      if (stripos($_lnk, '/index.php/') !== false) $_lnk = str_ireplace('/index.php/', '/', $_lnk);
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $url = $value;
      if(!empty($url)) $id = '<a title="View" target="_blank" href="'.$_lnk.$url.'">View</a>';
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_policy($value, $row, $column, $isExport) {
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->policy_code;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_cdesignation($value, $row, $column, $isExport){
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->company_contact_person->designation;
      return $id;
    }
    //--------------------------------------------------------------------------
    public function lookup_gdesignation($value, $row, $column, $isExport){
      $id = $value;
      //echo 'report: '.$report_type . ' '.$itpreport_type;
      $info = Mage::helper('globalfunction')->getQuoterequestInfo('request_session_completed_id',$id,'form_json_datas'); //date_created
      $result = json_decode($info);
      $id = $result->group_contact_person->designation;
      return $id;
    }
    //--------------------------------------------------------------------------

    protected function _prepareColumns() {
        $report_type = Mage::helper('globalfunction')->getfilterQuoteproduct('name','_report|type','record_type');
        $itpreport_type = Mage::helper('globalfunction')->getfilterQuoteproduct('name','_itpreport|type','record_type');
        $this->addColumn('id', array(
            'header'    => Mage::helper('builder')->__('Id'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'id',
            'filter'   => false,
            'sortable' => true,
            //'filter_condition_callback' => array($this, '_filterEmailCallback')
        ));
       /*
        $this->addColumn('completed_id', array(
            'header'    => Mage::helper('builder')->__('Completed Id'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'completed_id',
        ));
        //this should be id parametrized
        $this->addColumn('request_session_completed_id', array(
            'header'    => Mage::helper('builder')->__('Session Completed Id'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'request_session_completed_id',
        ));
        */
        $this->addColumn('date_formatted', array(
            'header'    => Mage::helper('builder')->__('Date'),
            //'align'     =>'right',
            'width'     => '20px',
            'type'      => 'datetime',
            'index'     => 'date_formatted',
            'filter_index'     => 'completed.date_created',
            //'filter'   => false,
            //'sortable' => false,
            //'filter_condition_callback' => array($this, '_filterDateCallback')
        ));

        /*
        $this->addColumn('form_json_datas', array(
            'header'    => Mage::helper('builder')->__('form_json_datas'),
            'width'     => '50px',
            'index'     => 'form_json_datas',
        ));
        */
        $this->addColumn('cover_type', array(
            'header'    => Mage::helper('builder')->__('Cover of Insurance'),
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'cover_type',
            'filter'   => false,
            'sortable' => false,
            'filter_condition_callback' => array($this, '_filterCovertypeCallback'),
            'frame_callback' => array($this, 'lookup_cover_type')
        ));

        //----------------------------------------------------------------------------
        /*
        switch($report_type) {
          case 'motor':
            //echo '</br>here: '.$report_type;
          $this->removeColumn('cover_type')->removeColumn('birth_date')->removeColumn('civil_status');
          break;
        }
        */
        //----------------------------------------------------------------------------
        switch($report_type) {
          case 'itp':
          case 'motor':
          //----------------------------------------------------------------
          $this->addColumn('first_name', array(
              'header'    => Mage::helper('builder')->__('First Name'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'first_name',
              'filter_condition_callback' => array($this, '_filterFNameCallback')
          ));

          $this->addColumn('midname', array(
              'header'    => Mage::helper('builder')->__('Middle Name'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'midname',
              'filter_condition_callback' => array($this, '_filterMNameCallback')
          ));

          $this->addColumn('last_name', array(
              'header'    => Mage::helper('builder')->__('Last Name'),
              'width'     => '50px',
              'index'     => 'last_name',
              'filter_condition_callback' => array($this, '_filterLNameCallback')
          ));

          $this->addColumn('birth_date', array(
              'header'    => Mage::helper('builder')->__('Date of Birth'),
              'align'     =>'right',
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'birth_date',
              'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
          ));

          $this->addColumn('civil_status', array(
              'header'    => Mage::helper('builder')->__('Civil Status'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'civil_status',
              'filter_condition_callback' => array($this, '_filtercivil_statusCallback')
          ));
          //----------------------------------------------------------------
                break;
          case 'fire':
                //nothing...will show in the future
                break;
        }
        //----------------------------------------------------------------------------
        switch($itpreport_type) {
          case 'family':
              //re-instantiate to get the new column data information
              $this->removeColumn('first_name')->removeColumn('midname')->removeColumn('last_name')->removeColumn('birth_date')->removeColumn('civil_status');
              $this->addColumn('first_name', array(
                  'header'    => Mage::helper('builder')->__('First Name'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_ffirst_name')
              ));
              $this->addColumn('midname', array(
                  'header'    => Mage::helper('builder')->__('Middle Name'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_fmid_name')
              ));
              $this->addColumn('last_name', array(
                  'header'    => Mage::helper('builder')->__('Last Name'),
                  'width'     => '50px',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_flas_name')
              ));
              $this->addColumn('birth_date', array(
                  'header'    => Mage::helper('builder')->__('Date of Birth'),
                  'align'     =>'right',
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_fdob')
              ));
              $this->addColumn('civil_status', array(
                  'header'    => Mage::helper('builder')->__('Civil Status'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_fstatus')
              ));
          break;
          case 'group':
              //re-instantiate to get the new column data information
              $this->removeColumn('first_name')->removeColumn('midname')->removeColumn('last_name')->removeColumn('birth_date')->removeColumn('civil_status');
              $this->addColumn('first_name', array(
                  'header'    => Mage::helper('builder')->__('First Name'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_gfirst_name')
              ));
              $this->addColumn('last_name', array(
                  'header'    => Mage::helper('builder')->__('Last Name'),
                  'width'     => '50px',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_glas_name')
              ));
              $this->addColumn('birth_date', array(
                  'header'    => Mage::helper('builder')->__('Date of Birth'),
                  'align'     =>'right',
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_gdob')
              ));
              $this->addColumn('civil_status', array(
                  'header'    => Mage::helper('builder')->__('Civil Status'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_gstatus')
              ));
              $this->addColumn('designation', array(
                  'header'    => Mage::helper('builder')->__('Designation'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_gdesignation')
              ));
          break;
          case 'company':
              //re-instantiate to get the new column data information
              $this->removeColumn('first_name')->removeColumn('midname')->removeColumn('last_name')->removeColumn('birth_date')->removeColumn('civil_status');
              $this->addColumn('first_name', array(
                  'header'    => Mage::helper('builder')->__('First Name'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_cfirst_name')
              ));
              $this->addColumn('last_name', array(
                  'header'    => Mage::helper('builder')->__('Last Name'),
                  'width'     => '50px',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_clas_name')
              ));
              $this->addColumn('birth_date', array(
                  'header'    => Mage::helper('builder')->__('Date of Birth'),
                  'align'     =>'right',
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_cdob')
              ));
              $this->addColumn('civil_status', array(
                  'header'    => Mage::helper('builder')->__('Civil Status'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_guardian_cstatus')
              ));
              $this->addColumn('designation', array(
                  'header'    => Mage::helper('builder')->__('Designation'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_cdesignation')
              ));
          break;
        }
        //----------------------------------------------------------------------------

        $this->addColumn('email', array(
            'header'    => Mage::helper('builder')->__('Email'),
            //'width'     => '10px!important',
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'email',
            'filter_condition_callback' => array($this, '_filterEmailCallback'),
        ));

        $this->addColumn('telephone', array(
            'header'    => Mage::helper('builder')->__('Telephone'),
            'align'     =>'right',
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'telephone',
            'filter_condition_callback' => array($this, '_filterphoneCallback')
        ));

        $this->addColumn('mobile', array(
            'header'    => Mage::helper('builder')->__('Mobile'),
            'align'     =>'right',
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'mobile',
            'filter_condition_callback' => array($this, '_filtermobileCallback')
        ));

        switch($report_type) {
          case 'motor':
          //echo '</br>here: '.$report_type;
        //----------------------------------------------------------------
        $this->addColumn('first_name', array(
            'header'    => Mage::helper('builder')->__('First Name'),
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'first_name',
            'filter_condition_callback' => array($this, '_filterFNameCallback')
        ));

        $this->addColumn('midname', array(
            'header'    => Mage::helper('builder')->__('Middle Name'),
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'midname',
            'filter_condition_callback' => array($this, '_filterMNameCallback')
        ));

        $this->addColumn('last_name', array(
            'header'    => Mage::helper('builder')->__('Last Name'),
            'width'     => '50px',
            'index'     => 'last_name',
            'filter_condition_callback' => array($this, '_filterLNameCallback')
        ));

        $this->addColumn('birth_date', array(
            'header'    => Mage::helper('builder')->__('Date of Birth'),
            'align'     =>'right',
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'birth_date',
            'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
        ));

        $this->addColumn('civil_status', array(
            'header'    => Mage::helper('builder')->__('Civil Status'),
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'civil_status',
            'filter_condition_callback' => array($this, '_filtercivil_statusCallback')
        ));
        //----------------------------------------------------------------

        $this->addColumn('province', array(
            'header'    => Mage::helper('builder')->__('Province'),
            'index'     => 'province',
            'width'     => '20px',
            'type'      => 'text',
            'filter_condition_callback' => array($this, '_filterprovinceCallback')
        ));

        $this->addColumn('city', array(
            'header'    => Mage::helper('builder')->__('City'),
            'index'     => 'city',
            'width'     => '20px',
            'type'      => 'text',
            'filter_condition_callback' => array($this, '_filtercityCallback')
        ));

        $this->addColumn('barangay', array(
            'header'    => Mage::helper('builder')->__('Barangay'),
            'index'     => 'barangay',
            'width'     => '20px',
            'type'      => 'text',
            'filter_condition_callback' => array($this, '_filterbarangayCallback')
        ));

        $this->addColumn('for_rent', array(
            'header'    => Mage::helper('builder')->__('Vehicle used for rent'),
            'index'     => 'for_rent',
            'width'     => '10px',
            'type'      => 'text',
            'filter'   => false,
            'sortable' => true,
            //'filter_condition_callback' => array($this, '_filtervehicleusedCallback')
        ));

        $this->addColumn('typeofinsurance', array(
            'header'    => Mage::helper('builder')->__('Type of Insurance'),
            'index'     => 'typeofinsurance',
            'width'     => '20px',
            'type'      => 'text',
            'filter_condition_callback' => array($this, '_filterEmailCallback')
        ));

        $this->addColumn('bodily_injury', array(
            'header'    => Mage::helper('builder')->__('Bodily Injury (VTPL-BI)'),
            'align'     =>'right',
            'index'     => 'bodily_injury',
            'width'     => '20px',
            'type'      => 'text',
            'filter_condition_callback' => array($this, '_filterBICallback')
        ));

        $this->addColumn('year', array(
            'header'    => Mage::helper('builder')->__('Year'),
            'align'     =>'right',
            'index'     => 'year',
            'width'     => '20px',
            'type'      => 'text',
            'filter_condition_callback' => array($this, '_filterYearCallback')
        ));

        $this->addColumn('brand', array(
            'header'    => Mage::helper('builder')->__('Brand'),
            'index'     => 'brand',
            'width'     => '20px',
            'type'      => 'text',
            'filter_condition_callback' => array($this, '_filterBrandCallback')
        ));

        $this->addColumn('model', array(
            'header'    => Mage::helper('builder')->__('Model'),
            'index'     => 'model',
            'width'     => '20px',
            'type'      => 'text',
            'filter_condition_callback' => array($this, '_filterModelCallback')
        ));
        break;
      }

      switch($report_type) {
        case 'itp':
          //echo '</br>here: '.$report_type;
          $this->addColumn('guardian_last_name', array(
              'header'    => Mage::helper('builder')->__('Guardian LastName'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'id', //'last_name',
              'filter'   => false,
              'sortable' => false,
              'frame_callback' => array($this, 'lookup_guardian_last_name')
              //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
          ));

          $this->addColumn('guardian_first_name', array(
              'header'    => Mage::helper('builder')->__('Guardian FirstName'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'id',
              'filter'   => false,
              'sortable' => false,
              'frame_callback' => array($this, 'lookup_guardian_first_name')
              //'filter_condition_callback' => array($this, '_filtercivil_statusCallback')
          ));

          $this->addColumn('relationship', array(
              'header'    => Mage::helper('builder')->__('Relationship to insured'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'relationship',
              'filter'   => false,
              'sortable' => false,
              //'filter_condition_callback' => array($this, '_filtercivil_statusCallback')
          ));

          $this->addColumn('purpose_of_travel', array(
              'header'    => Mage::helper('builder')->__('Travel Info'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'purpose_of_travel',
              'filter'   => false,
              'sortable' => false,
              //'filter_condition_callback' => array($this, '_filtercivil_statusCallback')
          ));

          $this->addColumn('purpose_others', array(
              'header'    => Mage::helper('builder')->__('Others (Specific)'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'purpose_others',
              'filter'   => false,
              'sortable' => false,
              //'filter_condition_callback' => array($this, '_filtercivil_statusCallback')
          ));

          $this->addColumn('itinerarytype', array(
              'header'    => Mage::helper('builder')->__('Itinerary type'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'itinerarytype',
              'filter'   => false,
              'sortable' => false,
              //'filter_condition_callback' => array($this, '_filtercivil_statusCallback')
          ));

          $this->addColumn('destination', array(
              'header'    => Mage::helper('builder')->__('Destinations'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'id',  //destination
              'filter'   => false,
              'sortable' => false,
              'frame_callback' => array($this, 'lookup_destination')
              //'filter_condition_callback' => array($this, '_filtercivil_statusCallback')
          ));

          $this->addColumn('start_date', array(
              'header'    => Mage::helper('builder')->__('Departure Date'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'id', //'start_date',
              'filter'   => false,
              'sortable' => false,
              //'filter_condition_callback' => array($this, '_filtercivil_statusCallback')
              'frame_callback' => array($this, 'lookup_sdate')
          ));

          $this->addColumn('end_date', array(
              'header'    => Mage::helper('builder')->__('Return Date'),
              'width'     => '20px',
              'type'      => 'text',
              'index'     => 'id', //'end_date',
              'filter'   => false,
              'sortable' => false,
              //'filter_condition_callback' => array($this, '_filtercivil_statusCallback')
              'frame_callback' => array($this, 'lookup_edate')
          ));
        }

        switch($itpreport_type) {
          case 'family':
            //echo '</br>here: '.$report_type;
            $this->addColumn('fm_last_name', array(
                'header'    => Mage::helper('builder')->__('Member LastName'),
                'width'     => '20px',
                'type'      => 'text',
                'index'     => 'id', //'last_name',
                'filter'   => false,
                'sortable' => false,
                'frame_callback' => array($this, 'lookup_flastname')
                //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            ));

            $this->addColumn('fm_first_name', array(
                'header'    => Mage::helper('builder')->__('Member FirstName'),
                'width'     => '20px',
                'type'      => 'text',
                'index'     => 'id', //'last_name',
                'filter'   => false,
                'sortable' => false,
                'frame_callback' => array($this, 'lookup_ffirstname')
                //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            ));

            $this->addColumn('fm_dob', array(
                'header'    => Mage::helper('builder')->__('Member Date of Birth'),
                'width'     => '20px',
                'type'      => 'text',
                'index'     => 'id', //'last_name',
                'filter'   => false,
                'sortable' => false,
                'frame_callback' => array($this, 'lookup_fm_dob')
                //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            ));

            $this->addColumn('fm_rel', array(
                'header'    => Mage::helper('builder')->__('Relationship'),
                'width'     => '20px',
                'type'      => 'text',
                'index'     => 'id', //'last_name',
                'filter'   => false,
                'sortable' => false,
                'frame_callback' => array($this, 'lookup_fm_rel')
                //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            ));
            /*
            $this->addColumn('fm_status', array(
                'header'    => Mage::helper('builder')->__('FM Civil Status'),
                'width'     => '20px',
                'type'      => 'text',
                'index'     => 'id', //'last_name',
                'filter'   => false,
                'sortable' => false,
                'frame_callback' => array($this, 'lookup_fm_fstatus')
                //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            ));
            */

          break;

          case 'group':
            //echo '</br>here: '.$report_type;
            $this->addColumn('fm_last_name', array(
                'header'    => Mage::helper('builder')->__('Member LastName'),
                'width'     => '20px',
                'type'      => 'text',
                'index'     => 'id', //'last_name',
                'filter'   => false,
                'sortable' => false,
                'frame_callback' => array($this, 'lookup_flastname')
                //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            ));

            $this->addColumn('fm_first_name', array(
                'header'    => Mage::helper('builder')->__('Member FirstName'),
                'width'     => '20px',
                'type'      => 'text',
                'index'     => 'id', //'last_name',
                'filter'   => false,
                'sortable' => false,
                'frame_callback' => array($this, 'lookup_ffirstname')
                //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            ));

            $this->addColumn('fm_dob', array(
                'header'    => Mage::helper('builder')->__('Member Date of Birth'),
                'width'     => '20px',
                'type'      => 'text',
                'index'     => 'id', //'last_name',
                'filter'   => false,
                'sortable' => false,
                'frame_callback' => array($this, 'lookup_g_dob')
                //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            ));

            $this->addColumn('fm_rel', array(
                'header'    => Mage::helper('builder')->__('Relationship'),
                'width'     => '20px',
                'type'      => 'text',
                'index'     => 'id', //'last_name',
                'filter'   => false,
                'sortable' => false,
                'frame_callback' => array($this, 'lookup_g_rel')
                //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            ));

            $this->addColumn('fm_gstatus', array(
                'header'    => Mage::helper('builder')->__('Member Civil Status'),
                'width'     => '20px',
                'type'      => 'text',
                'index'     => 'id', //'last_name',
                'filter'   => false,
                'sortable' => false,
                'frame_callback' => array($this, 'lookup_fm_gstatus')
                //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            ));

            break;
            case 'company':
              //echo '</br>here: '.$report_type;
              $this->addColumn('fm_last_name', array(
                  'header'    => Mage::helper('builder')->__('Member LastName'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id', //'last_name',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_clastname')
                  //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
              ));

              $this->addColumn('fm_first_name', array(
                  'header'    => Mage::helper('builder')->__('Member FirstName'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id', //'last_name',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_cfirstname')
                  //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
              ));

              $this->addColumn('fm_dob', array(
                  'header'    => Mage::helper('builder')->__('Member Date of Birth'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id', //'last_name',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_c_dob')
                  //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
              ));

              $this->addColumn('fm_rel', array(
                  'header'    => Mage::helper('builder')->__('Relationship'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id', //'last_name',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_c_rel')
                  //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
              ));

              $this->addColumn('fm_gstatus', array(
                  'header'    => Mage::helper('builder')->__('Member Civil Status'),
                  'width'     => '20px',
                  'type'      => 'text',
                  'index'     => 'id', //'last_name',
                  'filter'   => false,
                  'sortable' => false,
                  'frame_callback' => array($this, 'lookup_fm_cstatus')
                  //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
              ));

          break;
        }

        switch($report_type) {
          case 'motor':
          //$this->removeColumn('last_name')->removeColumn('midname')->removeColumn('first_name');
          $this->removeColumn('designation')->removeColumn('cover_type')->removeColumn('civil_status')->removeColumn('birth_date');
          $this->removeColumn('fm_last_name')->removeColumn('fm_first_name')->removeColumn('fm_dob')->removeColumn('fm_rel')->removeColumn('fm_gstatus');
          break;
        }

        $this->addColumn('promo_code', array(
            'header'    => Mage::helper('builder')->__('Promo Code'),
            'index'     => 'promo_code',
            'width'     => '20px',
            'type'      => 'text',
            'filter_condition_callback' => array($this, '_filterEmailCallback')
        ));

        $this->addColumn('remarks_status', array(
            'header'    => Mage::helper('builder')->__('Remarks'),
            'index'     => 'remarks_status',
            'width'     => '20px',
            'type'      => 'text',
            'filter_condition_callback' => array($this, '_filterRemarksCallback')
        ));

        $this->addColumn('policy_code', array(
            'header'    => Mage::helper('builder')->__('Policy Number'),
            'align'     =>'right',
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'id',
            'filter'   => false,
            'sortable' => false,
            //'filter_condition_callback' => array($this, '_filterPolicyCallback'),
            'frame_callback' => array($this, 'lookup_policy')
        ));

        $this->addColumn('order_id', array(
            'header'    => Mage::helper('builder')->__('Order #'),
            'align'     => 'right',
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'order_id',
            'filter_condition_callback' => array($this, '_filterYearCallback')
        ));

        $this->addColumn('formal_quote', array(
            'header'    => Mage::helper('builder')->__('Formal Quote'),
            'width'     => '20px',
            'type'      => 'text',
            'index'     => 'formal_quote', //'last_name',
            'filter'   => false,
            'sortable' => false,
            'is_system' => true,
            //'filter_condition_callback' => array($this, '_filterbirth_dateCallback')
            'frame_callback' => array($this, 'lookup_url')
        ));
        /*
        $this->addColumn('status', array(
            'header'    => Mage::helper('travelinsurance')->__('Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => array(
                1 => 'Enabled',
                2 => 'Disabled',
            ),
        ));
        */
          $this->addColumn('action',
              array(
                  'header'    =>  Mage::helper('builder')->__('Action'),
                  'width'     => '100',
                  'type'      => 'action',
                  'getter'    => 'getId',
                  'actions'   => array(
                      array(
                          'caption'   => Mage::helper('builder')->__('View'),
                          'url'       => array('base'=> '*/*/edit'),
                          'field'     => 'id'
                      )
                  ),
                  'filter'    => false,
                  'sortable'  => false,
                  'index'     => 'stores',
                  'is_system' => true,
          ));
      $this->removeColumn('action');
  		$this->addExportType('*/*/exportCsv', Mage::helper('builder')->__('CSV'));
  		$this->addExportType('*/*/exportXml', Mage::helper('builder')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('builder');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('builder')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('builder')->__('Are you sure?')
        ));
        /*
        $statuses = Mage::getSingleton('builder/status')->getOptionArray();
        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('travelinsurance')->__('Change status'),
             'url'  => $this->getUrl('asterisk/asterisk/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('travelinsurance')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        */
        $this->getMassactionBlock()->removeItem('delete');
        return $this;
    }

  public function getRowUrl($row)
  {
      //return $this->getUrl('*/*/edit', array('id' => $row->getCompleted_id()));
      return false;
  }

}
