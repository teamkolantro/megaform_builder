<?php
class MegaForm_Builder_Block_Adminhtml_Quoterequest_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'builder';
        $this->_controller = 'adminhtml_quoterequest';

        $this->_removeButton('add')->_removeButton('reset')->_removeButton('save')->_removeButton('delete');
        /*Ideally form reset should suffice but Magento uses a different DOM approach please keep the comments in this code, thanks*/
        /*
        $this->_updateButton('save', 'label', Mage::helper('builder')->__('Save Country'));
        $this->_updateButton('delete', 'label', Mage::helper('builder')->__('Delete Country'));
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
        */


        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('quoterequest_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'quoterequest_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'quoterequest_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('quoterequest_data') && Mage::registry('quoterequest_data')->getId() ) {
            return Mage::helper('builder')->__("View Quote Request '%s'", $this->htmlEscape(Mage::registry('quoterequest_data')->getId()));
        } else {
            return Mage::helper('builder')->__('View Quote Request');
        }
    }
}
