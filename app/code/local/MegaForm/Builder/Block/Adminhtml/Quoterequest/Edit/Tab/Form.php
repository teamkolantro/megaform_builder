<?php
class MegaForm_Builder_Block_Adminhtml_Quoterequest_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('quoterequest_form', array('legend'=>Mage::helper('builder')->__('Quote Request')));
      $m_id = Mage::app()->getRequest()->getParam('id');
      $_showLabel = 'text';
      $_showRequired = true;
      if(!empty($m_id)) {
        $_showLabel = 'label';
        $_showRequired = false;
      }
      /*
      $fieldset->addField('user_type', 'select', array(
          'label'     => Mage::helper('travelinsurance')->__('Tag as'),
          'name'      => 'user_type',
          'values'    => array(
              array(
                  'value'     => '',
                  'label'     => Mage::helper('travelinsurance')->__(''),
              ),

              array(
                  'value'     => 'individual',
                  'label'     => Mage::helper('travelinsurance')->__('Individual'),
              ),

              array(
                  'value'     => 'family',
                  'label'     => Mage::helper('travelinsurance')->__('Family'),
              ),
          ),
      ));
      */
      $fieldset->addField('email', $_showLabel, array(
          'label'     => Mage::helper('travelinsurance')->__('E-mail'),
          'maxlength' => 4,
          'class'     => 'required-entry validate-no-html-tags',
          'required'  => $_showRequired,
          'name'      => 'email',
      ));

      $fieldset->addField('first_name', 'text', array(
          'label'     => Mage::helper('travelinsurance')->__('First Name'),
          'maxlength' => 245,
          'class'     => 'required-entry validate-no-html-tags',
          'required'  => true,
          'name'      => 'first_name'
      ));
     /*
        $fieldset->addField('filename', 'file', array(
            'label'     => Mage::helper('travelinsurance')->__('File'),
            'required'  => false,
            'name'      => 'filename',
  	  ));
      $fieldset->addField('region', 'text', array(
          'label'     => Mage::helper('travelinsurance')->__('Region'),
          'maxlength' => 245,
          'class'     => 'required-entry validate-no-html-tags',
          'required'  => true,
          'name'      => 'region'
      ));
      */

      /*
      $fieldset->addField('isschengen', 'select', array(
          'label'     => Mage::helper('travelinsurance')->__('Schengen'),
          'name'      => 'isschengen',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('travelinsurance')->__('Yes'),
              ),

              array(
                  'value'     => 0,
                  'label'     => Mage::helper('travelinsurance')->__('No'),
              ),
          ),
      ));

      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('travelinsurance')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('travelinsurance')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('travelinsurance')->__('Disabled'),
              ),
          ),
      ));

      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('travelinsurance')->__('Content'),
          'title'     => Mage::helper('travelinsurance')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
      */
      if ( Mage::getSingleton('adminhtml/session')->getQuoterequestData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getQuoterequestData());
          Mage::getSingleton('adminhtml/session')->setQuoterequestData(null);
      } elseif ( Mage::registry('quoterequest_data') ) {
          //-------------------------------------------------set default values
          //-------------------------------------------------
          $form->setValues(Mage::registry('quoterequest_data')->getData());
      }
      return parent::_prepareForm();
  }
}
