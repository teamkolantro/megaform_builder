<?php
class MegaForm_Builder_Block_Adminhtml_Quoterequest extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_quoterequest';
    $this->_blockGroup = 'builder';
    $report_type = Mage::helper('globalfunction')->getfilterQuoteproduct('name','_report|type','record_type');
    $itpreport_type = Mage::helper('globalfunction')->getfilterQuoteproduct('name','_itpreport|type','record_type');
    $istr = '';
    switch($report_type) {
      case 'itp':
        $str = "ITP";
        $istr = ' '.$itpreport_type;
      break;
      case 'fire':
        $str = "Fire";
      break;
      default:
        $str = "Motor";
      break;
    }
    $this->_headerText = Mage::helper('builder')->__('View Quote Request for '.$str . $istr);
    $this->_addButtonLabel = Mage::helper('builder')->__('View Quote Request for '.$str . $istr);
    parent::__construct();
    $this->_removeButton('add');
  }
}
