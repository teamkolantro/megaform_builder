<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Helper_Data as CURR_HELPER;
use MegaForm_Builder_Model_Request_Forms as CURR_MODEL;
use MegaForm_Builder_Model_Request_Session_Completed as REQUEST_SESSION_COMPLETED_MODEL;

/**
 * Block Forms
 *
 * This class handles the rendering of the whole form by xml block,
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Forms extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    /**
     * Cart
     *
     * Handles checkout cart session|singelton default (null)
     *
     * @var mixed
     */
    protected $cart = null;

    /**
     * Form Code
     *
     * This contains form code that is necessary for what form to be rendered.
     *
     * Example:
     * - motor
     * - itp
     * - fire
     * - personalInfo
     * - personalInfoItp
     * - vehicleInfo
     *
     * @var string
     */
    public $form_code;

    /**
     * Class contructor...
     */
    public function _construct()
    {
        parent::_construct();
    }

    /**
     * Prepare Layout
     *
     * This method is fired before layout is rendered
     * yet this have a dirty implementation of loading form assets,
     * but i don't know because this is the only way i think,
     * force loading the assets wherever the form can be loaded,
     * by widget or by xml or by block creation
     *
     * @return mixed MegaForm_Builder_Block_Forms
     */
    protected function _prepareLayout()
    {
        echo Mage::helper(CURR_HELPER::HELPER_KEY)->getAssets();
        return parent::_prepareLayout();
    }

    /**
     * Get Test Create Block
     *
     * This method is for developer use only
     * it tends to test if a form rendering correctly,
     * it will simulate form rendering for you. :)
     *
     * Required arrays
     * - request_forms  => Should be the form code | example: motor, itp, fire, vehicleInfo
     * - form_show      => If always form is rendered always, if by_product form will only show if item on cart matches on form products
     * - on_success_url => This will become handy somtimes for when you have an ajax call or a direct post this url can serve the redirection.
     *
     * Ofcourse no form will be renderd unless you return or echo the variable $builder_forms.
     * It's up to you mate.
     *
     * @return void
     */
    public function getTestCreateBlock()
    {
        $builder_forms = Mage::app()->getLayout()->createBlock('builder/forms')
            ->setData(
                array(
                    'request_forms'  => "motor",
                    'form_show'      => "always",
                    'on_success_url' => "/checkout/onepage",
                )
            )
            ->toHtml();
    }

    /**
     * Initialize
     *
     * This method creates a block depending on the form_code,
     *
     * Form Code must be precent in the url query
     * <code>
     *  http://example.com/cms-page1/form_code/itp
     *  http://example.com/modulename/controllername/actionname/form_code/itp
     *  http://example.com/cms-page1?form_code=itp
     *  http://example.com/modulename/controllername/actionname/?form_code=itp
     * </code>
     *
     * Also loads the data of the recednt form if form_key is present.
     * <code>
     *  http://example.com/cms-page1/fofrm_key/as9dn8sam9dsa9dsa8mdsa98dmas98
     *  http://example.com/modulename/controllername/actionname/fofrm_key/as9dn8sam9dsa9dsa8mdsa98dmas98
     *  http://example.com/cms-page1?fofrm_key=as9dn8sam9dsa9dsa8mdsa98dmas98
     *  http://example.com/modulename/controllername/actionname/?fofrm_key=as9dn8sam9dsa9dsa8mdsa98dmas98
     * </code>
     *
     * Produce forms list rendered as html
     *
     * This methods creates a block from another,
     * thens sets the form_code and the data of the form if theres any
     * then return its html string.
     *
     * @return string|void Will not return if there is no form block created.
     */
    protected function _toHtml()
    {

        Mage::helper(CURR_HELPER::HELPER_KEY)->generateFormKey();

        $this->form_code = $this->getData('form_code');

        if (!empty($this->getRequest()->getParam('form_code')))
        {
            $this->form_code = $this->getRequest()->getParam('form_code');
        }

        if (!empty($this->getRequest()->getParam('form_key')))
        {
            $REQUEST_SESSION_COMPLETED_MODEL = Mage::getModel(REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL . "/" . REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL_ENTITY)
                ->load($this->getRequest()->getParam('form_key'), 'form_key');

            $Forms = Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)
                ->load($REQUEST_SESSION_COMPLETED_MODEL->getRequestFormId());

            $this->form_code = $Forms->getData('form_code');
            $this->getRequest()->setParam('form_code', $this->form_code);
        }

        if (empty($this->form_code))
        {
            return '';
        }

        //todo option to show by product or always
        if ($this->getData('form_show') == 'by_product')
        {
            if (Mage::helper('checkout/cart')->getItemsCount() == 0)
            {
                return '';
            }

            $_cart_item_ids = Mage::helper(CURR_HELPER::HELPER_KEY)->getCartItemProductsIds();

            $form_product_ds = Mage::getModel(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY)
                ->load($this->getData('form_code'), 'form_code')
                ->getFormProductIds();

            if ((!(count(array_intersect($form_product_ds, $_cart_item_ids)) > 0)) && empty(Mage::app()->getRequest()->getParam('form_key')))
            {
                return '';
            }
        }

        if (empty($this->getTemplate()))
        {
            $this->setTemplate('megaform_builder/forms/' . $this->form_code . '.phtml');
        }

        return $this->renderView();
    }

    /**
     * Get Singlton Data
     *
     * This returns the singleton of the REQUEST_SESSION_COMPLETED_MODEL if theres any
     *
     * @return mixed Singleton of the REQUEST_SESSION_COMPLETED_MODEL
     */
    public function getSingletonData()
    {
        return Mage::getSingleton(REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL . "/" . REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL_ENTITY);
    }

    /**
     * Get Previous Key
     *
     * It returns the url query previous_form_key,
     * But if form_key is present then it get
     * and return previous_form_key of that form_key's data
     *
     * @return string Previous Form key
     */
    public function getPreviousFormKey()
    {
        $previous_form_key = Mage::app()->getRequest()->getParam('previous_form_key', false);

        if($form_key = Mage::app()->getRequest()->getParam('form_key', false))
        {
            $forms_datas = Mage::helper("builder")->getDatasByFormKey($form_key);
            $previous_form_key = @$forms_datas['previous_form_key'];
        }

        return $previous_form_key;
    }

    /**
     * Get Form Key
     *
     * It generates for key,
     * returns form key on url query if present
     * returns form key generated
     * returns previous instance of generated form key if exists
     *
     * @return string Form key
     */
    public function getFormKey()
    {
        Mage::helper(CURR_HELPER::HELPER_KEY)->generateFormKey();

        if (!empty($this->getRequest()->getParam('form_key')))
        {
            return $this->getRequest()->getParam('form_key');
        }
        else
        {
            return "";
        }

        return Mage::getSingleton('core/session')->getMegaBuildFormKey();
    }

    /**
     * Get Form Action Url
     *
     * returns form_action If has beend set
     * if form_action is not set, then it returns current url.
     *
     * @return string Form Action Url
     */
    public function getFormActionUrl()
    {
        if(!empty($this->getData('form_action')))
        {
            return Mage::getBaseUrl() . $this->getData('form_action');
        }

        return Mage::getBaseUrl() . $this->getCurrentUrl();
    }

    /**
     * Get Controller Action Url
     *
     * Returns Form Builderr module form process url.
     * this url is used for ajax saving
     *
     * @return string Controller Action Url
     */
    public function getControllerActionUrl()
    {
        return $this->getUrl('builder/form/process');
    }

    /**
     * Get Form Code
     *
     * This returns the property <a href="#property_form_code"></a>
     *
     * @return string form code
     */
    public function getFormCode()
    {
        return $this->form_code;
    }

    /**
     * Get Current Url
     *
     * Returns the rewrite version of the url, for a clean submit.
     * this isused on <a href="#method_getFormActionUrl">get action url method</a>
     *
     * @return string Current Url
     */
    public function getCurrentUrl()
    {
        $params               = array(
            '_use_rewrite' => true,
            '_nosid' => true,
        );
        $url = Mage::getUrl('*/*/*', $params);
        $url                  = Mage::getSingleton('core/url')->parseUrl($url);
        $path                 = $url->getPath();

        return $path;
    }

    /**
     * Get Saved Url
     *
     * Returns the rewrite version of the url, for a clean submit.
     * it also adds form_key on the url
     *
     * @return string Saved Url
     */
    public function getSavedUrl()
    {
        $params               = array(
            '_use_rewrite' => true,
            '_nosid' => true,
            'form_key' => $this->getFormKey(),
        );
        $url = Mage::getUrl('*/*/*', $params);

        return $url;
    }

}
