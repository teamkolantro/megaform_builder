<?php
/**
 * Megaform
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Megaform to newer
 * versions in the future. If you wish to customize Megaform for your
 * needs please refer to http://transcosmos.com/ for more information.
 *
 * @category    Megaform
 * @package     Megaform_Builder
 * @copyright   Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://transcosmos.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

use MegaForm_Builder_Helper_Data as CURR_HELPER;
use MegaForm_Builder_Model_Items as CURR_MODEL;

/**
 * Block Index
 *
 * This class handles the rendering of the index layout by xml block,
 *
 * @category   Megaform
 * @package    Megaform_Builder
 * @author     TCAP OnShore Team
 * @author     TCAP OnShore Team | Josel Mariano <josel.mariano@transcosmos.com.ph>
 */
class MegaForm_Builder_Block_Index extends Mage_Core_Block_Template
{
    /**
     * Get Url Path
     *
     * This method gets and return current url path
     *
     * @return string Url string
     */
    public function getUrlPath()
    {
        $currentUrl = Mage::helper('core/url')->getCurrentUrl();
        $url        = Mage::getSingleton('core/url')->parseUrl($currentUrl);
        $path       = $url->getPath();
        return $path;
    }

    /**
     * Get Config Enabled
     *
     * This returns a boolean result depending on the module registry config enable key
     *
     * @return boolean Result from module registry config
     */
    public function getConfigEnabled()
    {
        return Mage::helper(CURR_HELPER::HELPER_KEY)->getConfigEnabled();
    }
}
