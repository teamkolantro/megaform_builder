<?php

use MegaForm_Builder_Model_Request_Forms as CURR_MODEL;

class MegaForm_Builder_Model_Mysql4_Request_Forms extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
    	$this->_init(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY, "id");
    }

}
