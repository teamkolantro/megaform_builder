<?php

use MegaForm_Builder_Model_Request_Summary_Templates as CURR_MODEL;

class MegaForm_Builder_Model_Mysql4_Request_Summary_Templates extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
    	$this->_init(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY, "id");
    }

}
