<?php

use MegaForm_Builder_Model_Request_Form_Records as CURR_MODEL;

class MegaForm_Builder_Model_Mysql4_Request_Form_Records_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init(CURR_MODEL::MODULE_MODEL . "/" . CURR_MODEL::MODULE_MODEL_ENTITY);
    }

    public function getAllEnabled()
    {
        return $this->addFieldToFilter('status', array('neq' => '0'));
    }

    public function filterSorted()
    {
        return $this->setOrder('sort', 'ASC');
    }

    public function filterSelectAll()
    {
        return $this->addFieldToSelect('*');
    }

    public function addGroupByNameFilter($field, $table = null)
    {
        if (empty($field))
        {
            return $this;
        }

        if (!empty($table))
        {
            $this->getSelect()->group($table . '.' . $field);
        }
        else
        {
            $this->getSelect()->group('main_table.' . $field);
        }
        return $this;
    }
}
