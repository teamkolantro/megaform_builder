<?php
class MegaForm_Builder_Model_Mysql4_Quoterequest extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        // Note that the id refers to the key field in your database table.
        $this->_init('builder/quoterequest', 'id');
    }
}
