<?php
class MegaForm_Builder_Model_Form_Show_Options
{
/**
 * Provide available options as a value/label array
 *
 * @return array
 */
    public function toOptionArray()
    {
        return array(
            array('value' => 'by_product', 'label' => 'By Product Settings'),
            array('value' => 'always', 'label' => 'Always'),
        );
    }
}
