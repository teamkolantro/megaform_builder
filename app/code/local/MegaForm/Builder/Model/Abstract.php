<?php

/**
 * Abstract extension
 * class MegaForm_Builder_Model_Abstract
 */
abstract class MegaForm_Builder_Model_Abstract extends Mage_Core_Model_Abstract
{

    public function deleteBy($field = '', $value = '')
    {
        $_collection = $this->getCollection()->addFieldToFilter($field, $value);

        foreach ($_collection as $item)
        {
            $item->delete();
        }

        return $this;
    }

    public function insertAll($_datas = array())
    {
        foreach ($_datas as $_data)
        {
            $this->setData($_data);
            $this->save();
            $this->unsetData();
        }
    }

}
