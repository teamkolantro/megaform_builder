<?php

class MegaForm_Builder_Model_Request_Email_Templates extends MegaForm_Builder_Model_Abstract
{
    const MODULE_MODEL        = "builder";
    const MODULE_MODEL_ENTITY = "request_email_templates";

    protected function _construct()
    {
        $this->_init(self::MODULE_MODEL . "/" . self::MODULE_MODEL_ENTITY);
    }

}
