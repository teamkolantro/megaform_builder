<?php

class MegaForm_Builder_Model_Request_Form_Records extends MegaForm_Builder_Model_Abstract
{
    const MODULE_MODEL        = "builder";
    const MODULE_MODEL_ENTITY = "request_form_records";

    protected function _construct()
    {
        $this->_init(self::MODULE_MODEL . "/" . self::MODULE_MODEL_ENTITY);
    }

    /**
     * Provide available options as a value/label array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $collections = $this->getCollection();

        $collections_options = array();

        foreach ($collections as $collection)
        {
            $collections_options[$collection->getData('id')] = array(
                'value' => $collection->getData('id'), 'label' => $collection->getData('name'),
            );
        }

        return $collections_options;
    }

    public function getCollectionToOptionArray()
    {
        $collections = $this->getCollection();

        $collections_options = array();

        foreach ($collections as $collection)
        {
            $collections_options[$collection->getData('id')] = $collection->getData('name');
        }

        return $collections_options;
    }

    public function getMaxSortPosition()
    {
        $maxSort = $this->getCollection()
            ->addFieldToSelect('sort')
            ->setOrder('sort', 'DESC')
            ->setPageSize(1)
            ->setCurPage(1)
            ->getData();

        return $maxSort[0]['sort'] + 1;
    }

}
