<?php

class MegaForm_Builder_Model_Request_Form_Products extends MegaForm_Builder_Model_Abstract
{
    const MODULE_MODEL        = "builder";
    const MODULE_MODEL_ENTITY = "request_form_products";

    protected function _construct()
    {
        $this->_init(self::MODULE_MODEL . "/" . self::MODULE_MODEL_ENTITY);
    }

    /**
     * Provide available options as a value/label array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $collections = $this->getCollection();

        $collections_options = array();

        foreach ($collections as $collection)
        {
            $collections_options[$collection->getData('id')] = array(
                'value' => $collection->getData('id'), 'label' => $collection->getData('name'),
            );
        }

        return $collections_options;
    }

    /**
     * Update all form products
     *
     * @return array
     */
    public function updateFormProducts($id = 0, $_datas = array())
    {
        if(!$id)
        {
            return $this;
        }

        $this->deleteBy('request_form_id', $id);
        foreach ($_datas as $key => $value)
        {
            $_datas[$key] = array(
                'request_form_id' => $id,
                'product_id'      => $value,
                'date_created'    => Mage::getModel('core/date')->date('Y-m-d H:i:s'),
            );
        }

        $this->insertAll($_datas);

        return $this;
    }

    public function getCollectionToOptionArray()
    {
        $collections = $this->getCollection();

        $collections_options = array();

        foreach ($collections as $collection)
        {
            $collections_options[$collection->getData('id')] = $collection->getData('name');
        }

        return $collections_options;
    }

    public function getMaxSortPosition()
    {
        $maxSort = $this->getCollection()
            ->addFieldToSelect('sort')
            ->setOrder('sort', 'DESC')
            ->setPageSize(1)
            ->setCurPage(1)
            ->getData();

        return $maxSort[0]['sort'] + 1;
    }

}
