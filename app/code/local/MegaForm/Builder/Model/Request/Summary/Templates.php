<?php

class MegaForm_Builder_Model_Request_Summary_Templates extends MegaForm_Builder_Model_Abstract
{
    const MODULE_MODEL        = "builder";
    const MODULE_MODEL_ENTITY = "request_summary_templates";

    protected function _construct()
    {
        $this->_init(self::MODULE_MODEL . "/" . self::MODULE_MODEL_ENTITY);
    }

}
