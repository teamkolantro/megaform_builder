<?php

use MegaForm_Builder_Model_Request_Form_Products as REQUEST_FORM_PRODUCTS_MODEL;
use MegaForm_Builder_Model_Request_Session_Completed as REQUEST_SESSION_COMPLETED_MODEL;

class MegaForm_Builder_Model_Observer
{
    public function renderFieldBefore($observer)
    {
        $event      = $observer->getEvent();
        $field_data = $event->getFieldData();

        if ($event->getFormCode() == "itp")
        {
            //modify single field_data in single field
            if ($field_data['field_name'] == 'cover_information')
            {
                // $field_data['form_field']->setTitle('asdsd');
            }
        }

        // getter method to fetch varien object passed from the dispatcher
        $event->setFieldData($field_data);
    }

    public function renderFieldAfter($observer)
    {
        $event      = $observer->getEvent();
        $field_data = $event->getFieldData();
        $build      = $event->getBuild();

        if ($event->getFormCode() == "itp")
        {
            //modify single field_data in single field
            if ($field_data['field_name'] == 'cover_information')
            {
                $new_html = $build->getHtml();
                $new_html .= "<p>Hi</p>";
                // $build->setHtml($new_html);
            }
        }

        // getter method to fetch varien object passed from the dispatcher
        $event->setBuild($build);
        $event->setFieldData($field_data);
    }

    public function sessionDataSaveBefore($observer)
    {
        $event = $observer->getEvent();
        $event->getFormCode(); //this is the form code
        // $event->getRequestSessionData()->setProductId(123);
    }

    public function sessionDataSaveAfter($observer)
    {
        $event = $observer->getEvent();
        $event->getFormCode(); //this is the form code
        // $event->getRequestSessionData()->setProductId(123);
    }

    public function formDataSaveBefore($observer)
    {
        $this->forVehicleInfo($observer);
        $event = $observer->getEvent();
        $event->getFormCode(); //this is the form code
    }

    public function forVehicleInfo(&$observer)
    {
        $event     = $observer->getEvent();
        $form_code = $event->getFormCode(); //this is the form code

        if ($form_code == "motor")
        {
            $form_json_datas = $event->getFormDatas()->getFormJsonDatas();
            $form_json_datas = json_decode($form_json_datas);
            $form_json_datas = Mage::helper('builder')->objToArray($form_json_datas);

            if (isset($form_json_datas['insurance_required_car_details']))
            {
                if (isset($form_json_datas['insurance_required_car_details']['year_model']))
                {
                    $form_json_datas['insurance_required_car_details']['year_model.label'] = Mage::getModel('vehicleinfo/makeyear')->load($form_json_datas['insurance_required_car_details']['year_model'])->getYear();
                }
                if (isset($form_json_datas['insurance_required_car_details']['brand']))
                {
                    $brands         = Mage::helper('vehicleinfo')->getVehicleMakeOptionsbyYear($form_json_datas['insurance_required_car_details']['year_model']);
                    $brand_haystack = array();
                    foreach ($brands as $brand)
                    {
                        $brand_haystack[$brand->getId()] = $brand->getName();
                    }
                    $form_json_datas['insurance_required_car_details']['brand.label'] = @$brand_haystack[$form_json_datas['insurance_required_car_details']['brand']];
                }
                if (isset($form_json_datas['insurance_required_car_details']['model_name']))
                {
                    $models         = Mage::helper('vehicleinfo')->getVehicleModelOptionsbyYear($form_json_datas['insurance_required_car_details']['brand']);
                    $model_haystack = array();
                    foreach ($models as $model)
                    {
                        $model_haystack[$model->getId()] = $model->getModel();
                    }
                    $form_json_datas['insurance_required_car_details']['model_name.label'] = @$model_haystack[$form_json_datas['insurance_required_car_details']['model_name']];
                }
            }
            $event->getFormDatas()->setFormJsonDatas(json_encode($form_json_datas));
        }

        if ($form_code == "vehicleInfo")
        {
            $form_json_datas = $event->getFormDatas()->getFormJsonDatas();
            $form_json_datas = json_decode($form_json_datas);
            $form_json_datas = Mage::helper('builder')->objToArray($form_json_datas);

            if (isset($form_json_datas['other_info']))
            {
                if (isset($form_json_datas['other_info']['color']))
                {
                    $form_json_datas['other_info']['color.label'] = Mage::getModel('vehicleinfo/vehicleotherinfo')->load($form_json_datas['other_info']['color'])->getName();
                }
            }

            $event->getFormDatas()->setFormJsonDatas(json_encode($form_json_datas));
        }
    }

    public function formDataSaveAfter($observer)
    {
        $event = $observer->getEvent();
        $event->getFormCode(); //this is the form code
    }

    public function renderElementBefore($observer)
    {
        $event       = $observer->getEvent();
        $element     = $event->getElement()->getData();
        $form_values = $event->getFormValues();

        if ($event->getFormCode() == "motor")
        {
            //modify single element in single field
            if ($element['field_name'] == 'insurance_required_car_details')
            {
                if ($element['element_name'] == 'brand')
                {
                    if((!empty($form_values)) && (!empty($form_values['insurance_required_car_details']['year_model'])))
                    {
                        $brands         = Mage::helper('vehicleinfo')->getVehicleMakeOptionsbyYear($form_values['insurance_required_car_details']['year_model']);
                        $option_array = array();
                        $option_array[] = array('label' => 'Brand *' , 'value' => '');
                        foreach ($brands as $brand)
                        {
                            $option_array[] = array(
                                'value' => $brand->getId(),
                                'label' => $brand->getName(),
                            );
                        }
                        $element['element_data']->addData(array('options' => $option_array));
                    }
                }
                if ($element['element_name'] == 'model_name')
                {
                    if((!empty($form_values)) && (!empty($form_values['insurance_required_car_details']['year_model'])))
                    {
                        $brands         = Mage::helper('vehicleinfo')->getVehicleModelOptionsbyYear($form_values['insurance_required_car_details']['brand']);
                        $option_array = array();
                        $option_array[] = array('label' => 'Model Name *' , 'value' => '');
                        foreach ($brands as $brand)
                        {
                            $option_array[] = array(
                                'value' => $brand->getId(),
                                'label' => $brand->getModel(),
                            );
                        }
                        $element['element_data']->addData(array('options' => $option_array));
                    }
                }
            }
        }

        // getter method to fetch varien object passed from the dispatcher
        $event->setElement($element);
    }

    public function renderElementAfter($observer)
    {
        $event = $observer->getEvent();
        //sample modify of field attributes
        $element = $event->getElement();

        $build = null;

        if ($event->getFormCode() == "itp")
        {
            //modify single element in single field
            if ($element['field_name'] == 'package')
            {
                if ($element['element_name'] == 'package')
                {
                    $build    = $event->getBuild();
                    $new_html = $build->getHtml();
                    $new_html .= "<p></p>";
                    $build->setHtml($new_html);
                }
            }
        }

        // getter method to fetch varien object passed from the dispatcher
        if(!is_null($build))
        {
            $event->setBuild($build);
        }

    }

    public function catalogControllerProductDelete($observer)
    {
        $event = $observer->getEvent();
        //sample modify of field attributes
        $product = $event->getProduct();

        /**
         * CLean products in this module
         */
        $REQUEST_SESSION_COMPLETED_MODEL = Mage::getModel(REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL . "/" . REQUEST_SESSION_COMPLETED_MODEL::MODULE_MODEL_ENTITY)
        ->getCollection()
        ->addFieldToFilter('product_id', array('eq' => $product->getId()));

        foreach ($REQUEST_SESSION_COMPLETED_MODEL as $key => $collection)
        {
            $collection->setProductId(0);
            $collection->save();
        }

        $REQUEST_FORM_PRODUCTS_MODEL = Mage::getModel(REQUEST_FORM_PRODUCTS_MODEL::MODULE_MODEL . "/" . REQUEST_FORM_PRODUCTS_MODEL::MODULE_MODEL_ENTITY)
        ->getCollection()
        ->addFieldToFilter('product_id', array('eq' => $product->getId()));

        foreach ($REQUEST_FORM_PRODUCTS_MODEL as $key => $collection)
        {
            $collection->delete();
        }
    }
}
